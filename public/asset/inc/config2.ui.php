<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
	"Home" => APP_URL
);



if (!empty($user)) {
	
 	if ($user->role=='1') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"mbsb" => array(
						"title" => "MBSB",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							/*"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),*/
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
							"package" => array(
								"title" => "Marketing List",
								"url" => url('/')."/marketing",
								"icon" => "fa-users"
							),
								"score" => array(
								"title" => "DCA Admin",
								"url" => url('/')."/manager",
								"icon" => "fa-user"
							)
						
						),

					),
				)
			),	
			"role" => array(
				"title" => "User Matrix",
				"url" => url('/')."/roles",
				"icon" => "fa-home"
			),
		);
	}
	else if ($user->role=='2') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"mbsb" => array(
						"title" => "MBSB",
						"icon" => "fa fa-bank",
						"sub" => array(
							"package" => array(
								"title" => "Package",
								"url" => url('/')."/admin/master/package",
								"icon" => "fa-tags"
							),
							"job" => array(
								"title" => "Job Sector",
								"url" => url('/')."/admin/master/job-sector",
								"icon" => "fa-suitcase"
							),
							"score" => array(
								"title" => "Score Rating",
								"url" => url('/')."/admin/master/scorerating",
								"icon" => "fa-bar-chart"
							),
							"scorecard" => array(
								"title" => "Score Card",
								"url" => url('/')."/admin/master/scorecard",
								"icon" => "fa-credit-card"
							),
							"occupation" => array(
								"title" => "Job Sector",
								"url" => url('/')."/admin/master/job-sector",
								"icon" => "fa-suitcase"
							),
							"country" => array(
								"title" => "Country",
								"url" => url('/')."/admin/master/country",
								"icon" => "fa-tags"
							),
							"state" => array(
								"title" => "State",
								"url" => url('/')."/admin/master/state",
								"icon" => "fa-tags"
							),
							"email" => array(
								"title" => "Email",
								"url" => url('/')."/admin/master/mail-systyem",
								"icon" => "fa-tags"
							),
							"jobstatus" => array(
								"title" => "Employment Status",
								"url" => url('/')."/admin/master/codejobstatus",
								"icon" => "fa-tags"
							),
							"position" => array(
								"title" => "Employment Position",
								"url" => url('/')."/admin/master/position",
								"icon" => "fa-tags"
							),
							"codework" => array(
								"title" => "Employment Type",
								"url" => url('/')."/admin/master/codework",
								"icon" => "fa-tags"
							),

							"relationship" => array(
								"title" => "Relationship",
								"url" => url('/')."/admin/master/relationship",
								"icon" => "fa-tags"
							),
					
						),

					),


				)
			),	
		);
	}
	else if ($user->role=='4') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),

			"Assign to FA" => array(
				"title" => "Assign to FA",
				"url" => url('/')."/assign-to-fa",
				"icon" => "fa fa-check-square"
			),
			"FA List" => array(
				"title" => "FA List",
				"url" => url('/')."/marketing",
				"icon" => "fa fa-chevron-circle-left"
			),
			/*"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"WAPS Status" => array(
				"title" => "WAPS Status",
				"url" => url('/')."/waps-status",
				"icon" => "fa fa-check-square"
			)*/
				
		);
	}
		else if ($user->role=='8') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Approved" => array(
				"title" => "Application Approved",
				"url" => url('/')."/approved",
				"icon" => "fa fa-check-square"
			)
			/*"WAPS Status" => array(
				"title" => "WAPS Status",
				"url" => url('/')."/waps-status",
				"icon" => "fa fa-check-square"
			)*/
			
			/*
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),
						"submitted" => array(
						"title" => "Submitted",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),

				)
			)		*/
		);
	}

}


//configuration variables
$page_title = "";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>