

		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">

				<span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/logo_bank.png" alt="SmartAdmin" width="79px" > </a></span>

				<!-- END AJAX-DROPDOWN -->
			</div>

	<?php if (!(Auth::user())) {?>
		<span id="extr-page-header-space"> <a class="btn hidden-xs btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="icon-append fa fa-lock"></i>  Login</a> </span>

		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='9')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/home" class="btn hidden-xs btn-primary">My Application</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role!='9')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
 	<?php } ?>
	

	
<?php 	if (empty($user))  { ?>
			
			<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/faq" class="btn hidden-xs btn-primary">FAQ</a> </span>-->
			
		
			<!-- Single button -->
			<span id="extr-page-header-space"> 
				<div class="dropdown visible-xs center" style="margin-right:67px!important;margin-top: 9px !important; ">
				  <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					 &nbsp; &nbsp;Menu &nbsp; &nbsp; &nbsp;<span class="caret"></span>
				  </a>
				  <ul class="dropdown-menu">
				  	<?php if (!(Auth::user())) {?>
						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a data-toggle="modal" data-target=".bd-example-modal-lg">Login</a></li>
					<?php } else if ((Auth::user()->role=='9')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/home">My Application</a></li>
					<?php } else if ((Auth::user()->role=='7')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
					<?php } ?>
				
				</ul>
				</div>
			</span>
			<span id="extr-page-header-space"> &nbsp;</span><span id="extr-page-header-space"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <?php 
		} 
		 ?>


		</header>



        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163298716-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163298716-1');
</script>

