

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="{{asset('/asset/js/plugin/pace/pace.min')}}"></script>

	

		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{asset('/asset/js/app.config')}}"></script>
		<script src="{{asset('/asset/js/jquery.steps')}}"></script>
		<script src="{{asset('/asset/js/jquery.steps.min')}}"></script>
			<script src="{{asset('/asset/js/jquery.simulate')}}"></script>
		

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{asset('/asset/js/plugin/jquery-touch/jquery.ui.touch-punch.min')}}"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="{{asset('/asset/js/bootstrap/bootstrap.min')}}"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="{{asset('/asset/js/notification/SmartNotification.min')}}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{asset('/asset/js/smartwidgets/jarvis.widget.min')}}"></script>

		<!-- EASY PIE CHARTS -->
		<script src="{{asset('/asset/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min')}}"></script>

		<!-- SPARKLINES -->
		<script src="{{asset('/asset/js/plugin/sparkline/jquery.sparkline.min')}}"></script>

		<!-- JQUERY VALIDATE -->
		<script src="{{asset('/asset/js/plugin/jquery-validate/jquery.validate.min')}}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{asset('/asset/js/plugin/masked-input/jquery.maskedinput.min')}}"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="{{asset('/asset/js/plugin/select2/select2.min')}}"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="{{asset('/asset/js/plugin/bootstrap-slider/bootstrap-slider.min')}}"></script>

		<!-- browser msie issue fix -->
		<script src="{{asset('/asset/js/plugin/msie-fix/jquery.mb.browser.min')}}"></script>

		<!-- FastClick: For mobile devices -->
		<script src="{{asset('/asset/js/plugin/fastclick/fastclick.min')}}"></script>

		<!--[if IE 8]>
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->

		<!-- Demo purpose only -->
		<script src="{{asset('/asset/js/demo.min')}}"></script>

		<!-- MAIN APP JS FILE -->
		<script src="{{asset('/asset/js/app.min')}}"></script>		
		<script src="{{asset('/asset/js/jssor.slider.min')}}"></script>		

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="{{asset('/asset/js/speech/voicecommand.min')}}"></script>	

		<!-- SmartChat UI : plugin -->
		<script src="{{asset('/asset/js/smart-chat-ui/smart.chat.ui.min')}}"></script>
		<script src="{{asset('/asset/js/smart-chat-ui/smart.chat.manager.min')}}"></script>
		<script src="{{asset('/asset/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min')}}"></script>
		<script src="{{asset('/asset/js/plugin/fuelux/wizard/wizard.min')}}"></script>

		<script src="{{asset('/asset/js/file/vendor/jquery.ui.widget')}}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{asset('/asset/js/file/jquery.iframe-transport')}}"></script>
<!-- The basic File Upload plugin -->
<script src="{{asset('/asset/js/file/jquery.fileupload')}}"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="{{asset('/asset/js/bootbox.min')}}"></script>


		<script type="text/javascript">
			// DO NOT REMOVE : GLOBAL FUNCTIONS!
			$(document).ready(function() {
				pageSetUp();
			})
		</script>



	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="{{asset('/asset/js/gambar/lib/jquery.mousewheel-3.0.6.pack')}}"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="{{asset('/asset/js/gambar/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
