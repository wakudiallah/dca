<?php

namespace App;

class Permission extends \Spatie\Permission\Models\Permission
{
    public $incrementing = true;  
    
    protected $guarded = ["id"]; 

    public static function defaultPermissions()
    {

        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_check_dsrs',
            'add_check_dsrs',
            'edit_check_dsrs',
            'delete_check_dsrs',


            'view_pending_verification',
            'add_pending_verification',
            'edit_pending_verification',
            'delete_pending_verification',


            'view_job_sector',
            'add_job_sector',
            'edit_job_sector',
            'delete_job_sector',


            'view_scored_card',
            'add_scored_card',
            'edit_scored_card',
            'delete_scored_card',


            'view_scored_rating',
            'add_scored_rating',
            'edit_scored_rating',
            'delete_scored_rating',


            'view_dca_admin',
            'add_dca_admin',
            'edit_dca_admin',
            'delete_dca_admin',
            
        ];
    }

    public function Role() {
        return $this->belongsTo('App\Role','role_id','id');  
    }
}
