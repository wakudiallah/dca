<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Model\User;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\MOMBsc;
use App\Soap\Response\MOMBscResponse;
use App\Soap\Request\MOMRef;
use App\Soap\Response\MOMRefResponse;
use App\Soap\Response\MOMEmpInfResponse;
use App\Soap\Response\MOMSpResponse;
use App\Soap\Response\MOMCommitResponse;
use App\Soap\Response\MOMCallBscResponse;
use App\Soap\Response\MOMDocResponse;
use App\Soap\Response\MOMCTransResponse;
use App\Soap\Response\MOMFinResponse;
use App\Soap\Response\MOMDSRAResponse;
use App\Soap\Response\MOMDSRBResponse;
use App\Soap\Response\MOMDSRCResponse;
use App\Soap\Response\MOMDSREResponse;

use App\Soap\Response\MOMLnResponse;
use App\Soap\Response\MOMRskResponse;
use App\Soap\Response\MOMTrmResponse;
use App\Model\Term;
use OneSignal;
use Storage;
use File;
use App\Model\Waps;
use Mail;
class WapsController extends Controller
{
     protected $soapWrapper;

          /**
           * SoapController constructor.
           *
           * @param SoapWrapper $soapWrapper
           */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
        $this->middleware('auth'); 
        $this->middleware('admin');
    }

     public function check_waps()
     {
     	$user = Auth::user();
     	$termas = Term::where('assign_to',$user->id_pra)->wherein('status', ['6'])->orderBy('file_created','DESC')->get();  
    	 $soap_Wrapper = new SoapWrapper;
    	 $this->soapWrapper->add('cds', function ($service) {
          	$service
          	->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
        });

        foreach ($termas as $termas) {
            $userId= '70b3f22c-db26-4d05-be06-2604778bafc2';
           /* if($termas->referral_id==$user->id){
               OneSignal::sendNotificationToAll("New Status for a ",   $userId, $url = 'http://localhost:8080/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
           }*/
            $param=array(
                'AppNo'   => $termas->id_praapplication,
                'txnCode' => 'ST003',
                'ActCode' => 'R',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#'
            );
            $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));
                $smsMessages = is_array( $response->MOMCallStatResult)
            	? $response->MOMCallStatResult
            	: array( $response->MOMCallStatResult );

                foreach ( $smsMessages as $smsMessage )
                {
                	$result=$smsMessage->ApvStat;
                	$validate  = Waps::latest('created_at')->where('id_praapplication', $termas->id_praapplication)->count();
                	
                       	// if($result=='DECL'){
                        //if($termas->status_waps=='0'){
                   
                        $waps = new Waps;
                        $waps->id_praapplication   =  $smsMessage->AppNo;
                        //$waps->ic =$smsMessage->IdNo;
                        //$waps->name =  $smsMessage->Nme;
                        $waps->AppvAmt =$smsMessage->ApvAm;
                        $waps->AppvInst =  $smsMessage->ApvIns;
                        $waps->AppvRate =$smsMessage->ApvRt;
                        $waps->AppvDt =  $smsMessage->ApvDt;
                        $waps->AppvTen =  $smsMessage->Tnr;
                        $waps->AppvStat =  $result;
                        $waps->save();
                        
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                            Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                            $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                            $message->subject('Loan has Approved');
                            $message->to($termas->email);
                            }); 
                        
                             //OneSignal::sendNotificationToAll("New Status", $url = 'http://localhost:8080/onesignal/public/admin', $data = null);

                             //admin/detail_pra
                            OneSignal::sendNotificationToAll("New Status for ".$smsMessage->IdNo, $url = 'http://localhost:8080/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
                            
                        //}
                }
    	}
    	  return redirect('admin');  
    }
}
