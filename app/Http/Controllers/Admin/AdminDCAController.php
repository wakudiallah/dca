<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Basic;
use App\Model\Log_download;
use App\Model\Blocked_ic;
use App\Model\Basic_v;
use App\Model\Empinfo;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount;
use App\Model\LoanAmmount_v;
use App\Model\Contact;
use App\Model\Contact_v;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Spouse;
use App\Model\Spouse_v;
use App\Model\Reference;
use App\Model\Reference_v;
use App\Model\Financial;
use App\Model\Financial_v;
use App\Model\Postcode;
use App\Model\Package;
use App\Model\PraApplication;
use App\Model\Employment;
use App\Model\Document;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Branch;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use App\Model\Commitments;
use App\Model\Foreigner;
use App\Model\Subscription;
use App\ScoreRating;
use App\ScoreCard;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Model\Country;
use App\Model\Occupations;
use App\Model\Position;
use App\Model\Relationship;
use App\Model\Title;
use Ramsey\Uuid\Uuid;
use Input;
use Mail;
use DateTime;
use App;
use PDF;
use DB;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\MOMBsc;
use App\Soap\Response\MOMBscResponse;
use App\Soap\Request\MOMRef;
use App\Soap\Response\MOMRefResponse;
use App\Soap\Response\MOMEmpInfResponse;
use App\Soap\Response\MOMSpResponse;
use App\Soap\Response\MOMCommitResponse;
use App\Soap\Response\MOMCallBscResponse;
use App\Soap\Response\MOMDocResponse;
use App\Soap\Response\MOMCTransResponse;
use App\Soap\Response\MOMFinResponse;
use App\Soap\Response\MOMDSRAResponse;
use App\Soap\Response\MOMDSRBResponse;
use App\Soap\Response\MOMDSRCResponse;
use App\Soap\Response\MOMDSREResponse;

use App\Soap\Response\MOMLnResponse;
use App\Soap\Response\MOMRskResponse;
use App\Soap\Response\MOMTrmResponse;

use OneSignal;
use Storage;
use File;
use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\MO;
Use Alert;
use App\Mail_System;

class AdminDCAController extends Controller
{
    

          /**
           * SoapController constructor.
           *
           * @param SoapWrapper $soapWrapper
           */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
        $this->middleware('auth'); 
        //$this->middleware('admin');
    }



    public function index()
    {
    	$user = Auth::user();
    	if($user->role=='4'){
    	      $terma = Term::where('status', '2')->wherenull('assign_to')->orderBy('file_created','DESC')->with('praapplication.user')->get();

    	//$fa = MO::where('id_manager',$user->id_pra)->get();
        $branch = Branch::all();
        $fa = User::where('role','8')->get();
    	}

    	return view('dca_admin.assign_to_fa', compact('fa','terma','blocked_ic','user','branch')); 
    }

    public function store(Request $request)
    {

    	$create_by = Auth::user()->id;

    	$fa = $request->fa;
         $email_fa = User::where('id',$fa)->limit('1')->first();

    	$item = array_map(null, $request->id, $request->id2, $request->id3);
       

        foreach($item as $val) {
            
            $pra = Term::where('id',$val[0])->update([
                "assign_to"             => $fa,
                "assign_by"             => $create_by,
                "mo_stage"				=> '99',
                "status"                => '3'
                
            ]);
            $time_log = date('Y-m-d H:i:s');
        	$log_download = new Log_download;
            $log_download->id_praapplication   =  $val[2];
			$log_download->id_user   =  $create_by;
			$log_download->Activity   =  'Assign to FA';
			$log_download->downloaded_at   =  $time_log;
				// $log_download->log_reason  =  $reason;
			$log_download->save();
        }

        try{  

            $main_sender = Mail_System::where('type',1)->first();
            $noreply_sender_name= $main_sender->name;
            $noreply_sender_email= $main_sender->email;

            Mail::send('mail.mo.assigned', compact('email_fa'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$email_fa ) {
                $message->from($noreply_sender_email, $noreply_sender_name);
                $message->subject('MBSB - PF-i - New Customer Assigned');
                $message->to($email_fa->email, $email_fa->name);
            });

            Alert::success('successfully assign to FA');
            return redirect('/assign-to-fa');
        }
        catch(\Swift_TransportException $e){
            Alert::success('Data added successfuly. But failed to sent email to user');
            return redirect('/assign-to-fa');
        }


    }

}
