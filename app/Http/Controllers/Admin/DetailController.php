<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Model\User;
use App\Model\Basic;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\Contact;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Postcode;
use App\Model\PraApplication;
use App\Model\Employment;
use App\Model\Document;
use App\Model\Term;

class DetailController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	 $this->middleware('auth');	
      $this->middleware('admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function detail($id_pra)
	{
	   
	      
        $user = Auth::user();
         $email =  $user->email;
       
       // $pra = PraApplication::latest('created_at')->where('email',$email)->limit('1')->first();
        //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;

     

        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();	
        $employment = Employment::get();
     
      	
     
	  $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();

        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance;

        $loan = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

         $id_loan= $loan->first()->id;  

         $tenure = Tenure::where('id_loan',$id_loan)->get();   

        	  $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        	$data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        	 $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

            $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
            $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        	$financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        	$document1 = Document::where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        	$term = Term::where('id_praapplication',  $id_pra )->first();
        	   if($status < 1) {
        	 return view('home.index', compact('user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1')); 	
        
        }
        else{

        	 return view('home.index2', compact('user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1','term')); 


        }

            
	}


	public function postcode($id)
	{
	   
	      $data = Postcode::where('id',$id)->with('city.state')->get();
         
         return $data;
	}

	
}
