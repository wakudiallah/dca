<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Term;
use App\Mail_System;
use App\Manager;
use Auth;
use Ramsey\Uuid\Uuid;
use Hash;
use Mail;
use Alert;
use DB;
use App\ModelHasRoles;
use App\Model\Branch;
use Session;
class UserCreationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view_user_creations');
        $user = Auth::user();
       	$branch = Branch::where('status','1')->get();
        $list = User::orderBy('created_at','DESC')->where('role','8')->get();
        return view('admin.master.user_creation.index', compact('list','user','branch'));
         
    }

    public function add()
    {
        $this->authorize('add_user_creations');
        $user = Auth::user();
       	$branch = Branch::where('status','1')->get();
        $list = User::orderBy('created_at','DESC')->where('role','!=','9')->get();
        return view('admin.master.user_creation.create', compact('list','user','branch'));
         
    }

     public function edit($id)
    {
       $this->authorize('edit_user_creations');
        $user = Auth::user();
        $branch = Branch::where('status','1')->orderBy('branchname','ASC')->get();
        $list = User::where('id_pra',$id)->first();
        return view('admin.master.user_creation.edit', compact('list','user','branch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        //$checkUser = User::Where("email",$request->input('email'))->count();
        //$checkID = User::Where("email",$request->input('manager_id'))->count();
        $email      = $request->input('email');
        $checkUser  = DB::table('users')->where('email', $email)->get();
        $countUser  = $checkUser->count();

        $checkID = DB::table('users')->where('employer_id', $request->input('employer_id'))->get();
        $countID =  $checkID->count();

        $fullname       = $request->input('name');
        $active_code    = str_random(100);
        $employer_id   = $request->input('employer_id');
        $branch_code   = $request->input('branch_code');
        $phone   = $request->input('phone');

        if(($countUser==0) AND ($countID==0)) {

            if($request->set_password=="1") {
                $password1 = $request->password;
                $password2 = $request->password_confirmation;

                if($password1!=$password2) {
                     Alert::error($checkUser);
                   return redirect('manager'); 
                }
            }
            else {
                $password2 = $this->generateStrongPassword('8',false,'ld');   
            }
            
            
            $bc = User::latest('created_at')->limit('1')->first();

            if(empty($bc->referral_mo)){
                $l = 'A000-181003';                
            }
            else{
                $l = $bc->referral_mo;
            }
            
            $letter = substr($l,0,1);
            $nu = substr($l,1,3);

            if($nu == 999){
                $letter++;
                $nu=1;
            }
            else{
                $nu++;
            }
          
            $cc=  $letter.str_pad($nu,3,'0',STR_PAD_LEFT);

            $user                   = new User;
            $user->password         = Hash::make($password2); 
            $user->id_pra           = Uuid::uuid4()->getHex();    
            $user->email            =  $email;
            $user->activation_code  = $active_code;
            $user->name             =  $fullname;
            $user->branch_code      =  $branch_code;
            //$user->referral_id      =  '1';
            $user->employer_id      =  $employer_id;
            $user->phone            =  $phone;
            $user->referral_mo      =  $cc;
            $user->first_login      =  '1';
            $user->active           =  "1";
            $user->role             =  "8";
            $user->save();


            $model              = new ModelHasRoles;
            $model->role_id     = '8';
            $model->model_type  = 'App\User';
            $model->model_id    = $user->id;
            $model->save();


            $noreply_sender = Mail_System::where('id',2)->first();
            $noreply_sender_name = $noreply_sender->name;
            $noreply_sender_email = $noreply_sender->email;


            // Send to NO Email
            try{  
                    Mail::send('mail.user.fa_register', compact('email','fullname','password2'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$email ,$fullname ) {
                        $message->from($noreply_sender_email, $noreply_sender_name);
                        $message->subject('MBSB - Financial Advisor Successfully Created');
                        $message->to($email, $fullname);
                    });

                    Alert::success('Data added successfuly');
                    return redirect('admin/user-creation/list');
                }
            catch(\Swift_TransportException $e){
                 Alert::success('Data added successfuly. But failed to sent email to user');
                return redirect('admin/user-creation/list');
                }

        }

        else if ($countUser!=0) {

            Session::flash('email', $email); 
            Session::flash('fullname', $fullname);
            Session::flash('employer_id', $employer_id); 
            Session::flash('phone', $phone); 

            Alert::error('Email Already Used. Please use another email address');
            return redirect('admin/user-creation/add'); 
        }

         else if ($countID!=0) {
            Session::flash('email', $email); 
            Session::flash('fullname', $fullname);
            Session::flash('employer_id', $employer_id); 
            Session::flash('phone', $phone); 

            Alert::error('ID DCA Admin Already Used.');
            return redirect('admin/user-creation/add'); 
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $email      = $request->input('email');
        $checkUser  = DB::table('users')->where('email', $email)->where('id_pra',$id)->get();
        $countUser  = $checkUser->count();

        $checkID = DB::table('users')->where('employer_id', $request->input('employer_id'))->where('id_pra',$id)->get();
        $countID =  $checkID->count();

        if(($countUser==1) AND ($countID==1)) {

        
            $user = User::where('id_pra', $id)
            ->update(['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'employer_id' => $request->employer_id, 'branch_code'=>$request->branch_code ]);
            
            Alert::success('Data updated successfuly');
            return redirect('admin/user-creation/list');
               

        }

        else if ($countUser!=1) {
            Alert::error('Email Already Used. Please use another email address');
            return redirect('admin/user-creation/edit/'.$id); 
        }

         else if ($countID!=1) {
            Alert::error('Employer ID Already Used.');
            return redirect('admin/user-creation/edit/'.$id); 
        }
    }


     public function update22(Request $request, $id)
    {
       $email      = $request->input('email');
        $checkUser  = DB::table('users')->where('email', $email)->where('id_pra',$id)->get();
        $countUser  = $checkUser->count();

      

           $checkID = DB::table('users')->where('employer_id', $request->input('employer_id'))->where('id_pra',$id)->get();
        $countID =  $checkID->count();

        if(($countUser=='0') AND ($count2=='0')) {
            Alert::error('Employer ID Already Used.');
            return redirect('admin/user-creation/edit/'.$id); 
           
        }else{

             $user = User::where('id_pra', $id)
            ->update(['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'employer_id' => $request->employer_id, 'branch_code'=>$request->branch_code ]);
            
            Alert::success('Data updated successfuly');
            return redirect('admin/user-creation/list');
        }


       
    }

    public function update2(Request $request, $id)
    {
        //$link = $request->input('link');
        $user = Auth::User();
       
        $user = User::where('id_pra', $id)
            ->update(['email' => $request->email, 'name' => $request->name, 'phoneno' => $request->phone, 'phoneno' => $request->phone ]);
        Alert::success('Data updated successfuly');
        return redirect('/admin/therapy')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_manager(Request $request)
    {
         $mo = Manager::where('user_id',$request->manager_id)->update([
                "status"       => $request->status,
                "phoneno"       => $request->phoneno,
                "name"       => $request->fullname,
                "employer_id"       => $request->employer_id,

            ]);

          $user = User::where('id_pra',$request->manager_id)->update([
                "active"       => $request->status,
                 "email"       => $request->email,

            ]);
       Alert::success('Edit Successfully');
        return redirect('manager'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
