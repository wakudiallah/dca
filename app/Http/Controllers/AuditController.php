<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Auth;
class AuditController extends Controller
{
    public function index()
    {
    	 $user = Auth::user();
        $audits = \OwenIt\Auditing\Models\Audit::with('user')
            ->orderBy('created_at', 'desc')->get();
        return view('audits', ['audits' => $audits, 'user'=> $user]);
    }
}
