<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MO;
use App\Model\AntiAttrition;
use App\User;
use App\Transformers\CLRTTransformer;
use App\Transformers\SettlementInfoTransformer;
use Auth;
use App\Model\SettlementInfo;
class CLRTController extends Controller
{
    public function add(Request $request, AntiAttrition $antiattrition,  SettlementInfo $settlement){
        $this->validate($request, [
           
            'mo_id'     			=>'required',
           	'assign_by'  			=>'required',
            'id_clrt'               => 'required',
            'TaskID'                => 'required',
            'ACID'                  => 'required',
            'CustIDNo'              => 'required',
            'Tenor'                 => 'required',
            'Year'                  => 'required',
            'SalIncrementPercent'   => 'required',
            'NewSalary'             => 'required',
            'NewAllowDeduct'        => 'required',
            'IntRate'               => 'required',
            'VarianceRate'          => 'required',
            'NewLoanOffer'          => 'required',
            'PayoutPercent'         => 'required',
            'NetProceed'            => 'required',
            'MonthlyInst'           => 'required',
            'NewDSR'                => 'required',
            'ProfitToEarn'          => 'required',
            'CalDt'                 => 'required',
            'CustWrkSec'            => 'required',
            'CustName'              => 'required',
            'Assign'                => 'required',
            'CustDob'               => 'required',
            'State'                 => 'required',
            'MaxAge' 				=> 'required',
            'DueDate' 				=> 'required',
            'BalOutStanding' 		=> 'required',
            'FullSettlement' 		=> 'required',
            'ProfitEarned' 			=> 'required',
            'Rebate'			 	=> 'required',
            'PaidInstallment' 		=> 'required',
            'RemInstallment' 		=> 'required',
            'DSRUtilise' 			=> 'required',
            'DSRUnutilise' 			=> 'required',
            'CalculateDate' 		=> 'required',
            'TaskIDNo' 				=> 'required'
        ]);
       // $auth = Auth::user();
        $antiattrition = $antiattrition->create([
            'mo_id' => $request->mo_id,
            'id_clrt' => $request->id,
            'TaskID' => $request->TaskID,
            'ACID' => $request->ACID,
            'CustIDNo' => $request->CustIDNo,
            'Tenor' => $request->Tenor,
            'Year' => $request->Year,
            'SalIncrementPercent' => $request->SalIncrementPercent,
            'NewSalary' => $request->NewSalary,
            'NewAllowDeduct' => $request->NewAllowDeduct,
            'IntRate' => $request->IntRate,
            'VarianceRate' => $request->VarianceRate,
            'NewLoanOffer' => $request->NewLoanOffer,
            'PayoutPercent' => $request->PayoutPercent,
            'NetProceed' => $request->NetProceed,
            'MonthlyInst' => $request->MonthlyInst,
            'NewDSR' => $request->NewDSR,
            'ProfitToEarn' => $request->ProfitToEarn,
            'CalDt' => $request->CalDt,
            'CustWrkSec' => $request->CustWrkSec,
            'CustName' => $request->CustName,
            'Assign' => $request->Assign,
            'CustDob' => $request->CustDob,
            'State' => $request->State,
            'assign_by' => $request->assign_by,
            //'assign_by' => Auth::guard('api')->id(),
        ]);

        $settlement = $settlement->create([
            'ACID' => $request->ACID,
            'CustIDNo' => $request->CustIDNo,
            'MaxAge' => $request->MaxAge,
            'DueDate' => $request->DueDate,
            'BalOutStanding' => $request->BalOutStanding,
            'FullSettlement' => $request->FullSettlement,
            'ProfitEarned' => $request->ProfitEarned,
            'Rebate' => $request->Rebate,
            'PaidInstallment' => $request->PaidInstallment,
            'RemInstallment' => $request->RemInstallment,
            'DSRUtilise' => $request->DSRUtilise,
            'DSRUnutilise' => $request->DSRUnutilise,
            'CalculateDate' => $request->CalculateDate,
            'TaskIDNo' => $request->TaskIDNo,
        ]);

        $response = fractal()
            ->item($antiattrition, $settlement)
            ->transformWith(new CLRTTransformer)
            //->transformWith(new SettlementInfoTransformer)
            ->toArray();

           /*$response2 = fractal()
            ->item($settlement)
            ->transformWith(new SettlementInfoTransformer)
            ->toArray();*/
        return response()->json($response,201);


    }

}
