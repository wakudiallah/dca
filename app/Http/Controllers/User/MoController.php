<?php

namespace App\Http\Controllers\User;


use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use Hash;
use Auth;
use Input;
use Mail;
use App\Http\Requests\UpdateUserRequest;
 
 
use Rhumsaa\Uuid\Uuid;


class MoController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
        // $user = Auth::user();
    

         return view('auth.moregister'); 
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
   public function create()
    {
        
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(StoreUserRequest $request)
    {
        $user = new User;
        $email = $request->input('Email');
        $fullname = $request->input('FullName2');
        $active_code = str_random(100);
        $password2 = $request->input('password');



        //Send request and receive json data by latitude and longitude

        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($request->latitude).','.trim($request->longitude).'&sensor=false';
        $json = @file_get_contents($url);
        $datajson = json_decode($json);
        $status = $datajson->status;
        if($status=="OK"){
            //Get address from json data
            $location = $datajson->results[0]->formatted_address;
        }else{
            $location =  '';
        }


        $user->password = Hash::make($request->password);
        $user->id   = Uuid::uuid4()->getHex(); // toString();     
        $user->email =  $email;
        $user->activation_code = $active_code;
        $user->name =  $fullname;
        $user->active =  "1";
        $user->role =  "5";
        $user->lat = $request->latitude;
        $user->lng = $request->longitude;
        $user->location = $request->location;
        $user->save();

          Mail::send('mail.moregister', compact('active_code','password2','fullname'), function ($message) use ($email, $fullname) {
          $message->from('mbsb@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Email Confirmation');
          $message->to($email, $fullname);
            });
          
     // Notif to Admin
     $email_admin = "mbsb@ezlestari.com.my";
     $fullname_admin = "mbsb@ezlestari.com.my";
     // Send to Admin Email
     
     $k = Mail::send('mail.moregister_notif', compact('email','fullname'), function ($message) use ($email_admin, $fullname_admin) {
     $message->from('no-reply@ezlestari.com.my', 'no-reply@ezlestari.com.my');
     $message->subject('New User Registration');
     $message->to($email_admin, $fullname_admin);
       });
    

       $credentials = array(
    'email' => $email,
    'password' => $password2 );

if (Auth::attempt($credentials)) {
   return redirect()->intended('admin');
}



         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function show($id)
    {
   $confirmation = User::where('activation_code', $id) ->update(['active' => 1 ]);
        if($confirmation) {
        return redirect('auth/login')->with('message', 'email confirmation success'); }
        else {

           return redirect('auth/login')->withErrors(['failed email confirmation ']);
        }
    }


    public function getLocation(Request $request) {


       if(!empty($request->latitude) && !empty($request->longitude)){
            //Send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($request->latitude).','.trim($request->longitude).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            if($status=="OK"){
                //Get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            //Print address 
            return response()->json([   'status' => "Detect Request", 
                                            'latitude' => $request->latitude, 'longitude' => $request->longitude, 'location' => $location ]);
        }
        else {
            return response()->json([   'status' => "Cannot Detect Request"]);
        }
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
  public function update()
    {


       
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {       
  
    }

      public function update_password(UpdateUserRequest $request)
            {

                $userx = Auth::user();
                $id =  $userx->id;
                 $user = User::find($id);
                 $password =  $request->input('password'); 
      
                      if (Hash::check($request->input('old_password'), $userx->password)) {
                             $user->password =  Hash::make($password); 
                             $user->save();  
                              return redirect('user')->with('message', 'update succsess');                   
                    }
                    else {

                         return redirect('user')->withErrors(['old password invalid ']);
                    }

            }
}
