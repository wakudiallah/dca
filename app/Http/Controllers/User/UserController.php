<?php

namespace App\Http\Controllers\User;


use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\PraApplication;
use App\Model\Basic;
use App\Model\Term;
use App\Mail_System;
use Hash;
use Auth;
use Input;
use Mail;
use App\Http\Requests\UpdateUserRequest;
 
 
use Ramsey\Uuid\Uuid;


use App\Model\Log_download;
use App\Model\Blocked_ic;
use App\Model\Basic_v;
use App\Model\Empinfo;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount;
use App\Model\LoanAmmount_v;
use App\Model\Contact;
use App\Model\Contact_v;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Spouse;
use App\Model\Spouse_v;
use App\Model\Reference;
use App\Model\Reference_v;
use App\Model\Financial;
use App\Model\Financial_v;
use App\Model\Postcode;
use App\Model\Package;
use App\Model\Employment;
use App\Model\Document;
use App\Model\Document_v;
use App\Model\Branch;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use App\Model\Commitments;
use App\Model\Foreigner;
use App\Model\Subscription;
use App\ScoreRating;
use App\ScoreCard;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Model\Country;
use App\Model\Occupations;
use App\Model\Position;
use App\Model\Relationship;
use App\Model\Title;

use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Background;
use App\Model\RbpRate;
use App\Model\FinancingPackage;
use App\Model\WapsStatus;
use DateTime;
use App;
use PDF;
use DB;
use Alert;

class UserController extends Controller
{

    public function __construct() {
                // $this->middleware('auth'); 
               
                 
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
        $user = Auth::user();
        
        return view('home.edit_user', compact('user')); 
      }

       public function first_login()
    {
        
        $user = Auth::user();
        
        return view('admin.first_login', compact('user')); 
      }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
   public function create()
    {
        
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $count_user = User::where('icnumber',$request->ic)->count();
        $count_email = User::where('email',$request->Email)->count();
        $idpra = $request->input('id_pras');

       // dd($count_user);
        if($count_user>=1){
               // Alert::error('IC Number aready registerded') ;
                return redirect('/praapplication/'.$idpra)->withErrors(['IC Number already registerded, Please log in to your account']);
        }
        else if ($count_email>=1){
            Alert::error('email already taken') ;
            return redirect('/praapplication/'.$idpra);
        }
        else{
            $user = new User;
            $email = $request->input('Email');
             $icnumber = $request->input('icnumber');
            $fullname = $request->input('FullName2');
            $active_code = str_random(100);
            $password2 = $request->input('password');
            $user->password = Hash::make($request->password);
            $user->id_pra   = Uuid::uuid4()->getHex(); // toString();     
            $user->email =  $email;
            $user->id_pra =  $idpra;
            $user->activation_code = $active_code;
            $user->name =  $fullname;
            $user->active =  "1";
            $user->role =  "9";
            $user->save();

             $contact       = new Contact;
            $empinfo       = new Empinfo;
            $loanammount   = new LoanAmmount;
            $spouse        = new Spouse;
            $document      = new Document;
            $reference     = new Reference;
            $financial     = new Financial;
            $term          = new Term;
            $dsr_a         = new Dsr_a;
           
            $dsr_c         = new Dsr_c;
            $dsr_d         = new Dsr_d;
            $dsr_e         = new Dsr_e;
            $riskrating    = new RiskRating;
            $waps          = new Waps;
             $foreigner = new Foreigner;
            $subscription = new Subscription;
             $rbp_rate = new RbpRate;
               $waps_status = new WapsStatus;

             $rbp_rate->id_pra   =  $idpra;
            $rbp_rate->save();

            $spouse->id_praapplication   =  $idpra;
            $spouse->save();

            $financial->id_praapplication   =  $idpra;
            $financial->save();

            $reference->id_praapplication   =  $idpra;
            $reference->save();

             

            $dsr_c->id_praapplication   =  $idpra;
            $dsr_c->save();

            $dsr_d->id_praapplication   =  $idpra;
            $dsr_d->save();

            $dsr_e->id_praapplication   =  $idpra;
            $dsr_e->save();

            $riskrating->id_praapplication   =  $idpra;
            $riskrating->save();

            $foreigner->id_praapplication   =  $idpra;
            $foreigner->save();

            $subscription->id_praapplication   =  $idpra;
            $subscription->save();

             $waps_status->id_praapplication   =  $idpra;
            $waps_status->save();
                
            //dd($idpra);
             $today = date('Y-m-d H:i:s');
            $praap = PraApplication::where('id_pra',$idpra)->update(['email' => $email,'date_registration' => $today  ]);
           

             $term = Term::where('id_praapplication', $idpra)
                ->update(['email' => $email,'fullname' => $fullname,'status'=>'1']);


            $basic = Basic::where('id_praapplication', $idpra)
                ->update(['corres_email' => $email  ]);

           
            $main_sender = Mail_System::where('id',1)->first();
            $main_sender_name= $main_sender->name;
            $main_sender_email= $main_sender->email;

            $noreply_sender = Mail_System::where('id',2)->first();
            $noreply_sender_name = $noreply_sender->name;
            $noreply_sender_email = $noreply_sender->email;
            $pra = PraApplication::latest('created_at')->where('id_pra', $idpra)->limit('1')->first(); 
            $dsr_b = Dsr_b::latest('created_at')->where('id_praapplication', $idpra)->limit('1')->first(); 
            $pdf = PDF::loadView('pdf.pds', compact('dsr_b','pra'));

            try{

                Mail::send('mail.register', compact('active_code','password2','email','pdf'), function ($message) use ($main_sender_email,$main_sender_name, $email, $fullname,$pdf) {
                        $message->from($main_sender_email, $main_sender_name);
                        $message->subject('Email Confirmation');
                        $message->to($email, $fullname);
                         $message->attachData($pdf->output(), "pds.pdf");
                    });
                      
                 // Notif to Admin
                 // Send to Admin Email
                 
                   $k = Mail::send('mail.register_notif', compact('email','fullname'), function ($message) use ($noreply_sender_email, $noreply_sender_name,$main_sender_email, $main_sender_name) {
                        $message->from($noreply_sender_email, $noreply_sender_name);
                        $message->subject('New User Registration');
                        $message->to( $main_sender_email, $main_sender_name);
                    });

                    $credentials = array(
                    'email' => $email,
                    'password' => $password2 );

                    if (Auth::attempt($credentials)) {
                         Alert::success('Registration successful');
                         return redirect('upload');
                    }
                }
                 catch(\Swift_TransportException $e){
                    //dd($e, app('mailer'));

                    $credentials = array(
                    'email' => $email,
                    'password' => $password2 );

                    if (Auth::attempt($credentials)) {
                         Alert::success('Registration successful!! (email not sent because you not connected to internet');
                         return redirect('upload');
                    }
                }  

        }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function show($id)
    {
        $confirmation = User::where('activation_code', $id) ->update(['active' => 1 ]);
            if($confirmation) {
                return redirect('auth/login')->with('message', 'email confirmation success'); }
            else {
                return redirect('auth/login')->withErrors(['failed email confirmation ']);
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
  public function update()
    {
       
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {       
  
    }

    public function update_password(UpdateUserRequest $request)
    {
        $userx = Auth::user();
        $id =  $userx->id;
        $user = User::find($id);
        $password =  $request->input('password'); 
        if(strcmp($request->input('old_password'), $request->input('password')) == 0){
            //Current password and new password are same
        return redirect('user')->withErrors(['New Password cannot be same as your current password. Please choose a different password ']);
        }
        if (Hash::check($request->input('old_password'), $userx->password)) {
            $user->password =  Hash::make($password); 
            $user->save();  
            Alert::success('update success');
            return redirect('home');                     
        }
        else {
            return redirect('user')->withErrors(['old password invalid ']);
        }

    }

    public function update_password_first(UpdateUserRequest $request)
    {
        $userx = Auth::user();
        $id =  $userx->id;
        $user = User::find($id);
        $password =  $request->input('password'); 
        if(strcmp($request->input('old_password'), $request->input('password')) == 0){
            //Current password and new password are same
        return redirect('first_login')->withErrors(['New Password cannot be same as your current password. Please choose a different password ']);
        }
        if (Hash::check($request->input('old_password'), $userx->password)) {
            $user->password =  Hash::make($password); 
            $user->first_login =  '0'; 
            $user->save();  
            Alert::success('update success');
            return redirect('admin');                     
        }
        else {
            return redirect('first_login')->withErrors(['old password invalid ']);
        }

    }

      public function downloadpdf($id_pra) {



        $user = Auth::user();
         $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id_pra',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
       
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $pra = PraApplication::latest('created_at')->where('id_pra',  $id_pra )->limit('1')->first();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
        $tenure = Tenure::where('id_loan',$id_loan)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
         $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 
    
      // Set Time Download
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_download = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id_pra;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'View PDF Document';
        $log_download->type   =  'PDF Document';
        $log_download->downloaded_at   =  $time_download;
        $log_download->save();
        $dsr_a = Dsr_a::where('id_praapplication',$id_pra)->first();
        $dsr_b = Dsr_b::where('id_praapplication',$id_pra)->first();
        $dsr_c = Dsr_c::where('id_praapplication',$id_pra)->first();
        $dsr_d = Dsr_d::where('id_praapplication',$id_pra)->first();
        $dsr_e = Dsr_e::where('id_praapplication',$id_pra)->first();
        $foreigner = Foreigner::where('id_praapplication',$id_pra)->first();
        $subs = Subscription::where('id_praapplication',$id_pra)->first();
      $pdf = PDF::loadView('pdf.app_form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary', 'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a','pra','foreigner','subs'))->setPaper('a4');
        //return $pdf->loadView('FORM-'.$time_name.'-'.$basic->new_ic.'.pdf');
  
         return $pdf->stream('report - '.$basic->new_ic.'.pdf', array('Attachment'=>false));

         /* return view('pdf.app_form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary', 'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a'));   */
       }
       

}
