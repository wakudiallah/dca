<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Auth;

use App\User;

class RoleController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $userx = Auth::user();
        $user = Auth::user();

        $roles = Role::all();
            $permissions = Permission::all();


            $idsaya = Auth::user()->id;
            
            $myid = User::where('id',$idsaya)->first();

        return view('role.index', compact('roles', 'permissions','userx', 'myid', 'user'));
        Auth::user()->endcan('edit_roles');

    }

 


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles'
            
        ]);

        if( Role::create($request->only('name')) ) {
            
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);

            $role->syncPermissions($permissions);

            
        } else {
            return redirect()->back();
        }

        return redirect()->route('roles.index');
    }
}
