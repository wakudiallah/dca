<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Relationship;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use App\Model\Branch;
use Auth;
use App\User;

class DashboardController extends Controller
{
    


    public function dashboard_dsu()
    {
        //$this->authorize('view_dashboards');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $total_branch = Branch::count();
        $total_fa = User::where('role','8')->count();
        $total_admin_dca = User::where('role','4')->count();
        $total_admin_2 = User::where('role','2')->count();
        $last_login = User::orderBy('updated_at', 'DESC')->where('role', '8')->limit(8)->get();
        return view('dashboard.dsu_dashboard', compact('total_branch','user', 'total_user', 'last_login', 'total_fa', 'total_admin_dca', 'total_admin_2'));
    }

     public function dashboard_admin_2()
    {
        //$this->authorize('view_dashboards');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $total_branch = Branch::count();
        $total_user = User::count();
        $last_login = User::orderBy('updated_at', 'DESC')->limit(5)->get();
        return view('dashboard.admin2_dashboard', compact('total_branch','user', 'total_user', 'last_login'));
    }

}
