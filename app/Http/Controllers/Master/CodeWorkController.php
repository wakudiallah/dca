<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\CodeWorkSec;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;

use Alert;


class CodeWorkController extends Controller
{
    public function index()
    {
        
    	

        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $country = CodeWorkSec::get();
        return view('admin.master.codework', compact('country','user'));
    }


    public function store(Request $request)
    {
        $user             = Auth::user()->id;
        $codework = $request->code;

        $check =  CodeWorkSec::where('workseccode', $codework)->first();

        if(empty($check->codework)){
        $data             =  new CodeWorkSec;

        $data->workseccode     = $request->code;
        $data->worksecdesc     = $request->name;
        $data->save();


        return redirect('/admin/master/codework')->with(['message' => 'Data Successfully Added']);
        }else{
        	return redirect('admin/master/codework')->with(['error' => ' Data Already axist']);
        }
    }


     public function update(Request $request, $id)
    {
        $code =  $request->input('code');
        $name =  $request->input('name');

        $count = CodeWorkSec::where('workseccode', $code)->count();
        $count2 = CodeWorkSec::where('workseccode', $code)->where('id',$id)->count();

         if(($count=='1') AND ($count2=='0')) {

            Alert::warning('Data already exist');
            return redirect('/admin/master/codework');
          
        }else{
                 $data = CodeWorkSec::where('id',$id)->update(array('workseccode' => $code, 'worksecdesc' => $name ));

        return redirect('admin/master/codework')->with(['message' => 'Data Successfully Added']);
        }




        
    }


    public function destroy($id)
    {
        CodeWorkSec::where('id',$id)->delete();   
        
        return redirect('admin/master/codework')->with(['message' => 'Data Successfully Added']);
    }
}
