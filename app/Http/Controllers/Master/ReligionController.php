<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Religion;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class ReligionController extends Controller
{
     public function index()
    {
        
        $this->authorize('view_parameter_religions');

        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Religion::get();
        return view('admin.master.religion', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $code = $request->code;
        $get = Religion::where('code', $code)->first();

        if(empty($get->code)){

            $user             = Auth::user()->id;

            $data             =  new Religion;

            $data->code      = $request->code;
            $data->religion     = $request->name;
            $data->is_active     = '1';
            $data->created_by     = $user;
            
            $data->save();

    		Alert::success('Data Successfully Added');
            return redirect('/admin/master/religion')->with(['message' => 'Data Successfully Added']);
         }
        else{
            Alert::warning('Data already axist');
            return redirect('/admin/master/religion')->with(['message' => 'Data Successfully Added']);
        }

    }


    
    public function updated(Request $request, $id)
    {
        $user = Auth::user()->id;

        $code =  $request->input('code');
        $religion =  $request->input('name');

        $get = Religion::where('id',$id)->first();
        $count = Religion::where('code', $code)->count();
        $count2 = Religion::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

            Alert::warning('Data already exist');
            return redirect('/admin/master/religion');
          
        }else{
                  $data = Religion::find($id)->update(array('code' => $code, 'religion' => $religion, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/religion');
        }

    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Religion::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Religion::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/religion')->with(['message' => 'Data Successfully Added']);
    }
}
