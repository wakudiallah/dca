<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\CodeJobStatus;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class CodeJobStatusController extends Controller
{
    public function index()
    {
        

        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $job = CodeJobStatus::get();
        return view('admin.master.codejobstatus', compact('job','user'));
    }


    public function store(Request $request)
    {
        $user             = Auth::user()->id;
        $codework = $request->code;

        $check =  CodeJobStatus::where('code', $codework)->first();

        if(empty($check->codework)){
        $data             =  new CodeJobStatus;

        $data->code     = $request->code;
        $data->descriptions     = $request->name;
        $data->bnmcode     = $request->bnm;
        $data->save();

        Alert::success('Data Successfully Added');
        return redirect('/admin/master/codejobstatus')->with(['message' => 'Data Successfully Added']);
        }else{
        	return redirect('/admin/master/codejobstatus')->with(['error' => ' Data Already axist']);
        }
    }


     public function update(Request $request, $id)
    {
        $code =  $request->input('code');
        $name =  $request->input('name');
        $bnm =  $request->input('bnm');

        

        $data = CodeJobStatus::where('id',$id)->update(array('code' => $code, 'descriptions' => $name, 'bnmcode' => $bnm ));

        Alert::success('Data Successfully Updated');
        return redirect('/admin/master/codejobstatus')->with(['message' => 'Data Successfully Added']);
    }


    public function destroy($id)
    {
        CodeJobStatus::where('id',$id)->delete();   
        
        Alert::success('Data Successfully Deleted');
        return redirect('/admin/master/codejobstatus')->with(['message' => 'Data Successfully Added']);
    }
}
