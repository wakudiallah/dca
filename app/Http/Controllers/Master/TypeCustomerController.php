<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TypeCustomer;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;
use Illuminate\Support\Facades\Validator;

class TypeCustomerController extends Controller
{
     public function index()
    {
        
        
        $this->authorize('view_parameter_type_customers');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = TypeCustomer::get();
        return view('admin.master.type_customer', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    	$code = $request->code;
    	$get = TypeCustomer::where('code', $code)->first();

    	if(empty($get->code)){
    		$user             = Auth::user()->id;

	        $data             =  new TypeCustomer;

	        $data->code      = $request->code;
	        $data->type_customer     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Submit Added');
	        return redirect('/admin/master/type-customer')->with(['message' => 'Data Successfully Added']);
    	}
    	else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/type-customer')->with(['message' => 'Data Successfully Added']);
    	}
	    
        
    }


    
    public function update(Request $request, $id)
    {
        

        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $type_customer =  $request->input('name');



        $get = TypeCustomer::where('code', $code)->where('id',$id)->count();

       $count = TypeCustomer::where('code', $code)->count();
        $count2 = TypeCustomer::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
                Alert::warning('Data already exist');
            return redirect('/admin/master/type-customer')->with(['message' => 'Data Successfully Added']);
	       
    	}else{
                 $data = TypeCustomer::find($id)->update(array('code' => $code, 'type_customer' => $type_customer, 'updated_by' => $user));

            Alert::success('Data Successfully Submit Updated');
            return redirect('admin/master/type-customer')->with(['message' => 'Data Successfully Updated']);
    		
    	}



           


    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = TypeCustomer::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        TypeCustomer::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/type-customer')->with(['message' => 'Data Successfully Deleted']);
    }
}
