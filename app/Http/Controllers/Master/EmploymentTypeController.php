<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\EmploymentType;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class EmploymentTypeController extends Controller
{
    public function index()
    {
        
        $this->authorize('view_parameter_employment_types');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = EmploymentType::get();
        return view('admin.master.employment_type', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    	$code = $request->code;
    	$get = EmploymentType::where('code', $code)->first();

    	if(empty($get->code)){


	        $user             = Auth::user()->id;

	        $data             =  new EmploymentType;

	        $data->code      = $request->code;
	        $data->employment_type     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Added');
	        return redirect('/admin/master/employmenttype')->with(['message' => 'Data Successfully Added']);

	    }
    	else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/employmenttype')->with(['message' => 'Data Successfully Added']);
    	}
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $employment_type =  $request->input('name');


        $get = EmploymentType::where('code', $code)->first();

        if(empty($get->code)){
	        $data = EmploymentType::where('id',$id)->update(array('code' => $code, 'employment_type' => $employment_type, 'updated_by' => $user));

	        Alert::success('Data Successfully Updated');
	        return redirect('admin/master/employmenttype')->with(['message' => 'Data Successfully Updated']);
	    }else{

    		Alert::warning('Data already exist');
    		return redirect('/admin/master/employmenttype')->with(['message' => 'Data Successfully Added']);
    	}
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = EmploymentType::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        EmploymentType::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Deleted');
        return redirect('admin/master/employmenttype')->with(['message' => 'Data Successfully Deleted']);
    }
}
