<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Occupations;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;
use Illuminate\Http\Request;
use App\Authorizable;
use Validator;

class OccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   // use Authorizable;

    public function index()
    {
      
        //dd(Auth::user()->role);
        //dd(Auth::user()->id);
       //$this->authorize('view_parameters');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $occupation = Occupations::OrderBy('id', 'DESC')->get();
        return view('admin.master.occupation', compact('occupation','user'));
    }

    public function store(Request $request)
    {
        
        $user             = Auth::user()->id;

        

        $validator = Validator::make($request->all(), [
            'occupation_code' => 'required|unique:occupations',
            'occupation_desc' => 'required',
        ]);

        if ($validator->fails()) {
            
            Alert::warning('Data already exist');
            return redirect()->back()->with(['message' => 'Mail Successfully Added']);

        }
        else{

            $data             =  new Occupations;

            $data->occupation_code     = $request->code;
            $data->occupation_desc     = $request->desc;
            $data->is_active            = '1';
            $data->save();

            Alert::success('Data Successfully Added');
            return redirect('/admin/master/occupation')->with(['message' => 'Mail Successfully Added']);
        }


    }


    public function update(Request $request, $id)
    {
        $user = Auth::user()->id;

        $code =  $request->input('code');
        $desc =  $request->input('desc');


        $get = Occupations::where('id',$id)->first();
        $count = Occupations::where('occupation_code', $code)->count();
        $count2 = Occupations::where('occupation_code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

            Alert::warning('Data already exist');
            return redirect('/admin/master/occupation');
          
        }else{
                  $data = Occupations::find($id)->update(array('occupation_code' => $code, 'occupation_desc' => $desc));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/occupation');
        }
    }


    public function destroy($id)
    {
       

         $data = Occupations::find($id)->update(array( 'is_active' => '0'));

        Occupations::where('id',$id)->delete();   
        
        return redirect('admin/master/occupation')->with(['message' => 'Mail Successfully Added']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
}
