<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Gender;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class GenderController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Gender::get();
        return view('admin.master.gender', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $code = $request->code;
        $get = Gender::where('code', $code)->first();


        if(empty($get->code)){

            $user             = Auth::user()->id;

            $data             =  new Gender;



            $data->code      = $request->code;
            $data->desc_gender     = $request->name;
            $data->is_active     = '1';
            
            $data->save();

            Alert::success('Data Successfully Added');
            return redirect('/admin/master/gender')->with(['message' => 'Data Successfully Added']);
        } 
        
        else{
            Alert::warning('Data already exist');
            return redirect('/admin/master/gender')->with(['message' => 'Data Successfully Added']);
        }

    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $gender =  $request->input('name');


        $get = Gender::where('code', $code)->where('id',$id)->count();

        $count = Gender::where('code', $code)->count();
        $count2 = Gender::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

                Alert::warning('Data already exist');
            return redirect('/admin/master/gender')->with(['message' => 'Data Successfully Added']);

        }else{
            $data = Gender::find($id)->update(array('code' => $code, 'desc_gender' => $gender));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/gender')->with(['message' => 'Data Successfully Added']);
            
        }

    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Gender::where('id',$id)->update(array('is_active' => '0'));
        Gender::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/gender')->with(['message' => 'Data Successfully Added']);
    }

}
