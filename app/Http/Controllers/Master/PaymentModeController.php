<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\PaymentMode;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class PaymentModeController extends Controller
{
    public function index()
    {
        $this->authorize('view_parameter_payment_modes');
        
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = PaymentMode::get();
        return view('admin.master.payment_mode', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->code;
    	$get = PaymentMode::where('code', $code)->first();

    	if(empty($get->code)){


	        $user             = Auth::user()->id;

	        $data             =  new PaymentMode;

	        $data->code      = $request->code;
	        $data->payment_mode     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Added');
	        return redirect('/admin/master/payment-mode')->with(['message' => 'Data Successfully Added']);
	    }
    	else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/payment-mode')->with(['message' => 'Data Successfully Added']);
    	}
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $payment_mode =  $request->input('name');



    	$get = PaymentMode::where('code', $code)->where('id',$id)->count();
         $count = PaymentMode::where('code', $code)->count();
        $count2 = PaymentMode::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
            Alert::warning('Data already exist');
            return redirect('/admin/master/payment-mode')->with(['message' => 'Data Successfully Added']);
	      

	    }else{

    		  $data = PaymentMode::find($id)->update(array('code' => $code, 'payment_mode' => $payment_mode, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/payment-mode')->with(['message' => 'Data Successfully Added']);
    	}
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = PaymentMode::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        PaymentMode::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/payment-mode')->with(['message' => 'Data Successfully Deleted']);
    }
}
