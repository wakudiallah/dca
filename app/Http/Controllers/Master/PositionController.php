<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Position;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class PositionController extends Controller
{
    public function index()
    {
               // $this->authorize('view_parameters');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $position = Position::orderBy('id','DESC')->get();
        return view('admin.master.position', compact('position','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;
        $position = $request->code;

        $position =  Position::where('position_code', $position)->first();

        if(empty($position->position_code)){
        $data             =  new Position;

        $data->position_code      = $request->code;
        $data->position_desc     = $request->name;
        $data->save();

        Alert::success('Data Successfully Added');
        return redirect('/admin/master/position');
        }else{
            Alert::warning('Data already axist');
        	return redirect('/admin/master/position');
        }
    }


    
    public function update(Request $request, $id)
    {
        $code =  $request->input('code');
        $name =  $request->input('name');

        $get = Position::where('position_code', $code)->where('id',$id)->count();

       $count = Position::where('position_code', $code)->count();
        $count2 = Position::where('position_code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
            Alert::warning('Data already exist');
            return redirect('/admin/master/position')->with(['message' => 'Data Successfully Updated']);
            
        }else{

            $data = Position::find($id)->update(array('position_code' => $code, 'position_desc' => $name ));
            Alert::success('Data Successfully Updated');
            return redirect('admin/master/position')->with(['message' => 'Mail Successfully Added']);
        }
    }


    public function destroy($id)
    {
        Position::where('id',$id)->delete();   
        
        return redirect('admin/master/position')->with(['message' => 'Mail Successfully Added']);
    }
}
