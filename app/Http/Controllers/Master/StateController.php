<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Occupations;
use App\Model\State;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              //  $this->authorize('view_parameters');

                
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $state = State::OrderBy('id', 'DESC')->get();
        return view('admin.master.state', compact('state','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        

        $user             = Auth::user()->id;
        $code = $request->code;


        $check_code = State::where('state_code', $code)->where('state_code', $code)->first();
        


        if( empty($check_code->state_code) ){
                
            
            $data             =  new State;

            $data->state_code     = $request->code;
            $data->state_name    = $request->name;
            $data->save();

            Alert::success('Data Successfully Added');
            return redirect('/admin/master/state')->with(['message' => 'State Successfully Added']);
        }else{
            Alert::warning('Data already exist');
            return redirect('admin/master/state');
        }
    }


    public function update(Request $request, $id)
    {
        $state_code =  $request->input('code');
        $state_name =  $request->input('name');


        $count = State::where('state_code', $state_code)->count();
        $count2 = State::where('state_code', $state_code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
            Alert::warning('Data already exist');
            return redirect('/admin/master/state');
          
        }else{

        $data = State::find($id)->update(array('state_code' => $state_code, 'state_name' => $state_name ));
         Alert::success('Data Successfully Updated');
        return redirect('admin/master/state')->with(['message' => 'State Successfully Added']);
        }
    }

        public function destroy($id)
    {
        State::where('id',$id)->delete();   
        
        return redirect('admin/master/state')->with(['message' => 'State Successfully Added']);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
}
