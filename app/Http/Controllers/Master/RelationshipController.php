<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Relationship;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;


class RelationshipController extends Controller
{
   public function index()
    {
                
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $relationship = Relationship::orderBy('id','DESC')->get();
        return view('admin.master.relationship', compact('relationship','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;
        $relationship = $request->code;

        $check =  Relationship::where('relationship_code', $relationship)->first();

        if(empty($check->relationship_code)){
        $data             =  new Relationship;

        $data->relationship_code      = $request->code;
        $data->relationship_desc     = $request->name;
        $data->is_active     = '1';

        $data->save();

        Alert::success('Data Successfully Added');
        return redirect('/admin/master/relationship');
        }else{
             Alert::warning('Data already exist');
            return redirect('/admin/master/relationship');
        }
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;
        
        $code =  $request->input('code');
        $name =  $request->input('name');

         $get = Relationship::where('relationship_code', $code)->where('id',$id)->count();

         $count = Relationship::where('relationship_code', $code)->count();
        $count2 = Relationship::where('relationship_code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

             Alert::warning('Data already exist');
            return redirect('/admin/master/relationship')->with(['message' => 'Data Successfully Added']);

        }else{
             $data = Relationship::find($id)->update(array('relationship_code' => $code, 'relationship_desc' => $name ));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/relationship')->with(['message' => 'Data Successfully Added']);
          
        }
    }


    public function destroy($id)
    {
        Relationship::where('id',$id)->delete();   
        
        return redirect('admin/master/relationship')->with(['message' => 'Data Successfully Added']);
    }
}
