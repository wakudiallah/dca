<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Resident;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class ResidentController extends Controller
{
     public function index()
    {
        $this->authorize('view_parameter_residents');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Resident::get();
        return view('admin.master.resident', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->code;
    	$get = Resident::where('code', $code)->first();

    	if(empty($get->code)){

	        $user             = Auth::user()->id;

	        $data             =  new Resident;

	        $data->code      = $request->code;
	        $data->resident     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Added');
	        return redirect('/admin/master/resident')->with(['message' => 'Data Successfully Added']);
	    }
	    else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/resident')->with(['message' => 'Data Successfully Added']);
    	}
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $resident =  $request->input('name');

         $get = Resident::where('code', $code)->where('id',$id)->count();

        $count = Resident::where('code', $code)->count();
        $count2 = Resident::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

            Alert::warning('Data already exist');
            return redirect('/admin/master/resident')->with(['message' => 'Data Successfully Added']);
	       

	    }else{

             $data = Resident::find($id)->update(array('code' => $code, 'resident' => $resident, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/resident')->with(['message' => 'Data Successfully Added']);
    	}
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Resident::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Resident::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/resident')->with(['message' => 'Data Successfully Deleted']);
    }
}
