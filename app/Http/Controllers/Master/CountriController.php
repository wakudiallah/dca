<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Country;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class CountriController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $country = Country::orderBy('id', 'DESC')->get();
        return view('admin.master.country', compact('country','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;
        $country = $request->code;

        $check =  Country::where('country_code', $country)->first();

        if(empty($check->country_code)){
        $data             =  new Country;

        $data->country_code      = $request->code;
        $data->country_desc     = $request->name;

        $data->is_active     = '1';
        $data->save();

        Alert::success('Data Successfully Added');
        return redirect('/admin/master/country')->with(['message' => 'Mail Successfully Added']);
        }else{
            Alert::warning('Data already axist');
        	return redirect('admin/master/country')->with(['error' => 'Country Code Already exist']);
        }
    }


    
    public function update(Request $request, $id)
    {
        

        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $name =  $request->input('name');


        $get = Country::where('country_code', $code)->where('id',$id)->count();

        $count = Country::where('country_code', $code)->count();
        $count2 = Country::where('country_code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

            Alert::warning('Data already exist');
            return redirect('/admin/master/country')->with(['message' => 'Data Successfully Updated']);


            

        }else{

            $data = Country::where('id',$id)->update(array('country_code' => $code, 'country_desc' => $name));

            Alert::success('Data Successfully Submit Updated');
            return redirect('admin/master/country')->with(['message' => 'Data Successfully Updated']);
        }

    }



    public function destroy($id)
    {
        Country::where('id',$id)->delete();   
        
         Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/country')->with(['message' => 'Mail Successfully Added']);
    }
}
