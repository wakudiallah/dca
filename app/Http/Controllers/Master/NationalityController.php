<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Nationality;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class NationalityController extends Controller
{
    public function index()
    {
        
        $this->authorize('view_parameter_nationalities');

        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Nationality::get();
        return view('admin.master.nationality', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $code = $request->code;
        $get = Nationality::where('code', $code)->first();

        if(empty($get->code)){

            $user             = Auth::user()->id;

            $data             =  new Nationality;

            $data->code      = $request->code;
            $data->nationality     = $request->name;
            $data->is_active     = '1';
            $data->created_by     = $user;
            
            $data->save();

    		Alert::success('Data Successfully Added');
            return redirect('/admin/master/nationality')->with(['message' => 'Data Successfully Added']);

            }
        else{
            Alert::warning('Data already exist');
            return redirect('/admin/master/nationality')->with(['message' => 'Data Successfully Added']);
        }
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $nationality =  $request->input('name');



        $get = Nationality::where('code', $code)->where('id',$id)->count();

        $count = Nationality::where('code', $code)->count();
        $count2 = Nationality::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {

            $data = Nationality::find($id)->update(array('code' => $code, 'nationality' => $nationality, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/nationality')->with(['message' => 'Data Successfully Added']);
        }else{

            Alert::warning('Data already exist');
            return redirect('/admin/master/nationality')->with(['message' => 'Data Successfully Added']);
        }
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Nationality::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Nationality::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/nationality')->with(['message' => 'Data Successfully Added']);
    }
}
