<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Race;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class RaceController extends Controller
{
     public function index()
    {
        $this->authorize('view_parameter_races');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Race::get();
        return view('admin.master.race', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->code;
    	$get = Race::where('code', $code)->first();

    	if(empty($get->code)){
	        $user             = Auth::user()->id;

	        $data             =  new Race;

	        $data->code      = $request->code;
	        $data->race     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Added');
	        return redirect('/admin/master/race')->with(['message' => 'Data Successfully Added']);
	    }
    	else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/race')->with(['message' => 'Data Successfully Added']);
    	}
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $race =  $request->input('name');

        $get = Race::where('code', $code)->where('id',$id)->count();

         $count = Race::where('code', $code)->count();
        $count2 = Race::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
            Alert::warning('Data already exist');
            return redirect('/admin/master/race')->with(['message' => 'Data Successfully Added']);
	       
	    }else{

    		 $data = Race::find($id)->update(array('code' => $code, 'race' => $race, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/race')->with(['message' => 'Data Successfully Updated']);
    	}
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Race::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Race::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/race')->with(['message' => 'Data Successfully Deleted']);
    }
}
