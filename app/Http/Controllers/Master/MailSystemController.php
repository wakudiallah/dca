<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail_System;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;
use Validator;

class MailSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                $this->authorize('view_parameter_email_systems');
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $mail = Mail_System::get();
        return view('admin.master.mailsystem.index', compact('mail','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user             = Auth::user()->id;

        

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:mail_systems',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            
            Alert::warning('Data already exist');
            return redirect('/admin/master/mail-system')->with(['message' => 'Mail Successfully Added']);

        }
        else{

            $data             =  new Mail_System;
            $data->name      = $request->name;
            $data->email     = $request->email;
            $data->save();



            Alert::success('Data Successfully Added');
            return redirect('/admin/master/mail-system')->with(['message' => 'Mail Successfully Added']);
        }

       
    }


    
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:mail_systems',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            
            Alert::warning('Data already exist');
            return redirect('/admin/master/mail-system')->with(['message' => 'Mail Successfully Added']);

        }
        else{

            $name =  $request->input('name');
            $email =  $request->input('email');

            $data = Mail_System::where('id',$id)->update(array('name' => $name, 'email' => $email ));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/mail-system')->with(['message' => 'Mail Successfully Added']);
        }


    }


    public function destroy($id)
    {
        Mail_System::where('id',$id)->delete();   
        
        Alert::success('Data Successfully Deleted');
        return redirect('admin/master/mail-system')->with(['message' => 'Mail Successfully Added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}
