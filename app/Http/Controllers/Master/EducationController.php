<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Education;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class EducationController extends Controller
{
     public function index()
    {
        
        $user = Auth::user();
        
        $data = Education::get();
        return view('admin.master.education', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->code;
    	$get = Education::where('code', $code)->first();

    	if(empty($get->code)){

	        $user             = Auth::user()->id;

	        $data             =  new Education;

	        $data->code      = $request->code;
	        $data->education     = $request->name;
	        $data->is_active     = '1';
	        $data->created_by     = $user;
	        
	        $data->save();

			Alert::success('Data Successfully Added');
	        return redirect('/admin/master/education')->with(['message' => 'Data Successfully Added']);
	    }
    	else{
    		Alert::warning('Data already exist');
    		return redirect('/admin/master/education')->with(['message' => 'Data Successfully Added']);
    	}
    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $education =  $request->input('desc');



        $get = Education::where('code', $code)->first();

    	$count = Education::where('code', $code)->count();
        $count2 = Education::where('code', $code)->where('id',$id)->count();
            //dd($count, $count2);
        if(($count=='1') AND ($count2=='0')) {

                Alert::warning('Data already exist');
            return redirect('/admin/master/education')->with(['message' => 'Data Successfully Added']);
	        

	    }else{
            $data = Education::find($id)->update(array('code' => $code, 'education' => $education, 'updated_by' => $user));

            Alert::success('Data Successfully Updated');
            return redirect('admin/master/education')->with(['message' => 'Data Successfully Added']);
    		
    	}
    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Education::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Education::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/education')->with(['message' => 'Data Successfully Deleted']);
    }
}
