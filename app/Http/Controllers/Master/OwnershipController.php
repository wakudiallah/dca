<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Ownership;
use App\Http\Requests;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;

class OwnershipController extends Controller
{
    public function index()
    {
        $this->authorize('view_parameter_ownerships');
        
        $user = Auth::user();
        /*if ($user->role<>4) {
            return redirect('admin');   
        }*/
        $data = Ownership::get();
        return view('admin.master.ownership', compact('data','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $code = $request->code;
        $get = Ownership::where('code', $code)->first();

        if(empty($get->code)){

            $user             = Auth::user()->id;

            $data             =  new Ownership;

            $data->code      = $request->code;
            $data->ownership     = $request->name;
            $data->is_active     = '1';
            $data->created_by     = $user;
            
            $data->save();

    		Alert::success('Data Successfully  Added');
            return redirect('/admin/master/ownership')->with(['message' => 'Data Successfully Added']);
         }
        else{
            Alert::warning('Data already exist');
            return redirect('/admin/master/ownership')->with(['message' => 'Data Successfully Added']);
        }

    }


    
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $code =  $request->input('code');
        $ownership =  $request->input('name');


        $get = Ownership::where('code', $code)->where('id',$id)->count();

        $count = Ownership::where('code', $code)->count();
        $count2 = Ownership::where('code', $code)->where('id',$id)->count();

        if(($count=='1') AND ($count2=='0')) {
             Alert::warning('Data already exist');
            return redirect('/admin/master/ownership')->with(['message' => 'Data Successfully Added']);

           

        }else{
             $data = Ownership::find($id)->update(array('code' => $code, 'ownership' => $ownership, 'updated_by' => $user));

            Alert::success('Data Successfully Submit Updated');
            return redirect('admin/master/ownership')->with(['message' => 'Data Successfully Added']);
           
        }

    }


    public function destroy($id)
    {
        $user             = Auth::user()->id;

        $data = Ownership::where('id',$id)->update(array('is_active' => '0', 'deleted_by' => $user));
        Ownership::where('id',$id)->delete();   
        

        Alert::success('Data Successfully Submit Deleted');
        return redirect('admin/master/ownership')->with(['message' => 'Data Successfully Added']);
    }
}
