<?php

namespace App\Http\Controllers\CLRT;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MO;
use App\Manager;
use App\Model\AntiAttrition;
use App\Model\SettlementInfo;
use App\User;
use App\Http\Requests;
use Auth;
use DB;
use App\Model\Employment;
use App\Model\PraApplication;
use App\Model\Loan;
use App\Model\Package;
use App\Model\Basic;
use App\Model\Contact;
use App\Model\Tenure;
use App\Model\Empinfo;
use App\Model\Document;
use App\Model\LoanAmmount;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Term;
use Input;
use DateTime;
use Ramsey\Uuid\Uuid;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use GuzzleHttp\Client;
class AntiAttritionController extends Controller
{
     public function index()
    {
        $user = Auth::user();
        $antiattrition = AntiAttrition::where('mo_id', $user->mo_id)->orderBy('created_at','DESC')->groupby('ACID')->get();
		return view('mo.clrt.index', compact('user','antiattrition')); 	  
    }

    public function antiattrition()
    {
        $user = Auth::user();
        $antiattrition = AntiAttrition::wherenotnull('mo_id')->orderBy('created_at','DESC')->groupby('ACID')->get();
        return view('clrt.antiattrition', compact('user','antiattrition'));    
    }

     public function multi()
    {
        $user = Auth::user();
        $mo = MO::get();
        $manager = Manager::get();
        //$antiattrition = AntiAttrition::where('mo_id', $user->mo_id)->orderBy('created_at','DESC')->get();
        $antiattrition = AntiAttrition::wherenull('mo_id')->orderBy('created_at','DESC')->get();
        $newoffer = SettlementInfo::where('status','0')->orderBy('created_at','DESC')->get();
        
		return view('mo.clrt.multi', compact('user','antiattrition','mo','manager','newoffer')); 	  
    }

   

    /**
     * Get Ajax Request and restun Data
     *
     * @return \Illuminate\Http\Response
     */
    public function myformAjax($id)
    {
        $cities = DB::table("mo")
                    ->where("id_manager",$id)
                    ->pluck("desc_mo","id_mo");
        return json_encode($cities);
    }

    public function assign_to_mo(Request $request){
        $user = Auth::user();
        $new = array_map(null, $request->CustIDNo, $request->ci);

        foreach($new as $new) {

             $anti = AntiAttrition::where('CustIDNo',$new[0])->update([
                "mo_id"       => $request->city,
                "assign_by"   => $user->id

            ]);
        }

        return redirect('/anti-attrition-multi');
    }

    public function send_to_mo(Request $request){
        $user   = Auth::user();
        $acid    = $request->acid;

       



        $praapplication = new PraApplication;
        $basic = new Basic;
        $contact = new Contact;
        $empinfo = new Empinfo;
        $loanammount = new LoanAmmount;
        $spouse = new Spouse;
        $document = new Document;
        $reference = new Reference;
        $financial = new Financial;
        $term = new Term;

        $dsr_a = new Dsr_a;
        $dsr_b = new Dsr_b;
        $dsr_c = new Dsr_c;
        $dsr_d = new Dsr_d;
        $dsr_e = new Dsr_e;
        $waps = new Waps;
        $riskrating = new RiskRating;

        $type = 'IN';
         $employment  = $request->input('employment');
        if($type=='IN')
            {
                $icnumber = $request->input('ic');
                $new_ic = $request->input('ic');
            }
        else{
            $icnumber = $request->input('dob');
            $other = $request->input('ic');
            $new_ic = $request->input('ic');
        }
        if($type=='IN')
            {
                $tanggal = substr($icnumber,4, 2);
                $bulan =  substr($icnumber,2, 2);
                $tahun = substr($icnumber,0, 2); 
                $jantina = substr($icnumber,10, 2); 
            }
        else{
                $genders = $request->input('gender');
                $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
                $jantina = $genders; 
        }
        
        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
            $tahun2 = "20".$tahun;
        }

         if($employment=='G') {
                $package=1;
                $package_name ="Mumtaz-i";
            }
            else if($employment=='G') {
               $package=2; 
               $package_name = "Afdhal-i";
            }
            else if($employment=='P') {
               $package=2; 
               $package_name = "Afdhal-i";
            }
            else if($employment=='P') {
               $package=3; 
               $package_name = "Private Sector PF-i";
            }
            else if($employment=='P') {
               $package=2; 
               $package_name = "Afdhal-i";
            }
             if($jantina % 2 ==0 )
                {
                    $gender           =  "F" ;
                }
                else
                {
                    $gender           =  "M" ;
                }
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
        $fullname = $request->input('fullname');
        $phone = $request->input('phone');
        $employer = $request->input('employer');
        $employer2 = $request->input('employer');
        $email = $request->input('email');
        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);
        $id = Uuid::uuid4()->getHex(); 
        $umur =  $oDateIntervall->y;

        $praapplication->id   =  $id;
        $praapplication->fullname = $fullname;
        $praapplication->email = $email;
        $praapplication->icnumber = $icnumber;
        $praapplication->phone = $phone;
        $praapplication->lat = $request->latitude;
        $praapplication->lng = $request->longitude;
        $praapplication->location = $request->location;
        $praapplication->referral_id = $request->city;
        $praapplication->save();

        $anti = AntiAttrition::where('ACID',$acid)->update([
            "mo_id"       => $request->city,
            "assign_by"   => $user->id,
            "id_praapplication"=>$id
        ]);

        $basic->id_praapplication   =  $id;
        $basic->new_ic = $new_ic;
        $basic->name =  $fullname ;
        $basic->dob =  $lahir ; 
        $basic->gender =  $gender ; 
        $basic->acid =  $acid ; 
        $basic->id_type =  $request->input('type') ; 
        $basic->anti_attrition_flag =  '1';
        $basic->save();

        $contact->id_praapplication   =  $id;
        $contact->handphone   =  $phone;
        $contact->save();

        $empinfo->id_praapplication   =  $id;
        if(!empty($employer)) {
            $empinfo->employer_id = $employer; }
        else {
            $empinfo->employer_id = 5;
        }

        if($employment  != '2' ){
            $empinfo->empname = $employer;
        }

        $empinfo->employment_id = $employment;
       // $empinfo->salary = $basicsalary;
        //$empinfo->allowance = $allowance;
        //$empinfo->deduction = $deduction;
        $empinfo->save();

        $loanammount->id_praapplication   =  $id;
        //$loanammount->loanammount   =  $loanAmount;
        $loanammount->save();

        $spouse->id_praapplication   =  $id;
        $spouse->save();

        $financial->id_praapplication   =  $id;
        $financial->save();

        $reference->id_praapplication   =  $id;
        $reference->save();

        $term->id_praapplication   =  $id;
        $term->status                   =  '0';
        $term->verification_status      =  '0';
        $term->verification_result      =  '0';
        $term->mo_stage                 =  '0';
        $term->verification_result_by_bank  =  '0';
        $term->id_branch                =  '0';
        $term->status_waps                =  '0';
        $term->referral_id = Auth::user()->id;
        $term->mo_id = Auth::user()->mo_id;
        $term->lat = $request->latitude;
        $term->lng = $request->longitude;
        $term->location = $request->location;
        $term->anti_attrition_flag =  '1';
        $term->save();

        //DSR Calculation

        $dsr_a->id_praapplication   =  $id;
        $dsr_a->sector   =  $request->employment;
        $dsr_a->save();

        $dsr_b->id_praapplication   =  $id;
        $dsr_b->save();

        $dsr_c->id_praapplication   =  $id;
        $dsr_c->save();

        $dsr_d->id_praapplication   =  $id;
        $dsr_d->save();

        $dsr_e->id_praapplication   =  $id;
        $dsr_e->save();

        $riskrating->id_praapplication   =  $id;
        $riskrating->save();
       

        return redirect('/clrt/new-loan-offer');
    }


    public function send_sms2(Request $request){
        $user = Auth::user();
        $code ='123456';
        

        return redirect('/anti-attrition-multi');
    }

    public function send_sms(Request $request){
        $client = new Client();
        $otp = rand(1000, 9999);
        $acid = $request->acid;
               

         
        //$message = "Your One Time Password is " . $otp;
         $res = $client->request('POST', 'https://www.isms.com.my/isms_send.php', [
            'header'=> [
            'Accept'     => 'application/json',
            ],

            'form_params' => [
                'un' => 'netxpert',
                'pwd' => 'netxpert123',
                'sendid' => '12345',
                'type' => '1',
                'dstno' => '60142449179',
                'msg' => 'Your One Time Password is ' . $otp
            ]
        ]);
       
        $count = Basic::where('acid',$acid)->limit('1')->first();

        $count_tac = $count->count_tac + 1 ;
       
        $basic = Basic::where('acid',$acid)->update([
            "otp"  => $otp,
            "count_tac"  => $count_tac

        ]);

        $basic = Term::where('id_praapplication',$count->id_praapplication)->update([
            "status"  => '1'

        ]);
        //$array = json_decode($res->getBody()->getContents(), true);
        //dd($array);
        return redirect('/clrt/loan-offer-detail-sms/'.$acid);
    }

    public function send_sms_next(Request $request){
        $client = new Client();
        $otp = rand(1000, 9999);
        $acid = $request->acid2;
               
        echo $acid;
         
        //$message = "Your One Time Password is " . $otp;
         $res = $client->request('POST', 'https://www.isms.com.my/isms_send.php', [
            'header'=> [
            'Accept'     => 'application/json',
            ],

            'form_params' => [
                'un' => 'netxpert',
                'pwd' => 'netxpert123',
                'sendid' => '12345',
                'type' => '1',
                'dstno' => '60142449179',
                'msg' => 'Your One Time Password is ' . $otp
            ]
        ]);
       
        $count = Basic::where('acid',$acid)->limit('1')->first();

        $count_tac = $count->count_tac + 1 ;
       
        $basic = Basic::where('acid',$acid)->update([
            "otp"  => $otp,
            "count_tac"  => $count_tac

        ]);
        //$array = json_decode($res->getBody()->getContents(), true);
        //dd($array);
        return redirect('/clrt/loan-offer-detail-sms/'.$acid);
    }

     public function verify_otp(Request $request){
        $acid   = $request->acid;
        $mobile = $request->mobile;
        $otp1    = $request->otp1;
        $otp2    = $request->otp2;
        $otp3    = $request->otp3;
        $otp4    = $request->otp4;

        $otp    = $otp1.''.$otp2.''.$otp3.''.$otp4;
                
        $basic = Basic::where('acid',$acid)->limit('1')->first();

        if($basic->otp != $otp){
            //alert("otp not match. Please try again");
             return redirect()->back()->with('alert', 'otp not match. Please try again');
        }
        else{
            return redirect('/clrt/request-ramcy/'.$acid)->with('alert', 'Send request to Ramcy');;
        }

    }

     public function send_sms_timeout(Request $request){
        $client = new Client();
        $otp = rand(1000, 9999);
        $acid = $request->acid;
                
        //$message = "Your One Time Password is " . $otp;
         $res = $client->request('POST', 'https://www.isms.com.my/isms_send.php', [
            'header'=> [
            'Accept'     => 'application/json',
            ],

            'form_params' => [
                'un' => 'netxpert',
                'pwd' => 'netxpert123',
                'sendid' => '12345',
                'type' => '1',
                'dstno' => '60142449179',
                'msg' => 'Your One Time Password is ' . $otp
            ]
        ]);
        $basic = Basic::where('acid',$acid)->update([
            "otp"  => $otp
        ]);

        //$array = json_decode($res->getBody()->getContents(), true);
        //dd($array);
        return redirect('/clrt/loan-offer-detail-sms/'.$acid);
    }
}
