<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\Employment;
use App\Model\PraApplication;
use App\Model\CodeIDType;
use App\Model\Loan;
use App\Model\FinancingPackage;
use App\Http\Controllers\Controller;
use Input;
use DateTime;
use Ramsey\Uuid\Uuid;
use App\Model\User;
use Auth;
use  Session;
use SOAPHeader;
use SoapClient;
use Alert;

class IndexController extends Controller

{
    public function mumtaz()
    {
        $employment = Employment::where('package_id','1')->get();
        $package    = FinancingPackage::where('code','M')->get();
        $idtype    = CodeIDType::get();
        return view('praapplication.mumtaz',compact('employment','idtype','package','background'));
    }

     public function privatesector()
    {
        $employment = Employment::wherein('package_id',['4','5'])->get();
        $package    = FinancingPackage::where('code','P')->get();
        $idtype    = CodeIDType::get();
        return view('praapplication.privatesector',compact('employment','idtype','package','background'));
    }
  

    public function afdhal()
    {
        $employment = Employment::wherein('package_id',['2','3'])->get();
        $package    = FinancingPackage::where('code','P')->get();
        $idtype    = CodeIDType::all();
        return view('praapplication.afdhal',compact('employment','idtype','package','background'));
    }

    public function package($id)
    {
        $data = Employment::where('id',$id)->with('package')->limit(1)->get();
        return $data;
    }

     public function ref($id)
    {
        $employment = Employment::where('status','1')->get();
        $package    = FinancingPackage::where('code','M')->get();
        $idtype    = CodeIDType::get();
        $ref  = User::where('id_pra',$id)->limit('1')->first();

        return view('praapplication.index_ref',compact('employment','idtype','package','background','id','ref'));
    }


    public function create()

    {

        //

    }


    
}

