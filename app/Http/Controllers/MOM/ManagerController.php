<?php

namespace App\Http\Controllers\MOM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Term;
use App\Mail_System;
use App\Manager;
use Auth;
use Ramsey\Uuid\Uuid;
use Hash;
use Mail;
use Alert;
use DB;
use App\ModelHasRoles;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view_creations');
        $user = Auth::user();
       
        $manager = Manager::orderBy('created_at','DESC')->get();
        return view('admin_mbsb.manager', compact('manager','user'));
         
    }

     public function mo_stats() {
         $user = Auth::user();
         $terma = Term::where("referral_id","!=","0")->wherein('status', ['1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->get();

         return view('admin_mbsb.stats', compact('user','terma'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_manager(Request $request)
    {
        //$checkUser = User::Where("email",$request->input('email'))->count();
        //$checkID = User::Where("email",$request->input('manager_id'))->count();
        $email      = $request->input('email');
        $checkUser  = DB::table('users')->where('email', $email)->get();
        $countUser  = $checkUser->count();

        $checkID = DB::table('managers')->where('employer_id', $request->input('manager_id'))->get();
        $countID =  $checkID->count();

        if(($countUser==0) AND ($countID==0)) {

            if($request->set_password=="1") {
                $password1 = $request->password;
                $password2 = $request->password_confirmation;

                if($password1!=$password2) {
                     Alert::error($checkUser);
                   return redirect('manager'); 
                }
            }
            else {
                $password2 = $this->generateStrongPassword('8',false,'ld');   
            }
            
            $fullname       = $request->input('name');
            $active_code    = str_random(100);
            $marketing_id   = $request->input('manager_id');
           

            $user                   = new User;
            $user->password         = Hash::make($password2); 
            $user->id_pra               = Uuid::uuid4()->getHex();    
            $user->email            =  $email;
            $user->activation_code  = $active_code;
            $user->name             =  $fullname;
            $user->active           =  "1";
            $user->role             =  "4";
            $user->save();

            $marketing              = new Manager;
            $marketing->phoneno     = $request->phone;
            $marketing->employer_id = $request->employer_id;
            $marketing->user_id = $user->id_pra;
            $marketing->name        = $fullname;
            $marketing->status        = '1';
            $marketing->save();


            $model              = new ModelHasRoles;
            $model->role_id     = '4';
            $model->model_type  = 'App\User';
            $model->model_id    = $user->id;
            $model->save();


            $noreply_sender = Mail_System::where('id',2)->first();
            $noreply_sender_name = $noreply_sender->name;
            $noreply_sender_email = $noreply_sender->email;


            // Send to NO Email
            try{  
                    Mail::send('mail.mo.manager_register', compact('email','fullname','password2'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$email ,$fullname ) {
                        $message->from($noreply_sender_email, $noreply_sender_name);
                        $message->subject('MBSB - DCA Admin Account Successfully Created');
                        $message->to($email, $fullname);
                    });

                    Alert::success('Data added successfuly');
                    return redirect('manager');
                }
            catch(\Swift_TransportException $e){
                 Alert::success('Data added successfuly. But failed to sent email to user');
                return redirect('manager');
                }

        }

        else if ($countUser!=0) {
            Alert::error('Email Already Used. Please use another email address');
            return redirect('manager'); 
        }

         else if ($countID!=0) {
            Alert::error('ID DCA Admin Already Used.');
            return redirect('manager'); 
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_manager(Request $request)
    {
         $mo = Manager::where('user_id',$request->manager_id)->update([
                "status"       => $request->status,
                "phoneno"       => $request->phoneno,
                "name"       => $request->fullname,
                "employer_id"       => $request->employer_id,

            ]);

          $user = User::where('id_pra',$request->manager_id)->update([
                "active"       => $request->status,
                 "email"       => $request->email,

            ]);
       Alert::success('Edit Successfully');
        return redirect('manager'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
