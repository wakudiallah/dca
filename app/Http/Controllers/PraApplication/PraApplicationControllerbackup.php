<?php



namespace App\Http\Controllers\PraApplication;



use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Http\Requests;

use App\Model\Employment;

use App\Model\PraApplication;

use App\Model\Loan;

use App\Model\Package;

use App\Model\Basic;

use App\Model\Contact;

use App\Model\Tenure;

use App\Model\Empinfo;

use App\Model\Document;

use App\Model\LoanAmmount;

use App\Model\Spouse;

use App\Model\Reference;

use App\Model\Financial;

use App\Model\Term;

use App\Model\Blocked_ic;

use App\Http\Controllers\Controller;

use Input;

use DateTime;

use Ramsey\Uuid\Uuid;

use App\Model\User;

use Auth;

use  Session;

use App\Dsr_a;

use App\Dsr_b;

use App\Dsr_c;

use App\Dsr_d;

use App\Dsr_e;

use App\RiskRating;

use Artisaninweb\SoapWrapper\SoapWrapper;

use App\Soap\Request\MOMBsc;

use App\Soap\Response\MOMBscResponse;

use App\Soap\Request\MOMRef;

use App\Soap\Response\MOMRefResponse;

use App\Soap\Response\MOMEmpInfResponse;

use App\Soap\Response\MOMSpResponse;

use App\Soap\Response\MOMCommitResponse;

use App\Soap\Response\MOMCallBscResponse;

use App\Soap\Response\MOMDocResponse;

use App\Soap\Response\MOMCTransResponse;

use App\Soap\Response\MOMFinResponse;

use App\Soap\Response\MOMDSRAResponse;

use App\Soap\Response\MOMDSRBResponse;

use App\Soap\Response\MOMDSRCResponse;

use App\Soap\Response\MOMDSREResponse;



use App\Soap\Response\MOMLnResponse;

use App\Soap\Response\MOMRskResponse;

use App\Soap\Response\MOMTrmResponse;

use App\Soap\Request\GetConversionAmount;

use App\Soap\Response\GetConversionAmountResponse;

use SOAPHeader;

use SoapClient;

use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Background;
use App\Model\Foreigner;
use App\Model\Subscription;
use App\Model\RbpRate;
use Alert;
class backupcontroller extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

     protected $soapWrapper;



  /**

   * SoapController constructor.

   *

   * @param SoapWrapper $soapWrapper

   */

  public function __construct(SoapWrapper $soapWrapper)

  {

    $this->soapWrapper = $soapWrapper;

  }



    public function index()

    {
        $employment = Employment::get();
        $package    = Package::all();
        $idtype    = CodeIDType::get();
        return view('praapplication.index',compact('employment','idtype','package','background'));
      
    }

  

  public function applynow()
    {
        $user = Auth::user();
        $employment = Employment::get();
        $package = Package::all();
        $idtype    = CodeIDType::all();
        return view('praapplication.applynow',compact('employment','idtype','package','background'));
      
    }





    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    { 

        $praapplication = new PraApplication;
        $basic         = new Basic;
        $contact       = new Contact;
        $empinfo       = new Empinfo;
        $loanammount   = new LoanAmmount;
        $spouse        = new Spouse;
        $document      = new Document;
        $reference     = new Reference;
        $financial     = new Financial;
        $term          = new Term;
        $dsr_a         = new Dsr_a;
        $dsr_b         = new Dsr_b;
        $dsr_c         = new Dsr_c;
        $dsr_d         = new Dsr_d;
        $dsr_e         = new Dsr_e;
        $riskrating    = new RiskRating;
        $waps          = new Waps;
         $foreigner = new Foreigner;
        $subscription = new Subscription;
                    $rbp_rate = new RbpRate;
        $type = $request->input('type');

           $icnumber = $request->input('ICNumber');
                 $new_ic = $request->input('ICNumber');
         

        //$icnumber = $request->input('ICNumber');

        $employment  = $request->input('Employment');
        $employment2 = $request->input('Employment2');
        $basicsalary = $request->input('BasicSalary');
        $fullname    = $request->input('FullName');
        $phone       = $request->input('PhoneNumber');
        $employer    = $request->input('Employer');
        $employer2   = $request->input('Employer2');
        $allowance   = $request->input('Allowance');
        $deduction   = $request->input('Deduction');  
        $majikan     = $request->input('majikan');
        $loanAmount  = $request->input('LoanAmount');
        $package  = $request->input('Package');
        $package_id  = $request->input('package_id');
        
        $get_package = Package::where('id',$package_id)->limit('1')->first();
        $batas      =  $allowance  +  $basicsalary;
        $JobSector= Employment::where('id',$employment)->limit('1')->first();
        $sector = $JobSector->code;

        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 
        $jantina = substr($icnumber,10, 2); 

        if($package_id==3){

              $max_priv     =  $batas *10;
               $max_finance      =  300000;
              if($max_priv>$max_finance){
                $max_other      =  300000;
              }
              else{
                $max_other      =  $max_priv;
              }
        }else{
              $max_other      =  $get_package->max_financing;
        }

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
            $tahun2 = "20".$tahun;
        }

        //if ic not valid
             if($bulan >= 13)
            {
                if($type=='IN'){
                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $new_ic);
                    Session::flash('phone', $phone); 
                    Session::flash('majikan', $majikan); 
                    Session::flash('basicsalary', $basicsalary);  
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount); 
                    //Session::flash('contact_via', $contact_via);
                    Alert::error('Please Enter a valid New IC No.');
                    return redirect('/applynow');
                }
                else{
                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $new_ic);
                    Session::flash('phone', $phone); 
                    Session::flash('majikan', $majikan); 
                    Session::flash('basicsalary', $basicsalary);  
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount); 
                    //Session::flash('contact_via', $contact_via);
                     Alert::error('Please Enter a valid Date of Birth (dd/mm/yyyy) ');
                    return redirect('/applynow');
                }
            }
            elseif($tanggal >= 32) 
            {
                if($type=='IN'){
                    Session::flash('fullname', $fullname);
                    //Session::flash('icnumber', $icnumber);
                    Session::flash('phone', $phone); 
                    Session::flash('majikan', $majikan); 
                    Session::flash('basicsalary', $basicsalary);  
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount); 
                    //Session::flash('contact_via', $contact_via);

                     Alert::error('Please Enter a valid New IC No.');
                    return redirect('/applynow');
                }

                 else{
                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $new_ic);
                    Session::flash('phone', $phone); 
                    Session::flash('majikan', $majikan); 
                    Session::flash('basicsalary', $basicsalary);  
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount); 
                    //Session::flash('contact_via', $contact_via);
                     Alert::error('Please Enter a valid Date of Birth (dd/mm/yyyy) ');
                    return redirect('/applynow');
                }
            }

             if($jantina % 2 ==0 )
            {
                $gender =  "F" ;
            }
            else
            {
                $gender =  "M" ;

            }

            $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
            $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 

            $oDateNow   = new DateTime();
            $oDateBirth = new DateTime($lahir);
            $oDateIntervall = $oDateNow->diff($oDateBirth);
            $umur =  $oDateIntervall->y;

            $dobirth = $tahun2.'-'.$bulan.'-'.$tanggal;
            $today = date("Y-m-d");
           // dd($lahir);
            $diff = date_diff(date_create($dobirth), date_create($today));
            $usia = $diff->format('%y');

            if(($usia > $get_package->max_age) OR ($usia < $get_package->min_age)){
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('basicsalary', $basicsalary); 
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount);
                Session::flash('employer2', $employer2);
                //Session::flash('employment', $employment);
                //Session::flash('employment2', $employment2);
                Session::flash('majikan', $majikan);
                Session::flash('package_name', $package);
                Session::flash('icnumber_error', $icnumber); 
                Alert::error('Sorry, exceed the age limit.');
                return redirect('/applynow');
            }

            else if(($loanAmount < $get_package->min_financing)){

            $min_financing =number_format((float)$get_package->min_financing, 0 , ',' , ',' );  

            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $new_ic);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            //Session::flash('employment', $employment);
            //Session::flash('employment2', $employment2);
            Session::flash('majikan', $majikan);
            Session::flash('package_name', $package);
            Session::flash('icnumber_error', $icnumber); 
            Alert::error('Sorry, minimum financing amount is RM '.$min_financing);
            return redirect('/applynow');

        }
       

         else if($loanAmount > $max_other){

            $max_financing =number_format((float)$get_package->max_financing, 0 , ',' , ',' );  

            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $new_ic);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            //Session::flash('employment', $employment);
           // Session::flash('employment2', $employment2);
            Session::flash('majikan', $majikan);
            Session::flash('package_name', $package);
            Session::flash('icnumber_error', $icnumber); 
            Alert::error('Sorry, maximum financing amount is RM '.$max_other);
            return redirect('/applynow');

        }


                if(  $batas >= $get_package->min_salary ) {
                    $totalsalary = $basicsalary + $allowance ;
                    $id = Uuid::uuid4()->getHex(); 
                    $praapplication->id_pra       = $id;
                    $praapplication->fullname = $fullname;
                    $praapplication->icnumber = $new_ic;
                    $praapplication->phone    = $phone;
                    $praapplication->basicsalary = $basicsalary;
                    $praapplication->allowance = $allowance;
                    $praapplication->deduction = $deduction;
                    $praapplication->id_package = $package_id;
                    $praapplication->loanAmount = $loanAmount;
                    $praapplication->majikan = $majikan;
                    $praapplication->id_employer = $employment;
                    $praapplication->save();
                    
                   
                        $icnumbers= $request->input('ICNumber');
                   

                  
                        $tanggal = substr($icnumbers,4, 2);
                        $bulan =  substr($icnumbers,2, 2);
                        $tahun = substr($icnumbers,0, 2); 
                        $jantina = substr($icnumbers,10, 2); 
                    
                    if($tahun > 30) {
                        $tahun2 = "19".$tahun;
                    }
                    else {
                         $tahun2 = "20".$tahun;
                    }
                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;     

                    $basic->id_praapplication   =  $id;
                    $basic->new_ic = $new_ic;
                    $basic->name =  $fullname ;
                    $basic->dob =  $lahir ; 
                    $basic->gender =  $gender ; 
                    $basic->corres_mobilephone = $phone;
                    $basic->gender =  $gender ; 
                    $basic->age =  $usia ;
                    $basic->id_type =  $request->input('type') ; 
                    $basic->save();

                    $contact->id_praapplication   =  $id;
                    $contact->handphone   =  $phone;
                    $contact->save();

                    $empinfo->id_praapplication   =  $id;
                        
                    $empinfo->employment_id = $employment;
                    $empinfo->salary = $basicsalary;
                    $empinfo->empname = $majikan;
                    $empinfo->employer_id = $employer;
                    $empinfo->allowance = $allowance;
                    $empinfo->deduction = $deduction;
                    $empinfo->save();

                    $loanammount->id_praapplication   =  $id;
                    $loanammount->loanammount   =  $loanAmount;
                    $loanammount->save();

                       $rbp_rate->id_pra   =  $id;
                    $rbp_rate->save();

                    $spouse->id_praapplication   =  $id;
                    $spouse->save();

                    $financial->id_praapplication   =  $id;
                    $financial->save();

                    $reference->id_praapplication   =  $id;
                    $reference->save();

                    $term->id_praapplication        =  $id;
                    $term->status                   =  '0';
                    $term->verification_status      =  '0';
                    $term->verification_result      =  '0';
                    $term->mo_stage                 =  '0';
                     $term->referral_id                 =  '0';
                    $term->verification_result_by_bank  =  '0';
                    $term->id_branch                =  '0';
                    $term->status_waps                =  '0';
                    $term->save();

                    //DSR Calculation

                    $dsr_a->id_praapplication   =  $id;
                    $dsr_a->sector   =  $sector;
                    $dsr_a->age   =  $usia;
                    $dsr_a->save();

                    $dsr_b->id_praapplication   =  $id;
                    $dsr_b->save();

                    $dsr_c->id_praapplication   =  $id;
                    $dsr_c->save();

                    $dsr_d->id_praapplication   =  $id;
                    $dsr_d->save();

                    $dsr_e->id_praapplication   =  $id;
                    $dsr_e->save();

                    $riskrating->id_praapplication   =  $id;
                    $riskrating->save();

                    $foreigner->id_praapplication   =  $id;
                    $foreigner->save();

                    $subscription->id_praapplication   =  $id;
                    $subscription->save();

                    /*$waps->id_praapplication   =  $id;
                    $waps->ic = $icnumber;
                    $waps->name =  $fullname ;
                    $waps->save();*/
                    // Proceed

                    $pra = PraApplication::where('id_pra',$id)->limit('1')->first();
                     
                    $id_type= $employment;
                    $total_salary = $pra->basicsalary + $pra->allowance ;
                    $zbasicsalary = $pra->basicsalary + $pra->allowance;
                      // dd($total_salary);
                    $zdeduction =  $pra->deduction ;

                    $loan = Loan::where('id_type',$id_type)
                            ->where('min_salary','<=',$total_salary)
                            ->where('max_salary','>=',$total_salary)->limit('1')->get();

                    // return $total_salary;

                    $id_loan= $loan->first()->id; 
                    $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;

  $icnumberx= $pra->first()->icnumber;
                    
                        $tanggal = substr($icnumberx,4, 2);
                        $bulan =  substr($icnumberx,2, 2);
                        $tahun = substr($icnumberx,0, 2); 
                        $jantina = substr($icnumberx,10, 2); 
                    
                    
                    if($tahun > 30) {
                        $tahun2 = "19".$tahun;
                    }
                    else {
                        $tahun2 = "20".$tahun;
                    }


                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
                    $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                    $oDateNow = new DateTime();
                    $oDateBirth = new DateTime($lahir);
                    $oDateIntervall = $oDateNow->diff($oDateBirth);

                    $umur = 61 - $oDateIntervall->y;
                    $durasix = 60 - $oDateIntervall->y;
                    if( $durasix  > 10) { 
                        $durasi = 10 ;
                    } 
                    else { 
                        $durasi = $durasix ;
                    }

                    function pembulatan($uang) {
                        $puluhan = substr($uang, -3);
                            if($puluhan<500) {
                                $akhir = $uang - $puluhan; 
                            } 
                            else {
                                $akhir = $uang - $puluhan;
                            }
                        return $akhir;
                    }

                    foreach($loan as $loan) {
                        $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                        $ndi = ($zbasicsalary - $zdeduction) -  1300;
                        $max  =  $salary_dsr * 12 * 10 ;

                            if(!empty($get_package->max_financing))  {
                                $ansuran = intval($salary_dsr)-1;
                                   
                                        $bunga = $get_package->flat_rate/100;
                                       //$bunga = $pra->package->effective_rate / 100;
                                    
                                   
                                $pinjaman = 0;

                                   for ($i = 0; $i <= $get_package->max_financing; $i++) {
                                  $bungapinjaman = $i  * $bunga * $durasi ;
                                  $totalpinjaman = $i + $bungapinjaman ;
                                  $durasitahun = $durasi * 12;
                                  $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                  if ($ansuran2 < $ndi)
                                  {
                                      $pinjaman = $i;
                                    //  dd($pinjaman);
                                  }
                                
                              }   

                                    if($pinjaman > 1) {
                                        $bulat = pembulatan($pinjaman);
                                        $loanx =   number_format((float)$bulat, 0 , ',' , ',' );  
                                        $loanz = $bulat;
                                    }
                                    else {
                                        $loanx =   number_format((float)$get_package->max_financing, 0 , ',' , ',' );  
                                        $loanz = $get_package->max_financing;
                                    }
                                }
                            else 
                            { 
                                $bulat = pembulatan(10 * $total_salary);
                                $loanx =   number_format((float)$bulat, 0 , ',' , ',' ) ; 
                                $loanz = $bulat;
                                
                                    $loanx =   number_format((float)$loanz, 0 , ',' , ',' ); 
                                

                            }

                    }

                    $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();  

                    if( $pra->first()->loanamount <= $loanz ) {
                        $ndi_limit=$loan->ndi_limit;
                        foreach($tenure as $tenure) {
                            $bunga2 =  $pra->first()->loanamount * $get_package->flat_rate /100   ;
                            $bunga = $bunga2 * $tenure->years;
                            $total = $pra->first()->loanamount + $bunga ;
                            $bulan = $tenure->years * 12 ;
                            $installment =  $total / $bulan ;
                            $ndi_state = ($total_salary - $zdeduction) - $installment; 
                            $count_installment=0;

                            if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                                $count_installment++;
                            }
                        }
                    } 
                    else {
                        $count_installment=0;
                    }

                    //dd($count_installment);

                    if($count_installment>0) {
                        if($package_id!='3'){
                        return redirect('praapplication/'.$id)->with('message', 'Congratulations, maximum loan eligibility up to RM '.$get_package->max_financing);
                        }
                        else{
                             return redirect('praapplication/'.$id)->with('message', 'Congratulations, maximum loan eligibility up to RM '.$max_other);
                        }

                    } 

                    else if($count_installment==0)  {

                       // $had2 = number_format((float)$had, 0 , ',' , ',' ); 

                        Session::flash('fullname', $fullname); 
                        Session::flash('icnumber', $new_ic);
                        Session::flash('phone', $phone); 
                        Session::flash('basicsalary', $basicsalary); 
                        Session::flash('allowance', $allowance);
                        Session::flash('deduction', $deduction);
                        Session::flash('loanAmount', $loanAmount);
                        Session::flash('employer2', $employer2);
                       // Session::flash('employment', $employment);
                        //Session::flash('employment2', $employment2);
                       // Session::flash('package_name', $package_name);
                        Session::flash('majikan', $majikan);
                        Session::flash('hadpotongan', $basicsalary); 
                        Alert::error('Sorry, you are not qualified. Exceed deduction or financing limit!');
                        return redirect('/applynow');
                    }


                }
                else if(  $batas < $get_package->min_salary ){
                    $had2 = number_format((float)$get_package->min_salary, 0 , ',' , ',' ); 

                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $new_ic);
                    Session::flash('phone', $phone); 
                    Session::flash('basicsalary', $basicsalary); 
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount);
                    Session::flash('employer2', $employer2);
                    Session::flash('employment', $employment);
                    //Session::flash('employment2', $employment2);
                    //Session::flash('package_name', $package);
                    //Session::flash('majikan', $majikan);
                    Alert::error('Minimum monthly income of RM '.$had2);
                     return redirect('/applynow');
                }
       
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        $pra = PraApplication::where('id_pra',$id)->limit('1')->get();

       

        $total_salary = $pra->first()->basicsalary + $pra->first()->allowance ;

        $zbasicsalary = $pra->first()->basicsalary + $pra->first()->allowance;

        $zdeduction =  $pra->first()->deduction ;

        $pra_latest = PraApplication::where('id_pra',$id)->limit('1')->first();
        $id_type= $pra_latest->id_employer
        ;
        $get_package = Package::where('id',$pra_latest->id_package)->limit('1')->first();

        $loan = Loan::where('id_type',$id_type)

                ->where('min_salary','<=',$total_salary)

                ->where('max_salary','>=',$total_salary)->limit('1')->get();



        $id_loan= $loan->first()->id;  



        $icnumber= $pra->first()->icnumber;

        $tanggal = substr($icnumber,4, 2);

        $bulan =  substr($icnumber,2, 2);

        $tahun = substr($icnumber,0, 2); 



        if($tahun > 30) {



            $tahun2 = "19".$tahun;

        }

        else {

             $tahun2 = "20".$tahun;



        }

       

        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 

        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));

        $oDateNow = new DateTime();

        $oDateBirth = new DateTime($lahir);

        $oDateIntervall = $oDateNow->diff($oDateBirth);



        $umur = 61 - $oDateIntervall->y;



         $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();   

         //  return $tenure;

    

         return view('praapplication.result', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','amount','get_package','pra_latest'));

       

    }

        



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

       

        $loanammount = $request->input('LoanAmount2');


        $pra_calculate = PraApplication::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);

        $today = date('Y-m-d H:i:s');

        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);

         $pra = PraApplication::where('id_praapplication', $id)->update(['date_registration' => $today]);

         $term = Term::where('id_praapplication', $id)->update(['ck_consest' => '1']);

   

        return redirect('praapplication/'.$id)->with('message', 'Please Choose Your Tenures');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

    

    public function update2(Request $request, $id)

    {

        $loanammount = $request->input('LoanAmount2');

        $today = date('Y-m-d H:i:s');
        $pra_calculate = PraApplication::where('id_pra', $id)->update(['loanamount' => $loanammount,'date_registration' => $today]);

       

        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);

        
   

        return redirect('praapplication/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');

    }



    public function updatemo(Request $request, $id)

    {

        $pra = PraApplication::find($id);

        $loanammount = $request->input('LoanAmount2');

        $pra->loanamount = $loanammount;

        $pra->save();



        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);



        return redirect('moapplication/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');

    }

}

