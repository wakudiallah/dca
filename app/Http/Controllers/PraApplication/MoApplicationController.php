<?php

namespace App\Http\Controllers\PraApplication;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\Employment;
use App\Model\PraApplication;
use App\Model\Loan;
use App\Model\Package;
use App\Model\Basic;
use App\Model\Contact;
use App\Model\Tenure;
use App\Model\Empinfo;
use App\Model\Document;
use App\Model\LoanAmmount;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Term;
use App\Model\Blocked_ic;
use App\Http\Controllers\Controller;
use Input;
use DateTime;
use Ramsey\Uuid\Uuid;
use App\Model\User;
use Auth;
use Session;

use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Model\Waps;
use OneSignal;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Foreigner;
use App\Model\Subscription;

class MoApplicationController extends Controller
{

    public function __construct() {
                 $this->middleware('auth'); 
                 $this->middleware('admin');
                 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
         $employment = Employment::get();
         $package = Package::all();
         $idtype    = CodeIDType::all();
        return view('moapplication.index', ['employment' => $employment, 'package' => $package, 'user' => $user ,'idtype' => $idtype ]);
         
    }
	
	public function applynow()
    {
        $user = Auth::user();
         $employment = Employment::get();
         $package = Package::all();
         $idtype    = CodeIDType::all();
        return view('moapplication.applynow', ['employment' => $employment, 'package' => $package, 'user' => $user,'idtype' => $idtype  ]);
         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $praapplication = new PraApplication;
        $basic = new Basic;
        $contact = new Contact;
        $empinfo = new Empinfo;
        $loanammount = new LoanAmmount;
        $spouse = new Spouse;
        $document = new Document;
        $reference = new Reference;
        $financial = new Financial;
        $term = new Term;

        $dsr_a = new Dsr_a;
        $dsr_b = new Dsr_b;
        $dsr_c = new Dsr_c;
        $dsr_d = new Dsr_d;
        $dsr_e = new Dsr_e;
        $foreigner = new Foreigner;
        $subscription = new Subscription;
        $waps = new Waps;
        $riskrating = new RiskRating;

        $type = $request->input('type');

        if($type=='IN')
            {
                $icnumber = $request->input('ICNumber');
                 $new_ic = $request->input('ICNumber');
            }
        else{
            $icnumber = $request->input('dob');
            $other = $request->input('other');
             $new_ic = $request->input('other');
        }

        $employment  = $request->input('Employment');
        $employment2  = $request->input('Employment2');
        $basicsalary = $request->input('BasicSalary');
        $fullname = $request->input('FullName');
        $phone = $request->input('PhoneNumber');
        $employer = $request->input('Employer');
        $employer2 = $request->input('Employer2');
        $allowance = $request->input('Allowance');
        $deduction = $request->input('Deduction');        
        //$package = $request->input('Package');
        $majikan = $request->input('majikan');
        $loanAmount = $request->input('LoanAmount');
        $email = $request->input('Email2');
        $batas =  $allowance  +  $basicsalary;

        if($type=='IN')
            {
                $tanggal = substr($icnumber,4, 2);
                $bulan =  substr($icnumber,2, 2);
                $tahun = substr($icnumber,0, 2); 
                $jantina = substr($icnumber,10, 2); 
            }
        else{
        		$genders = $request->input('gender');
                $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
                $jantina = $genders; 
        }
        
        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
            $tahun2 = "20".$tahun;
        }

        if($bulan >= 13)
        {
            if($type=='IN'){
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                return redirect('/moapplication')->with('message', "Please Enter a valid New IC No. / Sila Masukan IC Baru yang valid");
            }
            else{
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                return redirect('/moapplication')->with('message', "Please Enter a valid Date of Birth (dd/mm/yyyy) / Sila Masukan Tarikh Lahir yang valid sesuai format (dd/mm/yyyy)");
            }
        }

        elseif($tanggal >= 32) 
        {
            if($type=='IN'){
                Session::flash('fullname', $fullname);
                //Session::flash('icnumber', $icnumber);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                return redirect('/moapplication')->with('message', "Please Enter a valid New IC No. / Sila Masukan IC Baru yang valid");
            }

             else{
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                return redirect('/moapplication')->with('message', "Please Enter a valid Date of Birth (dd/mm/yyyy) / Sila Masukan Tarikh Lahir yang valid sesuai format (dd/mm/yyyy)");
            }
        }
        else{
            if($employment==1) {
                $package=1;
                $package_name ="Mumtaz-i";
            }
            else if($employment==2) {
               $package=2; 
               $package_name = "Afdhal-i";
            }
            else if($employment==3) {
               $package=2; 
               $package_name = "Afdhal-i";
            }
            else if($employment==4) {
               $package=3; 
               $package_name = "Private Sector PF-i";
            }
            else if($employment==5) {
               $package=2; 
               $package_name = "Afdhal-i";
            }

            if(!empty($employer)) { 
                $praapplication->id_employer = $employer;
                $JobSector= Employment::where('id',$employment)->limit('1')->first();
                $sector = $JobSector->code;
                }
                else {
                   $JobSector= Employment::where('id',$employment)->limit('1')->first();
                    $sector = $JobSector->code;
                }
                if($jantina % 2 ==0 )
                {
                    $gender           =  "F" ;
                }
                else
                {
                    $gender           =  "M" ;
                }

        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 

        $oDateNow       = new DateTime();
        $oDateBirth     = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {

            return redirect('/moapplication')->with('message', "Maaf umur anda melebihi had");

        }
        else {

            if($employment == 1  )  {   $had =  1500;  }
                else if ($employment == 3  )  {   $had =  3500;  } 
                else if ($employment == 2  )  {   $had =  3500;  } 
                else if ($employment == 4  )  {   $had =  3500;  } 
                else if ($employment == 5  )  {   $had =  15000;  } 
                else { $had = 2999; }
                if(  $batas > $had ) {
                    $totalsalary = $basicsalary + $allowance ;
                    $id = Uuid::uuid4()->getHex(); 
                    $praapplication->id   =  $id;
                    $praapplication->fullname = $fullname;
                    $praapplication->email = $email;
                    $praapplication->icnumber = $new_ic;
                    $praapplication->phone = $phone;
                    $praapplication->lat = $request->latitude;
                    $praapplication->lng = $request->longitude;
                    $praapplication->location = $request->location;
                    $praapplication->referral_id = Auth::user()->id;

                    if(!empty($employer)) { 
                        $praapplication->id_employer = $employer;
                    }
                    else {
                        if($employment == 1  )  {     $praapplication->id_employer = 10;   }
                        else if ($employment == 3  )  {    $praapplication->id_employer = 5;   } 
                    }

                    $praapplication->basicsalary = $basicsalary;
                    $praapplication->allowance = $allowance;
                    $praapplication->deduction = $deduction;
                    $praapplication->id_package = $package;
                    $praapplication->loanAmount = $loanAmount;
                    $praapplication->majikan = $majikan;
                    $praapplication->save();

                     if($type=='IN')
            {
                $tanggal = substr($icnumber,4, 2);
                $bulan =  substr($icnumber,2, 2);
                $tahun = substr($icnumber,0, 2); 
                $jantina = substr($icnumber,10, 2); 
            }
        else{
                $genders = $request->input('gender');
                $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
                $jantina = $genders; 
        } 

                    if($tahun > 30) {
                        $tahun2 = "19".$tahun;
                    }
                    else {
                        $tahun2 = "20".$tahun;
                    }
       
                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;                                                     
                                                           
                    $basic->id_praapplication   =  $id;
                    $basic->new_ic = $new_ic;
                    $basic->name =  $fullname ;
                    $basic->dob =  $lahir ; 
                    $basic->gender =  $gender ; 
                    $basic->id_type =  $request->input('type') ; 
                    $basic->save();

                    $contact->id_praapplication   =  $id;
                    $contact->handphone   =  $phone;
                    $contact->save();

                    $empinfo->id_praapplication   =  $id;
                    if(!empty($employer)) {
                        $empinfo->employer_id = $employer; }
                    else {
                        $empinfo->employer_id = 5;
                    }

                    if($employment  != '2' ){
                        $empinfo->empname = $majikan;
                    }

                    $empinfo->employment_id = $employment;
                    $empinfo->salary = $basicsalary;
                    $empinfo->allowance = $allowance;
                    $empinfo->deduction = $deduction;
                    $empinfo->save();

                    $loanammount->id_praapplication   =  $id;
                    $loanammount->loanammount   =  $loanAmount;
                    $loanammount->save();

                    $spouse->id_praapplication   =  $id;
                    $spouse->save();

                    $financial->id_praapplication   =  $id;
                    $financial->save();

                    $reference->id_praapplication   =  $id;
                    $reference->save();

                    $term->id_praapplication   =  $id;
                    $term->status                   =  '0';
                    $term->verification_status      =  '0';
                    $term->verification_result      =  '0';
                    $term->mo_stage                 =  '0';
                    $term->verification_result_by_bank  =  '0';
                    $term->id_branch                =  '0';
                    $term->status_waps                =  '0';
                    $term->referral_id = Auth::user()->id;
                    $term->mo_id = Auth::user()->mo_id;
                    $term->lat = $request->latitude;
                    $term->lng = $request->longitude;
                    $term->location = $request->location;
                    $term->save();

                    //DSR Calculation

                    $dsr_a->id_praapplication   =  $id;
                    $dsr_a->sector   =  $sector;
                    $dsr_a->save();

                    $dsr_b->id_praapplication   =  $id;
                    $dsr_b->save();

                    $dsr_c->id_praapplication   =  $id;
                    $dsr_c->save();

                    $dsr_d->id_praapplication   =  $id;
                    $dsr_d->save();

                    $dsr_e->id_praapplication   =  $id;
                    $dsr_e->save();

                    $riskrating->id_praapplication   =  $id;
                    $riskrating->save();

                    $foreigner->id_praapplication   =  $id;
                    $foreigner->save();

                    $subscription->id_praapplication   =  $id;
                    $subscription->save();

                    /*$waps->id_praapplication   =  $id;
                    $waps->ic = $icnumber;
                    $waps->name =  $fullname ;
                    $waps->save();*/

                   // OneSignal::sendNotificationToAll("Some Message", $url = 'http://localhost:8080/onesignal/public/admin', $data = null);

                    return redirect('moapplication/'.$id)->with('message', 'Select Loan Tenure');
        }

        else {
             $had2 = number_format($had + 1, 0 , ',' , ',' ).'.00' ; 
             Session::flash('fullname', $fullname); 
             Session::flash('email', $email); 
             //Session::flash('icnumber', $icnumber);
             Session::flash('phone', $phone); 
             Session::flash('basicsalary', $basicsalary); 
             Session::flash('allowance', $allowance);
             Session::flash('deduction', $deduction);
             Session::flash('loanAmount', $loanAmount);
            
             Session::flash('employer2', $employer2);
              Session::flash('employment', $employment);
             Session::flash('employment2', $employment2);
             if($employment  == '2' ){
              Session::flash('employer', $employer);
          }
          Session::flash('majikan', $majikan); 

                 
            return redirect('/moapplication')->with('message', "Syarat gaji minima RM  $had2 sebulan ");
            }
        }
    }
        
    
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
      $user = Auth::user();
        $pra = PraApplication::where('id',$id)->limit('1')->get();
        $id_type= $pra->first()->employer->employment->id;
        $total_salary = $pra->first()->basicsalary + $pra->first()->allowance ;
            $zbasicsalary = $pra->first()->basicsalary + $pra->first()->allowance;
        $zdeduction =  $pra->first()->deduction ;

        $loan = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

         $id_loan= $loan->first()->id;  

        $icnumber= $pra->first()->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

         $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   

    
       return view('moapplication.result', compact('pra','loan','total_salary','tenure','id','user', 'zbasicsalary', 'zdeduction'  ));
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pra = PraApplication::find($id);
        $loanammount = $request->input('LoanAmount2');
        $pra->loanammount = $loanammount;
        $pra->save();

        $loanx2 = LoanAmmount::where('id_praapplication',$id)->first();
        $loanx2->loanammount = $loanammount;
        $loanx2->save();

        return redirect('moapplication/'.$id)->with('message', 'insert data succsess');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function update2(Request $request, $id)
    {
        $pra = PraApplication::find($id);
        $loanammount = $request->input('LoanAmount2');
        $pra->loanamount = $loanammount;
        $pra->save();

           
        $loanx2 = LoanAmmount::where('id_praapplication',$id)->first();
        $loanx2->loanammount = $loanammount;
        $loanx2->save();


        return redirect('moapplication/'.$id)->with('message', 'insert data succsess');
    }
}
