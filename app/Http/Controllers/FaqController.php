<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\User;
use Auth;

class FaqController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}


	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function index() {	 
		$user = Auth::user();
		$faq=1;
       	return view('home.faq', compact('faq','user')); 	
         
    }
}