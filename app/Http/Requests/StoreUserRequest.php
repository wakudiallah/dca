<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;


class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'Email' => 'required|unique:users|max:255',
        'FullName2' => 'required|max:255',
            'password' => 'required|min:8|max:36|confirmed',
        'password_confirmation' => 'required|max:36|min:8'
        ];
    }

     public function messages()
    {
        return [
            'FullName2.required'  => 'name field can not be empty',
            'Email.required' => 'email field  can not be empty',
            'Email.email'    => 'email not valid',
            'Email.unique'   => 'this email already been used'
           
        ];
    }

}
