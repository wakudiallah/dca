<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;


class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          
    
        'password' => 'required|min:8|max:36|confirmed',
        'password_confirmation' => 'required|max:36|min:8'
        ];
    }

      public function messages()
    {
        return [
        
            'password.regex'   => 'password must be alpha numberik',

           
        ];
    }

   

}
