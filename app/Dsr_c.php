<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Dsr_c extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'dsr_c'; 
}
