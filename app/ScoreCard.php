<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class ScoreCard extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'scorecard'; 
}
