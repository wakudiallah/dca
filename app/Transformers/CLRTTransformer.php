<?php

namespace App\Transformers;

use App\Model\AntiAttrition;
use App\Model\SettlementInfo;
use League\Fractal\TransformerAbstract;

class CLRTTransformer extends TransformerAbstract
{
	public function transform (AntiAttrition $antiattrition)
	{
		$settlement = new SettlementInfo;
		return[
			'id' => $antiattrition->id,
			'mo_id'=> $antiattrition->mo_id,
			'assign_by'=> $antiattrition->assigned_by,
			'id_clrt' => $antiattrition->id_clrt,
			'TaskID'=> $antiattrition->TaskID,
			'ACID'=> $antiattrition->ACID,
			'CustIDNo'=> $antiattrition->CustIDNo,
			'Tenor' => $antiattrition->Tenor,
			'Year'=> $antiattrition->Year,
			'SalIncrementPercent'=> $antiattrition->SalIncrementPercent,
			'NewSalary'=> $antiattrition->NewSalary,
			'NewAllowDeduct' => $antiattrition->NewAllowDeduct,
			'IntRate'=> $antiattrition->IntRate,
			'VarianceRate'=> $antiattrition->VarianceRate,
			'NewLoanOffer'=> $antiattrition->NewLoanOffer,
			'PayoutPercent' => $antiattrition->PayoutPercent,
			'NetProceed'=> $antiattrition->NetProceed,
			'MonthlyInst'=> $antiattrition->MonthlyInst,
			'NewDSR'=> $antiattrition->NewDSR,
			'ProfitToEarn' => $antiattrition->ProfitToEarn,
			'CalDt'=> $antiattrition->CalDt,
			'CustWrkSec'=> $antiattrition->CustWrkSec,
			'CustName'=> $antiattrition->CustName,
			'Assign' => $antiattrition->Assign,
			'CustDob'=> $antiattrition->CustDob,
			'State'=> $antiattrition->State,
			'Settlement'  => [
				'ACID'=> $antiattrition->Settlement->ACID,
				'CustIDNo'=> $antiattrition->Settlement->CustIDNo,
            	'MaxAge'=> $antiattrition->Settlement->MaxAge,
				'DueDate'=> $antiattrition->Settlement->DueDate,
				'BalOutStanding'=> $antiattrition->Settlement->BalOutStanding,
				'FullSettlement'=> $antiattrition->Settlement->FullSettlement,
				'ProfitEarned'=> $antiattrition->Settlement->ProfitEarned,
				'Rebate'=> $antiattrition->Settlement->Rebate,
				'PaidInstallment'=> $antiattrition->Settlement->PaidInstallment,
				'RemInstallment'=> $antiattrition->Settlement->RemInstallment,
				'DSRUtilise'=> $antiattrition->Settlement->DSRUtilise,
				'DSRUnutilise'=> $antiattrition->Settlement->DSRUnutilise,
				'CalculateDate'=> $antiattrition->Settlement->CalculateDate,
				'TaskIDNo'=> $antiattrition->Settlement->TaskIDNo
        	],
			// "item_varients" => SettlementInfo::collection($variantItems)

			//settlement info
			//'ACID'=> $settlement->ACID,
			//'CustIDNo'=> $settlement->CustIDNo,
			/*'id_settlement'=> $settlement->id,
			'MaxAge'=> $settlement->MaxAge,
			'DueDate'=> $settlement->DueDate,
			'BalOutStanding'=> $settlement->BalOutStanding,
			'FullSettlement'=> $settlement->FullSettlement,
			'ProfitEarned'=> $settlement->ProfitEarned,
			'Rebate'=> $settlement->Rebate,
			'PaidInstallment'=> $settlement->PaidInstallment,
			'RemInstallment'=> $settlement->RemInstallment,
			'DSRUtilise'=> $settlement->DSRUtilise,
			'DSRUnutilise'=> $settlement->DSRUnutilise,
			'CalculateDate'=> $settlement->CalculateDate,
			'TaskIDNo'=> $settlement->TaskIDNo,*/

			//'token'=> $user->api_token,
		];
	}

	public function includeDepartments(AntiAttrition $antiattrition)
    {
        $dept  = $antiattrition->CustIDNo;

        return $this->collection($antiattrition, new SettlementInfoTransformer());
    }
}