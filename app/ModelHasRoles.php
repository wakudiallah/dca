<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelHasRoles extends Model
{
    protected $table = 'model_has_roles'; 
    public $timestamps = false;


    public function model_to_role() {
        return $this->belongsTo('App\Role','role_id', 'id');   
    }


}
