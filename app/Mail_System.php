<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Mail_System extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'mail_systems'; 
}
