<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Manager extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'managers'; 
    //public $incrementing = false;  
    
    protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
   
    public function User() {
        return $this->hasOne('App\Model\User','id_pra','user_id');	
	}
}
