<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use OwenIt\Auditing\Contracts\Auditable;
class Education extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
    protected $table 	= 'education';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
