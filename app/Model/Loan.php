<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Loan extends Model implements Auditable
{
       use \OwenIt\Auditing\Auditable;
	 protected $table = 'loan';
       protected $hidden = ['created_at', 'updated_at','create_by'];

public function type() {
		return $this->belongsTo('App\Model\Package','id_type');	
	}
}
