<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use OwenIt\Auditing\Contracts\Auditable;
class Religion extends Model implements Auditable
{
	 use \OwenIt\Auditing\Auditable;

    protected $table 	= 'religions';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


}
