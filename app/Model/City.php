<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class City extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'city';  


public function state() {
		return $this->belongsTo('App\Model\State','id_state');	
	}
}
