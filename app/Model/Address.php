<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Address extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'addresses';

     public function AddTypes()
    {
        return $this->hasOne('App\Model\PrAddType','AddTyCode','AddType');   
    }
}
