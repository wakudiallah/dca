<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class CodeWorkSec  extends Model implements Auditable
{
	  use \OwenIt\Auditing\Auditable;
   protected $table = 'codeworkseccode';  
}
