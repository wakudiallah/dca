<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Financial  extends Model implements Auditable
{
	  use \OwenIt\Auditing\Auditable;
   
   protected $table = 'financials';  

 public function PaymentMode() {
		return $this->belongsTo('App\Model\EmploymentType','payment_mode','id');	
	}

	 public function TypeCust() {
		return $this->belongsTo('App\Model\TypeCustomer','type_customer','id');	
	}



}
