<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Type extends Model implements Auditable
{
     
  use \OwenIt\Auditing\Auditable;
        protected $hidden = ['created_at', 'updated_at','create_by'];

public function package() {
		return $this->belongsTo('App\Model\Package','id_package');	
	}
}
