<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PraApplication extends Model implements Auditable
{

     use \OwenIt\Auditing\Auditable;
     
    protected $table = 'praapplications';

     public $incrementing = true; 
   

     public function package() {
		return $this->belongsTo('App\Model\FinancingPackage','id_package','id');	
	}

	public function employer() {
		return $this->belongsTo('App\Model\Employer','id_employer','employment_id');	
	}

    public function emp() {
        return $this->belongsTo('App\Model\Employment','id_employer');    
    }
	
	 public function term()
    {
        return $this->hasOne('App\Model\Term','id_pra','id_praapplication');	
    }

     public function empinfo()
    {
        return $this->hasOne('App\Model\Empinfo','id_pra','id_praapplication');	
    }


    public function user()
    {
        return $this->hasOne('App\Model\User','email', 'email');  
    }


     public function loanAmmount()
    {
        return $this->hasOne('App\Model\LoanAmmount', 'id_pra','id_praapplication');  
    }

     public function Basic() {
        return $this->belongsTo('App\Model\Basic','id_pra','id_praapplication'); 
    }

     




}
