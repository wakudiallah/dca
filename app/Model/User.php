<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;
class User extends Authenticatable implements Auditable
{
  use \OwenIt\Auditing\Auditable;
    protected $table = 'users';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function Branch() {
		return $this->belongsTo('App\Model\Branch','branch_code','branch_code');	
	}

     public function MarOfficer() {
        return $this->belongsTo('App\MarketingOfficer','id','id');   
    }

     public function Manager() {
        return $this->belongsTo('App\Manager','id','id');   
    }

     public function Role() {
        return $this->belongsTo('App\Role','role','id');  
    }

    
}
