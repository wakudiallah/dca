<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Empinfo  extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'empinfo';  

public function employment() {
		return $this->belongsTo('App\Model\Employment','employment_id');	
	}

	public function employer() {
		return $this->belongsTo('App\Model\Employer','employer_id');	
	}

	public function Nature() {
		return $this->belongsTo('App\Model\Occupations','nature_business','id');	
	}
	public function PositionDesc() {
		return $this->belongsTo('App\Model\Position','position','id');	
	}
	 public function StateOffice()
    {
        return $this->hasOne('App\Model\State2','state_code','state_code');  
    }

    public function EmpType() {
		return $this->belongsTo('App\Model\EmploymentType','emptype','id');	
	}
	public function Occupation() {
		return $this->belongsTo('App\Model\Occupations','occupation','id');	
	}

	 public function EmpStatus() {
		return $this->belongsTo('App\Model\CodeJobStatus','empstatus','id');	
	}
}
