<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Foreigner  extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'dec_foreigner_exchanges';  

   public function Country()
    {
        return $this->hasOne('App\Model\Country','country_code','country');  
    }

}
