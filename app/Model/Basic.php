<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Basic  extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
   protected $table = 'basics';  

 	public function term()
    {
        return $this->hasOne('App\Model\Term','id_praapplication');	
    }

    public function EmpInfo()
    {
        return $this->hasOne('App\Model\Empinfo','id_praapplication','id_praapplication');	
    }

    public function Spouse()
    {
        return $this->hasOne('App\Model\Spouse','id_praapplication','id_praapplication');	
    }

    public function Ref()
    {
        return $this->hasOne('App\Model\Reference','id_praapplication','id_praapplication');	
    }

    public function Financial()
    {
        return $this->hasOne('App\Model\Financial','id_praapplication','id_praapplication');	
    }
    public function Commitments()
    {
        return $this->hasOne('App\Model\Commitments','id_praapplication','id_praapplication');	
    }
    public function StateDOB()
    {
        return $this->hasOne('App\Model\State2','state_code','state_dob');  
    }
    public function CountryDOB()
    {
        return $this->hasOne('App\Model\Country','id','country_dob');  
    }
    public function StateAddress()
    {
        return $this->hasOne('App\Model\State2','state_code','state_code');  
    }
    public function StateCorres()
    {
        return $this->hasOne('App\Model\State2','state_code','corres_state1');  
    }

    public function Terma()
    {
        return $this->hasOne('App\Model\Term','id_praapplication','id_praapplication'); 
    }
    public function Credit()
    {
        return $this->hasOne('App\Model\Credit','id_praapplication','id_praapplication'); 
    }
    public function Pep()
    {
        return $this->hasOne('App\Model\Pep','id_praapplication','id_praapplication'); 
    }
     public function LoanAmount()
    {
        return $this->hasOne('App\Model\LoanAmmount','id_praapplication','id_praapplication');    
    }

    
     public function National()
    {
        return $this->hasOne('App\Model\Country','id','nationality');    
    }

     public function CoOrigin()
    {
        return $this->hasOne('App\Model\Country','id','country_origin');    
    }

    public function Title()
    {
        return $this->hasOne('App\Model\Title','id','title');    
    }

    public function Marital()
    {
        return $this->hasOne('App\Model\Marital','id','marital');    
    }

    public function Own()
    {
        return $this->hasOne('App\Model\Ownership','id','ownership');    
    }

     public function Race()
    {
        return $this->hasOne('App\Model\Race','id','race');    
    }

    public function Religion()
    {
        return $this->hasOne('App\Model\Religion','id','religion');    
    }
    
    public function Education()
    {
        return $this->hasOne('App\Model\Education','id','education');    
    }


}


//y