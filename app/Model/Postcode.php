<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Postcode extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'param_postcodes';


	public function state() {
		return $this->belongsTo('App\Model\State','state_code','state_code');	
		//Postcode dipunyai city
	}
}
