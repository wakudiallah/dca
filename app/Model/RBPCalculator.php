<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RBPCalculator extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'rbp_calculators';
    protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;  
}
