<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class PrAddType extends Model
{
	  use \OwenIt\Auditing\Auditable;
    protected $table = 'pr_add_types';
}
