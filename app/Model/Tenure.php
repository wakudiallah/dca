<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Tenure extends Model implements Auditable
{
       use \OwenIt\Auditing\Auditable;
	 protected $table = 'tenure';
       protected $hidden = ['created_at', 'updated_at','create_by'];



public function loan() {
		return $this->belongsTo('App\Model\Loan','id_loan');	
	}
}
