<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;

class Employment extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	protected $table = 'employments';  
    protected $fillable = ['name'];
        
 protected $hidden = ['created_at', 'updated_at','create_by'];
	 public function employer()
    {
        return $this->hasMany('App\Model\Employer');	
    }

    public function Package()
    {
        return $this->hasOne('App\Model\FinancingPackage','id','package_id'); 
    }

   

    
}
