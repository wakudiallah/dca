<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class FinancingPackage extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
   	protected $table = 'financing_packages';  

   	public function Type() {
		return $this->belongsTo('App\Model\Loan','id_type','id');	
	}
}
