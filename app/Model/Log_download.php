<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Log_download  extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'log_download';  

    public function term()
    {
        return $this->hasOne('App\Model\Term','id_praapplication');	
    }
    
     public function user()
    {
        return $this->hasOne('App\Model\User','id','id_user');	
    }



}
