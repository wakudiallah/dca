<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Spouse  extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'spouse';  
   	 public function EmpType() {
		return $this->belongsTo('App\Model\EmploymentType','emptype','id');	
	}



}
