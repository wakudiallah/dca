<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Position  extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
   protected $table = 'position';  
	protected $fillable = ['position_code'];
 
}


//y