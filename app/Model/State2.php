<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class State2 extends Model implements Auditable
{
		use \OwenIt\Auditing\Auditable;
		protected $table = 'state2s';
		protected $fillable = ['state_code'];
        
 

}
