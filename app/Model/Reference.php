<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Reference  extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
   protected $table = 'reference';  

   public function Relationship1()
    {
        return $this->hasOne('App\Model\Relationship','id','relationship1');	
    }

    public function Relationship2()
    {
        return $this->hasOne('App\Model\Relationship','id','relationship2');	
    }



}
