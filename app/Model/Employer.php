<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Employer extends Model implements Auditable
{
	  use \OwenIt\Auditing\Auditable;
     protected $table = 'employers'; 

        protected $hidden = ['created_at', 'updated_at','create_by'];

public function employment() {
		return $this->belongsTo('App\Model\Employment','employment_id');	
	}
}
