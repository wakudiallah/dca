<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Branch  extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
   protected $table = 'branch';  

    public function term()
    {
        return $this->hasOne('App\Model\Term','id_branch');	
    }
    
    public function User() {
        return $this->hasMany('App\Model\User','id_branch');	
	}
    
    

}
