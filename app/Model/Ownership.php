<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use OwenIt\Auditing\Contracts\Auditable;

class Ownership extends Model implements Auditable
{
      use \OwenIt\Auditing\Auditable;


      protected $table 	= 'ownerships';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


}
