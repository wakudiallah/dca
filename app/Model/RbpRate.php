<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RbpRate extends Model implements Auditable
{
    //rbp_rates
    use \OwenIt\Auditing\Auditable;
    protected $table = 'rbp_rates';  
}
