<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Geoloc_Error extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
     protected $table = 'geoloc_errors'; 
}
