<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use HasRoles;
    
    protected $table = 'users';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function Branch() {
        return $this->belongsTo('App\Model\Branch','id_branch','id');   
    }

     public function MarOfficer() {
        return $this->belongsTo('App\MarketingOfficer','id','id');   
    }

     public function Manager() {
        return $this->belongsTo('App\Manager','id','id');   
    }

    public function MO() {
        return $this->hasOne('App\Manager','id','manager_id');  
    }

      public function Officer() {
        return $this->belongsTo('App\MO','id','id');   
    }
     public function FA() {
        return $this->hasOne('App\MO','id_mo','id_pra');  
    }

    public function myrole() {
        return $this->belongsTo('App\ModelHasRoles','id','model_id');   
    }

    public function desc() {
        return $this->belongsTo('App\Role','roles','id');   
    }
    
}
