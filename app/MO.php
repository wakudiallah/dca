<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class MO extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;
     protected $table = 'mo'; 
     

   
    
    public function User() {
        return $this->hasOne('App\Model\User','id');	
	}

	public function Manager() {
        return $this->hasOne('App\Manager','user_id','id_manager');	
	}

	 public function Branch()
    {
        return $this->hasOne('App\Model\Branch','branch_code','branch');  
    }
	
}
