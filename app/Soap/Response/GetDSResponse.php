<?php

namespace App\Soap\Response;

class GetDSResponse
{
  /**
   * @var string
   */
  protected $GetDSResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($GetDSResult)
  {
    $this->GetDSResult = $GetDSResult;
  }

  /**
   * @return string
   */
  public function getResult()
  {
    return $this->GetDSResult;
  }
}