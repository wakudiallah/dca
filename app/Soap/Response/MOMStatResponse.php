<?php

namespace App\Soap\Response;

class MOMStatResponse
{
  /**
   * @var string
   */
  protected $MOMTrmResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMStatResult)
  {
    $this->MOMStatResult = $MOMStatResult;
  }

  /**
   * @return string
   */
  public function getMOMStatResult()
  {
    return $this->MOMStatResult;
  }
}