<?php

namespace App\Soap\Response;

class MOMCommitResponse
{
  /**
   * @var string
   */
  protected $MOMCommitResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMCommitResult)
  {
    $this->MOMCommitResult = $MOMCommitResult;
  }

  /**
   * @return string
   */
  public function getMOMCommitResult()
  {
    return $this->MOMCommitResult;
  }
}