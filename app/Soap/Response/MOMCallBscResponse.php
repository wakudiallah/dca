<?php

namespace App\Soap\Response;

class MOMCallBscResponse
{
  /**
   * @var string
   */
  protected $MOMCallBscResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMCallBscResult)
  {
    $this->MOMCallBscResult = $MOMCallBscResult;
  }

  /**
   * @return string
   */
  public function getMOMCallBscResult()
  {
    return $this->MOMCallBscResult;
  }
}