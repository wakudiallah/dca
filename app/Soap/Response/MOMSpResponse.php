<?php

namespace App\Soap\Response;

class MOMSpResponse
{
  /**
   * @var string
   */
  protected $MOMSpResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMSpResult)
  {
    $this->MOMSpResult = $MOMSpResult;
  }

  /**
   * @return string
   */
  public function getMOMSpResult()
  {
    return $this->MOMSpResult;
  }
}