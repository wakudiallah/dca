<?php

namespace App\Soap\Response;

class MOMDSRBResponse
{
  /**
   * @var string
   */
  protected $MOMDSRBResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMDSRBResult)
  {
    $this->MOMDSRBResult = $MOMDSRBResult;
  }

  /**
   * @return string
   */
  public function getMOMDSRBResult()
  {
    return $this->MOMDSRBResult;
  }
}