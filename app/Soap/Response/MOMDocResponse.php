<?php

namespace App\Soap\Response;

class MOMDocResponse
{
  /**
   * @var string
   */
  protected $MOMDocResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMDocResult)
  {
    $this->MOMDocResult = $MOMDocResult;
  }

  /**
   * @return string
   */
  public function getMOMDocResult()
  {
    return $this->MOMDocResult;
  }
}