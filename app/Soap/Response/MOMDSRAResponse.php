<?php

namespace App\Soap\Response;

class MOMDSRAResponse
{
  /**
   * @var string
   */
  protected $MOMDSRAResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMDSRAResult)
  {
    $this->MOMDSRAResult = $MOMDSRAResult;
  }

  /**
   * @return string
   */
  public function getMOMDSRAResult()
  {
    return $this->MOMDSRAResult;
  }
}