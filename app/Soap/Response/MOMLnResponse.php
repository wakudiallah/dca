<?php

namespace App\Soap\Response;

class MOMLnResponse
{
  /**
   * @var string
   */
  protected $MOMLnResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMLnResult)
  {
    $this->MOMLnResult = $MOMLnResult;
  }

  /**
   * @return string
   */
  public function getMOMLnResult()
  {
    return $this->MOMLnResult;
  }
}