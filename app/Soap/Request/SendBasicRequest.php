<?php

namespace App\Soap\Request;

class SendBasicRequest
{
  /**
   * @var string
   */
  protected $nmSp;

  /**
   * @var string
   */
  protected $hmphSp;

  /**
   * @var string
   */
  protected $mbphSp;

  /**
   * @var string
   */
  protected $emtySp;

  /**
   * @var string
   */
  protected $emtySp_ot;

protected $app_no;
  /**
   * @var string
   */
  
  /**
   * @var string
   */

  /**
   * GetConversionAmount constructor.
   *
   * @param string $CurrencyFrom
   * @param string $CurrencyTo
   * @param string $RateDate
   * @param string $Amount
   */
  public function __construct($nmSp, $hmphSp, $mbphSp, $emtySp, $emtySp_ot, $app_no)
  {
    $this->nmSp        = $nmSp;
    $this->hmphSp     = $hmphSp;
    $this->mbphSp     = $mbphSp;
    $this->emtySp       = $emtySp;
    $this->emtySp_ot      = $emtySp_ot;
     $this->app_no      = $app_no;
  }

  /**
   * @return string
   */
  public function getnmSp()
  {
    return $this->nmSp;
  }

  /**
   * @return string
   */
  public function gethmphSp()
  {
    return $this->hmphSp;
  }

  /**
   * @return string
   */
  public function getmbphSp()
  {
    return $this->mbphSp;
  }

  /**
   * @return string
   */
  public function getemtySp()
  {
    return $this->emtySp;
  }

  /**
   * @return string
   */
  public function getemtySp_ot()
  {
    return $this->emtySp_ot;
  }
   public function getapp_no()
  {
    return $this->app_no;
  }

  /**
   * @return string
   */
 
}