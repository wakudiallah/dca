<?php

namespace App\Soap\Request;

class MOMBsc
{
  /**
   * @var string
   */
  protected $ctry;

  /**
   * @var string
   */
  protected $ctry_ori;

  /**
   * @var string
   */
  protected $ctzn;

  /**
   * @var string
   */
  protected $n_id;

  /**
   * @var string
   */
  protected $o_id;
  /**
   * @var string
   */
  protected $ctry_dob;
  protected $p_id;
  protected $dob;
  protected $nm;
  protected $nm_pref;
  protected $ttl;
  protected $ttl_oth;

  protected $gdr;
  protected $marit;
  protected $depend;
  protected $add1;
  protected $add2;
  protected $add3;
  protected $add4;
  protected $add5;

  protected $cor_add1;
  protected $cor_add2;
  protected $cor_add3;
  protected $cor_add4;
  protected $cor_add5;
  protected $cor_add6;
  protected $cor_add7;
  protected $cor_add8;

  protected $own;
  protected $rce;
  protected $bpr;
  protected $rel;
  protected $edc;
  protected $ctry_oth;
  protected $rce_oth;
  protected $rel_oth;

  /**
   * @var string
   */
  
  /**
   * @var string
   */

  /**
   * GetConversionAmount constructor.
   *
   * @param string $ctry
   * @param string $ctry_ori
   * @param string $ctzn
   * @param string $n_id
   * @param string $o_id
   * @param string $ctry_dob
   * @param string $dob
   * @param string $nm
   * @param string $nm_pref
   * @param string $ttl
   * @param string $ttl_oth
   * @param string $gdr
   * @param string $marit
   * @param string $depend
   * @param string $add1
   * @param string $add2
   * @param string $add3
   * @param string $add4
   * @param string $add5
   * @param string $cor_add1
   * @param string $cor_add2
   * @param string $cor_add3
   * @param string $cor_add4
   * @param string $cor_add5
   * @param string $cor_add6
   * @param string $cor_add7
   * @param string $cor_add8
   * @param string $own
   * @param string $rce
   * @param string $bpr
   * @param string $rel
   * @param string $edc
   * @param string $ctry_oth
   * @param string $rce_oth
   * @param string $rel_oth
   * @param string $ctg
   * @param string $appno
   */
  public function __construct($ctry, $ctry_ori, $ctzn, $n_id)
  {
    $this->ctry        = $ctry;
    $this->ctry_ori     = $ctry_ori;
    $this->ctzn     = $ctzn;
    $this->n_id       = $n_id;

  }

  /**
   * @return string
   */
  public function getctry()
  {
    return $this->ctry;
  }

  /**
   * @return string
   */
  public function getctry_ori()
  {
    return $this->ctry_ori;
  }

  /**
   * @return string
   */
  public function getctzn()
  {
    return $this->ctzn;
  }

  /**
   * @return string
   */
  public function getn_id()
  {
    return $this->n_id;
  }

  /**
   * @return string
   */
  public function geto_id()
  {
    return $this->o_id;
  }
   public function getctry_dob()
  {
    return $this->ctry_dob;
  }

   public function getp_id()
  {
    return $this->p_id;
  }

   public function getdob()
  {
    return $this->dob;
  }

   public function getnm()
  {
    return $this->nm;
  }
   public function getnm_pref()
  {
    return $this->nm_pref;
  }
   public function getttl()
  {
    return $this->ttl;
  }
   public function getttl_oth()
  {
    return $this->ttl_oth;
  }
   public function getgdr()
  {
    return $this->gdr;
  }
   public function getmarit()
  {
    return $this->marit;
  } public function getdepend()
  {
    return $this->depend;
  }

   public function getadd1()
  {
    return $this->add1;
  } 
   public function getadd2()
  {
    return $this->add2;
  } 
   public function getadd3()
  {
    return $this->add3;
  } 
   public function getadd4()
  {
    return $this->add4;
  } 
   public function getadd5()
  {
    return $this->add5;
  } 

  public function getcor_add1()
  {
    return $this->cor_add1;
  }
    public function getcor_add2()
  {
    return $this->cor_add2;
  }
    public function getcor_add3()
  {
    return $this->cor_add3;
  }
    public function getcor_add4()
  {
    return $this->cor_add4;
  }
    public function getcor_add5()
  {
    return $this->cor_add5;
  }
    public function getcor_add6()
  {
    return $this->cor_add6;
  }
    public function getcor_add7()
  {
    return $this->cor_add7;
  }
    public function getcor_add8()
  {
    return $this->cor_add8;
  }
   public function getown()
  {
    return $this->own;
  }
   public function getrce()
  {
    return $this->rce;
  }
   public function getbpr()
  {
    return $this->bpr;
  }
   public function getrel()
  {
    return $this->rel;
  }

   public function getedc()
  {
    return $this->edc;
  }
   public function getctry_oth()
  {
    return $this->ctry_oth;
  }
    public function getrce_oth()
  {
    return $this->rce_oth;
  }
    public function getrel_oth()
  {
    return $this->rel_oth;
  }
    public function getctg()
  {
    return $this->ctg;
  }
    public function getapp_no()
  {
    return $this->app_no;
  }

  /**
   * @return string
   */
 
}