<?php



Route::get('/status', function () {
    return view('home.index_status', ['name' => 'James']);
});



Route::group(['middleware' => 'web'], function () {

Auth::routes();
Route::get('/home', 'HomeController@index');
Route::get('/upload', 'HomeController@upload');
Route::post('form/uploaddoc', 'LoanForm\LoanFormController@uploaddoc');
Route::post('save/upload/{id}', 'LoanForm\LoanFormController@upload_cust');

Route::post('verifiedDoc/upload/{id}', 'Admin\VerifiedDocController@upload');
Route::get('/admin/downloaddocpdf/{ic}/{file}', 'HomeController@downloaddocpdf');
Route::get('/agreement/{pra}', 'HomeController@agreement');
Route::get('/terma/{pra}', 'HomeController@terma');
Route::get('/downloadpdf/{id_pra}', 'User\UserController@downloadpdf');
Route::post('form/savesettlement', 'LoanForm\LoanFormController@savesettlement');
Route::get('/postcode/{id}', 'HomeController@postcode');
Route::get('/review/{id}', 'HomeController@review');
Route::get('/faq', 'FaqController@index');
Route::get('/contact', 'ContactController@index');
Route::get('/agent', 'ContactController@agent');
Route::post('/contact/contact_save', 'ContactController@contact_save');

Route::view('/excel', 'excel', ['name' => 'excel']);
Route::post('/move', 'Admin\AdminController@move');

Route::resource('admin/master/package', 'Master\PackageController'); 
Route::get('/admin/step1/{id_pra}/{view}', 'Admin\AdminController@step1_verification');
Route::get('/admin/step1_view/{id_pra}/{view}', 'Admin\AdminController@step1_view');
Route::post('/admin/scoring', 'Admin\AdminController@scoring');
Route::post('/admin/sector', 'Admin\AdminController@sector');
Route::get('grading/{id}', 'Admin\AdminController@grading');
Route::put('/admin/rbp', 'Admin\AdminController@rbp');
Route::put('/admin/installment', 'Admin\AdminController@installment');
Route::get('admin/master/scorerating', 'Master\PackageController@scorerating'); 
Route::post('/admin/master/save/scorerating', 'Master\PackageController@savescorerating'); 
Route::post('/admin/master/update/scorerating/{id}', 'Master\PackageController@update_scorerating'); 

Route::get('admin/master/scorecard', 'Master\PackageController@scorecard'); 
Route::post('/admin/master/save/scorecard', 'Master\PackageController@savescorecard'); 
Route::post('/admin/master/update/scorecard/{id}', 'Master\PackageController@update_scorecard'); 
//DETAIL
Route::get('/admin/detail_pra/{id_pra}', 'Admin\AdminController@detail_pra');
Route::get('/moform/{id_pra}/{verify}', 'LoanForm\MoFormController@show'); //new
Route::get('/admin/get_document/{id_pra}/{name}', 'Admin\AdminController@get_document');//new
Route::get('/get_document/{id_pra}/{name}', 'HomeController@get_document');//new
//add doc
Route::get('/add_document/{id_pra}/{verify}', 'LoanForm\MoFormController@add_document'); //new

Route::get('/', 'PraApplication\PraApplicationController@index');
//
Route::get('/mumtaz-i', 'IndexController@mumtaz');
Route::get('/afdhal-i', 'IndexController@afdhal');
Route::get('/private-sector-i', 'IndexController@privatesector');
Route::get('/package/{id}', 'IndexController@package');

Route::get('applynow', 'PraApplication\PraApplicationController@applynow');
Route::resource('praapplication', 'PraApplication\PraApplicationController'); 
Route::resource('referral', 'PraApplication\ReferralController');
Route::resource('form', 'LoanForm\LoanFormController'); 
Route::resource('employer', 'Master\EmployerController'); 
Route::post('employer/update', 'Master\EmployerController@update2');
Route::post('employer/delete', 'Master\EmployerController@delete'); 
Route::resource('user', 'User\UserController'); 
Route::get('auth/login', 'Auth\LoginController@getLogin');
Route::get('admdca/login', 'Auth\LoginController@adm');
Route::post('auth/login', 'Auth\LoginController@login');
Route::post('praapplication/{id}', 'PraApplication\PraApplicationController@update2');
Route::post('mopraapplication/{id}', 'PraApplication\PraApplicationController@updatemo');
Route::resource('admin', 'Admin\AdminController'); 
Route::get('/admin/user_detail/{id_pra}/{view}', 'Admin\AdminController@detail');
Route::get('/admin/user_detail_view/{id_pra}/{view}', 'Admin\AdminController@detail_view');
Route::get('/admin/downloadzip/{id_pra}', 'HomeController@downloadzip');
Route::get('/admin/downloadpdf/{id_pra}', 'Admin\AdminController@downloadpdf');
Route::get('/admin/form/view/{id}', 'Admin\AdminController@view_pdf'); 
Route::get('/admin/pds/{id_pra}', 'Admin\AdminController@downloadpds');
Route::resource('verifiedForm', 'Admin\VerifiedController'); 
Route::resource('verifiedMoForm', 'Admin\VerifiedMoController'); 
Route::post('verifiedMoForm/route', 'Admin\VerifiedMoController@route'); 
Route::post('fom/{id}', 'PraApplication\PraApplicationController@update2');
Route::post('form/upload/{id}', 'LoanForm\LoanFormController@upload');
Route::get('/admin/allhq_user/{id}', 'Admin\AdminController@allhq_user');
Route::get('/admin/allbranch_user/{id}', 'Admin\AdminController@allbranch_user');
Route::post('/admin/addhq_user', 'Admin\AdminController@addhq_user');
Route::post('/admin/addbranch_user', 'Admin\AdminController@addbranch_user');
Route::post('/admin/editbranch_user', 'Admin\AdminController@editbranch_user');
Route::post('/admin/deletebranch_user', 'Admin\AdminController@deletebranch_user');
Route::get('/admin/allbranch/{id}', 'Admin\AdminController@allbranch');
Route::post('/admin/addbranch', 'Admin\AdminController@addbranch');
Route::post('/admin/editbranch', 'Admin\AdminController@editbranch');
Route::post('/admin/deletebranch', 'Admin\AdminController@deletebranch');
Route::post('/admin/changed_bybank', 'Admin\AdminController@changed_bybank');
Route::post('/admin/confirm_approved', 'Admin\AdminController@confirm_approved');
Route::post('form/term', 'LoanForm\LoanFormController@term');
Route::get('form/requestedit/{id}', 'LoanForm\LoanFormController@requestedit');
Route::get('form/approveedit/{id}', 'LoanForm\LoanFormController@approvetedit');
Route::post('form/routeback', 'LoanForm\LoanFormController@routeback');
Route::post('verifiedForm/term', 'Admin\VerifiedController@term');
Route::resource('verifiedDoc', 'Admin\VerifiedDocController');
Route::post('verifiedDoc/term', 'Admin\VerifiedDocController@term');
Route::get('/admin/report/{id}', 'Admin\AdminController@report_first');
Route::get('/admin/report/{startdate}/{finisdate}/{status}/{branch}', 'Admin\AdminController@report');
Route::get('/admin/report_result/{startdate}/{finisdate}/{status}/{branch}', 'Admin\AdminController@report_result');
Route::resource('testemail', 'Admin\Testemail'); 
Route::get('test/{id}', 'Admin\AdminController@test');
Route::post('update_password', 'User\UserController@update_password'); 
Route::post('update_password_first', 'User\UserController@update_password_first'); 
Route::get('incomplated_doc', 'Admin\AdminController@incomplated_doc');
Route::post('/admin/deletepackage', 'Master\PackageController@destroy');

//report
Route::get('report/calculated', 'Report\ReportController@calculated');
Route::post('report/calculated', 'Report\ReportController@calculated_view');
Route::post('report/calculated_excel', 'Report\ReportController@calculated_excel');

Route::get('report/registered', 'Report\ReportController@registered');
Route::post('report/registered', 'Report\ReportController@registered_view');
Route::post('report/registered_excel', 'Report\ReportController@registered_excel');

Route::get('report/submitted', 'Report\ReportController@submitted');
Route::post('report/submitted', 'Report\ReportController@submitted_view');
Route::post('report/submitted_excel', 'Report\ReportController@submitted_excel');

Route::get('report/bystatus', 'Report\ReportController@bystatus');
Route::post('report/bystatus', 'Report\ReportController@bystatus_view');
Route::post('report/bystatus_excel', 'Report\ReportController@bystatus_excel');

Route::get('report/waps', 'Report\ReportController@waps');
Route::post('report/waps', 'Report\ReportController@waps_view');
Route::post('report/bystatus_excel', 'Report\ReportController@bystatus_excel');

//reset
/*Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/emails', 'Auth\PasswordController@postEmails');*/
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');;
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// aplikasi yang statusnya lagi di route back aja
Route::get('routeback_status', 'Admin\AdminController@routeback_status');

Route::resource('moapplication', 'PraApplication\MoApplicationController'); 
Route::resource('moinfo', 'ProfileController'); 

Route::get('loglogin', 'MoInfoController@loglogin')->name('admin');; 
Route::get('viewmap/{id}/{lat}/{lng}', 'MoInfoController@loglogin_map'); 

// mo register
Route::resource('auth/moregister', 'User\MoController');
Route::post('getLocation', 'User\MoController@getlocation');

// List aplikasi yang di reject
Route::get('reject', 'Admin\AdminController@reject');
Route::get('docsreject', 'Admin\AdminController@docsreject');
//Route::get('docsreject', 'Master\GenderController@index');
Route::get('approved', 'Admin\AdminController@approved');

Route::get('stats', 'Admin\AdminController@stats');
Route::get('activities/{id}', 'Admin\AdminController@activities');
Route::get('activities_user/{id}', 'HomeController@activities_user');

// tes query 
Route::get('tesquery', 'Admin\AdminController@tesquery');

// mo reg geo location
Route::resource('geolocation', 'GeoLocController');
Route::get('geolocation/error/{id}', 'GeoLocController@error');
//Route::get('geolocation/map/{lat}/{lng}/{location?}', 'GeoLocController@map');
Route::get('geolocation/map/{id}', 'GeoLocController@map');
Route::resource('marketing', 'MOM\MarketingController');
Route::post('activate_agent', 'MOM\MarketingController@activate_agent');
Route::post('edit_manager', 'MOM\ManagerController@edit_manager');
Route::get('marketing_list', 'Admin\AdminController@marketing_list');



Route::resource('manager', 'MOM\ManagerController');
Route::post('save_manager', 'MOM\ManagerController@save_manager');
Route::resource('moform', 'LoanForm\MoFormController'); 
Route::post('moform/term', 'LoanForm\MoFormController@term');
Route::get('/email_check/{emel}/{id_pra}', 'LoanForm\MoFormController@email_check');
Route::get('/email_save/{emel}/{id_pra}', 'LoanForm\MoFormController@email_save');


Route::get('mbsb_login', 'Auth\MBSBAuthController@getLogin');
Route::post('mbsb_login', 'Auth\MBSBAuthController@login');
Route::get('mbsb_logout', 'Auth\MBSBAuthController@logout');

Route::get('direct_application', 'Admin\AdminController@direct_application');
Route::get('/admin/download_form/{id_pra}', 'Admin\AdminController@download_form');

//WEB SERVICES
Route::get('/list_of_agent/{id}', 'WebServices\ListMOController@agent');
Route::get('/mo', 'MoInfoController@mo');
//Route::post('/send_to_mom', 'MoInfoController@send_to_mom');
//anti attrition
Route::get('/agent/anti-attrition', 'CLRT\AntiAttritionController@index');
Route::get('/agent/anti-attrition-multi', 'CLRT\AntiAttritionController@multi');

Route::get('/anti-attrition-multi', 'CLRT\AntiAttritionController@multi');

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'CLRT\AntiAttritionController@myformAjax'));
Route::get('branch/{id}','HomeController@myformAjax');
Route::post('/send_to_mom', 'CLRT\AntiAttritionController@assign_to_mo');
Route::get('select_fa/{id}','HomeController@fa');


Route::get('/clrt/new-loan-offer', 'CLRT\NewLoanOfferController@newloanoffer');
Route::post('/clrt/new_loan_offer_view', 'CLRT\NewLoanOfferController@newloanoffer_view');
Route::get('/clrt/loan-offer-detail/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer');
Route::get('/clrt/loan-offer-detail-sms/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer_sms');
Route::get('/clrt/loan-offer-detail-assigned/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer_assigned');
Route::get('/clrt/otp-timeout/{id}', 'CLRT\NewLoanOfferController@otp_timeout');
Route::get('/clrt/request-ramcy/{id}', 'CLRT\NewLoanOfferController@request_ramcy');
Route::post('/clrt/send_to_mo', 'CLRT\AntiAttritionController@send_to_mo');
Route::get('/clrt/select-tenure/{id}', 'CLRT\NewLoanOfferController@select_tenure');
Route::get('/clrt/anti-attrition', 'CLRT\AntiAttritionController@antiattrition');

Route::post('/send-sms', 'CLRT\AntiAttritionController@send_sms');
Route::post('/send-sms-next', 'CLRT\AntiAttritionController@send_sms_next');
Route::post('/verify-otp', 'CLRT\AntiAttritionController@verify_otp');
Route::post('/send-sms-timeout', 'CLRT\AntiAttritionController@send_sms_timeout');

Route::get('/insert','InsertController@index');
Route::post('/submitForm','InsertController@submitForm');
Route::post('/submitotp','InsertController@submitOtp');
Route::get('/showresult','InsertController@show');

Route::get('/assign-to-fa','Admin\AdminDCAController@index');


Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::get('first_login', 'User\UserController@first_login');
//parameter
Route::resource('admin/master/parameter/mail-system', 'Master\MailSystemController');
Route::post('admin/master/mail-system/{id}','Master\MailSystemController@update');
Route::post('admin/master/delete/mail-system/{id}','Master\MailSystemController@destroy');




Route::get('profile','ProfileController@profile');

//
Route::get('audits', 'AuditController@index');
//Route::get('/package/{id}', 'ContactController@package');

Route::post('/post-assign-to-fa','Admin\AdminDCAController@store');

 Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {


	
    //PARAMETER

        Route::group(['prefix' => '/user-creation'], function(){
            Route::get('list', 'Admin\UserCreationController@index');
            Route::get('add', 'Admin\UserCreationController@add');
            Route::post('/save', 'Admin\UserCreationController@save');
            Route::get('/edit/{id}','Admin\UserCreationController@edit');
            Route::post('/update/{id}','Admin\UserCreationController@update');
			Route::post('/delete/{id}','Admin\UserCreationController@destroy');
        });

          Route::group(['prefix' => '/admin-creation'], function(){
            Route::get('list', 'Admin\AdminCreationController@index');
            Route::get('add', 'Admin\AdminCreationController@add');
            Route::post('/save', 'Admin\AdminCreationController@save');
          Route::get('/edit/{id}','Admin\AdminCreationController@edit');
            Route::post('/update/{id}','Admin\AdminCreationController@update');
			Route::post('/delete/{id}','Admin\AdminCreationController@destroy');
        });

         Route::group(['prefix' => '/master'], function(){

			Route::resource('/occupation', 'Master\OccupationController');
			Route::get('/occupation', 'Master\OccupationController@index');
			Route::post('/occupation/store','Master\OccupationController@store');
			Route::post('/occupation/{id}','Master\OccupationController@update');
			Route::post('/delete/occupation/{id}','Master\OccupationController@destroy');


			Route::resource('/state', 'Master\StateController');
			Route::get('/state', 'Master\StateController@index');
			Route::post('/state/store','Master\StateController@store');
			Route::post('/state/{id}','Master\StateController@update');
			Route::post('/delete/state/{id}','Master\StateController@destroy');


			Route::resource('/country', 'Master\CountriController');
			Route::get('/country', 'Master\CountriController@index');
			Route::post('/country/store','Master\CountriController@store');
			Route::post('/country/{id}','Master\CountriController@update');
			Route::post('/delete/country/{id}','Master\CountriController@destroy');




			Route::resource('/codework', 'Master\CodeWorkController');
			Route::get('/codework', 'Master\CodeWorkController@index');
			Route::post('/codework/store','Master\CodeWorkController@store');
			Route::post('/codework/{id}','Master\CodeWorkController@update');
			Route::post('/delete/codework/{id}','Master\CodeWorkController@destroy');


			Route::resource('/codejobstatus', 'Master\CodeJobStatusController');
			Route::get('/codejobstatus', 'Master\CodeJobStatusController@index');
			Route::post('/codejobstatus/store','Master\CodeJobStatusController@store');
			Route::post('/codejobstatus/{id}','Master\CodeJobStatusController@update');
			Route::post('/delete/codejobstatus/{id}','Master\CodeJobStatusController@destroy');



			Route::resource('/position', 'Master\PositionController');
			Route::get('/position', 'Master\PositionController@index');
			Route::post('/position/store','Master\PositionController@store');
			Route::post('/position/{id}','Master\PositionController@update');
			Route::post('/delete/position/{id}','Master\PositionController@destroy');



			Route::resource('/relationship', 'Master\RelationshipController');
			Route::get('/relationship', 'Master\RelationshipController@index');
			Route::post('/relationship/store','Master\RelationshipController@store');
			Route::post('/relationship/{id}','Master\RelationshipController@update');
			Route::post('/delete/relationship/{id}','Master\RelationshipController@destroy');



			Route::resource('/gender', 'Master\GenderController');
			Route::get('/gender', 'Master\GenderController@index');
			Route::post('/gender/store','Master\GenderController@store');
			Route::post('/gender/{id}','Master\GenderController@update');
			Route::post('/delete/gender/{id}','Master\GenderController@destroy');


			Route::resource('/ownership', 'Master\OwnershipController');
			Route::get('/ownership', 'Master\OwnershipController@index');
			Route::post('/ownership/store','Master\OwnershipController@store');
			Route::post('/ownership/{id}','Master\OwnershipController@update');
			Route::post('/delete/ownership/{id}','Master\OwnershipController@destroy');

			Route::resource('/nationality', 'Master\NationalityController');
			Route::get('/nationality', 'Master\NationalityController@index');
			Route::post('/nationality/store','Master\NationalityController@store');
			Route::post('/nationality/{id}','Master\NationalityController@update');
			Route::post('/delete/nationality/{id}','Master\NationalityController@destroy');

			Route::resource('type-customer', 'Master\TypeCustomerController');
			Route::get('type-customer', 'Master\TypeCustomerController@index');
			Route::post('type-customer/store','Master\TypeCustomerController@store');
			Route::post('type-customer/{id}','Master\TypeCustomerController@update');
			Route::post('delete/type-customer/{id}','Master\TypeCustomerController@destroy');

			Route::resource('payment-mode', 'Master\PaymentModeController');
			Route::get('payment-mode', 'Master\PaymentModeController@index');
			Route::post('payment-mode/store','Master\PaymentModeController@store');
			Route::post('payment-mode/{id}','Master\PaymentModeController@update');
			Route::post('delete/payment-mode/{id}','Master\PaymentModeController@destroy');
						
						Route::resource('race', 'Master\RaceController');
			Route::get('race', 'Master\RaceController@index');
			Route::post('race/store','Master\RaceController@store');
			Route::post('race/{id}','Master\RaceController@update');
			Route::post('delete/race/{id}','Master\RaceController@destroy');

			Route::resource('education', 'Master\EducationController');
			Route::get('education', 'Master\EducationController@index');
			Route::post('education/store','Master\EducationController@store');
			Route::post('education/{id}','Master\EducationController@update');
			Route::post('delete/education/{id}','Master\EducationController@destroy');

			Route::resource('religion', 'Master\ReligionController');
			Route::get('religion', 'Master\ReligionController@index');
			Route::post('religion/store','Master\ReligionController@store');
			Route::post('religion/{id}','Master\ReligionController@updated');
			Route::post('delete/religion/{id}','Master\ReligionController@destroy');


			Route::resource('marital', 'Master\MaritalController');
			Route::get('marital', 'Master\MaritalController@index');
			Route::post('marital/store','Master\MaritalController@store');
			Route::post('marital/{id}','Master\MaritalController@update');
			Route::post('delete/marital/{id}','Master\MaritalController@destroy');


			Route::resource('resident', 'Master\ResidentController');
			Route::get('resident', 'Master\ResidentController@index');
			Route::post('resident/store','Master\ResidentController@store');
			Route::post('resident/{id}','Master\ResidentController@update');
			Route::post('/delete/resident/{id}','Master\ResidentController@destroy');


			
			Route::resource('mail-systyem', 'Master\MailSystemController');
			Route::get('mail-systyem', 'Master\MailSystemController@index');
			Route::post('mail-systyem/store','Master\MailSystemController@store');
			Route::post('mail-systyem/{id}','Master\MailSystemController@update');
			Route::post('delete//mail-systyem/{id}','Master\MailSystemController@destroy');

			Route::resource('employment-type', 'Master\EmploymentTypeController');
			Route::get('employment-type', 'Master\EmploymentTypeController@index');
			Route::post('employment-type/store','Master\EmploymentTypeController@store');
			Route::post('employment-type/{id}','Master\EmploymentTypeController@update');
			Route::post('/delete/employment-type/{id}','Master\EmploymentTypeController@destroy');

			Route::resource('employment', 'Master\EmploymentController');
			Route::get('employment', 'Master\EmploymentController@index');
			Route::post('employment/store','Master\EmploymentController@store');
			Route::post('employment/{id}','Master\EmploymentController@update');
			Route::post('/delete/employment/{id}','Master\EmploymentController@destroy');

		


        });



    });
});



Route::post('admin/check-waps', 'WapsController@check_waps');

Route::get('dashboard-dsu', 'DashboardController@dashboard_dsu');
Route::get('dashboard-admin-2', 'DashboardController@dashboard_admin_2');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::post('/save/uploadic/{id}','HomeController@uploadic');

Route::get('/ref/{id}', 'IndexController@ref');