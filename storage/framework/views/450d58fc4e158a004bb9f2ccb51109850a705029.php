<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Parameter - Email System";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["email"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <br>
                           
                            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-plus'></i> Add Email</a>
                          
                            	

                                <!-- Modal add -->
                                <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="addBranch">Add Email System</h4>
                                            </div>
                                            <div class="modal-body">
                                             <?php echo Form::open(['url' => 'admin/master/mail-system','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                                
                                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                                              <fieldset>
                                                	<section >
                                                      <label class="label">Name </label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="name" name="name" placeholder="Name" required>
                                                            <b class="tooltip tooltip-bottom-right">Name</b>
                                                        </label>
                                                    </section>
                                                    <section >
                                                      <label class="label">Email</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="email" id="effective_rate" name="email" placeholder="Email" required>
                                                            <b class="tooltip tooltip-bottom-right">Email</b>
                                                        </label>
                                                    </section>
                                                    
                                                </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Submit
                                                            </button>
                                                           
                                                   <?php echo Form::close(); ?>   
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->



                            
                            <br><br>

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
                                                <tr>
                                                    <th width="10">No</th>
                                                    <th width="50">Name</th>
                                                    <th width="50">Email</th>
                                                    <th width="20" align="center">Action</th>
                                                </tr>
											</thead>
                                            <tbody>
                                            <?php $i=1; ?>
                                            <?php $__currentLoopData = $mail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                  <td><?php echo e($i++); ?></td>
                                                    <td><?php echo e($data->name); ?></td>
                                                    <td><?php echo e($data->email); ?> </td>
                                                    <td align="center">
                                                        <a href='#' data-toggle='modal' class="btn btn-default btn-sm" data-target='#myModal<?php echo e($i); ?>'><i class="fa fa-pencil"></i> Edit</a>
                                                       
                                                        <a href='#' data-toggle='modal' data-target='#deleteModal<?php echo e($i); ?>' class="btn btn-sm btn-default" ><i class="fa fa-trash"></i> Delete</a>

                                                    <!-- Modal Edit -->
                                                    <div class="modal fade" id="myModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="myModal<?php echo e($i); ?>" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        &times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="addBranch">Edit Email <?php echo e($data->name); ?> </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                 <?php echo Form::open(['url' => 'admin/master/mail-system/'.$data->id,'class' => 'smart-form client-form', 'id' =>'smart-form-register3', 'method' => 'POST' ]); ?>

                                                                  <fieldset>
                                                                        <section >
                                                                            <input type="hidden" name="_method" value="PUT"/>
                                                                            <label class="label">Name </label>
                                                                                <label class="input">
                                                                                    <i class="icon-append fa fa-bank"></i>
                                                                                    <input type="text" id="Name" name="name" placeholder="Name" value="<?php echo e($data->name); ?>" required>
                                                                                    <b class="tooltip tooltip-bottom-right"> Name</b>
                                                                                </label>
                                                                        </section>
                                                                        <section >
                                                                          <label class="label">Email</label>
                                                                             <label class="input">
                                                                                <i class="icon-append fa fa-user"></i>
                                                                                <input type="text" value="<?php echo e($data->email); ?>"  id="effective_rate" name="email" placeholder="Email" required>
                                                                                <b class="tooltip tooltip-bottom-right">Email</b>
                                                                            </label>
                                                                        </section>
                                                                        

                                                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                        <input type="hidden" name="idUser" value="<?php echo e($data->id); ?>">
                                                                     </fieldset>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                        Cancel
                                                                    </button>
                                                                    <button type="submit" name="submit" class="btn btn-primary">
                                                                        Submit
                                                                    </button>
                                                                               
                                                                    <?php echo Form::close(); ?>   
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal edit-->



                                                     <!-- Modal -->
                                                    <div class="modal fade" id="deleteModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="deleteModal<?php echo e($i); ?>" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        &times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="DeleteBranch"><b>Are You Sure to Delete <?php echo e($data->name); ?> ? </b></h4>
                                                                </div>
                                                        
                                                                <div class="modal-body">
                                                                     <?php echo Form::open(['url' => 'delete/parameter/mail-system/'.$data->id, 'method' => 'POST' ]); ?>


                                                                     

                                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">


                                                                     [ Warning!. You can not restore this execution. The data will be permanently deleted and may involve the loss of related data ]
                                                                    <div align='right'>
                                                                    
                                                                    <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                                                                    


                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                        No
                                                                    </button>
                                                                    <button type="submit" name="submit" class="btn btn-primary">
                                                                        Yes
                                                                    </button></div>
                                                                               
                                                                       <?php echo Form::close(); ?>   
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->



                                                    </td>
                                                    
                                                    
                                                  
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</tbody>
                                        </table>
																					   
										
		<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
        <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
        <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->



<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email: {
                    required : true,
                    email : true
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                 Branch : {
                    required : true
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email: {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                    Branch: {
                    required : 'Please select a Branch'
                },
      
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>



          
<?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/admin/master/mailsystem/index.blade.php ENDPATH**/ ?>