<!DOCTYPE html>
<html lang="en">
<head>
    <title>Personal Financing-i | MBSB Bank</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="icon" type="image/png" href="<?php echo e(url('/asset')); ?>/img/favicon/favicon.ico"/>

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/bootstrap/css/bootstrap.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/fonts/font-awesome-4.7.0/css/font-awesome.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/fonts/iconic/css/material-design-iconic-font.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/animate/animate.css')); ?>">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/css-hamburgers/hamburgers.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/animsition/css/animsition.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/select2/select2.min.css')); ?>">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/vendor/daterangepicker/daterangepicker.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/css/util.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/css/main.css')); ?>">

    <link src="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>



<body style="background-color: #999999;">

    



    <?php echo $__env->yieldContent('content'); ?>
    
    






   


    <?php echo $__env->yieldPushContent('custom-scripts'); ?>

</body>
</html><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/layouts/front/template.blade.php ENDPATH**/ ?>