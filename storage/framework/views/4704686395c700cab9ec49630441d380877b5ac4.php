 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --><script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="<?php echo e(asset('new/temp/js/jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('asset/lib/valid/jquery.validate.js')); ?>"></script>
<script src="<?php echo e(asset('asset/lib/valid/additional-methods.js')); ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo e(asset('new/temp/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('new/temp/js/menumaker.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/jquery.sticky.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/sticky-header.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/owl.carousel.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/multiple-carousel.js')); ?>"></script>
    <script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
    </script>


        <!--================================================== -->

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo e(asset('/asset/js/plugin/pace/pace.min.js')); ?>"></script>

    

        <!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo e(asset('/asset/js/app.config.js')); ?>"></script>
        <script src="<?php echo e(asset('/asset/js/jquery.steps.js')); ?>"></script>
        <script src="<?php echo e(asset('/asset/js/jquery.steps.min.js')); ?>"></script>
            <script src="<?php echo e(asset('/asset/js/jquery.simulate.js')); ?>"></script>
        

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo e(asset('/asset/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js')); ?>"></script> 

        <!-- BOOTSTRAP JS -->
        <script src="<?php echo e(asset('/asset/js/bootstrap/bootstrap.min.js')); ?>"></script>

        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo e(asset('/asset/js/notification/SmartNotification.min.js')); ?>"></script>

        <!-- JARVIS WIDGETS -->
        <script src="<?php echo e(asset('/asset/js/smartwidgets/jarvis.widget.min.js')); ?>"></script>

        <!-- EASY PIE CHARTS -->
        <script src="<?php echo e(asset('/asset/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>

        <!-- SPARKLINES -->
        <script src="<?php echo e(asset('/asset/js/plugin/sparkline/jquery.sparkline.min.js')); ?>"></script>

        <!-- JQUERY VALIDATE -->
        <script src="<?php echo e(asset('/asset/js/plugin/jquery-validate/jquery.validate.min.js')); ?>"></script>

        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo e(asset('/asset/js/plugin/masked-input/jquery.maskedinput.min.js')); ?>"></script>

        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo e(asset('/asset/js/plugin/select2/select2.min.js')); ?>"></script>

        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo e(asset('/asset/js/plugin/bootstrap-slider/bootstrap-slider.min.js')); ?>"></script>

        <!-- browser msie issue fix -->
        <script src="<?php echo e(asset('/asset/js/plugin/msie-fix/jquery.mb.browser.min.js')); ?>"></script>

        <!-- FastClick: For mobile devices -->
        <script src="<?php echo e(asset('/asset/js/plugin/fastclick/fastclick.min.js')); ?>"></script>

        <!--[if IE 8]>
            <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
        <![endif]-->

        <!-- Demo purpose only -->
        <script src="<?php echo e(asset('/asset/js/demo.min.js')); ?>"></script>

        <!-- MAIN APP JS FILE -->
        <script src="<?php echo e(asset('/asset/js/app.min.js')); ?>"></script>      
        <script src="<?php echo e(asset('/asset/js/jssor.slider.min.js')); ?>"></script>     

        <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
        <!-- Voice command : plugin -->
        <script src="<?php echo e(asset('/asset/js/speech/voicecommand.min.js')); ?>"></script>  

      
        <script src="<?php echo e(asset('/asset/js/file/vendor/jquery.ui.widget.js')); ?>"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo e(asset('/asset/js/file/jquery.iframe-transport.js')); ?>"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo e(asset('/asset/js/file/jquery.fileupload.js')); ?>"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?php echo e(asset('/asset/js/bootbox.min.js')); ?>"></script>


        <script type="text/javascript">
            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            $(document).ready(function() {
                pageSetUp();
            })
        </script>



    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/lib/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/jquery.fancybox.js?v=2.1.5')); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/jquery.fancybox.css?v=2.1.5')); ?>" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')); ?>" />
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')); ?>"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')); ?>" />
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')); ?>"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-media.js?v=1.0.6')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

            /*
             *  Different effects
             */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title : {
                        type : 'outside'
                    },
                    overlay : {
                        speedOut : 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                helpers : {
                    title : {
                        type : 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS    : 'fancybox-custom',
                closeClick : true,

                openEffect : 'none',

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        css : {
                            'background' : 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect : 'elastic',
                openSpeed  : 150,

                closeEffect : 'elastic',
                closeSpeed  : 150,

                closeClick : true,

                helpers : {
                    overlay : null
                }
            });

            /*
             *  Button helper. Disable animations, hide close button, change title type and content
             */

            $('.fancybox-buttons').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    buttons : {}
                },

                afterLoad : function() {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
             *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
             */

            $('.fancybox-thumbs').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,
                arrows    : false,
                nextClick : true,

                helpers : {
                    thumbs : {
                        width  : 50,
                        height : 50
                    }
                }
            });

            /*
             *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect : 'none',
                    closeEffect : 'none',
                    prevEffect : 'none',
                    nextEffect : 'none',

                    arrows : false,
                    helpers : {
                        media : {},
                        buttons : {}
                    }
                });

            /*
             *  Open manually
             */

            $("#fancybox-manual-a").click(function() {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function() {
                $.fancybox.open({
                    href : 'iframe.html',
                    type : 'iframe',
                    padding : 5
                });
            });

            $("#fancybox-manual-c").click(function() {
                $.fancybox.open([
                    {
                        href : '1_b.jpg',
                        title : 'My title'
                    }, {
                        href : '2_b.jpg',
                        title : '2nd title'
                    }, {
                        href : '3_b.jpg'
                    }
                ], {
                    helpers : {
                        thumbs : {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });


        });
    </script>



<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                email : {
                    required : true,
                    validate_email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                   equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    validate_email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>

<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var maxloanz = $('#maxloanz').val();
    var package = $('#Package2').val();
    var loanamount = $('#LoanAmount2').val();
     var interests = $('#interests').val();
    if(tenure==2){
         var installment = $('#amount2').val();
        var rate = $('#rate').val();
    } 
    else if(tenure==3){
        var installment = $('#amount3').val();
        var rate = $('#rate').val();
    }
    else if(tenure==4){
        var installment = $('#amount4').val();
        var rate = $('#rates').val();
    }
    else if(tenure==5){
        var installment = $('#amount5').val();
        var rate = $('#rates').val();
    }
    else if(tenure==6){
        var installment = $('#amount6').val();
        var rate = $('#rates').val();
    }
    else if(tenure==7){
        var installment = $('#amount7').val();
        var rate = $('#rates').val();
    }
    else if(tenure==8){
        var installment = $('#amount8').val();
        var rate = $('#rates').val();
    }
    else if(tenure==9){
        var installment = $('#amount9').val();
        var rate = $('#rates').val();
    }
    else if(tenure==10){
        var installment = $('#amount10').val();
        var rate = $('#rates').val();
    }

  $.ajax({
        type: "PUT",
        url: '<?php echo e(url('/form/')); ?>'+'/99',
        data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, maxloan : maxloanz, package : package, rate : rate, installment : installment,loanamount:loanamount,interests:interests
        },
        success: function (data, status) {
        }
    });
});
</script>

<script type="text/javascript">

    $(document).ready(function() {

    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var LoanAmount = $('#LoanAmount2').val();
    var maxloanz = $('#maxloanz').val();
      
    $.ajax({
        type: "PUT",
        url: '<?php echo e(url('/form/')); ?>'+'/9',
        data: { id_praapplication: id_praapplication,  _token : _token, loanammount : LoanAmount, maxloan : maxloanz 
        },
        success: function (data, status) {
        }
        });
    });
</script>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    
<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
    var tenure = $('input[name=tenure]:checked').val();
    var terms = $('input[name=terms]:checked').val();
    if(tenure>0){
        if(terms>0){
            $('#myModal').modal('show');
        }
        else{
             bootbox.alert('Please check consent box if you want to proceed');
        }
    }else{
         bootbox.alert('Please Select Tenure');
    }
   
    });
});
</script>


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/praapplication/result_js.blade.php ENDPATH**/ ?>