<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Personal Financing-i ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>

<div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">


           
          <div class="">
            <?php echo $__env->make('auth.login_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
        </div>
	<div class="row">
		<div class="col-xs-10 col-sm-10 col-md-6 col-lg-3"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="well no-padding">
                <?php echo Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]); ?>

                <header>
                  <p class="txt-color-white"><b>  Check Financing Eligibility  </b> </p>
                </header>

                <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-12 col-md-12 col-lg-12">
                            <section class="col col-12 col-lg-12 col-md-12 col-xs-12">
                                <label class="label"> <b> Full Name &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"    <?php if(Session::has('fullname')): ?>  value="<?php echo e(Session::get('fullname')); ?>" <?php endif; ?> size="55">
                                    <b class="tooltip tooltip-bottom-right">FullName</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> ID Type &nbsp;<sup>*</sup></b></label>
                                  <label class="input">
                                    <i class="icon-append fa fa-id-card"></i>
                                    <input readonly="" disabled name="" id="" value="NEW IC - 12 NUMERIC"  size="55" >
                                    <input  name="type" id="type" value="IN"  type="hidden"  >
                                    <b class="tooltip tooltip-bottom-right">Full Name</b>
                                </label>
                              
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifNewIc" style="display: show">
                             <section class="col col-12 col-xs-12 col-md-12">
                            <label class="label"> <b> IC Number &nbsp;<sup>*</sup></b></label>
                            <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                <i class="icon-append fa fa-user"></i>
                                <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="Ex: 931111120101"  <?php if(Session::has("icnumber")): ?>  value="<?php echo e(Session::get('icnumber')); ?>" <?php endif; ?> >
                                <b class="tooltip tooltip-bottom-right">IC Number</b>
                            </label>
                        </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifOther" style="display: none">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Other ID Type &nbsp;<sup>*</sup></b></label>
                                <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                  <i class="icon-append fa fa-user"></i>
                                    <input  type="text" id="others" onkeyup="this.value = this.value.toUpperCase()" name="other"  placeholder="Other ID Type"  <?php if(Session::has('other')): ?>  value="<?php echo e(Session::get('other')); ?>" <?php endif; ?> minlength="6" maxlength="12" required="" >
                                    <b class="tooltip tooltip-bottom-right">Other ID Type</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12" id="ifDOB" style="display: none">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b>DOB (dd/mm/yyyy)</b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                    <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value=""  class="form-control startdate" id="dob" required=""/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifgender" style="display: none">
                             <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Gender </b></label>
                                <label class="select">
                                    <select name="gender" id="gender" class="form-control" required="">
                                        <!--<option>--Select--</option>-->
                                        <option value="11">Male</option>
                                        <option value="22">Female</option>
                                    </select> <i></i>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Handphone Number &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                

                     <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="PhoneNumber" name="PhoneNumber" minlength="12" maxlength="12" placeholder="Ex: 011000000"  <?php if(Session::has("phone")): ?>  value="<?php echo e(Session::get('phone')); ?>" <?php endif; ?> >
                                    <b class="tooltip tooltip-bottom-right">Handphone Number</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Employment &nbsp;<sup>*</sup></b></label>
                                <label class="select">
                                    <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                        <option value="0">--Select Employment--</option>
                                        <?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select> <i></i>
                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                 </label>
                            </section>
                         </div>
                    </div>
                </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Employer &nbsp;<sup>*</sup></b></label>

                                    <label id="majikan2" class="input <?php if(Session::has('employer')): ?> state-error <?php endif; ?> ">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Employer"  <?php if(Session::has('majikan')): ?>  value="<?php echo e(Session::get('majikan')); ?>" <?php endif; ?> maxlength="100">
                                        <b class="tooltip tooltip-bottom-right"> Employer</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Basic Salary (RM) &nbsp;<sup>*</sup></b>  </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input  <?php if(Session::has('basicsalary')): ?>  value="<?php echo e(Session::get('basicsalary')); ?>" <?php endif; ?>  id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                                        <b class="tooltip tooltip-bottom-right">Basic Salary (RM) </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Allowance (RM) &nbsp;<sup>*</sup></b></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                          <input <?php if(Session::has('allowance')): ?>  value="<?php echo e(Session::get('allowance')); ?>" <?php endif; ?>  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  >
                                        <b class="tooltip tooltip-bottom-right">Allowance (RM)</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"><b> Total Deduction in payslip only (RM) &nbsp;<sup>*</sup></b> </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input <?php if(Session::has('deduction')): ?>  value="<?php echo e(Session::get('deduction')); ?>" <?php endif; ?> id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"   >
                                        <b class="tooltip tooltip-bottom-right"> Total Deduction (RM)  </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Package &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <input type="text" name="package" id="package" <?php if(Session::has('package_name')): ?>  value="<?php echo e(Session::get('package_name')); ?>" <?php endif; ?> class="form-control" readonly>

                                  

                                    <input type="hidden" name="package_id" id="package_id" <?php if(Session::has('package_id')): ?>  value="<?php echo e(Session::get('package_id')); ?>" <?php else: ?> value="1" <?php endif; ?> class="form-control" readonly>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b>Financing Amount(RM) &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-credit-card"></i>
                                        <input id="LoanAmount" name="LoanAmount" placeholder="Financing Amount (RM)" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  type="text" required=""   class="fn" step=".01"  <?php if(Session::has('loanAmount')): ?>  value="<?php echo e(Session::get('loanAmount')); ?>" <?php endif; ?> >
                                        <b class="tooltip tooltip-bottom-right">Financing Amount (RM)</b>
                                </label>
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            </section>
                        </div>
                    </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                         <b>   Calculate  </b>
                        </button>
                        <div id="response"></div>
                    </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
            
                                   
        </div>
      </div>

    </div></div>
     
<?php echo $__env->make('layouts.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/praapplication/applynow.blade.php ENDPATH**/ ?>