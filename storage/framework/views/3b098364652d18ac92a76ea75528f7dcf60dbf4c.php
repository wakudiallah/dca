<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Branch Master";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["Profile"]["active"] = true;
include("asset/inc/nav.php");


?>
<style type="text/css">
 
#share-buttons img {
width: 35px;
padding: 5px;
border: 0;
box-shadow: 0;
display: inline;
}
 
</style>
<style>
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 140px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px;
  position: absolute;
  z-index: 1;
  bottom: 150%;
  left: 50%;
  margin-left: -75px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}
</style>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>     
                </header>
            <div>
            <div class="jarviswidget-editbox"></div>
                <div class="widget-body no-padding">
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                        <tbody>
                            <tr>
                                <td>Full Name</td>
                                <td><?php echo e($user->name); ?></td>
                           </tr>
                           <tr>
                                <td>Email Address</td>
                                <td><?php echo e($user->email); ?></td>
                           </tr>
                           <tr>
                                <td>Employer ID</td>
                                <td><?php echo e($user->employer_id); ?></td>
                           </tr>
                           <tr>
                                <td>Branch</td>
                                <td><?php echo e($user->branch_code); ?> </td>
                           </tr>
                           <?php
                            $url = url('/ref/'.$user->referral_mo) ;
                           ?>
                            <tr>
                                <td>Share Refferal Link</td>
                                <td>
                                    <a href=" <?php echo e(url('/ref/'.$user->referral_mo)); ?>">Link</a>
                                    <button onclick="myFunction()" onmouseout="outFunc()">
                                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                                        Copy text
                                    </button>
                                </td>
                            </tr>
                           <tr>
                                <td></td>
                                <td>
                                    <div id="share-buttons">                                        <!-- Email -->
                                        <a href="mailto:?Subject=Simple Share Buttons&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 https://simplesharebuttons.com">
                                            <img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email" />
                                        </a>
                                        <a href="https://www.facebook.com/sharer.php?u=<?php echo e($url); ?>" rel="me" title="Facebook" target="_blank"><i class="fa facebook"></i>ff</a>
                                        <!-- Facebook -->
                                        <a href="http://www.facebook.com/sharer.php?u=" target="_blank">
                                            <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                                        </a>
                                        <button id="copy-button" data-clipboard-text="Copy Me!" title="Click to copy me.">Copy to Clipboard</button>
                                         <script src="ZeroClipboard.js"></script>
                                        <!-- Google+ -->
                                        <a href="https://plus.google.com/share?url=https://simplesharebuttons.com" target="_blank">
                                            <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
                                        </a>
                                        <a href="https://twitter.com/share?url=<?php echo e($url); ?>&text=<?php echo e('67676'); ?>" rel="me" title="Twitter" target="_blank"><i class="fa twitter"></i>rr</a>
                                        
                                        <!-- LinkedIn -->
                                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://simplesharebuttons.com" target="_blank">
                                            <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
                                        </a>
                                      
                                        <a href="whatsapp://send?text=<URL>" data-action="share/whatsapp/share" onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on whatsapp">wa</a>
                                        <!-- Print -->
                                        <a href="javascript:;" onclick="window.print()">
                                            <img src="https://simplesharebuttons.com/images/somacro/print.png" alt="Print" />
                                        </a>
                                      
                                        <!-- Twitter -->
                                        <a href="https://twitter.com/share?url=https://simplesharebuttons.com&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons" target="_blank">
                                            <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                                        </a>
                                    </div>
                                </td>
                           </tr>
                       </tbody>
                   </table>
               </div>
           </div>
       </div>
        </article>
    </div>
</div>

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>


<script type="text/javascript">
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied: " + copyText.value;
}

function outFunc() {
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copy to clipboard";
}
</script>

          
<?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/admin/profile.blade.php ENDPATH**/ ?>