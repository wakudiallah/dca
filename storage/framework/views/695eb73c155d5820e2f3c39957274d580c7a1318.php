<html>
<head>
<style type="text/css">

body {

        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        font-size: 10px !important;
}
    
th {
        color:black;
    font-size: 14px;
  

}

br.border2  {
   display: block;
   margin: 20px 0;
}

td {
    color:black;
    font-size: 11px;
    line-height: 150%;

}
p {
    
    line-height: 130%;

}

ul
{
    list-style-type: none;
}

table.border {
    color:black;
  border-collapse: collapse;
    border: 1px solid black;
}

td.border {
    color:black;
  border-collapse: collapse;
    border: 0px solid black;
}
td.border2 {
   
}
td.header {
            color:black;
            
             background-color: black;
        }


</style>
</head>
<body>

   <table>
        <tr>
            <td>
                <img src="<?php echo e(url('/img/image003.png')); ?>" width="auto" height="auto">
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>PRODUCT DISCLOSURE SHEET</td>
        </tr>
        <tr>
            <td>Please read this Product Disclosure Sheet before you decide to take up this product. Be sure to read the general terms and conditions</td>
            <td>Personal Financing-i<br>
                Date :
            </td>
        </tr>
    </table>
    <table  width="530" class="" >
         <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>1 What is this product about?</b></font>
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                This is an unsecured Islamic personal financing, offered to individuals and the profit is calculated based on variable rate and monthly rest basis
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>2 What is the Shariah concept applicable?</b></font>
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               This Personal Financing-i product is based on the concept of Tawarruq, whereby:
                <li type='i' style="margin-left: 20px;">
                    Customer, via the application form, will execute Purchase Request and Promise to Buy the commodity (Wa`d) from the Bank at Selling Price
                    based on Murabahah consisting the cost price of the commodity and disclosed profit margin. At the same time, customer also appoints the Bank
                    as an agent (Wakalah) to sell the commodity to third party commodity trader
                </li>
                <li type='i' style="margin-left: 20px;">
                    Upon approval of the application, the Bank will offer the facility via short message system (SMS). Upon receiving customer’s reply to accept the facility via SMS, the Bank will purchase the commodity (tele-communication airtime) at cost price which is equivalent to Purchase Price/Financing Amount
                </li>
                <li type='i' style="margin-left: 20px;">
                    Upon transfer of ownership of the commodity to the Bank, the Bank will then sell the commodity to the customer at the Selling Price based on Murabahah which shall be payable by customer to the Bank by way of instalment or deferred payment basis. The customer will buy the same commodity at the Selling Price. The Murabahah sale transaction is concluded via SMS
                </li>
                <li type='i' style="margin-left: 20px;">
                    Upon completion of item ii and iii above, the Bank, acting as customer’s agent will sell the commodity to the third party commodity trader at the commodity cost price which is equivalent to the Purchase Price. The proceeds from the sale of the Commodity will be credited into the customer’s account during disbursement of the facility. 
                </li>
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>3 What is the Base Rate (BR)?</b></font>
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                Base Rate (BR) is the reference rate used as the basis for pricing retail financing facilities. The BR is determined based on the Bank's average Cost of Funds (COF) plus the Statutory Reserve Requirement (SRR) cost imposed by Bank Negara Malaysia
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>4 What is this product about?</b></font>
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                Our BR can rise or fall due to changes in the benchmark COF and changes in the SRR. Changes in the benchmark COF could occur due to changes in the Overnight Policy Rate (OPR) as decided by the Monetary Policy Committee of Bank Negara Malaysia, as well as other factors such as changes in the cost of funds used to fund floating rate financing.<br>
                Example:<br>
                If the cost of customer deposit rises, the COF will rise as customer deposits is part of the components used to fund floating rate financing. The higher
                COF will trigger a rate increase in the BR.
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>5 Historical benchmark COF in the last 3 years</b></font>
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                Please refer to www.mbsbbank.com (under Base Rate / Base Financing Rate) for the latest historical data
            </td>
        </tr>
         <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>6 What do I get from this product?</b></font>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="" align="justify">
               Financing Amount
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               RM <?php echo e(number_format((float)$dsr_b->financing_amount, 2, ',' , ',' )); ?>

            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               Financing Tenure
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               <?php echo e($dsr_b->tenure); ?> years
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               Profit Rate
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               BR <?php echo e($pra->package->base_rate); ?> % p.a
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                Effective Profit Rate
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
              <?php echo e($pra->package->effective_rate); ?> % p.a
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               Ceiling Profit Rate
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               15.00 % p.a
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
              Selling Price
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
                <?php
                    $selling    = $dsr_b->monthly_repayment * ($dsr_b->tenure *12);
                    $month      = $dsr_b->tenure * 12;
                    $monthly    = $month -1;
                    $financing_amount = $dsr_b->financing_amount;
                    $tenure = $dsr_b->tenure;
                    $i = $dsr_b->rate;
                   
                     $loans = $dsr_b->financing_amount;
                     $in = $dsr_b->rate;
                    function pmt( $financing_amount , $i, $tenure ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $tenure);

                        $pmt = $financing_amount*($int*$r1)/($r1-1);

                            return $pmt;

                        }
                        $amount =  round(pmt( $loans, $in, $tenure*12),2);
                         $installment =  round(pmt( $loans, $in, $tenure*12),0);

                        $selling_price    = $amount * ($dsr_b->tenure *12);

                        $final =($selling_price -($installment * ($month - 1)));

                       
                        $cpr = 15;
                        function pmt2( $financing_amount , $cpr, $tenure ) {

                        $int = $cpr/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $tenure);

                        $pmt = $financing_amount*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        $installment_cpr =  round(pmt2( $loans, $cpr, $tenure*12),2);
                        $selling_price_cpr    = $installment_cpr * ($dsr_b->tenure *12);

                        $profit_epr = $selling_price - $financing_amount;


                        
                        $goes_up1 = $pra->package->effective_rate + 1;
                        $min1 = min(15/100,$goes_up1/100);
                       
                        function pmt3( $financing_amount , $min1, $tenure ) {

                        $int = $min1/12;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $tenure);

                        $pmt = $financing_amount*($int*$r1)/($r1-1);

                            return $pmt;

                        }
                         $installment_goes_up1 =  round(pmt3( $loans, $min1, $tenure*12),0);
                         $payment_up1 = $month*$installment_goes_up1;
                         $profit_up1 = $payment_up1 - $financing_amount;

                        $goes_up2 = $pra->package->effective_rate + 2;
                         $min2 = min(15/100,$goes_up2/100);
                        function pmt4( $financing_amount , $min2, $tenure ) {

                        $int = $min2/12;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $tenure);

                        $pmt = $financing_amount*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        $br_up1 = $pra->package->base_rate +1;
                        $br_up2 = $pra->package->base_rate +2;
                         $installment_goes_up2 =  round(pmt4( $loans, $min2, $tenure*12),0);
                         $payment_up2 = $month*$installment_goes_up2;
                         $profit_up2 = $payment_up2 - $financing_amount;

                ?>
                RM <?php echo e(number_format((float)$selling_price_cpr, 2, ',' , ',' )); ?>  
                
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               *Current BR is <?php echo e($pra->package->base_rate); ?> %
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4" width="700px">  
                <font color="black"> <b>7 What are my financial obligations?</b></font>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="" align="justify">
              Monthy Payments
            </td>
            <td class="" align="justify">
               
            </td>
            <td class="" align="justify">
               
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               First   <?php echo e($monthly); ?>  months
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               RM  <?php echo e(number_format((float)$installment, 2, ',' , ',' )); ?>  and
            </td>
        </tr>
         <tr>
            <td class="" align="justify">
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Final     months
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
                 RM  <?php echo e(number_format((float)$final, 2, ',' , ',' )); ?>  
            </td>
        </tr>

    </table>
    <table>
        <tr>
            <td class="" align="justify">
                The amount of selling price you must pay is:  RM <?php echo e(number_format((float)$selling_price, 2, ',' , ',' )); ?>

            </td>
        </tr>
        <tr>
             <td class="" align="justify">
                If applicable, rebate will be granted based on the difference between the Ceiling Profit Rate and the Effective Profit Rate and as long as the Effective Profit Rate is lower than the Ceiling Profit Rate.
            </td>
        </tr>
    </table>  
     <table width="100%"  cellspacing="4" style="page-break-before: always">
    </table>
    <table>
         <tr>
            <td>
                <img src="<?php echo e(url('/img/image003.png')); ?>" width="auto" height="auto">
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td align="justify">  
                Important: In the event of change in BR, your monthly instalment may change accordingly. However, the total payment amount should not exceed the Selling Price 
            </td>
        </tr>
    </table>
    <table style="border:1px solid black;border-collapse:collapse; font-weight: bold;width: 100%">
        <thead>
            <tr>
                <th style="border:1px solid black; width: 25% !important">Rate</th>
                <th style="border:1px solid black; width: 25% !important;text-align: center !important">Current BR = <?php echo e($pra->package->base_rate); ?> %</th>
                <th style="border:1px solid black; width: 25% !important;text-align: center !important">If BR Goes up by 1% = <?php echo e($br_up1); ?> %</th>
                <th style="border:1px solid black; width: 25% !important;text-align: center !important">If BR Goes up by 2% = <?php echo e($br_up2); ?> %</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border:1px solid black;">Monthly Instalment</td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$installment, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$installment_goes_up1, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$installment_goes_up2, 2, ',' , ',' )); ?></td>
            </tr>
             <tr>
                <td style="border:1px solid black;">Total profit charged at the end of [<?php echo e($dsr_b->tenure); ?>] years</td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$profit_epr, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$profit_up1, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$profit_up2, 2, ',' , ',' )); ?></td>
            </tr>
             <tr>
                <td style="border:1px solid black;">Total payment amount at the end of [<?php echo e($dsr_b->tenure); ?>] years</td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$selling_price, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$payment_up1, 2, ',' , ',' )); ?></td>
                <td style="border:1px solid black;text-align: center !important">RM <?php echo e(number_format((float)$payment_up2, 2, ',' , ',' )); ?></td>
            </tr>
        </tbody>
    </table>
    <table>
        <tr>
            <td class="" align="justify" style="padding-left: 2em !important">
                <li type="i">The Bank shall provide you with particulars of the revised monthly instalment amount at least seven (7) calendar days prior to the revised monthly instalment date comes into effect.</li>
                <li type="i">Financing calculator is available in the Bank’s website for Customer’s reference. All calculations are estimations, based on an indicative profit rate. </li>            
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>8 What other fees and charges do I have to pay?</b></font>
            </td>
        </tr>
    </table>     
    <table>
         <tr>
            <td class="" align="justify">
               Wakalah Fee
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               RM 34.00
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
               Takaful Contribution
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
               
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                Product Bundling (if any)
            </td>
            <td class="" align="justify">
               
            </td>
            <td class="" align="justify">
            
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Name
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
            
            </td>
        </tr>
        <tr>
            <td class="" align="justify">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount
            </td>
            <td class="" align="justify">
               :
            </td>
            <td class="" align="justify">
            
            </td>
        </tr>
      
    </table>
    
    <table>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>9 What if I fail to fulfill my obligations?</b></font>
            </td>
        </tr>
         <tr>
            <td  align="justify" style="padding-left: 2em !important">  

                <li>During facility tenure: Late Payment Compensation (Ta'widh) will be charged at 1% p.a. on such overdue amount under the Facility or such part thereof or such other method or rate as shall be stipulated by Shariah Advisory Committee of the Bank from time to time and not compounded.</li>
                <li>After the maturity of the facility: Late Payment Compensation (Ta'widh) will be charged at a rate that shall not exceed the prevailing daily overnight Islamic Interbank Money Market rate (IIMM) on the outstanding balance</li>
                <li>The Bank has the right to set-off any credit balance in your account maintained with us against any outstanding balance in this financing account, however, it would be made known to the customer</li>
                <li>The Bank reserves the right to take legal action and all costs incurred will be borne by the customer. This legal action will have an effect on your credit rating and may lead to difficulties in your future financing applications.
                </li>
                
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>10 Do I need any Takaful coverage?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">  
                The Group Credit Family Takaful (GCFT) coverage is not compulsory. However it is strongly encouraged to protect you as it will cover the outstandin amount in circumstances of death or total permanent disability. Takaful policy is required to be taken from our panel of Takaful providers or other takaful providers of your choice approved by the Bank. The contribution amount will be deducted upfront from the financing amount. The Bank also offers other optional Bancatakaful products that will be applicable through product bundling packages. The Bancatakaful products may also be subject to renewal
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>11 If there is any excess payment arising from settlement or closure of my financing account, will I get a refund?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">  
                Yes you will. However, if the refund amount is RM5 and below, your consent to irrevocably agree to waive your rights to a refund will be obtained via our application form to authorize the Bank to donate the said amount to charitable organisations deemed appropriate by the Bank
            </td>
        </tr>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>12 What if I fully settle the financing before its maturity?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify" >  
               The Bank shall grant Ibra’ to the customer where there is :
                <li type="i" style="padding-left: 2em !important">Early settlement or early redemption of the Facility; or</li>
                <li type="i" style="padding-left: 2em !important">Settlement of the original financing contract due to financing restructuring exercise; or</li>
                <li type="i" style="padding-left: 2em !important">Settlement by Customer in the case of default; or</li>
                <li type="i" style="padding-left: 2em !important">Settlement by Customer in the event of termination or cancellation of financing before the maturity date</li></ol><br>
                The ibra' (rebate) is calculated as follows:
                <br>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <b>Ibra' at settlement</b><br>
                <b>Settlement Amount</b>
            </td>
            <td>=<br>
                =
            </td>
            <td>Deferred Profit - Early settlement charges (if any)<br>
                 Outstanding selling price + Instalments due + Late payment compensation – Adjustment on ibra’ due to fluctuations of Effective Profit Rate (EPR) (if any) – Ibra’ at settlement
            </td>
    </table>
    <table>
         <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>13 What are the major risks?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">
                <li  style="padding-left: 2em !important">The profit rate is variable rate and may change according to change(s) in the reference rate (i.e. BR). An increase in profit rate may result inn higher monthly payment. However, the profit rate increase is capped at the ceiling profit rate</li>
                <li  style="padding-left: 2em !important">Should you encounter any difficulties in meeting your obligations, please contact us in advance to discuss other payment alternatives. This is to avoid any legal action against you in the event you are unable to pay your monthly payment</li>
            </td>
        </tr>
    </table>
     <table width="100%"  cellspacing="4" style="page-break-before: always" >
          <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>14 Do I need a guarantor or collateral?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">
                No guarantor or collateral is required.
            </td>
        </tr>
         <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>15 What do I need to do if there are changes to my contact details?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">
                It is important that you inform us of any change in your contact details to ensure that all correspondence reaches you in a timely manner. Please contact 03-2096 3000 (Customer Service Center) or visit the nearest branch to update the change(s)
            </td>
        </tr>
         <tr>
            <td class="border" align="left" bgcolor="#cbd0d4">  
                <font color="black"> <b>16 Where can I get assistance and redress?</b></font>
            </td>
        </tr>
    </table> 
    <table style="border:1px solid black;border-collapse:collapse; font-weight: bold">
        <tr>
            <td style="border:1px solid black;">If you have difficulties in making payments, you should contact us earliest</td>
            <td style="border:1px solid black;">Alternatively, you may seek the services of Agensi Kaunseling dan Pengurusan Kredit (AKPK) that provide money management and credit counselling for individuals. You can contact AKPK at : </td>
        </tr>
         <tr>
            <td style="border:1px solid black;">Collection and Recovery Department,<br>
                Ground Floor, Menara MBSB,<br>
                46, Jalan Dungun,Damansara Heights,<br>
                50490 Kuala Lumpur<br>
                Tel No. : 03-2082 8000<br>
                Fax No. : 03-2092 1035<br> 
            </td>
            <td style="border:1px solid black;">Agensi Kaunseling dan Pengurusan Kredit<br>
                8th Floor, Maju Junction Mall,<br>
                1001, Jalan Sultan Ismail,<br>
                50250 Kuala Lumpur<br>
                Tel No. : 1 800 88 2575<br>
                E-mail : enquiry@akpk.org.my
            </td>
        </tr>
        <tr>
            <td style="border:1px solid black;">If you wish to complain on the products or services provided by us, you may contact us at :</td>
            <td style="border:1px solid black;">If you query or compliant is not satisfactorily resolved by us, you may contact Bank Negara Malaysia LINK or TELELINK a </td>
        </tr>
        <tr>
            <td style="border:1px solid black;">Collection and Recovery Department,<br>
                Customer Experience Management<br> 
                7th Floor, Wisma MBSB, <br>
               48, Jalan Dungun, Damansara Heights<br>
                50490 Kuala Lumpur<br>
                Tel No. : 03-2096 3000<br>
                Fax No. : 03-2092 1035<br>
                E-mail : complaint@mbsbbank.com
            </td>
            <td style="border:1px solid black;">Block D, Bank Negara Malaysia<br>
                Jalan Dato’ Onn<br>
                50480 Kuala Lumpur,<br>
                50250 Kuala Lumpur<br>
                Tel. No. : 1-300-88-5465<br>
                Fax No. : 03-22174 1515<br>
                E-mail : bnmtelelink@bnm.gov.my 
            </td>
        </tr>
    </table>  
    <table>
        <tr>
            <td class="border" align="left" bgcolor="#cbd0d4" width="700px">  
                <font color="black"> <b>17 Where can I get further information?</b></font>
            </td>
        </tr>
        <tr>
            <td  align="justify">
               Should you require additional information on the Personal Financing-i, please visit our website www.mbsbbank.com or contact us at:
            </td>
        </tr>
    </table>
    <table style="border:1px solid black;">
          <tr>
            <td>
                Personal Financing-i Department<br>
                Retail Business Division<br>
                7th Floor, Menara MBSB,<br>
                46, Jalan Dungun, Damansara Heights,<br>
                50490 Kuala Lumpur<br>
                Tel No. : 03-2082 8000<br>
                Faks No. : 03-2096 1063<br>
                Website : www.mbsbbank.com
            </td>
        </tr>
    </table> 
    <table style="background-color: black; color: white !important;text-align: center;" width="675px">
        <tr>
            <td>IMPORTANT NOTE :</td>
        </tr>
        <tr>
            <td>LEGAL ACTION MAY BE TAKEN AGAINST YOU IF YOU FAIL TO PAY THE MONTHLY INSTALMENTS OF YOUR PERSONAL </td>
        </tr>
        <tr>
            <td>FINANCING-I FACILITY</td>
        </tr>
    </table>
    <table>
        <tr>
            <td>The information provided in this disclosure sheet is valid as at</td>
            <td></td>
        </tr>
    </table>      
</body>
</html>



<?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/pdf/pds.blade.php ENDPATH**/ ?>