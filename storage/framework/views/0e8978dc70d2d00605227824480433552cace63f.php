<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Admin Creation";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["creation"]["sub"]["admin_creations"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br>
       
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>     
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body no-padding">
                          <form action="<?php echo e(url('/')); ?>/admin/admin-creation/update/<?php echo e($list->id_pra); ?>" method="POST" enctype="multipart/form-data" class="smart-form client-form">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <fieldset>
                                <section>
                                    <label>Fullname <sup>*</sup>:</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" value="<?php echo e($list->name); ?>" id="Name" name="name" placeholder="Full Name" required onkeyup="this.value = this.value.toUpperCase()" onkeypress="return FullName(event);">
                                        <b class="tooltip tooltip-bottom-right">Full Name</b>
                                    </label>
                                </section>
                                <section>      
                                     <label>Employer ID <sup>*</sup>:</label>     
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" value="<?php echo e($list->employer_id); ?>" id="employer_id" name="employer_id" placeholder="Employer ID" required autocomplete="off" minlength="4" maxlength="20" onkeyup="this.value = this.value.toUpperCase()">
                                        <b class="tooltip tooltip-bottom-right">Employer ID</b>
                                    </label>
                                </section>
                                <section>   
                                    <label>Phone NO <sup>*</sup>:</label>    
                                     <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" id="phone" value="<?php echo e($list->phone); ?>" name="phone" placeholder="Phone No" required  minlength="9" maxlength="12"  >
                                        <b class="tooltip tooltip-bottom-right">Phone No</b>
                                    </label>
                                </section>
                                <section>
                                     <label>Email <sup>*</sup>:</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="email" value="<?php echo e($list->email); ?>" id="Email" name="email" placeholder="Email" required maxlength="50">
                                        <b class="tooltip tooltip-bottom-right">Email</b>
                                    </label>
                                </section>
                                <section>
                                    <label>Role <sup>*</sup>:</label>
                                    <label class="select">
                                        <i class="icon-append fa fa-user"></i>
                                         <select class="select2" name="role" id="branch_code" required="">
                                            <option selected=""></option>
                                            <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($list->role !=NULL): ?>
                                                    <?php 
                                                        if($list->role==$role->id) {
                                                          $selected = "selected";
                                                        }
                                                        else {
                                                           $selected = "";
                                                        } 
                                                    ?>
                                                        <option <?php echo e($selected); ?> value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                    </label>
                                </section>
                               
                               
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            
                               <a class="btn btn-default"href="<?php echo e(url('/')); ?>/admin/user-creation/list"> Cancel</a>
                            
                            <button type="submit" name="submit" class="btn btn-primary">
                                Submit
                            </button>
                        <?php echo Form::close(); ?>   
                    </div><br><br><br><br>
                </div>
            </div>
        </article>
    </div>
</div>
<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script src="<?php echo e(asset('asset/lib/valid/jquery.validate.js')); ?>"></script>
<script src="<?php echo e(asset('asset/lib/valid/additional-methods.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

        $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }           
        });
    })
</script>
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });  
</script>
<script type="text/javascript">
    function checkPass()
{
    var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('password_confirmation');
    var message = document.getElementById('error-nwl');
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    
    if(pass1.value.length > 7)
    {
        pass1.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "character number ok!"
    }
    else
    {
        pass1.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = ""
        return;
    }
  
    if(pass1.value == pass2.value)
    {
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = ""
    }
    else
    {
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = " Passwords do not match"
    }
}  
</script>

<script>
    $(document).ready(function() {
    // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove(); 
        });
        }, 2800);
    });
</script>
    


<script type="text/javascript">
    $(document).ready(function(){
        $("#phone").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 48 && inputValue <= 57)){
                event.preventDefault();
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#set_password" ).change(function() {
        var set_password = $('#set_password').val();
            if(set_password == 1) {
                $("#section_password").show();
                $("#password").prop('required',true);
                $("#password_confirmation").prop('required',true);
            }
        else {
            $("#section_password").hide();
            $("#password").prop('required',false);
            $("#password_confirmation").prop('required',false);
        }   
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });

});
</script>

<script type="text/javascript">
      $(function () {
        $("#employer_id").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
 
            $("#lblError").html("");
 
            //Regex for Valid Characters i.e. Alphabets and Numbers.
            var regex = /^[a-zA-Z z0-9 ]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets and Numbers allowed.");
            }
 
            return isValid;
        });
    });
</script><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/admin/master/admin_creation/edit.blade.php ENDPATH**/ ?>