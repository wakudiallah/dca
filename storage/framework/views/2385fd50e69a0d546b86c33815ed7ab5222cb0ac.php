<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Personal Financing-i | MBSB Bank</title>
    <!-- Bootstrap -->
    <link href="<?php echo e(asset('new/temp/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Style CSS -->
    <link href="<?php echo e(asset('new/temp/css/style.css')); ?>" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="<?php echo e(asset('new/temp/css/owl.carousel.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('new/temp/css/owl.theme.default.css')); ?>" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/temp/css/fontello.css')); ?>">
    <link href="<?php echo e(asset('new/temp/css/font-awesome.min.css" rel="stylesheet')); ?>">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>




	
<body>

	
 
     <?php echo $__env->yieldContent('content'); ?>


 <div class="footer">
        <div class="container">
            <div class="row">
                <!-- footer-contact links -->
                <div class=" col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title">Contact Info </h3>
                        <div class="contact-info">
                            <span class="contact-icon"><i class="fa fa-map-marker"></i></span>
                            <span class="contact-text">1847 Goldleaf Lane River <br> Edge, USA 07649 </span>
                        </div>
                        <div class="contact-info">
                            <span class="contact-icon"><i class="fa fa-phone"></i></span>
                            <span class="contact-text">+180-123-4567 / 89</span>
                        </div>
                        <div class="contact-info">
                            <span class="contact-icon"><i class="fa fa-envelope"></i></span>
                            <span class="contact-text">info@demo.com</span>
                        </div>
                        <div class="ft-social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.footer-useful links -->
                <!-- footer-company-links -->
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title">Company</h3>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="arrow">
                                    <li><a href="#">Home </a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Job List</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="arrow">
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Client reviews</a></li>
                                    <li><a href="#">Partner</a></li>
                                    <li><a href="#">Consultation</a></li>
                                    <li><a href="#">Help Center</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.footer-services-links -->
                <!-- footer-useful links -->
                <div class=" col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="ft-logo"><img src="./images/ft_logo.png" alt=""></div>
                </div>
                <!-- /.footer-useful links -->
            </div>
        </div>
        <!-- tiny-footer -->
        <div class="tiny-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Copyright © All Rights Reserved 2020 Template Design by
                            <a href="https://easetemplate.com/" target="_blank" class="copyrightlink">EaseTemplate</a></p>
                    </div>
                </div>
            </div>
            <!-- /. tiny-footer -->
        </div>
    </div>
    <!-- /.footer -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo e(asset('new/temp/js/jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('asset/lib/valid/jquery.validate.js')); ?>"></script>
<script src="<?php echo e(asset('asset/lib/valid/additional-methods.js')); ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo e(asset('new/temp/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('new/temp/js/menumaker.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/jquery.sticky.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/sticky-header.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/owl.carousel.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/multiple-carousel.js')); ?>"></script>
    <script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
    </script>

<script src="http://localhost:7777/dca_uat/dca/public/asset/js/plugin/select2/select2.min.js"></script>
</body>
</html><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/layouts/dca/template2.blade.php ENDPATH**/ ?>