<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="<?php echo e(url('/asset')); ?>/img/favicon/favicon.ico"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Personal Financing-i | MBSB Bank</title>
    <!-- Bootstrap -->
    <link href="<?php echo e(asset('new/temp/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Style CSS -->
    <link href="<?php echo e(asset('new/temp/css/style.css')); ?>" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="<?php echo e(asset('new/temp/css/owl.carousel.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('new/temp/css/owl.theme.default.css')); ?>" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('new/temp/css/fontello.css')); ?>">
    <link href="<?php echo e(asset('new/temp/css/font-awesome.min.css')); ?>" rel="stylesheet')}}">


    <link href="<?php echo e(asset('new/temp/css/update.css')); ?>" rel="stylesheet')}}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    




    <style type="text/css">
        @font-face {
          font-family: 'Helvetica', 'Arial', sans-serif !important;
          src: url('<?php echo e(asset('font/HelveticaNeue/HelveticaNeue.otf')); ?>');
        }
        body {
            height: 100%;
            background-image: url('<?php echo e(asset('dca/images/background-blue.jpg')); ?>') !important;
            background-repeat: repeat-y;
            z-index: 2;
            background-size: 50% auto;
             font-family: 'Helvetica', 'Arial', sans-serif !important;
              font-color:#0155a2 !important;
        }

        


        @media  only screen and (max-width: 980px) {
            body {
            background-image: none!important;
            }

            .logo{
                line-height: 0px;
            }
        }

        .content {
            padding-top: 0px !important;
            padding-bottom: 80px;
        }

        .front-right{
            margin-top: 50px !important;
        }

        .front-left{
            margin-top: 180px !important;
            padding-right: 100px !important;
        }

        .consultantion-form{
            background-color: #ffffff !important;
            padding: 30px 70px;
        }
    </style>
</head>




	
<body>

	
 
    <?php echo $__env->yieldContent('content'); ?>

    <style type="text/css">
        .img-islam{
            height: 70px !important;
            width: 60px !important;
            float: left !important;
            margin-left: 40px !important;
        }

        .txt-color-black{
            margin-right: 40px !important;
            color: #ffffff !important;
        }

       

    </style>

        <div class="tiny-footer">
            
            <div class="row">
               
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="txt-color-black">Copyright © <?php echo date('Y'); ?> MBSB Bank Berhad</span><br>
                    <span class="txt-color-black">Registration No: 200501033981 (716122-P). All Rights Reserved.</span>
                </div>
            </div>
            
        </div>
    <!-- /.footer -->
    



<?php echo $__env->yieldPushContent('addjs'); ?>

</body>
</html><?php /**PATH C:\xampp\htdocs\dca\dca7\dca\resources\views/layouts/dca/template.blade.php ENDPATH**/ ?>