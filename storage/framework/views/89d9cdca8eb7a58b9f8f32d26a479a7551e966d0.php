 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --><script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
 <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>  
<script async src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.js"></script>

    <script src="<?php echo e(asset('new/temp/js/jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('asset/lib/valid/jquery.validate.js')); ?>"></script>
<script src="<?php echo e(asset('asset/lib/valid/additional-methods.js')); ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo e(asset('new/temp/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('new/temp/js/menumaker.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/jquery.sticky.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/sticky-header.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/owl.carousel.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('new/temp/js/multiple-carousel.js')); ?>"></script>
    <script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
    </script>

<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
        <!--================================================== -->

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo e(asset('/asset/js/plugin/pace/pace.min.js')); ?>"></script>

    

        <!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo e(asset('/asset/js/app.config.js')); ?>"></script>
        <script src="<?php echo e(asset('/asset/js/jquery.steps.js')); ?>"></script>
        <script src="<?php echo e(asset('/asset/js/jquery.steps.min.js')); ?>"></script>
            <script src="<?php echo e(asset('/asset/js/jquery.simulate.js')); ?>"></script>
        

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo e(asset('/asset/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js')); ?>"></script> 

        <!-- BOOTSTRAP JS -->
        <script src="<?php echo e(asset('/asset/js/bootstrap/bootstrap.min.js')); ?>"></script>

        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo e(asset('/asset/js/notification/SmartNotification.min.js')); ?>"></script>

        <!-- JARVIS WIDGETS -->
        <script src="<?php echo e(asset('/asset/js/smartwidgets/jarvis.widget.min.js')); ?>"></script>

        <!-- EASY PIE CHARTS -->
        <script src="<?php echo e(asset('/asset/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>

        <!-- SPARKLINES -->
        <script src="<?php echo e(asset('/asset/js/plugin/sparkline/jquery.sparkline.min.js')); ?>"></script>

        <!-- JQUERY VALIDATE -->
        <script src="<?php echo e(asset('/asset/js/plugin/jquery-validate/jquery.validate.min.js')); ?>"></script>

        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo e(asset('/asset/js/plugin/masked-input/jquery.maskedinput.min.js')); ?>"></script>

        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo e(asset('/asset/js/plugin/select2/select2.min.js')); ?>"></script>

        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo e(asset('/asset/js/plugin/bootstrap-slider/bootstrap-slider.min.js')); ?>"></script>

        <!-- browser msie issue fix -->
        <script src="<?php echo e(asset('/asset/js/plugin/msie-fix/jquery.mb.browser.min.js')); ?>"></script>

        <!-- FastClick: For mobile devices -->
        <script src="<?php echo e(asset('/asset/js/plugin/fastclick/fastclick.min.js')); ?>"></script>

        <!--[if IE 8]>
            <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
        <![endif]-->

        <!-- Demo purpose only -->
        <script src="<?php echo e(asset('/asset/js/demo.min.js')); ?>"></script>

        <!-- MAIN APP JS FILE -->
        <script src="<?php echo e(asset('/asset/js/app.min.js')); ?>"></script>      
        <script src="<?php echo e(asset('/asset/js/jssor.slider.min.js')); ?>"></script>     

        <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
        <!-- Voice command : plugin -->
        <script src="<?php echo e(asset('/asset/js/speech/voicecommand.min.js')); ?>"></script>  

      
        <script src="<?php echo e(asset('/asset/js/file/vendor/jquery.ui.widget.js')); ?>"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo e(asset('/asset/js/file/jquery.iframe-transport.js')); ?>"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo e(asset('/asset/js/file/jquery.fileupload.js')); ?>"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?php echo e(asset('/asset/js/bootbox.min.js')); ?>"></script>



        <script type="text/javascript">
            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            $(document).ready(function() {
                pageSetUp();
            })
        </script>



    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/lib/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/jquery.fancybox.js?v=2.1.5')); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/jquery.fancybox.css?v=2.1.5')); ?>" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')); ?>" />
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')); ?>"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')); ?>" />
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')); ?>"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="<?php echo e(asset('/asset/js/gambar/source/helpers/jquery.fancybox-media.js?v=1.0.6')); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

            /*
             *  Different effects
             */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title : {
                        type : 'outside'
                    },
                    overlay : {
                        speedOut : 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                helpers : {
                    title : {
                        type : 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS    : 'fancybox-custom',
                closeClick : true,

                openEffect : 'none',

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        css : {
                            'background' : 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect : 'elastic',
                openSpeed  : 150,

                closeEffect : 'elastic',
                closeSpeed  : 150,

                closeClick : true,

                helpers : {
                    overlay : null
                }
            });

            /*
             *  Button helper. Disable animations, hide close button, change title type and content
             */

            $('.fancybox-buttons').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    buttons : {}
                },

                afterLoad : function() {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
             *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
             */

            $('.fancybox-thumbs').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,
                arrows    : false,
                nextClick : true,

                helpers : {
                    thumbs : {
                        width  : 50,
                        height : 50
                    }
                }
            });

            /*
             *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect : 'none',
                    closeEffect : 'none',
                    prevEffect : 'none',
                    nextEffect : 'none',

                    arrows : false,
                    helpers : {
                        media : {},
                        buttons : {}
                    }
                });

            /*
             *  Open manually
             */

            $("#fancybox-manual-a").click(function() {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function() {
                $.fancybox.open({
                    href : 'iframe.html',
                    type : 'iframe',
                    padding : 5
                });
            });

            $("#fancybox-manual-c").click(function() {
                $.fancybox.open([
                    {
                        href : '1_b.jpg',
                        title : 'My title'
                    }, {
                        href : '2_b.jpg',
                        title : '2nd title'
                    }, {
                        href : '3_b.jpg'
                    }
                ], {
                    helpers : {
                        thumbs : {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });


        });
    </script>


<script>
$(document).ready(function(){
    $("#login").click(function(){
     
        $('#myModalLogin').modal('show');
       
    });
});
</script>





<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $("#smart-form-register2").hide();
        $("#smart-form-register").validate({
          // Rules for form validation
        rules : {
            FullName: {
                required : true,
                maxlength:100
            },
            ICNumber : {
                required : true,
                minlength:12,
                maxlength:12,
                pattern:/^\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])\d{6}$/
            },
            PhoneNumber: {
                required: true,
                maxlength:14,
                minlength:9,
                pattern:/^(01)[0-46-9]*[0-9]{7,8}$/
            },
            Deduction: {
                required: true
            },
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true,
                min:1,
            },
            Employer: {
                required: true,
                maxlength:20,
            },
            BasicSalary: {
                required: true
                // min:3000          
            },
            LoanAmount: {
                required: true,
                minlength:4
            },
            majikan: {
                required: true,
                maxlength: 60
            },
        },

    // Messages for form validation
        messages : {
            FullName: {
                required : 'Please enter your full name'
            },
            ICNumber: {
                required: 'Please enter your New IC Number',
                minlength: "Please enter 12 digits of your New IC Number",
                pattern:'Please enter a valid New IC Number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number',
                minlength: "Please enter at least {0} digits",
                pattern:'Please enter a valid Phone Number',
                maxlength: "Please enter maximal {0} digits"
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
           
            Employment: {
                required: 'Please select employement type',
                min: 'Please select employement type'
            },
            Employer: {
                required: 'Please select employer'
            },
             majikan: {
                required: 'Please enter employer'
            },
            BasicSalary: {
                required: 'Please enter your basic salary'
               //  min: 'Minimum Basic Salary is RM 3000'
            },
            LoanAmount: {
                required: 'Please select your financing amount',
                minlength:'Please enter valid financing amount'
            },
            Package: {
                required: 'Please select Employment first'
            }

          }
        });
    });
</script>


<script type="text/javascript">
       
$( "#Employment" ).change(function() {
    var package = $('#Employment').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/package/"+package,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#package").val(data[k].package.name );
                          $("#package_id").val(data[k].package.id );
                    });

                }
            });

   
});

</script>

<script>
    function isNumberKey(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
            if (key.length == 0) return;
                var regex = /^[0-9.,\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    function minmax(value, min, max) 
        {
            if(parseInt(value) < min || isNaN(value)) 
                return 2; 
            else if(parseInt(value) > max) 
                return max; 
            else return value;
        }
</script>

<script type="text/javascript">

    $(document).ready(function() {
    var found = [];
        $("#Employment option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
     });
</script>


<script type="text/javascript">
  $( "#BasicSalary" ).blur(function() {
     if (document.getElementById('BasicSalary').value == '') 
        { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0 }
    else{
    this.value = parseFloat(this.value).toFixed(2);
    }
});
</script>
<script type="text/javascript">

 
</script>
<script type="text/javascript">
  $( "#Allowance" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#Deduction" ).blur(function() {
     if (document.getElementById('Deduction').value == '') 
        { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0 }
    else{
    this.value = parseFloat(this.value).toFixed(2);
    }
});
</script>
<script type="text/javascript">
  $( "#LoanAmount" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(0);
});
</script>
<script language="javascript">
    function getkey(e)
    {
        if (window.event)
            return window.event.keyCode;
        else if (e)
            return e.which;
        else
            return null;
    }
    function goodchars(e, goods, field)
    {
        var key, keychar;
        key = getkey(e);
        if (key == null) return true;
 
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        goods = goods.toLowerCase();
 
        // check goodkeys
        if (goods.indexOf(keychar) != -1)
            return true;
        // control keys
        if ( key==null || key==0 || key==8 || key==9 || key==27 )
            return true;
        if (key == 13) 
            {
                var i;
                for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                break;
                i = (i + 1) % field.form.elements.length;
                field.form.elements[i].focus();
                return false;
            };
        // else return false
            return false;
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#inputTextBox").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){
            event.preventDefault();
        }
    });
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#FullName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });

});
</script>

<script type="text/javascript">
      $(function () {
        $("#majikantext").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
 
            $("#lblError").html("");
 
            //Regex for Valid Characters i.e. Alphabets and Numbers.
            var regex = /^[a-zA-Z z0-9 ]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets and Numbers allowed.");
            }
 
            return isValid;
        });
    });
</script>





<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    
<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
    var tenure = $('input[name=tenure]:checked').val();
    var terms = $('input[name=terms]:checked').val();
    if(tenure>0){
        if(terms>0){
            $('#myModal').modal('show');
        }
        else{
             bootbox.alert('Please check consent box if you want to proceed');
        }
    }else{
         bootbox.alert('Please Select Tenure');
    }
   
    });
});
</script>


<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register1").validate({

            // Rules for form validation
           rules : {
                
                email : {
                    required : true,
                    validate_email : true
                },
                password:{
                  required:true
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    validate_email : 'Please enter a VALID email address'
                },
                password : {
                    required : 'Please enter your password'
                }
                    }
        });

    });
</script><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/praapplication/upload_js.blade.php ENDPATH**/ ?>