<?php $__env->startSection('content'); ?>
<style type="text/css">
    .img-mbsb{
        margin-bottom: 50px !important;
        width: 300px !important;
        height: 100px !important;

        display: block;
      margin-left: auto;
      margin-right: auto;
      width: 50%;
    }
    .left{
        color: #ffffff !important;     
        z-index: 1;
    }
    .content {
        padding-top: 0px !important;
        padding-bottom: 80px;
    }
    .front-right{
        margin-top: 24px !important;

    }
    .front-left{
        margin-top: 180px !important;
        padding-right: 100px !important;
    }
</style>

<?php echo $__env->make('layouts.dca.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
 <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>  
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                        <div class="front-left">
                            <h2 class="mb40">Welcome to MBSB Bank Online Application Form</h2>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=">Share on Facebook</a>
                            <?php echo $__env->make('layouts.dca.left_front', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 front-right">
                        <div class="consultantion-form">
                            <img src="https://mbsb.insko.my/public/asset/img/logo_bank.png" class="img-mbsb" >
                            <h2 class="mb30">Please Fill In Your Personal Details</h2>
                        <?php echo Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]); ?>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Full Name <sup>*</sup></label>
                                <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName"  <?php if(Session::has('fullname')): ?>  value="<?php echo e(Session::get('fullname')); ?>" <?php endif; ?> size="55" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name"> New IC Number <sup>*</sup></label>
                                <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12"   
                                <?php if(Session::has("icnumber")): ?>  value="<?php echo e(Session::get('icnumber')); ?>" <?php endif; ?> class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Handphone Number <sup>*</sup></label>
                                 <input type="number" id="PhoneNumber" name="PhoneNumber" minlength="7" class="form-control fn" step=".01" maxlength="12"  onkeypress="return isNumberKey(event)"  <?php if(Session::has('phone')): ?>  value="<?php echo e(Session::get('phone')); ?>" <?php endif; ?>>
                            </div>
                        </div>
                       
                        <div class="col-md-12 wrap-form-control validate-input" data-validate = "IC Number">
                            <div class="form-group">
                                <label class="control-label" for="name">Employment <sup>*</sup></label>
                                <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                    <option value="0">--Select Employment--</option>
                                    <?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select> 
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Employer <sup>*</sup></label>
                                <input class="form-control" type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"   <?php if(Session::has('majikan')): ?>  value="<?php echo e(Session::get('majikan')); ?>" <?php endif; ?> maxlength="100">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Monthly Income (Basic Salary + Allowance + Other income) (RM) <sup>*</sup></label>
                               <input id="BasicSalary" value="" name="BasicSalary" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('basicsalary')): ?>  value="<?php echo e(Session::get('basicsalary')); ?>" <?php endif; ?> >
                            </div>
                        </div>

                        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Allowance (RM) <sup>*</sup></label>
                                <input class="form-control" <?php if(Session::has('allowance')): ?>  value="<?php echo e(Session::get('allowance')); ?>" <?php endif; ?>  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106)|| (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                            </div>
                        </div>-->

                         <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Total Deduction (RM) <sup>*</sup></label>
                                <input id="Deduction" value="" name="Deduction" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('deduction')): ?>  value="<?php echo e(Session::get('deduction')); ?>" <?php endif; ?> >
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Package <sup>*</sup></label>
                                <input class="form-control" type="text" name="package" id="package" <?php if(Session::has('package_name')): ?>  value="<?php echo e(Session::get('package_name')); ?>" <?php endif; ?>  readonly>
                                <input type="hidden" name="package_id" id="package_id" <?php if(Session::has('package_id')): ?>  value="<?php echo e(Session::get('package_id')); ?>" <?php else: ?> value="1" <?php endif; ?> class="form-control" readonly>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 ">
                            <div class="form-group">
                                <label class="control-label" for="name">Financing Amount(RM) <sup>*</sup></label>
                               <input id="LoanAmount" name="LoanAmount" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('loanAmount')): ?>  value="<?php echo e(Session::get('loanAmount')); ?>" <?php endif; ?> >
                            </div>
                        </div> 
                          <div class="col-lg-6 col-md-6">
                            <button type="reset" name="singlebutton" class="btn btn-danger  btn-lg ">Reset</button>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            
                            <button type="submit" name="singlebutton" class="btn btn-primary  btn-lg ">Calculate</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
 <?php echo $__env->make('praapplication.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
 <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dca.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/praapplication/privatesector.blade.php ENDPATH**/ ?>