<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Front Agent";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
  <script type="text/javascript" src=""></script>
 <!-- jQuery 2.1.3 -->
<script src="<?php echo e(asset ("/bin/push.min.js")); ?>"></script>

<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	   <section id="widget-grid" class="">
            <div class="row">
                 <?php echo $__env->make('shared.fa_statistic', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </section>
						
						
		<section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="true">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application List</h2>
                        </header>
                            <?php echo Form::open(['url' => 'admin/check-waps', 'enctype' => 'multipart/form-data']); ?>

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <button type="submit" name="submit" class="btn btn-primary">
                                Submit
                                </button>
                           <?php echo Form::close(); ?>    

                        <!-- widget div-->
                        <div>
                            <div class="jarviswidget-editbox"></div>
                                <div class="widget-body">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>                         
                                            <tr>
                                                <th>No.</th>
                                                <th class="hidden-xs">IC Number</th>
                                                <th>Name</th>
                                                <th class="hidden-xs">Phone</th>
                                                <th>Submit Date</th>
                                                <th>Status</th>
                                                <th>TAT</th>
                                                <th class="hidden-xs">History</th>
                                                <th>Action</th>
                                                <th class="hidden-xs">Download</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; ?>
                                             <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($i); ?></td>
                                                <td class="hidden-xs"><?php echo e($term->Basic->new_ic); ?></td>
                                                <td><?php $name = strtoupper($term->Basic->name ); ?><?php echo e($name); ?></td>
                                                <td class="hidden-xs"><?php echo e($term->PraApplication->phone); ?></td>
                                                <td><?php echo e($term->file_created); ?></td>
                                                <td><div align='center'>
                                                    <?php if($term->status==3): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                    <?php elseif(($term->status==4) && ($term->fill_by==0)): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'><b>Customer Ready Fill Up Form</b></span>
                                                    <?php elseif(($term->status==4) && ($term->fill_by==1)): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'><b>Fill Up Form</b></span>
                                                     <?php elseif($term->status==5): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'><b>Pending Verification</b></span>
                                                    <?php elseif($term->status==99): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'><b>Documents Rejected</b></span>
                                                    <?php elseif($term->verification_result ==1): ?>                                                
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-success'>Ready for 2nd Verification</span>
                                                    <?php elseif($term->status ==6): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-primary'>Submitted to Processor/WAPS</span>
                                                            <br>
                                                            <?php if($term->id_branch=='0'): ?>
                                                                <div align='center'> - </div> 
                                                            <?php else: ?>                                            
                                                                <i><?php echo e($term->Branch->branchname); ?></i>
                                                            <?php endif; ?> 
                                                            <br>
                                                            <?php if($term->financial->l3_jumlah > 0): ?>
                                                                <font color="red"><i>Overlapping Case</i></font>
                                                            <?php endif; ?>
                                                    <?php elseif($term->status ==8): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-danger'>Rejected</span>
                                                    <?php elseif($term->status ==7): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-default'>Route Back to User</span>
                                                    <?php endif; ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div align='center'>
                                                     <?php if($term->status>=3): ?>
                                                        <?php
                                                            //if($term->status==3) {
                                                                $key_in = strtotime($term->updated_at);
                                                                $sekarang      = time(); // Waktu sekarang
                                                                $diff          = $sekarang - $key_in;
                                                                $hari          = floor($diff / (60 * 60 * 24));
                                                                
                                                            //}
                                                        ?>
                                                        <?php if($hari >= 5): ?>
                                                             <span data-toggle="tooltip" title=""  class='label label-danger'><?php echo e($hari); ?> day(s)</span>
                                                        <?php else: ?>
                                                            <span data-toggle="tooltip" title=""  class='label label-warning'><?php echo e($hari); ?> day(s)</span>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    </div>  
                                                </td>
                                                <td class="hidden-xs">
                                                    <a href="JavaScript:newPopup('<?php echo e(url('/')); ?>/activities/<?php echo e($term->id_praapplication); ?>');" class='btn btn-sm btn-primary' ><i class='fa fa-history'></i>  History</a>
                                                </td>
                                                <td align='center'>
                                                    <?php if($term->status==5): ?>
                                                        <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-danger fa fa-book' >Verify Form</a>
                                                       <?php endif; ?>
                                                    <?php if($term->status==3): ?>
                                                        <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-warning' ><i class='fa fa-file'></i> Verify Docs</a>
                                                    <?php endif; ?>
                                                    <?php if(($term->status==4) && ($term->fill_by==1)): ?>
                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_check_dsrs')): ?>
                                                             <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-primary' ><i class='fa fa-file'></i> 
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if(($term->status>=4)): ?>
                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_check_dsrs')): ?>
                                                        <a href="<?php echo e(url('/admin/step1_view/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if(($term->status==6)||($term->status==8)): ?>
                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_forms')): ?>
                                                        <a href="<?php echo e(url('/admin/user_detail_view/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    
                                                    <?php if($term->edit=='1'): ?>
                                                        <?php if($term->verification_result !=3): ?>
                                                            <?php if($term->verification_result !=2): ?>
                                                                <a href="<?php echo e(url('/')); ?>/form/approveedit/<?php echo e($term->id_praapplication); ?>" 
                                                                onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 
                                                    <?php endif; ?>

                                                    <?php if($term->status=='7'): ?>
                                                        <div class="modal fade" id="amyModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="amyModal<?php echo e($i); ?>" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                            &times;
                                                                        </button>
                                                                        <h4 class="modal-title" id="addBranch">Route Back Application to User</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php echo Form::open(['url' => 'form/routeback','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                                                            <fieldset>
                                                                                <section >
                                                                                    <label class="label">Customer Name</label>
                                                                                    <label class="input">
                                                                                    <input type='text' value='<?php echo e($term->Basic->name); ?>' readonly disabled/>
                                                                                    <b class="tooltip tooltip-bottom-right">Customer Name</b>
                                                                                    </label><br>
                                                                                </section>
                                                                                <section>
                                                                                    <label class="label">IC Number</label>
                                                                                    <label class="input">
                                                                                        <input type='text' value='<?php echo e($term->Basic->new_ic); ?>' readonly disabled/>
                                                                                    <b class="tooltip tooltip-bottom-right">IC Number</b>
                                                                                    </label><br>
                                                                                </section>
                                                                                <section>
                                                                                    <label class="label">Reason</label>
                                                                                    <label class="input">
                                                                                        <textarea id='reason' name='reason' class='form-control' rows="4" cols="73"></textarea>
                                                                                    <b class="tooltip tooltip-bottom-right">Reason</b>
                                                                                    </label><br>
                                                                                </section>
                                                                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                                <input type="hidden" name="id_praapplication" value="<?php echo e($term->id_praapplication); ?>">
                                                                                <input type="hidden" name="cus_ic" value="<?php echo e($term->Basic->new_ic); ?>">
                                                                                <input type="hidden" name="cus_name" value="<?php echo e($term->Basic->name); ?>">
                                                                                <input type="hidden" name="cus_email" value="<?php echo e($term->PraApplication->email); ?>">
                                                                            </fieldset>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                                    Cancel
                                                                                </button>
                                                                                <button type="submit" name="submit" class="btn btn-lg txt-color-darken">
                                                                                    Submit
                                                                                </button> 
                                                                            </div>
                                                                        <?php echo Form::close(); ?>  
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                            </div><!-- /.modal -->
                                                        </div>
                                                    <?php endif; ?>  
                                                </td>
                                                <td class="hidden-xs"> 
                                                 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_zips')): ?>
                                                    <a href="<?php echo e(url('/')); ?>/admin/downloadzip/<?php echo e($term->id_praapplication); ?>"
                                                    ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                    <?php endif; ?>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_application_forms')): ?>
                                                        <?php if(($term->status==6)||($term->status==8)): ?>
                                                            <a href="<?php echo e(url('/')); ?>/admin/downloadpdf/<?php echo e($term->id_praapplication); ?>"><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a>
                                                        <?php endif; ?>
                                                    <?php endif; ?> 
                                                    <?php if($term->status>=3): ?>
                                                        <a href="<?php echo e(url('/')); ?>/admin/pds/<?php echo e($term->id_praapplication); ?>"><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img>Download PDS</a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </article>
            </div>
        </section>
    </div>
</div>


<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            /* END BASIC */
            
            /* COLUMN SHOW - HIDE */
            $('#datatable_col_reorder').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }           
        });
            /* END COLUMN SHOW - HIDE */
    })
</script>
          
<?php 
    //include required scripts
    include("asset/inc/scripts_notoggle.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;
	
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};
	$('#dt_basic').dataTable({
		  
			"scrollX": true,
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_dt_basic) {
				responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_dt_basic.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_dt_basic.respond();
		}
	});	
</script>
<!--<script>
    $(document).ready(function() {
    // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
        }, 800);
     
    });
</script>-->

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=600,width=1000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>



<script src="//code.jquery.com/jquery-1.12.1.min.js"></script>
          
<script src="<?php echo e(asset('asset/lib/jquery.userTimeout.js')); ?> "></script>
<script type="text/javascript">
    $(document).userTimeout({
        session: 3000
    });
</script>
<script type="text/javascript">
    $(document).userTimeout({

  // ULR to redirect to, to log user out
  logouturl: null,              

  // URL Referer - false, auto or a passed URL     
  referer: false,            

  // Name of the passed referal in the URL
  refererName: 'refer',        

  // Toggle for notification of session ending
  notify: true,                      

  // Toggle for enabling the countdown timer
  timer: true,             

  // 10 Minutes in Milliseconds, then notification of logout
  session: 100,                   

  // 5 Minutes in Milliseconds, then logout
  force: 3000,       

  // Model Dialog selector (auto, bootstrap, jqueryui)              
  ui: 'auto',                        

  // Shows alerts
  debug: false,            

  // <a href="https://www.jqueryscript.net/tags.php?/Modal/">Modal</a> Title
  modalTitle: 'Session Timeout',     

  // Modal Body
  modalBody: 'You\'re being timed out due to inactivity. Please choose to stay signed in or to logoff. Otherwise, you will logged off automatically.',

  // Modal log off button text
  modalLogOffBtn: 'Log Off',  

  // Modal stay logged in button text        
  modalStayLoggedBtn: 'Stay Logged In'  
    
});
</script>

<?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/mo/index.blade.php ENDPATH**/ ?>