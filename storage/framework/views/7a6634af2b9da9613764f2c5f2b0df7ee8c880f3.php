<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Registered Customer Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");

include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            
                           

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                              
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
									 <?php echo Form::open(['url' => 'report/registered','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                              <fieldset>
													<section>
                                                          <label class="label">Branch</label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                  <select name="branch" id="branch" class="form-control" >
                                    <option disabled="" value="0" selected="">--- SELECT BRANCH ---</option> 
                                    <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     
                                    
                                      <option  value="<?php echo e($branch->branch_code); ?>"><?php echo e($branch->branchname); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section>
                                                        <section>
                                                          <label class="label">FA</label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select id="fa" class="select2" name="fa" required="">
                                                     <option   selected="selected" value="0">Select FA</option>
                                                  <?php $__currentLoopData = $fa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($data->mo_id); ?>" ><?php echo e($data->name); ?> </option>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>

                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section>
                                                         
                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                              
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Generate
                                                            </button>
                                                           
                                                   <?php echo Form::close(); ?> 
																					   
										
													
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>


          
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="branch"]').on('change', function() {
          
            var stateID = $(this).val();
           
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/select_fa/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        $('select[name="fa"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="fa"]').append('<option value="'+ key +'">'+ value +'</option>');
                          //  $("#id_branch").val(data[k].state.state_code );
                        });


                    }
                });
            }else{
                $('select[name="fa"]').empty();
            }
        });
    });
</script><?php /**PATH C:\xampp\htdocs\dca_uat\dca\resources\views/dca_admin/assign.blade.php ENDPATH**/ ?>