<!-- ==========================CONTENT ENDS HERE ========================== -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>  
<script async src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.js"></script>

<script src="<?php echo e(asset('asset/lib/valid/jquery.validate.js')); ?>"></script>
<script src="<?php echo e(asset('asset/lib/valid/additional-methods.js')); ?>"></script>

<script src="<?php echo e(asset('asset/lib/valid/jquery.min.js')); ?>"></script>


<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>

<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $("#smart-form-register2").hide();
        $("#smart-form-register").validate({
          // Rules for form validation
        rules : {
            FullName: {
                required : true,
                maxlength:100
            },
            ICNumber : {
                required : true,
                minlength:12,
                maxlength:12,
                pattern:/^\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])\d{6}$/
            },
            PhoneNumber: {
                required: true,
                maxlength:14,
                minlength:9,
                pattern:/^(01)[0-46-9]*[0-9]{7,8}$/
            },
            Deduction: {
                required: true
            },
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true,
                min:1,
            },
            Employer: {
                required: true,
                maxlength:20,
            },
            BasicSalary: {
                required: true
                // min:3000          
            },
            LoanAmount: {
                required: true,
                minlength:4
            },
            majikan: {
                required: true,
                maxlength: 60
            },
        },

    // Messages for form validation
        messages : {
            FullName: {
                required : 'Please enter your full name'
            },
            ICNumber: {
                required: 'Please enter your New IC Number',
                minlength: "Please enter 12 digits of your New IC Number",
                pattern:'Please enter a valid New IC Number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number',
                minlength: "Please enter at least {0} digits",
                pattern:'Please enter a valid Phone Number',
                maxlength: "Please enter maximal {0} digits"
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
           
            Employment: {
                required: 'Please select employement type',
                min: 'Please select employement type'
            },
            Employer: {
                required: 'Please select employer'
            },
             majikan: {
                required: 'Please enter employer'
            },
            BasicSalary: {
                required: 'Please enter your basic salary'
               //  min: 'Minimum Basic Salary is RM 3000'
            },
            LoanAmount: {
                required: 'Please select your financing amount',
                minlength:'Please enter valid financing amount'
            },
            Package: {
                required: 'Please select Employment first'
            }

          }
        });
    });
</script>


<script type="text/javascript">
       
$( "#Employment" ).change(function() {
    var package = $('#Employment').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/package/"+package,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#package").val(data[k].package.name );
                          $("#package_id").val(data[k].package.id );
                    });

                }
            });

   
});

</script>

<script>
    function isNumberKey(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
            if (key.length == 0) return;
                var regex = /^[0-9.,\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    function minmax(value, min, max) 
        {
            if(parseInt(value) < min || isNaN(value)) 
                return 2; 
            else if(parseInt(value) > max) 
                return max; 
            else return value;
        }
</script>

<script type="text/javascript">

    $(document).ready(function() {
    var found = [];
        $("#Employment option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
     });
</script>

<script type="text/javascript">

    function getSelectedText(elementId) {
        var elt = document.getElementById(elementId);

        if (elt.selectedIndex == -1)
            return null;
        return elt.options[elt.selectedIndex].text;
    }

    var Employment = getSelectedText('Employment');
    var Employer = getSelectedText('Employer');
    $("#Employment2").val(Employment);
    $("#Employer2").val(Employer); 
</script>
<script type="text/javascript">
  $( "#BasicSalary" ).blur(function() {
    this.value = parseFloat(this.value).toFixed();
});
</script>
<script type="text/javascript">
  $( "#Allowance" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(0);
});
</script>
<script type="text/javascript">
  $( "#Deduction" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(0);
});
</script>
<script type="text/javascript">
  $( "#LoanAmount" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(0);
});
</script>
<script language="javascript">
    function getkey(e)
    {
        if (window.event)
            return window.event.keyCode;
        else if (e)
            return e.which;
        else
            return null;
    }
    function goodchars(e, goods, field)
    {
        var key, keychar;
        key = getkey(e);
        if (key == null) return true;
 
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        goods = goods.toLowerCase();
 
        // check goodkeys
        if (goods.indexOf(keychar) != -1)
            return true;
        // control keys
        if ( key==null || key==0 || key==8 || key==9 || key==27 )
            return true;
        if (key == 13) 
            {
                var i;
                for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                break;
                i = (i + 1) % field.form.elements.length;
                field.form.elements[i].focus();
                return false;
            };
        // else return false
            return false;
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#inputTextBox").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){
            event.preventDefault();
        }
    });
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#FullName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });

});
</script>

<script type="text/javascript">
      $(function () {
        $("#majikantext").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
 
            $("#lblError").html("");
 
            //Regex for Valid Characters i.e. Alphabets and Numbers.
            var regex = /^[a-zA-Z z0-9 ]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets and Numbers allowed.");
            }
 
            return isValid;
        });
    });
</script>

<?php /**PATH C:\xampp\htdocs\dca\dca5\dca\resources\views/layouts/js.blade.php ENDPATH**/ ?>