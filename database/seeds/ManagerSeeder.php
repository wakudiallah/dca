<?php

use Illuminate\Database\Seeder;
use App\Manager;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
      public function run()
    {
       DB::unprepared('SET IDENTITY_INSERT managers ON');

  
	       Manager::create( [
	        'id'=>'1',
	        'user_id'=>'32489e613a1641e899cdefefeef',
	        'name'=>'DCA Admin 1',
	        ] );

		 Manager::create( [
		        'id'=>'2',
		        'user_id'=>'32489e613a164sssssdefefeef',
		          'name'=>'DCA Admin 2'
		        ] );

		 Manager::create( [
		        'id'=>'3',
		        'user_id'=>'32489e61ddddddssdefefeef',
		     'name'=>'DCA Admin 3'
		        ] );

            DB::unprepared('SET IDENTITY_INSERT managers OFF');
    }
}
