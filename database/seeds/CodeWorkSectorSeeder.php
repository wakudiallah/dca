<?php

use Illuminate\Database\Seeder;
use App\Model\CodeWorkSec;

class CodeWorkSectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT codeworkseccode ON');

    		CodeWorkSec::create( [
			'id'=>1,
			'workseccode'=>'G',
			'worksecdesc'=>'GOVERNMENT',
			
			] );


						
			CodeWorkSec::create( [
			'id'=>2,
			'workseccode'=>'P',
			'worksecdesc'=>'PRIVATE',
			
			] );


						
			CodeWorkSec::create( [
			'id'=>3,
			'workseccode'=>'S',
			'worksecdesc'=>'SELF-EMPLOYED',
			
			] );


       
		DB::unprepared('SET IDENTITY_INSERT codeworkseccode OFF');
    }
}
