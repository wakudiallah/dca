<?php

use Illuminate\Database\Seeder;
use App\Model\Package;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT packages ON');

    	Package::create( [
            'id'=>1,
            'name'=>'MUMTAZ-I',
            'flat_rate'=>2.93,
            'effective_rate'=>5.55,
            'min_financing'=>1000,
            'max_financing'=>250000,
            'max_tenure'=>10,
            'min_salary'=>3000,
            'min_age'=>19,
            'max_age'=>60,
            'description'=>NULL,
            'create_by'=>'43e89e2475f64344a9d35f4b972c22a2',
            'status'=>1
            ] );
            			
            Package::create( [
            'id'=>2,
            'name'=>'AFDHAL-I',
            'flat_rate'=>3.58,
            'effective_rate'=>6.75,
            'min_financing'=>1000,
            'max_financing'=>400000,
            'max_tenure'=>10,
            'min_salary'=>3000,
            'min_age'=>19,
            'max_age'=>60,
            'description'=>NULL,
            'create_by'=>'233520dba131407b834765e03235c7cb',
            'status'=>1
            ] );
            			
            Package::create( [
            'id'=>3,
            'name'=>'PRIVATE SECTOR PF-I',
            'flat_rate'=>6.15,
            'effective_rate'=>11.35,
            'min_financing'=>50000,
            'max_financing'=>300000,
            'max_tenure'=>10,
            'min_salary'=>3500,
            'min_age'=>21,
            'max_age'=>60,
            'description'=>NULL,
            'create_by'=>'233520dba131407b834765e03235c7cb',
            'status'=>1
            ] );
       
			DB::unprepared('SET IDENTITY_INSERT packages OFF');
    }
}
