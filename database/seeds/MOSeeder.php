<?php

use Illuminate\Database\Seeder;
use App\MO;

class MOSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::unprepared('SET IDENTITY_INSERT mo ON');

      MO::create( [
        'id'=>'21',
        'id_mo'=>'32489e613a1641e899cca83ce24298ef',
        'name'=>'rozita',
        'email'=>'admin@netxpert.com.my',
        'id_manager'=>'32489e613a1641e899cdefefeef',
        'status'=>'1'
        ] );


       MO::create( [
        'id'=>'11',
        'id_mo'=>'32489e613a1641e899cca83ce2429821',
        'name'=>'FA 11',
        'email'=>'fa11@mbsb.insko.my',
        'id_manager'=>'32489e613a1641e899cdefefeef',
        'status'=>'1'
        ] );

		 MO::create( [
		        'id'=>'12',
		        'id_mo'=>'32489e613a1641e899cca83ce2429822',
		        'name'=>'FA 12',
		        'email'=>'fa12@mbsb.insko.my',
		        'id_manager'=>'32489e613a1641e899cdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'13',
		        'id_mo'=>'32489e613a1641e899cca83ce2429823',
		        'name'=>'FA 13',
		        'email'=>'fa13@mbsb.insko.my',
		        'id_manager'=>'32489e613a1641e899cdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'14',
		        'id_mo'=>'32489e613a1641e899cca83ce2429824',
		        'name'=>'FA 14',
		        'email'=>'fa14@mbsb.insko.my',
		        'id_manager'=>'32489e613a1641e899cdefefeef',
        'status'=>'1'
		        ] );

		MO::create( [
		        'id'=>'15',
		        'id_mo'=>'32489e613a1641e899cca83ce2429825',
		        'name'=>'FA 15',
		        'email'=>'fa15@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		         MO::create( [
		        'id'=>'16',
		        'id_mo'=>'32489e613a1641e899cca83ce2429826',
		        'name'=>'FA 26',
		        'email'=>'fa26@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'17',
		        'id_mo'=>'32489e613a1641e899cca83ce2429827',
		        'name'=>'FA 27',
		        'email'=>'fa7@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'18',
		        'id_mo'=>'32489e613a1641e899cca83ce2429828',
		        'name'=>'FA 18',
		        'email'=>'fa18@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'19',
		        'id_mo'=>'32489e613a1641e899cca83ce2429829',
		        'name'=>'FA 19',
		        'email'=>'fa19@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		MO::create( [
		        'id'=>'20',
		        'id_mo'=>'32489e613a1641e899cca83ce2429830',
		        'name'=>'FA 20',
		        'email'=>'fa20@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'1',
		        'id_mo'=>'32489e613a1641e899cca83ce2429811',
		        'name'=>'FA 1',
		        'email'=>'fa1@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'2',
		        'id_mo'=>'32489e613a1641e899cca83ce2429812',
		        'name'=>'FA 2',
		        'email'=>'fa2@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'3',
		        'id_mo'=>'32489e613a1641e899cca83ce2429813',
		        'name'=>'FA 3',
		        'email'=>'fa3@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'4',
		        'id_mo'=>'32489e613a1641e899cca83ce2429814',
		        'name'=>'FA 4',
		        'email'=>'fa4@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		MO::create( [
		        'id'=>'5',
		        'id_mo'=>'32489e613a1641e899cca83ce2429815',
		        'name'=>'FA 5',
		        'email'=>'fa5@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		         MO::create( [
		        'id'=>'6',
		        'id_mo'=>'32489e613a1641e899cca83ce2429816',
		        'name'=>'FA 6',
		        'email'=>'fa6@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'7',
		        'id_mo'=>'32489e613a1641e899cca83ce2429817',
		        'name'=>'FA 7',
		        'email'=>'fa7@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'8',
		        'id_mo'=>'32489e613a1641e899cca83ce2429818',
		        'name'=>'FA 8',
		        'email'=>'fa8@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		 MO::create( [
		        'id'=>'9',
		        'id_mo'=>'32489e613a1641e899cca83ce2429819',
		        'name'=>'FA 9',
		        'email'=>'fa9@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

		MO::create( [
		        'id'=>'10',
		        'id_mo'=>'32489e613a1641e899cca83ce2429810',
		        'name'=>'FA 10',
		        'email'=>'fa10@mbsb.insko.my',
		        'id_manager'=>'32489e613a164sssssdefefeef',
        'status'=>'1'
		        ] );

            DB::unprepared('SET IDENTITY_INSERT mo OFF');
    }
}