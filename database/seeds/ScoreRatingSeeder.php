<?php

use Illuminate\Database\Seeder;
use App\ScoreRating;

class ScoreRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT scoreratings ON');

      ScoreRating::create( [
'id'=>1,
'sector'=>'G',
'variable'=>'DSR',
'value'=>'',
'float_value'=>0.0000,
'float_value2'=>60.0000,
'score'=>'75',

] );


			
ScoreRating::create( [
'id'=>2,
'sector'=>'G',
'variable'=>'DSR',
'value'=>'',
'float_value'=>60.0001,
'float_value2'=>70.0000,
'score'=>'50',

] );


			
ScoreRating::create( [
'id'=>3,
'sector'=>'G',
'variable'=>'DSR',
'value'=>'',
'float_value'=>70.0001,
'float_value2'=>85.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>4,
'sector'=>'G',
'variable'=>'DSR',
'value'=>'',
'float_value'=>85.0001,
'float_value2'=>999.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>5,
'sector'=>'G',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>0.0000,
'float_value2'=>499.9900,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>6,
'sector'=>'G',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>500.0000,
'float_value2'=>1000.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>7,
'sector'=>'G',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>1000.0100,
'float_value2'=>1000000.0000,
'score'=>'40',

] );


			
ScoreRating::create( [
'id'=>8,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Non Civil Servant: New Customer - No Salary Deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>9,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Non Civil Servant: New customer - Salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>10,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Non Civil Servant: Existing customer - No salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>11,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Non Civil Servant: Existing customer - Salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>12,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: New Customer - Non Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>13,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: New Customer - Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>14,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: Existing Customer - Non Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>15,
'sector'=>'G',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: Existing Customer - Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'45',

] );


			
ScoreRating::create( [
'id'=>16,
'sector'=>'G',
'variable'=>'Age',
'value'=>'18-30',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>17,
'sector'=>'G',
'variable'=>'Age',
'value'=>'31-40',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>18,
'sector'=>'G',
'variable'=>'Age',
'value'=>'41-50',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>19,
'sector'=>'G',
'variable'=>'Age',
'value'=>'51-60',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>20,
'sector'=>'G',
'variable'=>'Education Level',
'value'=>'Secondary & below',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>21,
'sector'=>'G',
'variable'=>'Education Level',
'value'=>'Diploma',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>22,
'sector'=>'G',
'variable'=>'Education Level',
'value'=>'Degree',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>23,
'sector'=>'G',
'variable'=>'Education Level',
'value'=>'Master & above / Professional Qualification',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>24,
'sector'=>'G',
'variable'=>'Employment',
'value'=>'Self Employed / Partnership / Sole Prop',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>25,
'sector'=>'G',
'variable'=>'Employment',
'value'=>'Salaried Non-Civil Servant/ PLC/ GLC',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>26,
'sector'=>'G',
'variable'=>'Employment',
'value'=>'Salaried Civil Servant',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'30',

] );


			
ScoreRating::create( [
'id'=>27,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Non Civil Servant: Clerical /Retired/ Others',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>28,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Non Civil Servant: Officer / Sales',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>29,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Non Civil Servant: Manager and above',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>30,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Civil Servant: Clerical',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>31,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Civil Servant: Officer',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>32,
'sector'=>'G',
'variable'=>'Position',
'value'=>'Civil Servant: Manager and above',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'40',

] );


			
ScoreRating::create( [
'id'=>33,
'sector'=>'G',
'variable'=>'Marital Status',
'value'=>'Single / Divorced / Widow / Widower',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>34,
'sector'=>'G',
'variable'=>'Marital Status',
'value'=>'Married',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>35,
'sector'=>'G',
'variable'=>'Spouse Employment',
'value'=>'No',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>36,
'sector'=>'G',
'variable'=>'Spouse Employment',
'value'=>'Yes',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>37,
'sector'=>'G',
'variable'=>'Property Loan Remaining',
'value'=>'No Property',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>38,
'sector'=>'G',
'variable'=>'Property Loan Remaining',
'value'=>'> 70% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>39,
'sector'=>'G',
'variable'=>'Property Loan Remaining',
'value'=>'60% - 70% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>40,
'sector'=>'G',
'variable'=>'Property Loan Remaining',
'value'=>'50% - 59% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>41,
'sector'=>'G',
'variable'=>'Property Loan Remaining',
'value'=>'< 50% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>42,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'No trigger of adverse CCRIS record',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>43,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'1. Last 3 months: >=2 MIA x 1 time \r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>44,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2. Last 6 Months: >=2 MIA x 4 times or >=3 MIA x 1 times',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>45,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2a(ii). Legal Action with settlement with Release Letter and arrears cleared:\r\n- if legal action was initiated more than 6 months prior to the date of financing application.\"\r\n',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'-45',

] );


			
ScoreRating::create( [
'id'=>46,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2b. if legal action without settlement or doesn\'t meet criteria above.\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>47,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'3. Restructured / Rescheduled\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>48,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4a. Bankrupt Customer\r\n- Discharged Bankrupt (more than 12 months from date of financing application)\"\r\n',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'-45',

] );


			
ScoreRating::create( [
'id'=>49,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4b. Bankrupt Customer\r\n- Discharged Bankrupt (12 months or less from the date of financing application)\"\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>50,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4c. Currently Bankrupt\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>52,
'sector'=>'G',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'7. AKPK',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>53,
'sector'=>'P',
'variable'=>'DSR',
'value'=>'',
'float_value'=>0.0000,
'float_value2'=>60.0000,
'score'=>'75',

] );


			
ScoreRating::create( [
'id'=>54,
'sector'=>'P',
'variable'=>'DSR',
'value'=>'',
'float_value'=>60.0001,
'float_value2'=>70.0000,
'score'=>'50',

] );


			
ScoreRating::create( [
'id'=>55,
'sector'=>'P',
'variable'=>'DSR',
'value'=>'',
'float_value'=>70.0001,
'float_value2'=>85.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>56,
'sector'=>'P',
'variable'=>'DSR',
'value'=>'',
'float_value'=>85.0001,
'float_value2'=>999.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>57,
'sector'=>'P',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>0.0000,
'float_value2'=>499.9900,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>58,
'sector'=>'P',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>500.0000,
'float_value2'=>1000.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>59,
'sector'=>'P',
'variable'=>'Net Disposable Income',
'value'=>'',
'float_value'=>1000.0100,
'float_value2'=>1000000.0000,
'score'=>'40',

] );


			
ScoreRating::create( [
'id'=>60,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Private Sector/GLC: No Salary Deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>61,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Private Sector/GLC: New customer - Salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>62,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Private Sector/GLC: Existing customer - No salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>63,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Private Sector/GLC: Existing customer - Salary deduction',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>64,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: New Customer - Non Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>65,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: New Customer - Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>66,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: Existing Customer - Non Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>67,
'sector'=>'P',
'variable'=>'Salary Deduction',
'value'=>'Civil Servant: Existing Customer - Biro',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'45',

] );


			
ScoreRating::create( [
'id'=>68,
'sector'=>'P',
'variable'=>'Age',
'value'=>'21-30',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>69,
'sector'=>'P',
'variable'=>'Age',
'value'=>'31-40',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>70,
'sector'=>'P',
'variable'=>'Age',
'value'=>'41-50',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>71,
'sector'=>'P',
'variable'=>'Age',
'value'=>'51-60',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>72,
'sector'=>'P',
'variable'=>'Education Level',
'value'=>'Secondary & below',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>73,
'sector'=>'P',
'variable'=>'Education Level',
'value'=>'Diploma',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>74,
'sector'=>'P',
'variable'=>'Education Level',
'value'=>'Degree',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>75,
'sector'=>'P',
'variable'=>'Education Level',
'value'=>'Master & above / Professional Qualification',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>76,
'sector'=>'P',
'variable'=>'Employment',
'value'=>'Self Employed / Partnership / Sole Prop',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>77,
'sector'=>'P',
'variable'=>'Employment',
'value'=>'Salaried Non-Civil Servant/ PLC/ GLC',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>78,
'sector'=>'P',
'variable'=>'Employment',
'value'=>'Salaried Civil Servant',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'30',

] );


			
ScoreRating::create( [
'id'=>79,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Private Sector/GLC: Clerical /Retired/ Others',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>80,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Private Sector/GLC: Officer / Sales',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>81,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Private Sector/GLC: Manager and above',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'35',

] );


			
ScoreRating::create( [
'id'=>82,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Civil Servant: Clerical',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>83,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Civil Servant: Officer',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'25',

] );


			
ScoreRating::create( [
'id'=>84,
'sector'=>'P',
'variable'=>'Position',
'value'=>'Civil Servant: Manager and above',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'40',

] );


			
ScoreRating::create( [
'id'=>85,
'sector'=>'P',
'variable'=>'Marital Status',
'value'=>'Single / Divorced / Widow / Widower',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>86,
'sector'=>'P',
'variable'=>'Marital Status',
'value'=>'Married',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>87,
'sector'=>'P',
'variable'=>'Spouse Employment',
'value'=>'No',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>88,
'sector'=>'P',
'variable'=>'Spouse Employment',
'value'=>'Yes',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>89,
'sector'=>'P',
'variable'=>'Property Loan Remaining',
'value'=>'No Property',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>90,
'sector'=>'P',
'variable'=>'Property Loan Remaining',
'value'=>'> 70% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'5',

] );


			
ScoreRating::create( [
'id'=>91,
'sector'=>'P',
'variable'=>'Property Loan Remaining',
'value'=>'60% - 70% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'10',

] );


			
ScoreRating::create( [
'id'=>92,
'sector'=>'P',
'variable'=>'Property Loan Remaining',
'value'=>'50% - 59% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'15',

] );


			
ScoreRating::create( [
'id'=>93,
'sector'=>'P',
'variable'=>'Property Loan Remaining',
'value'=>'< 50% of limit',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'20',

] );


			
ScoreRating::create( [
'id'=>95,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'No trigger of adverse CCRIS record',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>96,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'1. Last 3 months: >=2 MIA x 1 time \r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>97,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2a(i). Legal Action with settlement with Release Letter and arrears cleared:\r\n- if legal action was initiated within 6 months prior to the date of financing application.\"\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>98,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2a(ii). Legal Action with settlement with Release Letter and arrears cleared:\r\n- if legal action was initiated more than 6 months prior to the date of financing application.',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'-45',

] );


			
ScoreRating::create( [
'id'=>99,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'2b. if legal action without settlement or doesn\'t meet criteria above.\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>100,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'3. Restructured / Rescheduled\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>101,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4a. Bankrupt Customer\r\n- Discharged Bankrupt (more than 12 months from date of financing application)\"\r\n',
'float_value'=>0.0000,
'float_value2'=>0.0000,
'score'=>'-45',

] );


			
ScoreRating::create( [
'id'=>102,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'7. AKPK',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>104,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4b. Bankrupt Customer\r\n- Discharged Bankrupt (12 months or less from the date of financing application)',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			
ScoreRating::create( [
'id'=>105,
'sector'=>'P',
'variable'=>'Adverse CCRIS Parameters',
'value'=>'4c. Currently Bankrupt\r\n',
'float_value'=>1.0000,
'float_value2'=>0.0000,
'score'=>'0',

] );


			DB::unprepared('SET IDENTITY_INSERT scoreratings OFF');
    }
}
