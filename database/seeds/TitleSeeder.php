<?php

use Illuminate\Database\Seeder;
use App\Model\Title;

class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT title ON');

      
		Title::create( [
		'id'=>1,
		'title_code'=>'CIK',
		'title_desc'=>'CIK',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>2,
		'title_code'=>'DATIN',
		'title_desc'=>'DATIN',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>3,
		'title_code'=>'DATO`',
		'title_desc'=>'DATO`',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>4,
		'title_code'=>'DATUK',
		'title_desc'=>'DATUK',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>5,
		'title_code'=>'DR',
		'title_desc'=>'DR',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>6,
		'title_code'=>'ENCIK',
		'title_desc'=>'ENCIK',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>7,
		'title_code'=>'HAJI',
		'title_desc'=>'HAJI',
		'status'=>1
		] );


			
		Title::create( [
		'id'=>8,
		'title_code'=>'HAJJAH',
		'title_desc'=>'HAJJAH',
		'status'=>1
		] );


			
Title::create( [
'id'=>9,
'title_code'=>'PUAN',
'title_desc'=>'PUAN',

'status'=>1
] );


			
Title::create( [
'id'=>10,
'title_code'=>'PROFESSOR',
'title_desc'=>'PROFESSOR',

'status'=>1
] );


			
Title::create( [
'id'=>11,
'title_code'=>'SENATOR',
'title_desc'=>'SENATOR',

'status'=>1
] );


			
Title::create( [
'id'=>12,
'title_code'=>'TAN SRI',
'title_desc'=>'TAN SRI',

'status'=>1
] );


			
Title::create( [
'id'=>13,
'title_code'=>'YANG BERHO',
'title_desc'=>'YANG BERHORMAT',

'status'=>1
] );


			
Title::create( [
'id'=>14,
'title_code'=>'DATIN SERI',
'title_desc'=>'DATIN SERI',

'status'=>1
] );


			
Title::create( [
'id'=>15,
'title_code'=>'TUN',
'title_desc'=>'TUN',

'status'=>1
] );


			
Title::create( [
'id'=>16,
'title_code'=>'TOH PUAN',
'title_desc'=>'TOH PUAN',

'status'=>1
] );


			
Title::create( [
'id'=>17,
'title_code'=>'PUAN SRI',
'title_desc'=>'PUAN SRI',

'status'=>1
] );


			
Title::create( [
'id'=>18,
'title_code'=>'IR',
'title_desc'=>'IR',

'status'=>1
] );


			
Title::create( [
'id'=>19,
'title_code'=>'DATO` SRI',
'title_desc'=>'DATO` SRI',

'status'=>1
] );


			
Title::create( [
'id'=>20,
'title_code'=>'OTH',
'title_desc'=>'OTHER',

'status'=>1
] );
			DB::unprepared('SET IDENTITY_INSERT title OFF');
    }
}
