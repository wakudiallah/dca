<?php

use Illuminate\Database\Seeder;
use App\Model\CodeJobStatus;

class JobRunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT state2s ON');

       
			DB::unprepared('SET IDENTITY_INSERT state2s OFF');
    }
}
