<?php

use Illuminate\Database\Seeder;
use App\Model\PrAddType;

class AddTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT pr_add_types ON');

        	PrAddType::create( [
				'id'=>1,
				'AddTyCode'=>'1',
				'AddTyDesc'=>'Home',
				'created_at'=>NULL,
				'updated_at'=>NULL
				] );
							
			PrAddType::create( [
				'id'=>2,
				'AddTyCode'=>'2',
				'AddTyDesc'=>'Permanent',
				'created_at'=>NULL,
				'updated_at'=>NULL
				] );
						
			PrAddType::create( [
				'id'=>3,
				'AddTyCode'=>'3',
				'AddTyDesc'=>'Office',
				'created_at'=>NULL,
				'updated_at'=>NULL
				] );
						
			PrAddType::create( [
				'id'=>4,
				'AddTyCode'=>'4',
				'AddTyDesc'=>'Office',
				'created_at'=>NULL,
				'updated_at'=>NULL
				] );
        	
		DB::unprepared('SET IDENTITY_INSERT pr_add_types OFF');
    }
}
