<?php

use Illuminate\Database\Seeder;
use App\Model\Occupations;

class OccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT occupations ON');

    	Occupations::create( [
'id'=>1,
'occupation_code'=>'UEMP',
'occupation_desc'=>'UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>2,
'occupation_code'=>'0003',
'occupation_desc'=>'FISHING AND ACTIVITIES INCIDENTAL TO FIS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>3,
'occupation_code'=>'0004',
'occupation_desc'=>'MINING, QUARRYING AND METAL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>4,
'occupation_code'=>'0005',
'occupation_desc'=>'BEVERAGE INDUSTRIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>5,
'occupation_code'=>'0006',
'occupation_desc'=>'PROCESSING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>6,
'occupation_code'=>'0007',
'occupation_desc'=>'PETROLEUM REFINERIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>7,
'occupation_code'=>'0008',
'occupation_desc'=>'BASIC METAL INDUSTRIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>8,
'occupation_code'=>'0009',
'occupation_desc'=>'ELECTRICITY, GAS AND WATER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>9,
'occupation_code'=>'0010',
'occupation_desc'=>'CONSTRUCTION',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>10,
'occupation_code'=>'0014',
'occupation_desc'=>'AIR TRANSPORT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>11,
'occupation_code'=>'0015',
'occupation_desc'=>'SERVICES ALLIED TO TRANSPORT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>12,
'occupation_code'=>'0016',
'occupation_desc'=>'COMMUNICATION (EXCLUDE RADIO AND TV BROA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>13,
'occupation_code'=>'0017',
'occupation_desc'=>'FINANCIAL SERVICES (INSTITUTIONS)',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>14,
'occupation_code'=>'0018',
'occupation_desc'=>'REAL ESTATE AND BUSINESS SERVICES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>15,
'occupation_code'=>'0020',
'occupation_desc'=>'PUBLIC ADMINISTRATION AND DEFENSE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>16,
'occupation_code'=>'0021',
'occupation_desc'=>'SOCIAL AND RELATED COMMUNITY SERVICES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>17,
'occupation_code'=>'0022',
'occupation_desc'=>'MEDICAL, RENTAL, OTHER HEALTH AND VETERI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>18,
'occupation_code'=>'0024',
'occupation_desc'=>'MOTION PICTURE AND OTHER ENTERTAINMENT S',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>19,
'occupation_code'=>'0025',
'occupation_desc'=>'REPAIR SERVICES NOT ELSEWHERE CLASSIFIED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>20,
'occupation_code'=>'0026',
'occupation_desc'=>'MISCELLANEOUS PERSONAL SERVICES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>21,
'occupation_code'=>'0027',
'occupation_desc'=>'PERSONAL CONSUMPTION (INDIVIDUALS)',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>22,
'occupation_code'=>'0028',
'occupation_desc'=>'MANUFACTURING FOOD',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>23,
'occupation_code'=>'0030',
'occupation_desc'=>'MANUFACTURING OF LEATHER PRODUCTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>24,
'occupation_code'=>'0031',
'occupation_desc'=>'MANUFACTURING OF WOOD PRODUCTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>25,
'occupation_code'=>'0032',
'occupation_desc'=>'MANUFACTURING OF PAPER PRODUCTS   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>26,
'occupation_code'=>'0033',
'occupation_desc'=>'MANUFACTURING OF INDUSTRIAL CHEMICAL PRO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>27,
'occupation_code'=>'0034',
'occupation_desc'=>'MANUFACTURING OF RUBBER PRODUCTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>28,
'occupation_code'=>'0036',
'occupation_desc'=>'MANUFACTURING OF FABRICATED METAL PRODUC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>29,
'occupation_code'=>'0037',
'occupation_desc'=>'MANUFACTURING OF MACHINERY',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>30,
'occupation_code'=>'0038',
'occupation_desc'=>'MANUFACTURING OF ELECTRICAL MACHINERY/EN',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>31,
'occupation_code'=>'0039',
'occupation_desc'=>'MANUFACTURING OF TRANSPORT EQUIPMENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>32,
'occupation_code'=>'0040',
'occupation_desc'=>'MANUFACTURING OF SCIENTIFIC/PHOTOGRAPHIC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>33,
'occupation_code'=>'0041',
'occupation_desc'=>'MANUFACTURING - OTHER INDUSTRIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>34,
'occupation_code'=>'0043',
'occupation_desc'=>'WHOLESALE TRADE - DRINKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>35,
'occupation_code'=>'0044',
'occupation_desc'=>'WHOLESALE TRADE - TOBACCO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>36,
'occupation_code'=>'0045',
'occupation_desc'=>'WHOLESALE TRADE:HOUSEHOLD AND PERSONAL G',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>37,
'occupation_code'=>'0046',
'occupation_desc'=>'WHOLESALE TRADE:MOTORCYCLES, MOTOR VEHIC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>38,
'occupation_code'=>'0047',
'occupation_desc'=>'WHOLESALE TRADE PARTS AND ACCESSORIES FO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>39,
'occupation_code'=>'0048',
'occupation_desc'=>'WHOLESALE TRADE-INDS, AGRICULTURAL,BUSIN',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>40,
'occupation_code'=>'0049',
'occupation_desc'=>'WHOLESALE TRADE:RUBBER ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>41,
'occupation_code'=>'0050',
'occupation_desc'=>'WHOLESALE TRADE:PALM OIL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>42,
'occupation_code'=>'0051',
'occupation_desc'=>'RETAIL TRADE-FOOD ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>43,
'occupation_code'=>'0054',
'occupation_desc'=>'RETAIL TRADE:HOUSEHOLD AND PERSONAL GOOD',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>44,
'occupation_code'=>'0055',
'occupation_desc'=>'RETAIL TRADE:MOTORCYCLES,MOTOR VEHICLES,',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>45,
'occupation_code'=>'0056',
'occupation_desc'=>'RETAIL TRADE:PARTS AND ACCESSORIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>46,
'occupation_code'=>'0057',
'occupation_desc'=>'WHOLESALE TRADE:PRINTING,PUBLISHING AND ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>47,
'occupation_code'=>'0058',
'occupation_desc'=>'RETAIL TRADE:PRINTING,PUBLISHING AND STA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>48,
'occupation_code'=>'0061',
'occupation_desc'=>'CONSTRUCTION',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>49,
'occupation_code'=>'0062',
'occupation_desc'=>'ELECTRICITY,GAS & WATER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>50,
'occupation_code'=>'0063',
'occupation_desc'=>'FINANCIAL SERVICES ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>51,
'occupation_code'=>'0064',
'occupation_desc'=>'GOVERNMENT SERVICE ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>52,
'occupation_code'=>'0065',
'occupation_desc'=>'HOTELS & RESTAURANT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>53,
'occupation_code'=>'0067',
'occupation_desc'=>'MANUFACTURING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>54,
'occupation_code'=>'0068',
'occupation_desc'=>'OTHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>55,
'occupation_code'=>'0069',
'occupation_desc'=>'QUARRYING & MINING ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>56,
'occupation_code'=>'0072',
'occupation_desc'=>'WHOLESALE & RETAIL ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>57,
'occupation_code'=>'1001',
'occupation_desc'=>'ARMED FORCES - LIEUTENANT-MEJAR & ABOVE ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>58,
'occupation_code'=>'1002',
'occupation_desc'=>'ARMED FORCES - WARRANT OFFICER & BELOW  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>59,
'occupation_code'=>'1003',
'occupation_desc'=>'FIRE BRIGADE - OFFICER & ABOVE - GOV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>60,
'occupation_code'=>'1005',
'occupation_desc'=>'LOCAL / STATE COUNSELOR - GOV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>61,
'occupation_code'=>'1007',
'occupation_desc'=>'POLICE - INSPECTOR & ABOVE - GOV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>62,
'occupation_code'=>'1008',
'occupation_desc'=>'POLICE - SERGEANT & BELOW - GOV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>63,
'occupation_code'=>'1009',
'occupation_desc'=>'POLITICIAN / POLITICS RELATED - GOV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>64,
'occupation_code'=>'1010',
'occupation_desc'=>'ATTORNEY GENERAL / MAGISTRATE / JUDGE   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>65,
'occupation_code'=>'1011',
'occupation_desc'=>'PUBLIC PROSECUTOR - GOV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>66,
'occupation_code'=>'1012',
'occupation_desc'=>'DIPLOMAT / FOREIGN DIGNITARIES - GOV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>67,
'occupation_code'=>'1014',
'occupation_desc'=>'LECTURER - GOV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>68,
'occupation_code'=>'1015',
'occupation_desc'=>'TEACHER / TUTOR - GOV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>69,
'occupation_code'=>'1016',
'occupation_desc'=>'STAFF NURSE - GOV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>70,
'occupation_code'=>'1018',
'occupation_desc'=>'WARDEN - OFFICER & ABOVE - GOV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>71,
'occupation_code'=>'1019',
'occupation_desc'=>'WARDEN - OTHERS - GOV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>72,
'occupation_code'=>'1020',
'occupation_desc'=>'RANGER - GOV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>73,
'occupation_code'=>'1021',
'occupation_desc'=>'MANAGEMENT - GOV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>74,
'occupation_code'=>'1022',
'occupation_desc'=>'EXECUTIVE - GOV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>75,
'occupation_code'=>'1023',
'occupation_desc'=>'NON-EXECUTIVE - GOV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>76,
'occupation_code'=>'2001',
'occupation_desc'=>'ACCOUNTANT - PROF  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>77,
'occupation_code'=>'2002',
'occupation_desc'=>'ARCHITECT - PROF   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>78,
'occupation_code'=>'2005',
'occupation_desc'=>'ENGINEER - PROF    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>79,
'occupation_code'=>'2006',
'occupation_desc'=>'LAWYER - PROF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>80,
'occupation_code'=>'2007',
'occupation_desc'=>'ACTUARIST - PROF   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>81,
'occupation_code'=>'2008',
'occupation_desc'=>'ADJUSTOR - PROF    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>82,
'occupation_code'=>'2009',
'occupation_desc'=>'PHARMACIST / CHEMIST - PROF ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>83,
'occupation_code'=>'2010',
'occupation_desc'=>'PILOT - PROF ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>84,
'occupation_code'=>'2011',
'occupation_desc'=>'QUANTITY SURVEYOR / VALUER - PROF ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>85,
'occupation_code'=>'2014',
'occupation_desc'=>'OTHERS - PROF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>86,
'occupation_code'=>'3001',
'occupation_desc'=>'ADVERTISING - MGR & ABV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>87,
'occupation_code'=>'3002',
'occupation_desc'=>'CONSTRUCTION - MGR & ABV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>88,
'occupation_code'=>'3003',
'occupation_desc'=>'FINANCIAL INSTN/SECURITIES-MGR & ABV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>89,
'occupation_code'=>'3004',
'occupation_desc'=>'MANUFACTURING - MGR & ABV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>90,
'occupation_code'=>'3006',
'occupation_desc'=>'PLANTATION - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>91,
'occupation_code'=>'3007',
'occupation_desc'=>'PUBLISHING / PRINTING - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>92,
'occupation_code'=>'3008',
'occupation_desc'=>'SERVICES - MGR & ABV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>93,
'occupation_code'=>'3009',
'occupation_desc'=>'TELECOMMUNICATION - MGR & ABV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>94,
'occupation_code'=>'3010',
'occupation_desc'=>'INSURANCE - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>95,
'occupation_code'=>'3011',
'occupation_desc'=>'COMPANY DIRECTOR - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>96,
'occupation_code'=>'3012',
'occupation_desc'=>'FINANCIAL CONTROLLER - MGR & ABV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>97,
'occupation_code'=>'3013',
'occupation_desc'=>'PETROLIUM / GAS - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>98,
'occupation_code'=>'3014',
'occupation_desc'=>'FOOD / RESTAURANT - MGR & ABV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>99,
'occupation_code'=>'3015',
'occupation_desc'=>'MINING - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>100,
'occupation_code'=>'3018',
'occupation_desc'=>'ELECTRONIC / ELECTRICAL - MGR & ABV     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>101,
'occupation_code'=>'3019',
'occupation_desc'=>'EDUCATION / HIGHER LEARNING - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>102,
'occupation_code'=>'3020',
'occupation_desc'=>'DIRECT SELLING - MGR & ABV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>103,
'occupation_code'=>'3021',
'occupation_desc'=>'RESEARCH & DEVELOPMENT- R&D - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>104,
'occupation_code'=>'3022',
'occupation_desc'=>'USED AUTO/DEALER &MACHINE PARTS-MGR&ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>105,
'occupation_code'=>'3024',
'occupation_desc'=>'JEWEL AND PRECIOUS METAL DEALER-MGR&ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>106,
'occupation_code'=>'3025',
'occupation_desc'=>'RETAIL STORES - MGR & ABV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>107,
'occupation_code'=>'3026',
'occupation_desc'=>'CONVENIENCE STORES - MGR & ABV    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>108,
'occupation_code'=>'3027',
'occupation_desc'=>'MONEY CHANGER - MGR & ABV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>109,
'occupation_code'=>'3028',
'occupation_desc'=>'GAMING / CASINO - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>110,
'occupation_code'=>'3030',
'occupation_desc'=>'PAWNSHOP - MGR & ABV  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>111,
'occupation_code'=>'3031',
'occupation_desc'=>'TRAVEL AGENCY - MGR & ABV   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>112,
'occupation_code'=>'3032',
'occupation_desc'=>'AUTO/VEHICLE/HEAVY EQUIP CO - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>113,
'occupation_code'=>'3033',
'occupation_desc'=>'OTHERS - MGR & ABV ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>114,
'occupation_code'=>'4002',
'occupation_desc'=>'CONSTRUCTION - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>115,
'occupation_code'=>'4004',
'occupation_desc'=>'MANUFACTURING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>116,
'occupation_code'=>'4005',
'occupation_desc'=>'MEDIA - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>117,
'occupation_code'=>'4006',
'occupation_desc'=>'PLANTATION - EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>118,
'occupation_code'=>'1004',
'occupation_desc'=>'MiCOB - FIRE BRIGADE - OTHERS - GOV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>119,
'occupation_code'=>'1006',
'occupation_desc'=>'MiCOB - POLICE - ASP & ABOVE - GOV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>120,
'occupation_code'=>'4008',
'occupation_desc'=>'SERVICES - EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>121,
'occupation_code'=>'4009',
'occupation_desc'=>'TELECOMMUNICATION - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>122,
'occupation_code'=>'4010',
'occupation_desc'=>'INSURANCE - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>123,
'occupation_code'=>'4011',
'occupation_desc'=>'PETROLIUM / GAS  - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>124,
'occupation_code'=>'4012',
'occupation_desc'=>'FOOD / RESTAURANT - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>125,
'occupation_code'=>'4014',
'occupation_desc'=>'TOURISM / HOTEL - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>126,
'occupation_code'=>'4017',
'occupation_desc'=>'EDUCATION / HIGHER LEARNING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>127,
'occupation_code'=>'4018',
'occupation_desc'=>'DIRECT SELLING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>128,
'occupation_code'=>'4019',
'occupation_desc'=>'RESEARCH & DEVELOPMENT- R&D - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>129,
'occupation_code'=>'4020',
'occupation_desc'=>'USED AUTO/DEALERS & MACHINE PARTS - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>130,
'occupation_code'=>'4021',
'occupation_desc'=>'STOCK BROKERS - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>131,
'occupation_code'=>'4022',
'occupation_desc'=>'JEWEL AND PRECIOUS METAL DEALER - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>132,
'occupation_code'=>'4023',
'occupation_desc'=>'PROFESSIONAL SERVICE PROVIDER - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>133,
'occupation_code'=>'4026',
'occupation_desc'=>'MONEY CHANGER - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>134,
'occupation_code'=>'4027',
'occupation_desc'=>'GAMING / CASINO - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>135,
'occupation_code'=>'4029',
'occupation_desc'=>'PAWNSHOP - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>136,
'occupation_code'=>'5001',
'occupation_desc'=>'ADVERTISING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>137,
'occupation_code'=>'5002',
'occupation_desc'=>'CONSTRUCTION - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>138,
'occupation_code'=>'5004',
'occupation_desc'=>'MANUFACTURING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>139,
'occupation_code'=>'5005',
'occupation_desc'=>'MEDIA - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>140,
'occupation_code'=>'5006',
'occupation_desc'=>'PLANTATION - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>141,
'occupation_code'=>'5007',
'occupation_desc'=>'PUBLISHING / PRINTING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>142,
'occupation_code'=>'5008',
'occupation_desc'=>'SERVICES - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>143,
'occupation_code'=>'5009',
'occupation_desc'=>'TELECOMMUNICATION - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>144,
'occupation_code'=>'5010',
'occupation_desc'=>'INSURANCE - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>145,
'occupation_code'=>'5014',
'occupation_desc'=>'TOURISM / HOTEL - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>146,
'occupation_code'=>'5015',
'occupation_desc'=>'TRADING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>147,
'occupation_code'=>'5016',
'occupation_desc'=>'ELECTRONIC / ELECTRICAL - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>148,
'occupation_code'=>'5017',
'occupation_desc'=>'EDUCATION / HIGHER LEARNING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>149,
'occupation_code'=>'5018',
'occupation_desc'=>'DIRECT SELLING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>150,
'occupation_code'=>'5020',
'occupation_desc'=>'USED AUTO/DEALERS&MACHINE PARTS-NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>151,
'occupation_code'=>'5021',
'occupation_desc'=>'STOCK BROKERS - NON EXEC    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>152,
'occupation_code'=>'5022',
'occupation_desc'=>'JEWEL & PRECIOUS METAL DEALER - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>153,
'occupation_code'=>'5024',
'occupation_desc'=>'RETAIL STORES - NON EXEC    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>154,
'occupation_code'=>'5025',
'occupation_desc'=>'CONVENIENCE STORES - NON EXEC     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>155,
'occupation_code'=>'5028',
'occupation_desc'=>'TRADING / IMPORT / EXPORT CO - NON EXEC ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>156,
'occupation_code'=>'5029',
'occupation_desc'=>'PAWNSHOP - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>157,
'occupation_code'=>'5031',
'occupation_desc'=>'AUTO/VEHICLE/HEAVY EQUIP CO-NON EXEC    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>158,
'occupation_code'=>'5032',
'occupation_desc'=>'OTHERS - NON EXEC  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>159,
'occupation_code'=>'6001',
'occupation_desc'=>'ARTIST / GRAPHIC DESIGNER - SKILLED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>160,
'occupation_code'=>'6003',
'occupation_desc'=>'BEAUTICIAN/HAIRDRESSER/MASSEUR - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>161,
'occupation_code'=>'6004',
'occupation_desc'=>'COOK / BAKER - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>162,
'occupation_code'=>'6005',
'occupation_desc'=>'CHEF - SKILLED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>163,
'occupation_code'=>'6008',
'occupation_desc'=>'ETRICIAN/CARPENTER/PAINTER/WELDER/FITTER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>164,
'occupation_code'=>'6009',
'occupation_desc'=>'SINGER/ACTOR/ACTRESS/CLOWN/DANCER ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>165,
'occupation_code'=>'6011',
'occupation_desc'=>'MECHANIC/TECHNICIAN/FOREMAN - SKILLED   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>166,
'occupation_code'=>'6012',
'occupation_desc'=>'NURSE - PRIVATE - SKILLED   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>167,
'occupation_code'=>'6015',
'occupation_desc'=>'SPORTS INSTRCTR/SPORTSMAN/FITNESS TRNER ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>168,
'occupation_code'=>'6016',
'occupation_desc'=>'TAILOR / SEAMSTRESS - SKILLED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>169,
'occupation_code'=>'6017',
'occupation_desc'=>'LECTURER - PRIVATE - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>170,
'occupation_code'=>'6018',
'occupation_desc'=>'TEACHER / KINDERGARTEN TEACHER / TUTOR  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>171,
'occupation_code'=>'6019',
'occupation_desc'=>'BROADCASTER - SKILLED ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>172,
'occupation_code'=>'6020',
'occupation_desc'=>'PROGRAMMER - SKILLED  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>173,
'occupation_code'=>'6024',
'occupation_desc'=>'SYSTEM ANALYST - SKILLED    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>174,
'occupation_code'=>'6025',
'occupation_desc'=>'JOURNALIST/REPORTER/EDITORS/WRITERS     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>175,
'occupation_code'=>'6027',
'occupation_desc'=>'OTHERS - SKILLED   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>176,
'occupation_code'=>'7001',
'occupation_desc'=>'DRIVER - NON SKILLED  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>177,
'occupation_code'=>'7004',
'occupation_desc'=>'CONSTRUCTION WORKER - NON SKILLED ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>178,
'occupation_code'=>'7006',
'occupation_desc'=>'FARMER/AGRICULTURE/LIVE STOCK     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>179,
'occupation_code'=>'7007',
'occupation_desc'=>'FISHERMAN - NON SKILLED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>180,
'occupation_code'=>'7008',
'occupation_desc'=>'GARDENER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>181,
'occupation_code'=>'7009',
'occupation_desc'=>'MAID / TEA LADY - NON SKILLED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>182,
'occupation_code'=>'7013',
'occupation_desc'=>'BARTENDER/WAITER/WAITRESS/CASHIER ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>183,
'occupation_code'=>'7014',
'occupation_desc'=>'MODEL - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>184,
'occupation_code'=>'7015',
'occupation_desc'=>'SALESMAN/COMMISSION EARNER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>185,
'occupation_code'=>'7017',
'occupation_desc'=>'STEWARD / STEWARDESS - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>186,
'occupation_code'=>'7018',
'occupation_desc'=>'REPOSSESSOR / STAFF OF COLLECTION AGENCY',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>187,
'occupation_code'=>'7019',
'occupation_desc'=>'OTHERS - NON-SKILLED  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>188,
'occupation_code'=>'7021',
'occupation_desc'=>'MMBR OF RELIGIOUS BODY-MONK,PRIEST,IMAM ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>189,
'occupation_code'=>'7022',
'occupation_desc'=>'BABYSITTER - NON SKILLED    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>190,
'occupation_code'=>'7023',
'occupation_desc'=>'MISCELLANEOUS OTHERS - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>191,
'occupation_code'=>'8002',
'occupation_desc'=>'AGRICULTURE - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>192,
'occupation_code'=>'8003',
'occupation_desc'=>'AQUACULTURE - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>193,
'occupation_code'=>'8004',
'occupation_desc'=>'ART / ANTIQUE DEALER - SELF EMP   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>194,
'occupation_code'=>'8006',
'occupation_desc'=>'CONSULTATION AGENCY - SELF EMP    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>195,
'occupation_code'=>'8007',
'occupation_desc'=>'CONTRACTOR - SELF EMP ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>196,
'occupation_code'=>'8009',
'occupation_desc'=>'EVENT ORGANISER - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>197,
'occupation_code'=>'8010',
'occupation_desc'=>'GAMING / CASINO OWNER - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>198,
'occupation_code'=>'8011',
'occupation_desc'=>'IMPORT / EXPORT / TRADER - SELF EMP     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>199,
'occupation_code'=>'8012',
'occupation_desc'=>'INSURANCE AGENT / FINANCIAL PLANNER     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>200,
'occupation_code'=>'8013',
'occupation_desc'=>'IT MERCHANDISER - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>201,
'occupation_code'=>'8016',
'occupation_desc'=>'MEDIA / DESIGN AGENCY - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>202,
'occupation_code'=>'8017',
'occupation_desc'=>'PAWNSHOP OWNER / PAWN BROKERS - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>203,
'occupation_code'=>'8018',
'occupation_desc'=>'PUBLISHING / PRINTING COMPANY - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>204,
'occupation_code'=>'8020',
'occupation_desc'=>'REMISER - SELF EMP ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>205,
'occupation_code'=>'8021',
'occupation_desc'=>'BROKER / STOCK BROKER - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>206,
'occupation_code'=>'8024',
'occupation_desc'=>'SURVEYING AGENCY - SELF EMP ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>207,
'occupation_code'=>'8025',
'occupation_desc'=>'WHOLESALER / RETAILER - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>208,
'occupation_code'=>'8026',
'occupation_desc'=>'TRADING - SERVICES - SELF EMP     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>209,
'occupation_code'=>'8028',
'occupation_desc'=>'TRADING - SELF EMP ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>210,
'occupation_code'=>'8029',
'occupation_desc'=>'TRAVEL AGENCY - SELF EMP    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>211,
'occupation_code'=>'8032',
'occupation_desc'=>'RECON AUTO/VEHICLE/HEAVY EQUIP DEALER   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>212,
'occupation_code'=>'8033',
'occupation_desc'=>'FREELANCE - SELF EMP  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>213,
'occupation_code'=>'8034',
'occupation_desc'=>'SUPPLIER TO THE MILITARY - SELF EMP     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>214,
'occupation_code'=>'9001',
'occupation_desc'=>'PENSIONER - UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>215,
'occupation_code'=>'9003',
'occupation_desc'=>'STUDENT - UNEMPLOYED  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>216,
'occupation_code'=>'9004',
'occupation_desc'=>'HOMEMAKER - UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>217,
'occupation_code'=>'9005',
'occupation_desc'=>'INFANT / CHILD / MINOR - UNEMPLOYED     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>218,
'occupation_code'=>'9006',
'occupation_desc'=>'OTHERS - UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>219,
'occupation_code'=>'CDIR',
'occupation_desc'=>'COMPANY DIRECTOR   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>220,
'occupation_code'=>'GJPA',
'occupation_desc'=>'GOVERNMENT - JPA   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>221,
'occupation_code'=>'GSBB',
'occupation_desc'=>'GOVT - STATUTORY BODIES/BADAN BERKANUN  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>222,
'occupation_code'=>'HWIF',
'occupation_desc'=>'HOUSEWIFE    ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>223,
'occupation_code'=>'NOEX',
'occupation_desc'=>'NON-EXECUTIVE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>224,
'occupation_code'=>'PEXS',
'occupation_desc'=>'PRIVATE - EXECUTIVE, SUPERVISOR   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>225,
'occupation_code'=>'PMSM',
'occupation_desc'=>'PRIVATE - MIDDLE MGMT, SENIOR MANAGEMENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>226,
'occupation_code'=>'PNEX',
'occupation_desc'=>'PRIVATE - NON EXECUTIVE     ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>227,
'occupation_code'=>'PPRO',
'occupation_desc'=>'PRIVATE - PROFESSIONAL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>228,
'occupation_code'=>'PROF',
'occupation_desc'=>'PROFESSIONAL ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>229,
'occupation_code'=>'PRT2',
'occupation_desc'=>'PARTNERSHIP(TURNOVER UP TO RM250 K PA)  ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>230,
'occupation_code'=>'SEM1',
'occupation_desc'=>'SELF-EMPLOYED (TURNOVER > RM250 K PA)   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>231,
'occupation_code'=>'SEMP',
'occupation_desc'=>'SELF EMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>232,
'occupation_code'=>'SMTG',
'occupation_desc'=>'SENIOR MANAGEMENT - GOVERNMENT SECTOR   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>233,
'occupation_code'=>'SPVR',
'occupation_desc'=>'SUPERVISOR   ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>234,
'occupation_code'=>'1013',
'occupation_desc'=>'MiCOB - DOCTOR / DENTIST / VETERINARIAN - GOV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>235,
'occupation_code'=>'1017',
'occupation_desc'=>'MiCOB - NURSE - GOV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>236,
'occupation_code'=>'2003',
'occupation_desc'=>'MiCOB - AUDITOR - PROF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>237,
'occupation_code'=>'2004',
'occupation_desc'=>'MiCOB - DOCTOR/DENTIST/VETERINARIAN - NON-GOVT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>238,
'occupation_code'=>'2012',
'occupation_desc'=>'MiCOB - COMPANY SECRETARY - PROF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>239,
'occupation_code'=>'2013',
'occupation_desc'=>'MiCOB - DIETITION / NUTRIONIST - PROF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>240,
'occupation_code'=>'3005',
'occupation_desc'=>'MiCOB - MEDIA - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>241,
'occupation_code'=>'3016',
'occupation_desc'=>'MiCOB - TOURISM / HOTEL - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>242,
'occupation_code'=>'3017',
'occupation_desc'=>'MiCOB - TRADING - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>243,
'occupation_code'=>'3023',
'occupation_desc'=>'MiCOB - STOCK BROKERS - MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>244,
'occupation_code'=>'3029',
'occupation_desc'=>'MiCOB - TRADING / IMPORT / EXPORT CO-MGR & ABV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>245,
'occupation_code'=>'4001',
'occupation_desc'=>'MiCOB - ADVERTISING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>246,
'occupation_code'=>'4003',
'occupation_desc'=>'MiCOB - FINANCIAL INSTITUTION / SECURITIES-EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>247,
'occupation_code'=>'4007',
'occupation_desc'=>'MiCOB - PUBLISHING / PRINTING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>248,
'occupation_code'=>'4013',
'occupation_desc'=>'MiCOB - MINING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>249,
'occupation_code'=>'4015',
'occupation_desc'=>'MiCOB - TRADING - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>250,
'occupation_code'=>'4016',
'occupation_desc'=>'MiCOB - ELECTRONIC / ELECTRICAL - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>251,
'occupation_code'=>'4024',
'occupation_desc'=>'MiCOB - RETAIL STORES - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>252,
'occupation_code'=>'4025',
'occupation_desc'=>'MiCOB - CONVENIENCE STORES - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>253,
'occupation_code'=>'4028',
'occupation_desc'=>'MiCOB - TRADING / IMPORT / EXPORT CO - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>254,
'occupation_code'=>'4031',
'occupation_desc'=>'MiCOB - AUTO/VEHICLE/HEAVY EQUIP COMPANY - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>255,
'occupation_code'=>'4032',
'occupation_desc'=>'MiCOB - OTHERS - EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>256,
'occupation_code'=>'5003',
'occupation_desc'=>'MiCOB - FINANCIAL INSTN/SECURITIES - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>257,
'occupation_code'=>'5011',
'occupation_desc'=>'MiCOB - PETROLIUM / GAS - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>258,
'occupation_code'=>'5012',
'occupation_desc'=>'MiCOB - FOOD / RESTAURANT - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>259,
'occupation_code'=>'5013',
'occupation_desc'=>'MiCOB - MINING - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>260,
'occupation_code'=>'5019',
'occupation_desc'=>'MiCOB - RESEARCH & DEVELOPMENT- R&D - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>261,
'occupation_code'=>'5023',
'occupation_desc'=>'MiCOB - PROFESSIONAL SERVICE PROVIDER - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>262,
'occupation_code'=>'5026',
'occupation_desc'=>'MiCOB - MONEY CHANGER - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>263,
'occupation_code'=>'5027',
'occupation_desc'=>'MiCOB - GAMING / CASINO - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>264,
'occupation_code'=>'5030',
'occupation_desc'=>'MiCOB - TRAVEL AGENCY - NON EXEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>265,
'occupation_code'=>'6002',
'occupation_desc'=>'MiCOB - BOILER - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>266,
'occupation_code'=>'6006',
'occupation_desc'=>'MiCOB - DESIGNER - FASHION / INTERIOR - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>267,
'occupation_code'=>'6007',
'occupation_desc'=>'MiCOB - DRAUGHTSMAN - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>268,
'occupation_code'=>'6010',
'occupation_desc'=>'MiCOB - LIBRARIAN - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>269,
'occupation_code'=>'6013',
'occupation_desc'=>'MiCOB - OPTICIAN - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>270,
'occupation_code'=>'6014',
'occupation_desc'=>'MiCOB - SAILOR / SEAMAN - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>271,
'occupation_code'=>'6021',
'occupation_desc'=>'MiCOB - HEAVY MACHINERY OPERATOR - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>272,
'occupation_code'=>'6022',
'occupation_desc'=>'MiCOB - INSTRUCTOR / COORDINATOR - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>273,
'occupation_code'=>'6023',
'occupation_desc'=>'MiCOB - CONSULTANT - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>274,
'occupation_code'=>'6026',
'occupation_desc'=>'MiCOB - PHOTOGRAPHER - SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>275,
'occupation_code'=>'7002',
'occupation_desc'=>'MiCOB - FACTORY WORKER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>276,
'occupation_code'=>'7003',
'occupation_desc'=>'MiCOB - GENERAL WORKER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>277,
'occupation_code'=>'7005',
'occupation_desc'=>'MiCOB - FELDA SETTLERS - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>278,
'occupation_code'=>'7010',
'occupation_desc'=>'MiCOB - SECURITY / GUARD / POLICE-PRIVATE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>279,
'occupation_code'=>'7011',
'occupation_desc'=>'MiCOB - GRO/PRIVATE/SOCIAL ESCORT - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>280,
'occupation_code'=>'7012',
'occupation_desc'=>'MiCOB - CADDY/BELLBOY/PORTER/CROUPIER/JOCKEY',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>281,
'occupation_code'=>'7016',
'occupation_desc'=>'MiCOB - SECRETARY / STENOGRAPHER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>282,
'occupation_code'=>'7020',
'occupation_desc'=>'MiCOB - SOCIAL / WELFARE WORKER - NON SKILLED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>283,
'occupation_code'=>'8001',
'occupation_desc'=>'MiCOB - ADVERTISING AGENCY - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>284,
'occupation_code'=>'8005',
'occupation_desc'=>'MiCOB - COLLECTION AGENCY - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>285,
'occupation_code'=>'8008',
'occupation_desc'=>'MiCOB - RETAIL STORE/RTAURANT OWNER - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>286,
'occupation_code'=>'8014',
'occupation_desc'=>'MiCOB - JEWEL ANDPRECIOUS METAL DEALERS-SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>287,
'occupation_code'=>'8015',
'occupation_desc'=>'MiCOB - LICENSED MONEY CHANGER - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>288,
'occupation_code'=>'8019',
'occupation_desc'=>'MiCOB - RECRUITMENT AGENCY - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>289,
'occupation_code'=>'8022',
'occupation_desc'=>'MiCOB - AGENT - DIRECT SELLING AGENT - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>290,
'occupation_code'=>'8023',
'occupation_desc'=>'MiCOB - HAWKER - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>291,
'occupation_code'=>'8027',
'occupation_desc'=>'MiCOB - TRADING - TRANSPORTATION - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>292,
'occupation_code'=>'8030',
'occupation_desc'=>'MiCOB - USED AUTO/VEHICLE/HEAVY EQUIP DEALER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>293,
'occupation_code'=>'8031',
'occupation_desc'=>'MiCOB - NEW AUTO/VEHICLE/HEAVY EQUIP DEALER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>294,
'occupation_code'=>'8035',
'occupation_desc'=>'MiCOB - ENTERTAINMENT - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>295,
'occupation_code'=>'8036',
'occupation_desc'=>'MiCOB - OTHERS - SELF EMP',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>296,
'occupation_code'=>'9002',
'occupation_desc'=>'MiCOB - RETIREE - UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>297,
'occupation_code'=>'9007',
'occupation_desc'=>'MiCOB - JOBLESS - UNEMPLOYED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>298,
'occupation_code'=>'AGRI',
'occupation_desc'=>'ICBA - AGRIGULTURE & FORESTRY',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>299,
'occupation_code'=>'CEMD',
'occupation_desc'=>'CEO OR MANAGING DIRECTOR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>300,
'occupation_code'=>'CONS',
'occupation_desc'=>'ICBA - CONSTRUCTION',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>301,
'occupation_code'=>'EGWA',
'occupation_desc'=>'ICBA - ELECTRICITY, GAS & WATER',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>302,
'occupation_code'=>'EXEC',
'occupation_desc'=>'EXECUTIVE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>303,
'occupation_code'=>'FISE',
'occupation_desc'=>'ICBA - FINANCIAL SERVICES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>304,
'occupation_code'=>'GOVT',
'occupation_desc'=>'ICBA - GOVERNMENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>305,
'occupation_code'=>'HOTE',
'occupation_desc'=>'ICBA - HOTELS & RESTAURANT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>306,
'occupation_code'=>'HUNT',
'occupation_desc'=>'ICBA - HUNTING & FISHING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>307,
'occupation_code'=>'IPAD',
'occupation_desc'=>'TEACHER-IPAD CAMPAIGN',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>308,
'occupation_code'=>'MANU',
'occupation_desc'=>'ICBA - MANUFACTURING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>309,
'occupation_code'=>'MMTG',
'occupation_desc'=>'MIDDLE MANAGEMENT - GOVERNMENT SECTOR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>310,
'occupation_code'=>'MMTP',
'occupation_desc'=>'MIDDLE MANAGEMENT - PRIVATE SECTOR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>311,
'occupation_code'=>'OTHE',
'occupation_desc'=>'ICBA - OTHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>312,
'occupation_code'=>'PRT1',
'occupation_desc'=>'PARTNERSHIP (TURNOVER MORE THAN RM250,000 PER YEAR)',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>313,
'occupation_code'=>'QUAR',
'occupation_desc'=>'ICBA - QUARRYING & MINING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>314,
'occupation_code'=>'REBU',
'occupation_desc'=>'ICBA - REAL ESTATE/ BUSINESS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>315,
'occupation_code'=>'SEM2',
'occupation_desc'=>'SELF-EMPLOYED (TURNOVER UP TO RM250,000 PER YEAR)',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>316,
'occupation_code'=>'SMTP',
'occupation_desc'=>'SENIOR MANAGEMENT - PRIVATE SECTOR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>317,
'occupation_code'=>'TRAN',
'occupation_desc'=>'ICBA - TRANSPORT, STORAGE, COMMS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>318,
'occupation_code'=>'WHOL',
'occupation_desc'=>'ICBA - WHOLESALE & RETAIL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>319,
'occupation_code'=>'1112',
'occupation_desc'=>'SENIOR GOVERNMENT OFFICIALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>320,
'occupation_code'=>'111X',
'occupation_desc'=>'OTHER LEGISLATORS AND SENIOR OFFICIAL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>321,
'occupation_code'=>'1120',
'occupation_desc'=>'MANAGING DIRECTORS AND CHIEF EXECUTIVES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>322,
'occupation_code'=>'1200',
'occupation_desc'=>'ADMINISTRATIVE AND COMMERCIAL MANAGERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>323,
'occupation_code'=>'1300',
'occupation_desc'=>'PRODUCTION AND MANUFACTURING MANAGERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>324,
'occupation_code'=>'1400',
'occupation_desc'=>'HOSPITALITY, RETAIL AND OTHER SERVICES M',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>325,
'occupation_code'=>'1500',
'occupation_desc'=>'INFORMATION AND COMM TECHNOLOG MANAGERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>326,
'occupation_code'=>'1600',
'occupation_desc'=>'SERVICES MANAGERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>327,
'occupation_code'=>'2111',
'occupation_desc'=>'PHYSICISTS AND ASTRONOMERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>328,
'occupation_code'=>'2112',
'occupation_desc'=>'METEREOLOGISTS AND SEISMOLOGISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>329,
'occupation_code'=>'2113',
'occupation_desc'=>'CHEMISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>330,
'occupation_code'=>'2114',
'occupation_desc'=>'GEOLOGISTS AND GEOPHYSICISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>331,
'occupation_code'=>'2120',
'occupation_desc'=>'MATHEMATICIANS, ACTUARIES AND STATISTICI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>332,
'occupation_code'=>'2132',
'occupation_desc'=>'FARMING, FORESTRY AND FISHERIES ADVISERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>333,
'occupation_code'=>'213X',
'occupation_desc'=>'OTHER LIFE SCIENCE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>334,
'occupation_code'=>'2140',
'occupation_desc'=>'ENGINEERING PROFESSIONALS (INCLUDING ELE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>335,
'occupation_code'=>'2161',
'occupation_desc'=>'BUILDING ARCHITECTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>336,
'occupation_code'=>'2162',
'occupation_desc'=>'LANDSCAPE ARCHITECTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>337,
'occupation_code'=>'2163',
'occupation_desc'=>'PRODUCT AND GARMENT DESIGNERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>338,
'occupation_code'=>'2164',
'occupation_desc'=>'TOWN AND TRAFFIC PLANNERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>339,
'occupation_code'=>'2165',
'occupation_desc'=>'CARTOGRAPHERS AND SURVEYORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>340,
'occupation_code'=>'2166',
'occupation_desc'=>'GRAPHIC AND MULTIMEDIA DESIGNERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>341,
'occupation_code'=>'2171',
'occupation_desc'=>'SHIPS ENGINEERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>342,
'occupation_code'=>'2172',
'occupation_desc'=>'SHIPS DECK OFFICERS AND PILOTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>343,
'occupation_code'=>'2173',
'occupation_desc'=>'AIRCRAFT PILOTS AND RELATED PROFESSIONAL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>344,
'occupation_code'=>'217X',
'occupation_desc'=>'OTHER SHIP, AIRCRAFT AND TRAIN/LOCOMOTIV',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>345,
'occupation_code'=>'2180',
'occupation_desc'=>'MINING, MANUFACTURING AND CONSTRUCTION P',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>346,
'occupation_code'=>'2210',
'occupation_desc'=>'MEDICAL DOCTORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>347,
'occupation_code'=>'2220',
'occupation_desc'=>'NURSING AND MIDWIFERY PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>348,
'occupation_code'=>'2261',
'occupation_desc'=>'DENTISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>349,
'occupation_code'=>'2262',
'occupation_desc'=>'PHARMACISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>350,
'occupation_code'=>'2267',
'occupation_desc'=>'OPTOMETRISTS AND OPHTHALMIC OPTICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>351,
'occupation_code'=>'2269',
'occupation_desc'=>'HEALTH PROFESSIONALS NOT ELSEWHERE CLASS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>352,
'occupation_code'=>'2310',
'occupation_desc'=>'UNIVERSITY AND HIGHER EDUCATION TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>353,
'occupation_code'=>'2320',
'occupation_desc'=>'VOCATIONAL EDUCATION TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>354,
'occupation_code'=>'2330',
'occupation_desc'=>'SECONDARY EDUCATION TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>355,
'occupation_code'=>'2340',
'occupation_desc'=>'PRIMARY SCHOOL AND EARLY CHILDHOOD TEACH',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>356,
'occupation_code'=>'2350',
'occupation_desc'=>'MUSIC, ARTS AND PERFORMING ARTS TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>357,
'occupation_code'=>'2360',
'occupation_desc'=>'LANGUAGE TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>358,
'occupation_code'=>'2370',
'occupation_desc'=>'RELIGIOUS TEACHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>359,
'occupation_code'=>'2380',
'occupation_desc'=>'TECHNOLOGY SKILL AND TECHNICAL TRAINERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>360,
'occupation_code'=>'2390',
'occupation_desc'=>'OTHER TEACHING PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>361,
'occupation_code'=>'2411',
'occupation_desc'=>'ACCOUNTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>362,
'occupation_code'=>'2412',
'occupation_desc'=>'FINANCIAL AND INVESTMENT ADVISERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>363,
'occupation_code'=>'2413',
'occupation_desc'=>'FINANCIAL ANALYSTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>364,
'occupation_code'=>'2400',
'occupation_desc'=>'ADMINISTRATION PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>365,
'occupation_code'=>'2430',
'occupation_desc'=>'SALES, MARKETING AND PUBLIC RELATIONS PR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>366,
'occupation_code'=>'2511',
'occupation_desc'=>'SYSTEM ANALYSTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>367,
'occupation_code'=>'2512',
'occupation_desc'=>'SOFTWARE DEVELOPERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>368,
'occupation_code'=>'2513',
'occupation_desc'=>'WEB AND MULTIMEDIA DEVELOPERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>369,
'occupation_code'=>'2514',
'occupation_desc'=>'APPLICATIONS PROGRAMMERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>370,
'occupation_code'=>'2519',
'occupation_desc'=>'SOFTWARE AND APPLICATIONS DEVELOPERS AND',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>371,
'occupation_code'=>'2521',
'occupation_desc'=>'DATABASE DESIGNERS AND ADMINISTRATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>372,
'occupation_code'=>'2522',
'occupation_desc'=>'INFORMATION TECHNOLOGY SYSTEM ADMINISTRA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>373,
'occupation_code'=>'2523',
'occupation_desc'=>'OTHER DATABASE AND NETWORK PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>374,
'occupation_code'=>'2611',
'occupation_desc'=>'LAWYERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>375,
'occupation_code'=>'2612',
'occupation_desc'=>'JUDGES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>376,
'occupation_code'=>'2619',
'occupation_desc'=>'LEGAL PROFESSIONALS N.E.C.',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>377,
'occupation_code'=>'2700',
'occupation_desc'=>'HOSPITALITY AND RELATED SERVICES PROFESS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>378,
'occupation_code'=>'2810',
'occupation_desc'=>'LIBRARIANS, ARCHIVISTS AND CURATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>379,
'occupation_code'=>'2821',
'occupation_desc'=>'ECONOMISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>380,
'occupation_code'=>'2822',
'occupation_desc'=>'SOCIOLOGISTS, ANTHROPOLOGISTS AND RELATE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>381,
'occupation_code'=>'2823',
'occupation_desc'=>'PHILOSOPHERS, HISTORIANS AND POLITICAL S',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>382,
'occupation_code'=>'2824',
'occupation_desc'=>'PSYCHOLOGISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>383,
'occupation_code'=>'2825',
'occupation_desc'=>'SOCIAL WORK AND COUNSELING PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>384,
'occupation_code'=>'2826',
'occupation_desc'=>'RELIGIOUS PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>385,
'occupation_code'=>'2831',
'occupation_desc'=>'AUTHORS AND RELATED WRITERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>386,
'occupation_code'=>'2832',
'occupation_desc'=>'JOURNALISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>387,
'occupation_code'=>'2833',
'occupation_desc'=>'TRANSLATORS, INTERPRETERS AND OTHER LING',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>388,
'occupation_code'=>'2841',
'occupation_desc'=>'VISUAL ARTISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>389,
'occupation_code'=>'2842',
'occupation_desc'=>'MUSICIANS, SINGERS AND COMPOSERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>390,
'occupation_code'=>'2843',
'occupation_desc'=>'DANCERS AND CHOREOGRAPHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>391,
'occupation_code'=>'2844',
'occupation_desc'=>'FILM, STAGE AND RELATED DIRECTORS AND PR',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>392,
'occupation_code'=>'2845',
'occupation_desc'=>'ACTORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>393,
'occupation_code'=>'2846',
'occupation_desc'=>'ANNOUNCERS ON RADIO, TELEVISION AND OTHE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>394,
'occupation_code'=>'2847',
'occupation_desc'=>'CLOWNS, MAGICIANS, ACROBATS AND RELATED ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>395,
'occupation_code'=>'2658',
'occupation_desc'=>'ANIMALS KEEPERS AND TRAINERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>396,
'occupation_code'=>'2659',
'occupation_desc'=>'CREATIVE AND PERFORMING ARTISTS N.E.C.',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>397,
'occupation_code'=>'2911',
'occupation_desc'=>'PROFESSIONAL CUSTOMS AND BORDER INSPECTO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>398,
'occupation_code'=>'2912',
'occupation_desc'=>'PROFESSIONAL TAXATION AND EXCISE OFFICIA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>399,
'occupation_code'=>'2913',
'occupation_desc'=>'PROFESSIONAL GOVERNMENT SOCIAL BENEFITS ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>400,
'occupation_code'=>'2914',
'occupation_desc'=>'PROFESSIONAL GOVERNMENT LICENSING OFFICI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>401,
'occupation_code'=>'2915',
'occupation_desc'=>'PROFESSIONAL POLICE INSPECTORS AND DETEC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>402,
'occupation_code'=>'2916',
'occupation_desc'=>'PROFESSIONAL CIVIL DEFENCE OFFICIALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>403,
'occupation_code'=>'2919',
'occupation_desc'=>'REGULATORY GOVERNMENT PROFESSIONALS NOT ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>404,
'occupation_code'=>'3110',
'occupation_desc'=>'PHYSICAL AND ENGINEERING SCIENCE TECHNIC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>405,
'occupation_code'=>'3120',
'occupation_desc'=>'MINING, MANUFACTURING AND CONSTRUCTION S',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>406,
'occupation_code'=>'3130',
'occupation_desc'=>'PROCESS CONTROL TECHNICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>407,
'occupation_code'=>'3140',
'occupation_desc'=>'LIFE SCIENCE TECHNICIANS AND RELATED ASS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>408,
'occupation_code'=>'3150',
'occupation_desc'=>'SHIP, AIRCRAFT AND TRAIN TECHNICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>409,
'occupation_code'=>'3160',
'occupation_desc'=>'NUCLEAR SCIENCE ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>410,
'occupation_code'=>'3210',
'occupation_desc'=>'MEDICAL AND PHARMACEUTICAL TECHNICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>411,
'occupation_code'=>'3220',
'occupation_desc'=>'NURSING AND MIDWIFERY ASSOCIATE PROFESSI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>412,
'occupation_code'=>'3230',
'occupation_desc'=>'TRADITIONAL AND COMPLEMENTARY MEDICINE A',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>413,
'occupation_code'=>'3240',
'occupation_desc'=>'VETERINARY TECHNICIANS AND ASSISTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>414,
'occupation_code'=>'3251',
'occupation_desc'=>'DENTAL ASSISTANTS AND THERAPISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>415,
'occupation_code'=>'3252',
'occupation_desc'=>'MEDICAL RECORDS AND HEALTH INFORMATION T',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>416,
'occupation_code'=>'3254',
'occupation_desc'=>'DISPENSING OPTICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>417,
'occupation_code'=>'3255',
'occupation_desc'=>'PHYSIOTHERAPY TECHNICIANS AND ASSISTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>418,
'occupation_code'=>'3256',
'occupation_desc'=>'MEDICAL ASSISTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>419,
'occupation_code'=>'3259',
'occupation_desc'=>'HEALTH ASSOCIATE PROFESSIONALS NOT ELSEW',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>420,
'occupation_code'=>'3311',
'occupation_desc'=>'SECURITIES AND FINANCE DEALERS AND BROKE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>421,
'occupation_code'=>'3312',
'occupation_desc'=>'CREDIT AND LOANS OFFICERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>422,
'occupation_code'=>'3313',
'occupation_desc'=>'ACCOUNTING ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>423,
'occupation_code'=>'3314',
'occupation_desc'=>'STATISTICAL, MATHEMATICAL AND ACTUARIAL ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>424,
'occupation_code'=>'3315',
'occupation_desc'=>'VALUERS AND LOSS ASSESSORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>425,
'occupation_code'=>'3321',
'occupation_desc'=>'INSURANCE AGENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>426,
'occupation_code'=>'3322',
'occupation_desc'=>'COMMERCIAL SALES AGENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>427,
'occupation_code'=>'3323',
'occupation_desc'=>'BUYERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>428,
'occupation_code'=>'3324',
'occupation_desc'=>'TRADE BROKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>429,
'occupation_code'=>'3330',
'occupation_desc'=>'BUSINESS SERVICES AGENTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>430,
'occupation_code'=>'3341',
'occupation_desc'=>'ADMINISTRATIVE ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>431,
'occupation_code'=>'3343',
'occupation_desc'=>'ADMINISTRATIVE AND EXECUTIVE SECRETARIES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>432,
'occupation_code'=>'334X',
'occupation_desc'=>'OTHER ADMINISTRATIVE AND SPECIALIZED ASS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>433,
'occupation_code'=>'3400',
'occupation_desc'=>'LEGAL ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>434,
'occupation_code'=>'3500',
'occupation_desc'=>'INFORMATION AND COMMUNICATIONS TECHNICIA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>435,
'occupation_code'=>'3610',
'occupation_desc'=>'SOCIAL AND RELIGOUS ASSOCIATE PROFESSION',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>436,
'occupation_code'=>'3421',
'occupation_desc'=>'ATHELETES AND SPORTS PLAYERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>437,
'occupation_code'=>'3622',
'occupation_desc'=>'SPORTS COACHES, INSTRUCTORS AND OFFICIAL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>438,
'occupation_code'=>'3423',
'occupation_desc'=>'FITNESS AND RECREATION INSTRUCTORS AND P',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>439,
'occupation_code'=>'3631',
'occupation_desc'=>'PHOTOGRAPHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>440,
'occupation_code'=>'3632',
'occupation_desc'=>'INTERIOR DESIGNERS AND DECORATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>441,
'occupation_code'=>'3433',
'occupation_desc'=>'GALLERY, MUSEUM AND LIBRARY TECHNICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>442,
'occupation_code'=>'3630',
'occupation_desc'=>'CULTURAL ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>443,
'occupation_code'=>'3639',
'occupation_desc'=>'ARTISTIC AND CULTURAL ASSOCIATE PROFESSI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>444,
'occupation_code'=>'3641',
'occupation_desc'=>'CHEFS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>445,
'occupation_code'=>'3711',
'occupation_desc'=>'CUSTOMS AND BORDER INSPECTOR ASSOCIATE P',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>446,
'occupation_code'=>'3712',
'occupation_desc'=>'TAXATION AND EXCISE OFFICIAL ASSOCIATE P',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>447,
'occupation_code'=>'3713',
'occupation_desc'=>'GOVERNMENT SOCIAL BENEFIT OFFICIAL ASSOC',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>448,
'occupation_code'=>'3714',
'occupation_desc'=>'GOVERNMENT LICENSING OFFICIAL ASSOCIATE ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>449,
'occupation_code'=>'3715',
'occupation_desc'=>'POLICE OFFICERS REGULATORY GOVERNMENT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>450,
'occupation_code'=>'3716',
'occupation_desc'=>'CIVIL DEFENCE ASSOCIATE PROFESSIONALS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>451,
'occupation_code'=>'3719',
'occupation_desc'=>'REGULATORY GOVERNMENT ASSOCIATE PROFESSI',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>452,
'occupation_code'=>'4110',
'occupation_desc'=>'GENERAL OFFICE CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>453,
'occupation_code'=>'4120',
'occupation_desc'=>'SECRETARIES (GENERAL)',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>454,
'occupation_code'=>'4131',
'occupation_desc'=>'TYPIST AND WORD PROCESSOR OPERATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>455,
'occupation_code'=>'4132',
'occupation_desc'=>'DATA ENTRY CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>456,
'occupation_code'=>'4211',
'occupation_desc'=>'BANK TELLERS AND RELATED CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>457,
'occupation_code'=>'4213',
'occupation_desc'=>'PAWNBROKERS AND MONEY-LENDERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>458,
'occupation_code'=>'421X',
'occupation_desc'=>'OTHER TELLERS, MONEY COLLECTORS AND RELA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>459,
'occupation_code'=>'4221',
'occupation_desc'=>'TRAVEL CONSULTANTS AND RELATED CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>460,
'occupation_code'=>'4222',
'occupation_desc'=>'CONTACT/CALL CENTRE INFORMATION CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>461,
'occupation_code'=>'4223',
'occupation_desc'=>'TELEPHONE SWITCHBOARD OPERATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>462,
'occupation_code'=>'4224',
'occupation_desc'=>'RECEPTIONISTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>463,
'occupation_code'=>'4225',
'occupation_desc'=>'ENQUIRY CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>464,
'occupation_code'=>'4300',
'occupation_desc'=>'NUMERICAL AND MATERIAL RECORDING CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>465,
'occupation_code'=>'4400',
'occupation_desc'=>'OTHER CLERICAL SUPPORT WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>466,
'occupation_code'=>'5111',
'occupation_desc'=>'TRAVEL ATTENDANTS AND TRAVEL STEWARDS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>467,
'occupation_code'=>'5112',
'occupation_desc'=>'TRANSPORT CONDUCTORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>468,
'occupation_code'=>'5113',
'occupation_desc'=>'TRAVEL GUIDES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>469,
'occupation_code'=>'5120',
'occupation_desc'=>'COOKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>470,
'occupation_code'=>'5130',
'occupation_desc'=>'WAITERS AND BARTENDERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>471,
'occupation_code'=>'5141',
'occupation_desc'=>'HAIRDRESSERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>472,
'occupation_code'=>'5142',
'occupation_desc'=>'BEAUTICIANS AND RELATED WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>473,
'occupation_code'=>'5150',
'occupation_desc'=>'BUILDING AND HOUSEKEEPING SUPERVISORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>474,
'occupation_code'=>'5165',
'occupation_desc'=>'DRIVING INSTRUCTORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>475,
'occupation_code'=>'5169',
'occupation_desc'=>'PERSONAL SERVICES WORKERS NOT ELSEWHERE ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>476,
'occupation_code'=>'5211',
'occupation_desc'=>'STALL AND MARKET SALESPERSONS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>477,
'occupation_code'=>'5212',
'occupation_desc'=>'STREET FOOD SALESPERSONS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>478,
'occupation_code'=>'5221',
'occupation_desc'=>'SHOP KEEPERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>479,
'occupation_code'=>'5222',
'occupation_desc'=>'SHOP SUPERVISORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>480,
'occupation_code'=>'5223',
'occupation_desc'=>'SHOP SALES ASSISTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>481,
'occupation_code'=>'5230',
'occupation_desc'=>'CASHIERS AND TICKET CLERKS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>482,
'occupation_code'=>'5241',
'occupation_desc'=>'FASHION AND OTHER MODELS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>483,
'occupation_code'=>'5242',
'occupation_desc'=>'SALES DEMONSTRATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>484,
'occupation_code'=>'5249',
'occupation_desc'=>'SALES WORKERS NOT ELSEWHERE CLASSIFIED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>485,
'occupation_code'=>'5311',
'occupation_desc'=>'CHILD CARE WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>486,
'occupation_code'=>'5312',
'occupation_desc'=>'TEACHERS AIDE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>487,
'occupation_code'=>'5321',
'occupation_desc'=>'HEALTH CARE ASSISTANTS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>488,
'occupation_code'=>'5329',
'occupation_desc'=>'PERSONAL CARE WORKERS IN HEALTH SERVICES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>489,
'occupation_code'=>'5411',
'occupation_desc'=>'FIRE-FIGHTERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>490,
'occupation_code'=>'5412',
'occupation_desc'=>'POLICE OFFICERS PROTECTIVE SERVICE WORKE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>491,
'occupation_code'=>'5413',
'occupation_desc'=>'PRISON GUARDS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>492,
'occupation_code'=>'5414',
'occupation_desc'=>'SECURITY GUARDS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>493,
'occupation_code'=>'5415',
'occupation_desc'=>'IMMIGRATION/CUSTOM OFFICERS AND ASSISTAN',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>494,
'occupation_code'=>'5419',
'occupation_desc'=>'PROTECTIVE SERVICES WORKERS N.E.C.',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>495,
'occupation_code'=>'6113',
'occupation_desc'=>'GARDENERS, HORTICULTURAL AND NURSERY GRO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>496,
'occupation_code'=>'611X',
'occupation_desc'=>'OTHER MARKET GARDENERS AND CROP GROWERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>497,
'occupation_code'=>'6121',
'occupation_desc'=>'LIVESTOCK AND DAIRY PRODUCERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>498,
'occupation_code'=>'6129',
'occupation_desc'=>'ANIMAL PRODUCERS AND RELATED WORKERS NOT',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>499,
'occupation_code'=>'6130',
'occupation_desc'=>'MIXED CROP AND ANIMAL PRODUCERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>500,
'occupation_code'=>'6210',
'occupation_desc'=>'FORESTRY AND RELATED WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>501,
'occupation_code'=>'6221',
'occupation_desc'=>'FISHERY AND AQUACULTURE PRODUCERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>502,
'occupation_code'=>'6224',
'occupation_desc'=>'HUNTERS AND TRAPPERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>503,
'occupation_code'=>'622X',
'occupation_desc'=>'OTHER SKILLED FISHERY WORKERS, HUNTERS A',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>504,
'occupation_code'=>'6300',
'occupation_desc'=>'SUBSISTENCE FARMERS, FISHERMAN, HUNTERS ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>505,
'occupation_code'=>'7111',
'occupation_desc'=>'HOUSE BUILDERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>506,
'occupation_code'=>'7115',
'occupation_desc'=>'CARPENTERS AND JOINERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>507,
'occupation_code'=>'711X',
'occupation_desc'=>'OTHER BUILDING FRAME AND RELATED TRADES ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>508,
'occupation_code'=>'7126',
'occupation_desc'=>'PLUMBERS AND PIPE FITTERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>509,
'occupation_code'=>'7127',
'occupation_desc'=>'AIR CONDITIONING AND REFRIGERATION MECHA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>510,
'occupation_code'=>'712X',
'occupation_desc'=>'OTHER BUILDING FINISHERS AND RELATED TRA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>511,
'occupation_code'=>'7131',
'occupation_desc'=>'PAINTERS AND RELATED WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>512,
'occupation_code'=>'7132',
'occupation_desc'=>'SPRAY PAINTERS AND VARNISHERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>513,
'occupation_code'=>'7133',
'occupation_desc'=>'BUILDING STRUCTURE CLEANERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>514,
'occupation_code'=>'7211',
'occupation_desc'=>'METAL MOULDERS AND COREMAKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>515,
'occupation_code'=>'7212',
'occupation_desc'=>'WELDERS AND FLAME CUTTERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>516,
'occupation_code'=>'721X',
'occupation_desc'=>'OTHER SHEET AND STRUCTURAL METAL WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>517,
'occupation_code'=>'7223',
'occupation_desc'=>'METAL WORKING MACHINE TOOL SETTERS AND O',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>518,
'occupation_code'=>'722X',
'occupation_desc'=>'OTHER BLACKSMITHS, TOOLMAKERS AND RELATE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>519,
'occupation_code'=>'7231',
'occupation_desc'=>'MOTOR VEHICLE MECHANICS AND REPAIRERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>520,
'occupation_code'=>'7232',
'occupation_desc'=>'AIRCRAFT ENGINE MECHANICS AND REPAIRERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>521,
'occupation_code'=>'7233',
'occupation_desc'=>'AGRICULTURAL AND INDUSTRIAL MACHINERY ME',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>522,
'occupation_code'=>'7234',
'occupation_desc'=>'BICYCLE AND RELATED REPAIRERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>523,
'occupation_code'=>'7239',
'occupation_desc'=>'MACHINERY MECHANICS AND REPAIRERS NOT EL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>524,
'occupation_code'=>'7313',
'occupation_desc'=>'JEWELLERY AND PRECIOUS-METAL WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>525,
'occupation_code'=>'7316',
'occupation_desc'=>'SIGN WRITERS, DECORATIVE PAINTERS, ENGRA',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>526,
'occupation_code'=>'7319',
'occupation_desc'=>'HANDICRAFT WORKERS NOT ELSEWHERE CLASSIF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>527,
'occupation_code'=>'7321',
'occupation_desc'=>'PRE-PRESS WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>528,
'occupation_code'=>'732X',
'occupation_desc'=>'OTHER PRINTING TRADES WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>529,
'occupation_code'=>'7411',
'occupation_desc'=>'BUILDING AND RELATED ELECTRICIANS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>530,
'occupation_code'=>'7412',
'occupation_desc'=>'ELECTRICAL MECHANICS AND FITTERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>531,
'occupation_code'=>'7413',
'occupation_desc'=>'ELECTRICAL LINE INSTALLERS AND REPAIRERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>532,
'occupation_code'=>'7421',
'occupation_desc'=>'ELECTRONICS MECHANICS AND SERVICERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>533,
'occupation_code'=>'7422',
'occupation_desc'=>'INFORMATION AND COMMUNICATIONS TECHNOLOG',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>534,
'occupation_code'=>'7511',
'occupation_desc'=>'MEAT AND FISH PROCESSING WORKERS AND REL',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>535,
'occupation_code'=>'7512',
'occupation_desc'=>'BAKERS, PASTRY AND PASTA COOKS, AND CONF',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>536,
'occupation_code'=>'751X',
'occupation_desc'=>'OTHER FOOD PROCESSING AND RELATED TRADES',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>537,
'occupation_code'=>'7613',
'occupation_desc'=>'WOODWORKING-MACHINE TOOL SETTERS AND OPE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>538,
'occupation_code'=>'761X',
'occupation_desc'=>'OTHER WOOD TREATERS, CABINET-MAKERS AND ',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>539,
'occupation_code'=>'7621',
'occupation_desc'=>'TAILORS, DRESSMAKERS, FURRIERS AND HATTE',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>540,
'occupation_code'=>'7624',
'occupation_desc'=>'UPHOLSTERERS AND RELATED WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>541,
'occupation_code'=>'762X',
'occupation_desc'=>'OTHER GARMENT AND RELATED TRADES WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>542,
'occupation_code'=>'7630',
'occupation_desc'=>'OTHER CRAFT AND RELATED WORKERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>543,
'occupation_code'=>'8100',
'occupation_desc'=>'STATIONARY PLANT AND MACHINE OPERATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>544,
'occupation_code'=>'8211',
'occupation_desc'=>'MECHANICAL MACHINERY ASSEMBLERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>545,
'occupation_code'=>'8219',
'occupation_desc'=>'ASSEMBLERS NOT ELSEWHERE CLASSIFIED',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>546,
'occupation_code'=>'8220',
'occupation_desc'=>'MACHINE-TOOL SETTER-OPERATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>547,
'occupation_code'=>'8310',
'occupation_desc'=>'LOCOMOTIVE ENGINE DRIVERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>548,
'occupation_code'=>'8312',
'occupation_desc'=>'RAILWAY BRAKE, SIGNAL AND SWITCH OPERATO',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>549,
'occupation_code'=>'8320',
'occupation_desc'=>'CAR, VAN AND MOTORCYCLE DRIVERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>550,
'occupation_code'=>'8330',
'occupation_desc'=>'HEAVY TRUCK AND BUS DRIVERS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>551,
'occupation_code'=>'8300',
'occupation_desc'=>'MOBILE PLANT OPERATORS',
'is_active'=>1,

] );


			
Occupations::create( [
'id'=>552,
'occupation_code'=>'8350',
'occupation_desc'=>'SHIPS',
'is_active'=>1,

] );  
			DB::unprepared('SET IDENTITY_INSERT occupations OFF');
    }
}
