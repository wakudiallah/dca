<?php

use Illuminate\Database\Seeder;
use App\Model\Branch;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT branch ON');

        Branch::create( [
'id'=>1,
'branch_code'=>'05002',
'branchname'=>'PENANG',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>2,
'branch_code'=>'05003',
'branchname'=>'IPOH',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>3,
'branch_code'=>'05004',
'branchname'=>'MELAKA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>4,
'branch_code'=>'05005',
'branchname'=>'KLANG',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>5,
'branch_code'=>'05006',
'branchname'=>'KUANTAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>6,
'branch_code'=>'05007',
'branchname'=>'PETALING JAYA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>7,
'branch_code'=>'05008',
'branchname'=>'JOHOR BAHRU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>8,
'branch_code'=>'05009',
'branchname'=>'ALOR SETAR',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>9,
'branch_code'=>'05010',
'branchname'=>'INACTIVE KOTA BHARU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>10,
'branch_code'=>'05011',
'branchname'=>'SEREMBAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>11,
'branch_code'=>'05012',
'branchname'=>'KUALA TERENGGANU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>12,
'branch_code'=>'05013',
'branchname'=>'KUALA LUMPUR',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>13,
'branch_code'=>'05014',
'branchname'=>'SG PETANI',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>14,
'branch_code'=>'05015',
'branchname'=>'BATU CAVES',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>15,
'branch_code'=>'05016',
'branchname'=>'TEBRAU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>16,
'branch_code'=>'05017',
'branchname'=>'BAYAN BARU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>17,
'branch_code'=>'05018',
'branchname'=>'DAMANSARA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>18,
'branch_code'=>'05019',
'branchname'=>'BATU PAHAT',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>19,
'branch_code'=>'05020',
'branchname'=>'CHERAS',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>20,
'branch_code'=>'05021',
'branchname'=>'BUTTERWORTH',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>21,
'branch_code'=>'05022',
'branchname'=>'KOTA KINABALU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>22,
'branch_code'=>'05023',
'branchname'=>'KUCHING',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>23,
'branch_code'=>'05024',
'branchname'=>'KLUANG',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>24,
'branch_code'=>'05025',
'branchname'=>'BANGI',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>25,
'branch_code'=>'05026',
'branchname'=>'MUAR',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>26,
'branch_code'=>'05027',
'branchname'=>'MIRI',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>27,
'branch_code'=>'05028',
'branchname'=>'TAIPING',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>28,
'branch_code'=>'05029',
'branchname'=>'SANDAKAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>29,
'branch_code'=>'05030',
'branchname'=>'KANGAR',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>30,
'branch_code'=>'05031',
'branchname'=>'KEMAMAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>31,
'branch_code'=>'05032',
'branchname'=>'SIBU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>32,
'branch_code'=>'05033',
'branchname'=>'SITIAWAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>33,
'branch_code'=>'05034',
'branchname'=>'KULIM',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>34,
'branch_code'=>'05035',
'branchname'=>'SKUDAI',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>35,
'branch_code'=>'05037',
'branchname'=>'TAWAU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>36,
'branch_code'=>'05038',
'branchname'=>'BINTULU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>37,
'branch_code'=>'05039',
'branchname'=>'PUCHONG',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>38,
'branch_code'=>'05040',
'branchname'=>'SHAH ALAM',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>39,
'branch_code'=>'05041',
'branchname'=>'MUKAH',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>40,
'branch_code'=>'05042',
'branchname'=>'WANGSA MAJU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>41,
'branch_code'=>'05043',
'branchname'=>'KENINGAU',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>42,
'branch_code'=>'05044',
'branchname'=>'KELANA JAYA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>43,
'branch_code'=>'05045',
'branchname'=>'PUTRAJAYA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>44,
'branch_code'=>'05046',
'branchname'=>'KOTA KINABALU MAIN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>45,
'branch_code'=>'05047',
'branchname'=>'KULAIJAYA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>46,
'branch_code'=>'05048',
'branchname'=>'LANGKAWI',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>47,
'branch_code'=>'05049',
'branchname'=>'KAJANG',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>48,
'branch_code'=>'05050',
'branchname'=>'LABUAN',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>49,
'branch_code'=>'05052',
'branchname'=>'KENANGA',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );


			
Branch::create( [
'id'=>50,
'branch_code'=>'05053',
'branchname'=>'TAMAN MOLEK',
'address'=>NULL,
'phone'=>NULL,
'fax'=>'',
'status'=>1
] );

		DB::unprepared('SET IDENTITY_INSERT branch OFF');
    }
}
