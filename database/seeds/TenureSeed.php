<?php

use Illuminate\Database\Seeder;
use App\Model\Tenure;
class TenureSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT tenure ON');

       Tenure::create( [
'id'=>1,
'years'=>1,
'rate'=>3.4,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>2,
'years'=>2,
'rate'=>3.34,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>3,
'years'=>3,
'rate'=>3.34,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>4,
'years'=>4,
'rate'=>3.34,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>5,
'years'=>5,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>6,
'years'=>6,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>7,
'years'=>7,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>8,
'years'=>8,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>9,
'years'=>9,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>10,
'years'=>10,
'rate'=>3.8,
'id_loan'=>1,

] );


			
Tenure::create( [
'id'=>11,
'years'=>1,
'rate'=>3.4,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>12,
'years'=>2,
'rate'=>3.34,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>13,
'years'=>3,
'rate'=>3.34,
'id_loan'=>2,
] );


			
Tenure::create( [
'id'=>14,
'years'=>4,
'rate'=>3.34,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>15,
'years'=>5,
'rate'=>3.8,
'id_loan'=>2,
] );


			
Tenure::create( [
'id'=>16,
'years'=>6,
'rate'=>3.8,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>17,
'years'=>7,
'rate'=>3.8,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>18,
'years'=>8,
'rate'=>3.8,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>19,
'years'=>9,
'rate'=>3.8,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>20,
'years'=>10,
'rate'=>3.8,
'id_loan'=>2,

] );


			
Tenure::create( [
'id'=>21,
'years'=>1,
'rate'=>3.34,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>22,
'years'=>2,
'rate'=>3.34,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>23,
'years'=>3,
'rate'=>3.4,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>24,
'years'=>4,
'rate'=>3.4,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>25,
'years'=>5,
'rate'=>3.8,
'id_loan'=>3,
] );


			
Tenure::create( [
'id'=>26,
'years'=>6,
'rate'=>3.8,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>27,
'years'=>7,
'rate'=>3.8,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>28,
'years'=>8,
'rate'=>3.8,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>29,
'years'=>9,
'rate'=>3.8,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>30,
'years'=>10,
'rate'=>3.8,
'id_loan'=>3,

] );


			
Tenure::create( [
'id'=>31,
'years'=>1,
'rate'=>3.4,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>32,
'years'=>2,
'rate'=>3.34,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>33,
'years'=>3,
'rate'=>3.34,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>34,
'years'=>4,
'rate'=>3.52,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>35,
'years'=>5,
'rate'=>3.54,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>36,
'years'=>6,
'rate'=>3.57,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>37,
'years'=>7,
'rate'=>3.61,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>38,
'years'=>8,
'rate'=>3.62,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>39,
'years'=>9,
'rate'=>3.67,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>40,
'years'=>10,
'rate'=>3.71,
'id_loan'=>4,

] );


			
Tenure::create( [
'id'=>41,
'years'=>1,
'rate'=>4.5,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>42,
'years'=>2,
'rate'=>3.99,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>43,
'years'=>3,
'rate'=>4.5,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>44,
'years'=>4,
'rate'=>4.5,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>45,
'years'=>5,
'rate'=>4.9,
'id_loan'=>5,
] );


			
Tenure::create( [
'id'=>46,
'years'=>6,
'rate'=>4.9,
'id_loan'=>5,
] );


			
Tenure::create( [
'id'=>47,
'years'=>7,
'rate'=>4.9,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>48,
'years'=>8,
'rate'=>4.9,
'id_loan'=>5,
] );


			
Tenure::create( [
'id'=>49,
'years'=>9,
'rate'=>4.9,
'id_loan'=>5,

] );


			
Tenure::create( [
'id'=>50,
'years'=>10,
'rate'=>4.9,
'id_loan'=>5,
] );


			
Tenure::create( [
'id'=>51,
'years'=>1,
'rate'=>4.5,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>52,
'years'=>2,
'rate'=>3.99,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>53,
'years'=>3,
'rate'=>4.5,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>54,
'years'=>4,
'rate'=>4.5,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>55,
'years'=>5,
'rate'=>4.9,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>56,
'years'=>6,
'rate'=>4.9,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>57,
'years'=>7,
'rate'=>4.9,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>58,
'years'=>8,
'rate'=>4.9,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>59,
'years'=>9,
'rate'=>4.9,
'id_loan'=>6,

] );


			
Tenure::create( [
'id'=>60,
'years'=>10,
'rate'=>4.9,
'id_loan'=>6,
] );


			
Tenure::create( [
'id'=>61,
'years'=>1,
'rate'=>4.5,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>62,
'years'=>2,
'rate'=>3.99,
'id_loan'=>7,
] );


			
Tenure::create( [
'id'=>63,
'years'=>3,
'rate'=>4.5,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>64,
'years'=>4,
'rate'=>4.5,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>65,
'years'=>5,
'rate'=>4.9,
'id_loan'=>7,
] );


			
Tenure::create( [
'id'=>66,
'years'=>6,
'rate'=>4.9,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>67,
'years'=>7,
'rate'=>4.9,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>68,
'years'=>8,
'rate'=>4.9,
'id_loan'=>7,
] );


			
Tenure::create( [
'id'=>69,
'years'=>9,
'rate'=>4.9,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>70,
'years'=>10,
'rate'=>4.9,
'id_loan'=>7,

] );


			
Tenure::create( [
'id'=>71,
'years'=>1,
'rate'=>4.5,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>72,
'years'=>2,
'rate'=>3.99,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>73,
'years'=>3,
'rate'=>4.5,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>74,
'years'=>4,
'rate'=>4.5,
'id_loan'=>8,
] );


			
Tenure::create( [
'id'=>75,
'years'=>5,
'rate'=>4.9,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>76,
'years'=>6,
'rate'=>4.9,
'id_loan'=>8,
] );


			
Tenure::create( [
'id'=>77,
'years'=>7,
'rate'=>4.9,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>78,
'years'=>8,
'rate'=>4.9,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>79,
'years'=>9,
'rate'=>4.9,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>80,
'years'=>10,
'rate'=>4.9,
'id_loan'=>8,

] );


			
Tenure::create( [
'id'=>81,
'years'=>1,
'rate'=>4.5,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>82,
'years'=>2,
'rate'=>3.99,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>83,
'years'=>3,
'rate'=>4.5,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>84,
'years'=>4,
'rate'=>4.5,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>85,
'years'=>5,
'rate'=>4.9,
'id_loan'=>9,


] );


			
Tenure::create( [
'id'=>86,
'years'=>6,
'rate'=>4.9,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>87,
'years'=>7,
'rate'=>4.9,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>88,
'years'=>8,
'rate'=>4.9,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>89,
'years'=>9,
'rate'=>4.9,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>90,
'years'=>10,
'rate'=>4.9,
'id_loan'=>9,

] );


			
Tenure::create( [
'id'=>91,
'years'=>1,
'rate'=>4.5,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>92,
'years'=>2,
'rate'=>3.99,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>93,
'years'=>3,
'rate'=>3.99,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>94,
'years'=>4,
'rate'=>4.5,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>95,
'years'=>5,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>96,
'years'=>6,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>97,
'years'=>7,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>98,
'years'=>8,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>99,
'years'=>9,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>100,
'years'=>10,
'rate'=>4.9,
'id_loan'=>10,

] );


			
Tenure::create( [
'id'=>101,
'years'=>1,
'rate'=>4.5,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>102,
'years'=>2,
'rate'=>3.99,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>103,
'years'=>3,
'rate'=>4.5,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>104,
'years'=>4,
'rate'=>4.5,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>105,
'years'=>5,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>106,
'years'=>6,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>107,
'years'=>7,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>108,
'years'=>8,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>109,
'years'=>9,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>110,
'years'=>10,
'rate'=>4.9,
'id_loan'=>11,

] );


			
Tenure::create( [
'id'=>111,
'years'=>1,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>112,
'years'=>2,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>113,
'years'=>3,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>114,
'years'=>4,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>115,
'years'=>5,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>116,
'years'=>6,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>117,
'years'=>7,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>118,
'years'=>8,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>119,
'years'=>9,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>120,
'years'=>10,
'rate'=>5.96,
'id_loan'=>12,

] );


			
Tenure::create( [
'id'=>121,
'years'=>1,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>122,
'years'=>2,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>123,
'years'=>3,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>124,
'years'=>4,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>125,
'years'=>5,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>126,
'years'=>6,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>127,
'years'=>7,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>128,
'years'=>8,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>129,
'years'=>9,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>130,
'years'=>10,
'rate'=>5.96,
'id_loan'=>13,


] );


			
Tenure::create( [
'id'=>131,
'years'=>1,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>132,
'years'=>2,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>133,
'years'=>3,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>134,
'years'=>4,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>135,
'years'=>5,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>136,
'years'=>6,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>137,
'years'=>7,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>138,
'years'=>8,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>139,
'years'=>9,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>140,
'years'=>10,
'rate'=>5.96,
'id_loan'=>14,


] );


			
Tenure::create( [
'id'=>141,
'years'=>1,
'rate'=>4.5,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>142,
'years'=>2,
'rate'=>3.99,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>143,
'years'=>3,
'rate'=>4.5,
'id_loan'=>15,
] );


			
Tenure::create( [
'id'=>144,
'years'=>4,
'rate'=>4.5,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>145,
'years'=>5,
'rate'=>4.9,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>146,
'years'=>6,
'rate'=>4.9,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>147,
'years'=>7,
'rate'=>4.9,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>148,
'years'=>8,
'rate'=>4.9,
'id_loan'=>15,

] );


			
Tenure::create( [
'id'=>149,
'years'=>9,
'rate'=>4.9,
'id_loan'=>15,


] );


			
Tenure::create( [
'id'=>150,
'years'=>10,
'rate'=>4.9,
'id_loan'=>15,


] );


			
Tenure::create( [
'id'=>151,
'years'=>1,
'rate'=>4.5,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>152,
'years'=>2,
'rate'=>3.99,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>153,
'years'=>3,
'rate'=>4.5,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>154,
'years'=>4,
'rate'=>4.5,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>155,
'years'=>5,
'rate'=>4.9,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>156,
'years'=>6,
'rate'=>4.9,
'id_loan'=>16,
] );


			
Tenure::create( [
'id'=>157,
'years'=>7,
'rate'=>4.9,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>158,
'years'=>8,
'rate'=>4.9,
'id_loan'=>16,

] );


			
Tenure::create( [
'id'=>159,
'years'=>9,
'rate'=>4.9,
'id_loan'=>16,


] );


			
Tenure::create( [
'id'=>160,
'years'=>10,
'rate'=>4.9,
'id_loan'=>16,


] );


			
Tenure::create( [
'id'=>161,
'years'=>1,
'rate'=>4.5,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>162,
'years'=>2,
'rate'=>4.5,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>163,
'years'=>3,
'rate'=>4.5,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>164,
'years'=>4,
'rate'=>4.5,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>165,
'years'=>5,
'rate'=>4.9,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>166,
'years'=>6,
'rate'=>4.9,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>167,
'years'=>7,
'rate'=>4.9,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>168,
'years'=>8,
'rate'=>4.9,
'id_loan'=>17,

] );


			
Tenure::create( [
'id'=>169,
'years'=>9,
'rate'=>4.9,
'id_loan'=>17,


] );


			
Tenure::create( [
'id'=>170,
'years'=>10,
'rate'=>4.9,
'id_loan'=>17,


] );
			DB::unprepared('SET IDENTITY_INSERT tenure OFF');
    }
}
