<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT permissions ON');

    		Permission::create( [
				'id'=>1,
				'name'=>'view_user_creations',
				'guard_name'=>'web',
				] );

    		Permission::create( [
				'id'=>2,
				'name'=>'add_user_creations',
				'guard_name'=>'web',
				] );

    		Permission::create( [
				'id'=>3,
				'name'=>'edit_user_creations',
				'guard_name'=>'web',
				] );

    		Permission::create( [
				'id'=>4,
				'name'=>'delete_user_creations',
				'guard_name'=>'web',
				] );

    		Permission::create( [
				'id'=>5,
				'name'=>'view_admin_creations',
				'guard_name'=>'web',
				] );
    		Permission::create( [
				'id'=>6,
				'name'=>'add_admin_creations',
				'guard_name'=>'web',
				] );
    		Permission::create( [
				'id'=>7,
				'name'=>'edit_admin_creations',
				'guard_name'=>'web',
				] );
    		Permission::create( [
				'id'=>8,
				'name'=>'delete_admin_creations',
				'guard_name'=>'web',
				] );
			

		DB::unprepared('SET IDENTITY_INSERT permissions OFF');
    }
}
