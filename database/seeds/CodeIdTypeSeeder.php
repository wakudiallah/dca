<?php

use Illuminate\Database\Seeder;
use App\Model\CodeIDType;

class CodeIDTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT codeidtype ON');

    		CodeIDType::create( [
				'id'=>1,
				'idtypecode'=>'IN',
				'idtypedesc'=>'NEW IC - 12 NUMERIC',
				
				] );

			CodeIDType::create( [
				'id'=>2,
				'idtypecode'=>'IP',
				'idtypedesc'=>'OLD IC - 1 ALPHA 7 NUMERIC',
				
				] );

			CodeIDType::create( [
				'id'=>3,
				'idtypecode'=>'IO',
				'idtypedesc'=>'OLD IC - 1 ALPHA 7 NUMERIC',
				
				] );

			CodeIDType::create( [
				'id'=>4,
				'idtypecode'=>'PN',
				'idtypedesc'=>'POLICE NUMBER',
				
				] );

			CodeIDType::create( [
				'id'=>5,
				'idtypecode'=>'AN',
				'idtypedesc'=>'ARMED FORCES NO',
				
				] );

			CodeIDType::create( [
				'id'=>6,
				'idtypecode'=>'PP',
				'idtypedesc'=>'PASSPORT',
				
				] );

		DB::unprepared('SET IDENTITY_INSERT codeidtype OFF');
    }
}
