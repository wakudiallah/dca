<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT roles ON');

        Role::create( [
			'id'=>0,
			'name'=>'User / Customer',
			'guard_name'=>'web',
			
			] );

         Role::create( [
			'id'=>1,
			'name'=>'Admin DSU',
			'guard_name'=>'web',
			
			] );

         Role::create( [
			'id'=>2,
			'name'=>'Admin 2',
			'guard_name'=>'web',
			
			] );

         Role::create( [
			'id'=>4,
			'name'=>'Admin 3',
			'guard_name'=>'web',
			
			] );

         Role::create( [
			'id'=>8,
			'name'=>'FA',
			'guard_name'=>'web',
			
			] );



		DB::unprepared('SET IDENTITY_INSERT roles OFF');


    }
}
