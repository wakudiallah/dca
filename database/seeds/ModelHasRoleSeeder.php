<?php

use Illuminate\Database\Seeder;
use App\ModelHasRoles;

class ModelHasRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT model_has_roles ON');

        Role::ModelHasRoles( [
			'role_id'=>1,
			'model_type'=>'App\User',
			'model_id'=>'21',
			
			] );


		DB::unprepared('SET IDENTITY_INSERT model_has_roles OFF');
    }
}
