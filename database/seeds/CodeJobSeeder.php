<?php

use Illuminate\Database\Seeder;
use App\Model\CodeJobStatus;

class CodeJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT codejobstatus ON');

        CodeJobStatus::create( [
		'id'=>1,
		'code'=>'01',
		'descriptions'=>'PERMANENT',
		'bnmcode'=>'112',
		'is_active'=>1,
	
		] );


					
		CodeJobStatus::create( [
		'id'=>2,
		'code'=>'02',
		'descriptions'=>'TEMPORARY',
		'bnmcode'=>'112',
		'is_active'=>1,
	
		] );


					
		CodeJobStatus::create( [
		'id'=>3,
		'code'=>'03',
		'descriptions'=>'CONTRACT',
		'bnmcode'=>'112',
		'is_active'=>1,
	
		] );
			DB::unprepared('SET IDENTITY_INSERT codejobstatus OFF');
    }
}
