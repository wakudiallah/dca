<?php

use Illuminate\Database\Seeder;
use App\ScoreCard;

class ScorecardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT scorecard ON');
    		Scorecard::create( [
				'id'=>1,
				'grade'=>'D',
				'score'=>0,
				'score2'=>72,
				'decision'=>'FAIL',
				
				] );
							
				Scorecard::create( [
				'id'=>2,
				'grade'=>'C',
				'score'=>73,
				'score2'=>96,
				'decision'=>'FAIL',
				
				] );
							
				Scorecard::create( [
				'id'=>3,
				'grade'=>'CC',
				'score'=>97,
				'score2'=>122,
				'decision'=>'FAIL',
				
				] );
							
				Scorecard::create( [
				'id'=>4,
				'grade'=>'CCC',
				'score'=>123,
				'score2'=>148,
				'decision'=>'SPECIAL CONSIDERATION',
				
				] );
							
				Scorecard::create( [
				'id'=>5,
				'grade'=>'B',
				'score'=>149,
				'score2'=>172,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>6,
				'grade'=>'BB',
				'score'=>173,
				'score2'=>198,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>7,
				'grade'=>'BBB',
				'score'=>199,
				'score2'=>224,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>8,
				'grade'=>'A',
				'score'=>225,
				'score2'=>248,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>9,
				'grade'=>'AA',
				'score'=>249,
				'score2'=>274,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>10,
				'grade'=>'AAA',
				'score'=>275,
				'score2'=>99999,
				'decision'=>'PASS',
				
				] );
							
				Scorecard::create( [
				'id'=>11,
				'grade'=>'ASA',
				'score'=>3,
				'score2'=>0,
				'decision'=>'0',
				] );
        	

		DB::unprepared('SET IDENTITY_INSERT scorecard OFF');
    }
}
