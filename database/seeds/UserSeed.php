<?php

use Illuminate\Database\Seeder;
use App\Model\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::unprepared('SET IDENTITY_INSERT users ON');

      User::create( [
        'id'=>'21',
        'id_pra'=>'32489e613a1641e899cca83ce24298ef',
        'name'=>'rozita',
        'email'=>'admin@netxpert.com.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>'630dcedd96aa370891b1649a972b577b2f262c90093b1903ae0a2ea63cf2c07c'
        ] );

       User::create( [
        'id'=>'22',
        'id_pra'=>'32489e613a1641e899cdefefeef',
        'name'=>'DCA Admin 1',
        'email'=>'dca_admin1@mbsb.com.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>4,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>'630dcedd96aa370891b1649a972b577b2f262c90093b1903ae0a2ea63cf2c07c'
        ] );
            

      User::create( [
        'id'=>'23',
        'id_pra'=>'32489e613a164sssssdefefeef',
        'name'=>'DCA Admin 2',
        'email'=>'dca_admin2@mbsb.com.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>4,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>'630dcedd96aa370891b1649a972b577b2f262c90093b1903ae0a2ea63cf2c07c'
        ] );
      


      User::create( [
        'id'=>'24',
        'id_pra'=>'32489e61ddddddssdefefeef',
        'name'=>'DCA Admin 3',
        'email'=>'dca_admin3@mbsb.com.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>4,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>'630dcedd96aa370891b1649a972b577b2f262c90093b1903ae0a2ea63cf2c07c'
        ] );

       User::create( [
        'id'=>'11',
        'id_pra'=>'32489e613a1641e899cca83ce2429821',
        'name'=>'FA 11',
        'email'=>'fa11@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'12',
        'id_pra'=>'32489e613a1641e899cca83ce2429822',
        'name'=>'FA 12',
        'email'=>'fa12@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'13',
        'id_pra'=>'32489e613a1641e899cca83ce2429823',
        'name'=>'FA 13',
        'email'=>'fa13@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'14',
        'id_pra'=>'32489e613a1641e899cca83ce2429824',
        'name'=>'FA 14',
        'email'=>'fa14@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

User::create( [
        'id'=>'15',
        'id_pra'=>'32489e613a1641e899cca83ce2429825',
        'name'=>'FA 15',
        'email'=>'fa15@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

         User::create( [
        'id'=>'16',
        'id_pra'=>'32489e613a1641e899cca83ce2429826',
        'name'=>'FA 26',
        'email'=>'fa26@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'17',
        'id_pra'=>'32489e613a1641e899cca83ce2429827',
        'name'=>'FA 27',
        'email'=>'fa7@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'18',
        'id_pra'=>'32489e613a1641e899cca83ce2429828',
        'name'=>'FA 18',
        'email'=>'fa18@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'19',
        'id_pra'=>'32489e613a1641e899cca83ce2429829',
        'name'=>'FA 19',
        'email'=>'fa19@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

User::create( [
        'id'=>'20',
        'id_pra'=>'32489e613a1641e899cca83ce2429830',
        'name'=>'FA 20',
        'email'=>'fa20@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'1',
        'id_pra'=>'32489e613a1641e899cca83ce2429811',
        'name'=>'FA 1',
        'email'=>'fa1@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'2',
        'id_pra'=>'32489e613a1641e899cca83ce2429812',
        'name'=>'FA 2',
        'email'=>'fa2@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'3',
        'id_pra'=>'32489e613a1641e899cca83ce2429813',
        'name'=>'FA 3',
        'email'=>'fa3@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'4',
        'id_pra'=>'32489e613a1641e899cca83ce2429814',
        'name'=>'FA 4',
        'email'=>'fa4@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

User::create( [
        'id'=>'5',
        'id_pra'=>'32489e613a1641e899cca83ce2429815',
        'name'=>'FA 5',
        'email'=>'fa5@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

         User::create( [
        'id'=>'6',
        'id_pra'=>'32489e613a1641e899cca83ce2429816',
        'name'=>'FA 6',
        'email'=>'fa6@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'7',
        'id_pra'=>'32489e613a1641e899cca83ce2429817',
        'name'=>'FA 7',
        'email'=>'fa7@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'8',
        'id_pra'=>'32489e613a1641e899cca83ce2429818',
        'name'=>'FA 8',
        'email'=>'fa8@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

 User::create( [
        'id'=>'9',
        'id_pra'=>'32489e613a1641e899cca83ce2429819',
        'name'=>'FA 9',
        'email'=>'fa9@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

User::create( [
        'id'=>'10',
        'id_pra'=>'32489e613a1641e899cca83ce2429810',
        'name'=>'FA 10',
        'email'=>'fa10@mbsb.insko.my',
        'password'=>'$2y$10$rg12FKTDq6fg0Vqra7b11ulo5JRESZbv.OKX7BwUgcuDzqz5rXqwm',
        'role'=>8,
        'remember_token'=>'eYCDDZV8QNeufIVutI6F9ZH8OgdSvGrHrT4NWRhsVQ8jtpwAiYbG52SSO6mY',
        'activation_code'=>'iJMk9xsbQU0q6JuOh8ckqDwCoyTIqUGAfzD5uQaWO1B8Zc6av25ArhkO6IoPxxkx12BIqoM2eAMbVb2cpgftGzwGjPPIJKwoRkgU',
        'active'=>1,
        'id_branch'=>0,
        'edit'=>0,
        'promosi'=>0,
        'lat'=>0.000000,
        'lng'=>0.000000,
        'location'=>'',
        'mo_id'=>NULL,
        'referral_mo'=>NULL,
        'first_login'=>0,
        'api_token'=>''
        ] );

            DB::unprepared('SET IDENTITY_INSERT users OFF');
    }
}