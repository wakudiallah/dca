<?php

use Illuminate\Database\Seeder;
use App\Model\Relationship;

class RelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::unprepared('SET IDENTITY_INSERT relationships ON');

        Relationship::create( [
'id'=>1,
'relationship_code'=>'CD',
'relationship_desc'=>'Child',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>2,
'relationship_code'=>'FA',
'relationship_desc'=>'Father ',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>3,
'relationship_code'=>'MO',
'relationship_desc'=>'Mother ',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>4,
'relationship_code'=>'SP',
'relationship_desc'=>'Spouse',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>5,
'relationship_code'=>'SB',
'relationship_desc'=>'Siblings',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>6,
'relationship_code'=>'FR',
'relationship_desc'=>'Friends',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>7,
'relationship_code'=>'OT',
'relationship_desc'=>'Others',
'is_active'=>1
] );
			
Relationship::create( [
'id'=>8,
'relationship_code'=>'RL',
'relationship_desc'=>'Relative',
'is_active'=>1
] );

DB::unprepared('SET IDENTITY_INSERT relationships OFF');

    }
}
