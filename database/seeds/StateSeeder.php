<?php

use Illuminate\Database\Seeder;
use App\Model\State2;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT state2s ON');

        State2::create( [
			'id'=>1,
			'state_code'=>'J',
			'state_name'=>'Johor',
			'alias_name'=>'JOHOR',
			'clrt_name'=>'JOHOR'
			] );
						
			State2::create( [
			'id'=>2,
			'state_code'=>'K',
			'state_name'=>'Kedah',
			'alias_name'=>'KEDAH',
			'clrt_name'=>'KEDAH'
			] );
						
			State2::create( [
			'id'=>3,
			'state_code'=>'D',
			'state_name'=>'Kelantan',
			'alias_name'=>'KELANTAN',
			'clrt_name'=>'KELANTAN'
			] );
						
			State2::create( [
			'id'=>4,
			'state_code'=>'W',
			'state_name'=>'Wilayah Persekutuan Kuala Lumpur',
			'alias_name'=>'WP KUALA LUMPUR',
			'clrt_name'=>'W.P KUALA LUMPUR'
			] );
						
			State2::create( [
			'id'=>5,
			'state_code'=>'L',
			'state_name'=>'Wilayah Persekutuan Labuan',
			'alias_name'=>'WP LABUAN',
			'clrt_name'=>'W.P LABUAN'
			] );
						
			State2::create( [
			'id'=>6,
			'state_code'=>'M',
			'state_name'=>'Melaka',
			'alias_name'=>'MELAKA',
			'clrt_name'=>'MELAKA'
			] );
						
			State2::create( [
			'id'=>7,
			'state_code'=>'N',
			'state_name'=>'Negeri Sembilan',
			'alias_name'=>'NEGERI SEMBILAN',
			'clrt_name'=>'NEGERI SEMBILAN'
			] );
						
			State2::create( [
			'id'=>8,
			'state_code'=>'C',
			'state_name'=>'Pahang',
			'alias_name'=>'PAHANG',
			'clrt_name'=>'PAHANG'
			] );
						
			State2::create( [
			'id'=>9,
			'state_code'=>'O',
			'state_name'=>'Wilayah Persekutuan Putra Jaya',
			'alias_name'=>'WP PUTRA JAYA',
			'clrt_name'=>'W.P PUTRAJAYA'
			] );
						
			State2::create( [
			'id'=>10,
			'state_code'=>'R',
			'state_name'=>'Perlis',
			'alias_name'=>'PERLIS',
			'clrt_name'=>'PERLIS'
			] );
						
			State2::create( [
			'id'=>11,
			'state_code'=>'P',
			'state_name'=>'Pulau Pinang',
			'alias_name'=>'PULAU PINANG',
			'clrt_name'=>'PULAU PINANG'
			] );
						
			State2::create( [
			'id'=>12,
			'state_code'=>'A',
			'state_name'=>'Perak',
			'alias_name'=>'PERAK',
			'clrt_name'=>'PERAK'
			] );
						
			State2::create( [
			'id'=>13,
			'state_code'=>'S',
			'state_name'=>'Sabah',
			'alias_name'=>'SABAH',
			'clrt_name'=>'SABAH\r\n'
			] );
						
			State2::create( [
			'id'=>14,
			'state_code'=>'B',
			'state_name'=>'Selangor',
			'alias_name'=>'SELANGOR',
			'clrt_name'=>'SELANGOR'
			] );
						
			State2::create( [
			'id'=>15,
			'state_code'=>'Q',
			'state_name'=>'Sarawak',
			'alias_name'=>'SARAWAK',
			'clrt_name'=>'SARAWAK'
			] );
						
			State2::create( [
			'id'=>16,
			'state_code'=>'T',
			'state_name'=>'Terengganu',
			'alias_name'=>'TERENGGANU',
			'clrt_name'=>'TERENGGANU'
			] );
			DB::unprepared('SET IDENTITY_INSERT state2s OFF');
    }
}
