<?php

use Illuminate\Database\Seeder;
use App\Model\Loan;

class LoanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT loan ON');
		        Loan::create( [
		'id'=>1,
		'id_type'=>1,
		'min_salary'=>1500,
		'max_salary'=>1999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>2,
		'id_type'=>1,
		'min_salary'=>2000,
		'max_salary'=>3000,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>3,
		'id_type'=>1,
		'min_salary'=>3000,
		'max_salary'=>5000,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>4,
		'id_type'=>1,
		'min_salary'=>5001,
		'max_salary'=>9999999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>5,
		'id_type'=>2,
		'min_salary'=>3500,
		'max_salary'=>7000,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>6,
		'id_type'=>2,
		'min_salary'=>7001,
		'max_salary'=>10000,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>7,
		'id_type'=>2,
		'min_salary'=>10001,
		'max_salary'=>9999999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>9,
		'id_type'=>3,
		'min_salary'=>3500,
		'max_salary'=>9999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>65,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>10,
		'id_type'=>3,
		'min_salary'=>10000,
		'max_salary'=>9999999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>80,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>12,
		'id_type'=>4,
		'min_salary'=>3500,
		'max_salary'=>9999,
		'max_bysalary'=>0,
		'max_byammount'=>300000,
		'dsr'=>65,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>13,
		'id_type'=>4,
		'min_salary'=>10000,
		'max_salary'=>9999999,
		'max_bysalary'=>0,
		'max_byammount'=>300000,
		'dsr'=>85,
		'ndi_limit'=>1300,
		] );
					
		Loan::create( [
		'id'=>15,
		'id_type'=>5,
		'min_salary'=>15000,
		'max_salary'=>9999999,
		'max_bysalary'=>0,
		'max_byammount'=>250000,
		'dsr'=>80,
		'ndi_limit'=>1300,
		
		] );

		DB::unprepared('SET IDENTITY_INSERT loan OFF');
    }
}
