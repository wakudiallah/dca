<?php

use Illuminate\Database\Seeder;
use App\Model\Employment;

class EmploymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT employments ON');

    		Employment::create( [
'id'=>1,
'name'=>'GOVERNMENT / GLC COMPANY',
'package_id'=>1,
'code'=>'G',
'create_by'=>'43e89e2475f64344a9d35f4b972c22a2',
'status'=>1
] );


			
Employment::create( [
'id'=>2,
'name'=>'GOVERNMENT SECTOR UNDER CONTRACT STATUS (WITH AT LEAST 5 YEARS OF SERVICE)',
'package_id'=>2,
'code'=>'G',

'create_by'=>'8b42bde89b5b484cbc3f1e0c7615e12a',
'status'=>1
] );


			
Employment::create( [
'id'=>3,
'name'=>'PRIVATE SECTOR WITH SALARY DEDUCTION',
'package_id'=>2,
'code'=>'P',
'create_by'=>'8b42bde89b5b484cbc3f1e0c7615e12a',
'status'=>1
] );


			
Employment::create( [
'id'=>4,
'name'=>'PRIVATE SECTOR WITHOUT SALARY DEDUCTION',
'package_id'=>3,
'code'=>'P',
'create_by'=>'8b42bde89b5b484cbc3f1e0c7615e12a',
'status'=>1
] );


			
Employment::create( [
'id'=>5,
'name'=>'PREMIUM CATEGORY (INCOME >15K PER MONTH)',
'package_id'=>1,
'code'=>'P',
'create_by'=>'8b42bde89b5b484cbc3f1e0c7615e12a',
'status'=>1
] );
        
		DB::unprepared('SET IDENTITY_INSERT employments OFF');
    }
}
