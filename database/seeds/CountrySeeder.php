<?php

use Illuminate\Database\Seeder;
use App\Model\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT countries ON');

    		Country::create( [
				'id'=>1,
				'country_code'=>'AU',
				'country_desc'=>'AUSTRALIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>2,
				'country_code'=>'BD',
				'country_desc'=>'BANGLADESH',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>3,
				'country_code'=>'BE',
				'country_desc'=>'BELGIUM',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>4,
				'country_code'=>'BN',
				'country_desc'=>'BRUNEI',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>5,
				'country_code'=>'CA',
				'country_desc'=>'CANADA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>6,
				'country_code'=>'CH',
				'country_desc'=>'SWITZERLAND',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>7,
				'country_code'=>'CN',
				'country_desc'=>'PEOPLE REPUBLIC OF CHINA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>8,
				'country_code'=>'DE',
				'country_desc'=>'WEST GERMANY',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>9,
				'country_code'=>'DK',
				'country_desc'=>'DENMARK',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>10,
				'country_code'=>'EG',
				'country_desc'=>'EGYPT',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>11,
				'country_code'=>'ES',
				'country_desc'=>'SPAIN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>12,
				'country_code'=>'FR',
				'country_desc'=>'FRANCE',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>13,
				'country_code'=>'GB',
				'country_desc'=>'GREAT BRITAIN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>14,
				'country_code'=>'HK',
				'country_desc'=>'HONG KONG',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>15,
				'country_code'=>'ID',
				'country_desc'=>'INDONESIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>16,
				'country_code'=>'IE',
				'country_desc'=>'IRELAND',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>17,
				'country_code'=>'IL',
				'country_desc'=>'ISRAEL',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>18,
				'country_code'=>'IN',
				'country_desc'=>'INDIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>19,
				'country_code'=>'IQ',
				'country_desc'=>'IRAQ',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>20,
				'country_code'=>'IR',
				'country_desc'=>'IRAN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>21,
				'country_code'=>'IT',
				'country_desc'=>'ITALY',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>22,
				'country_code'=>'JP',
				'country_desc'=>'JAPAN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>23,
				'country_code'=>'KH',
				'country_desc'=>'CAMBODIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>24,
				'country_code'=>'KR',
				'country_desc'=>'SOUTH KOREA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>25,
				'country_code'=>'KW',
				'country_desc'=>'KUWAIT',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>26,
				'country_code'=>'LB',
				'country_desc'=>'LEBANON',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>27,
				'country_code'=>'LK',
				'country_desc'=>'SRI LANKA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>28,
				'country_code'=>'LY',
				'country_desc'=>'LIBYA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>29,
				'country_code'=>'MM',
				'country_desc'=>'MYAMMAR',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>30,
				'country_code'=>'MY',
				'country_desc'=>'MALAYSIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>31,
				'country_code'=>'NL',
				'country_desc'=>'NETHERLANDS',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>32,
				'country_code'=>'NZ',
				'country_desc'=>'NEW ZEELAND',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>33,
				'country_code'=>'PH',
				'country_desc'=>'PHILIPPINES',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>34,
				'country_code'=>'PK',
				'country_desc'=>'PAKISTAN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>35,
				'country_code'=>'PR',
				'country_desc'=>'PERMANENT RESIDENT',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>36,
				'country_code'=>'SA',
				'country_desc'=>'SAUDI ARABIA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>37,
				'country_code'=>'SE',
				'country_desc'=>'SWEDEN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>38,
				'country_code'=>'SG',
				'country_desc'=>'SINGAPORE',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>39,
				'country_code'=>'TH',
				'country_desc'=>'THAILAND',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>40,
				'country_code'=>'TP',
				'country_desc'=>'EAST TIMOR',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>41,
				'country_code'=>'TW',
				'country_desc'=>'TAIWAN',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>42,
				'country_code'=>'US',
				'country_desc'=>'UNITED STATES OF AMERICA',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>43,
				'country_code'=>'VN',
				'country_desc'=>'VIETNAM',
				'is_active'=>1,
				
				] );


							
				Country::create( [
				'id'=>44,
				'country_code'=>'ZA',
				'country_desc'=>'SOUTH AFRICA',
				'is_active'=>1,
				
				] );
        
		DB::unprepared('SET IDENTITY_INSERT countries OFF');
    }
}
