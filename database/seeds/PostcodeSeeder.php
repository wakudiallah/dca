<?php

use Illuminate\Database\Seeder;
use App\Model\Postcode;

class PostcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT postcode ON');

        Postcode::create( [
'id'=>'54000',
'name'=>'Jalan Datuk Keramat',
'id_city'=>1,
'create_by'=>''
] );


			
Postcode::create( [
'id'=>'57000',
'name'=>'Bandar Baru Seri Petaling',
'id_city'=>2,
'create_by'=>''
] );

		DB::unprepared('SET IDENTITY_INSERT postcode OFF');
    }
}
