<?php

use Illuminate\Database\Seeder;
use App\Model\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::unprepared('SET IDENTITY_INSERT type ON');

        Type::create( [
'id'=>1,
'id__employement'=>1,
'id__package'=>1,
'create_by'=>''
] );


			
Type::create( [
'id'=>2,
'id__employement'=>2,
'id__package'=>2,
'create_by'=>''
] );


			
Type::create( [
'id'=>3,
'id__employement'=>3,
'id__package'=>2,
'create_by'=>''
] );


			
Type::create( [
'id'=>4,
'id__employement'=>4,
'id__package'=>3,
'create_by'=>''
] );


			
Type::create( [
'id'=>5,
'id__employement'=>5,
'id__package'=>2,
'create_by'=>''
] );

DB::unprepared('SET IDENTITY_INSERT type OFF');
    }
}
