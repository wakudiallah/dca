<?php

use Illuminate\Database\Seeder;

use App\Mail_System;

class MailSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET IDENTITY_INSERT mail_systems ON');

        Mail_System::create( [
'id'=>1,
'name'=>'MBSB Personal Financing-i',
'email'=>'mbsb@netxpert.com.my'
] );


			
Mail_System::create( [
'id'=>2,
'name'=>'no-reply@mbsb',
'email'=>'mbsb@netxpert.com.my'
] );




DB::unprepared('SET IDENTITY_INSERT mail_systems OFF');
    }
}
