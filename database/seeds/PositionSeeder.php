<?php

use Illuminate\Database\Seeder;
use App\Model\Position;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::unprepared('SET IDENTITY_INSERT position ON');
        
        Position::create( [
'id'=>1,
'position_code'=>'C',
'position_desc'=>'CLERK',
'is_active'=>1
] );
			
Position::create( [
'id'=>2,
'position_code'=>'E',
'position_desc'=>'CHIEF EXECUTIVE OFFICER',
'is_active'=>1
] );
			
Position::create( [
'id'=>3,
'position_code'=>'F',
'position_desc'=>'OFFICER',
'is_active'=>1
] );
			
Position::create( [
'id'=>4,
'position_code'=>'M',
'position_desc'=>'MANAGEMENT',
'is_active'=>1
] );
			
Position::create( [
'id'=>5,
'position_code'=>'N',
'position_desc'=>'NON-CLERICAL',
'is_active'=>1
] );
			
Position::create( [
'id'=>6,
'position_code'=>'O',
'position_desc'=>'OTHER',
'is_active'=>1
] );
			
Position::create( [
'id'=>7,
'position_code'=>'P',
'position_desc'=>'PROFESSIONAL',
'is_active'=>1
] );
			
Position::create( [
'id'=>8,
'position_code'=>'S',
'position_desc'=>'SELF-EMPLOYED',
'is_active'=>1
] );
			
Position::create( [
'id'=>9,
'position_code'=>'A',
'position_desc'=>'CHAIRMAN',
'is_active'=>1
] );
			
Position::create( [
'id'=>10,
'position_code'=>'B',
'position_desc'=>'DIRECTOR',
'is_active'=>1
] );
			
Position::create( [
'id'=>11,
'position_code'=>'D',
'position_desc'=>'DIPLOMAT',
'is_active'=>1
] );
			
Position::create( [
'id'=>12,
'position_code'=>'G',
'position_desc'=>'DRIVER',
'is_active'=>1
] );
			
Position::create( [
'id'=>13,
'position_code'=>'H',
'position_desc'=>'EXECUTIVE',
'is_active'=>1
] );
			
Position::create( [
'id'=>14,
'position_code'=>'I',
'position_desc'=>'GENERAL MANAGER',
'is_active'=>1
] );
			
Position::create( [
'id'=>15,
'position_code'=>'J',
'position_desc'=>'MANAGER',
'is_active'=>1
] );
			
Position::create( [
'id'=>16,
'position_code'=>'K',
'position_desc'=>'MESSENGER',
'is_active'=>1
] );
			
Position::create( [
'id'=>17,
'position_code'=>'L',
'position_desc'=>'PERSONAL ASSISTANT',
'is_active'=>1
] );
			
Position::create( [
'id'=>18,
'position_code'=>'Q',
'position_desc'=>'SECRETARY',
'is_active'=>1
] );
			
Position::create( [
'id'=>19,
'position_code'=>'R',
'position_desc'=>'SUPERVISOR',
'is_active'=>1
] );

	DB::unprepared('SET IDENTITY_INSERT position OFF');

    }
}
