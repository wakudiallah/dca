<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_mo', 200)->nullable();
            $table->string('employer_id', 50)->nullable();
            $table->string('desc_mo', 200)->nullable();
            $table->integer('type')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('id_manager', 250)->nullable();
            $table->string('branch', 50)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mos');
    }
}
