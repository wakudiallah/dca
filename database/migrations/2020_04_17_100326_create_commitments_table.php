<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commitments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('id_praapplication', 32)->nullable();

            $table->string('name1', 50)->nullable();

            $table->decimal('monthly_payment1', 8,2)->nullable();

            $table->string('financing1', 60)->nullable();

            $table->integer('remaining1')->nullable();

            $table->string('name2', 60)->nullable();
            $table->decimal('monthly_payment2', 8,2)->nullable();
            $table->string('financing2', 60)->nullable();

            $table->integer('remaining2')->nullable();



            $table->text('remark')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commitments');
    }
}
