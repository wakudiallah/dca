<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrDocTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_doc_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 10)->nullable();
            $table->string('type', 100)->nullable();
            $table->integer('status')->nullable();
            $table->string('updated_by',50)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_doc_types');
    }
}
