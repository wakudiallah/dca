<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name1', 100)->nullable();
            $table->string('name2', 100)->nullable();
            $table->string('name3', 100)->nullable();

            $table->string('relationship1', 70)->nullable();
            $table->string('relationship2', 70)->nullable();
            $table->string('relationship3', 70)->nullable();

            $table->string('status1', 70)->nullable();
            $table->string('status2', 70)->nullable();
            $table->string('status3', 70)->nullable();

            $table->string('prominent1', 70)->nullable();
            $table->string('prominent2', 70)->nullable();
            $table->string('prominent3', 70)->nullable();

            $table->string('id_praapplication', 70)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peps');
    }
}
