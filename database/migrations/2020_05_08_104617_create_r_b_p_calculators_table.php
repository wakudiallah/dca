<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRBPCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         if(!Schema::hasTable('rbp_calculators'))
        {

            Schema::create('rbp_calculators', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('category', 100)->nullable();
                $table->string('desc', 100)->nullable();
                $table->float('value', 4,2)->nullable();
                $table->string('created_by', 10)->nullable();

                $table->string('updated_by', 10)->nullable();
                $table->string('deleted_by', 10)->nullable();
                $table->timestamps();

                 $table->SoftDeletes();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbp_calculators');
    }
}
