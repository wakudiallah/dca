<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pra',50)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('name', 255)->nullable();
 $table->string('password', 255)->nullable();

            $table->integer('role')->nullable();

            $table->string('remember_token', 100)->nullable();

            
            $table->string('activation_code', 255)->nullable();
            $table->integer('active')->nullable();

            $table->integer('id_branch')->nullable();
            $table->integer('edit')->nullable();
            $table->integer('promosi')->nullable();

            $table->float('lat', 10,6)->nullable();

            $table->float('lng', 10,6)->nullable();
            $table->text('location')->nullable();
            $table->string('mo_id', 32)->nullable();
            $table->string('referral_mo', 40)->nullable();

            $table->integer('first_login')->nullable();
            $table->string('api_token', 250)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
