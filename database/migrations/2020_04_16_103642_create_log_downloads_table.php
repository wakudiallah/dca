<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_download', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 32)->nullable();
            $table->datetime('downloaded_at')->nullable();
            $table->string('id_user', 52)->nullable();
            $table->string('type', 35)->nullable();
            $table->text('activity')->nullable();
            $table->text('log_remark')->nullable();
            $table->text('remark_bank')->nullable();
            $table->text('log_reason')->nullable();
            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_download');
    }
}
