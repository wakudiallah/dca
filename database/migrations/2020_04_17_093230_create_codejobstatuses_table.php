<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodejobstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codejobstatus', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('code', 2)->nullable();
            $table->string('descriptions', 200)->nullable();

            $table->string('bnmcode', 10)->nullable();
            $table->string('is_active', 11)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codejobstatus');
    }
}
