<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTakafulCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('takaful_coverages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',10)->nullable();
            $table->string('desc', 255)->nullable();
            $table->float('value_rbp', 10,2)->nullable();
            $table->string('is_active', 10)->nullable();
            $table->string('created_by', 10)->nullable();

            $table->string('updated_by', 10)->nullable();
            $table->string('deleted_by', 10)->nullable();
            $table->timestamps();

             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('takaful_coverages');
    }
}
