<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 32)->nullable();
            $table->text('name')->nullable();
            $table->text('download')->nullable();
            $table->text('upload')->nullable();
            $table->text('base64')->nullable();
            $table->string('verified_by', 32)->nullable();
            $table->tinyInteger('verification')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
