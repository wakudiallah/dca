<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamPostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_postcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('postcode', 5)->nullable();

            $table->string('area', 70)->nullable();
            $table->string('post_office', 30)->nullable();

            $table->string('state_code', 3)->nullable();

            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_postcodes');
    }
}
