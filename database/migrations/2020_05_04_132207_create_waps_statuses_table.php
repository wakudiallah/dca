<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWapsStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waps_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pra', 10)->nullable();
            $table->string('MOMBsc', 50)->nullable();
            $table->string('MOMEmpInf', 10)->nullable();
            $table->string('MOMSp', 10)->nullable();
            $table->string('MOMRef', 10)->nullable();
            $table->string('MOMDoc', 10)->nullable();
            $table->string('MOMCTrans', 10)->nullable();
            $table->string('MOMPEP', 10)->nullable();
            $table->string('MOMFin', 10)->nullable();
            $table->string('MOMLn', 10)->nullable();
            $table->string('MOMTrm', 10)->nullable();
            $table->string('MOMRsk', 10)->nullable();
            $table->string('MOMDSRA', 10)->nullable();
            $table->string('MOMDSRB', 10)->nullable();
            $table->string('MOMDSRC', 10)->nullable();
            $table->string('MOMDSRE', 10)->nullable();
            $table->string('MOMStat', 10)->nullable();
            $table->integer('status')->nullable();
            $table->string('updated_by',50)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waps_statuses');
    }
}
