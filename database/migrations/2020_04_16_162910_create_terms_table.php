<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remark_bank')->nullable();
            $table->integer('status')->nullable();
            $table->integer('status_application')->nullable();
            $table->string('id_praapplication', 32)->nullable();
            $table->integer('verification_status')->nullable();
            $table->integer('verification_result')->nullable();
            $table->text('verification_remark')->nullable();
            $table->integer('verification_result_by_bank')->nullable();
            $table->string('mo_stage', 10)->nullable();
            $table->string('referral_id', 55)->nullable();
            
            $table->string('verified_by', 30)->nullable();
            $table->string('id_branch', 20)->nullable();
            $table->integer('edit')->nullable();
            $table->integer('promosi')->nullable();
            $table->integer('bahasa')->nullable();
            $table->integer('confirm_approved')->nullable();
            $table->datetime('file_created')->nullable();
            $table->float('aproved_loan')->nullable();
            $table->float('aproved_tenure')->nullable();
            $table->text('rejected_reason')->nullable();
            $table->datetime('time_upload')->nullable();
            $table->integer('purchase_application')->nullable();
            $table->integer('appointment_mbsb')->nullable();
            $table->integer('credit_transactions')->nullable();
            $table->integer('consent_for')->nullable();
            $table->integer('high_networth')->nullable();
            $table->integer('politically')->nullable();
            $table->integer('for_goods')->nullable();
            $table->integer('product_disclosure')->nullable();
            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();
            $table->text('location')->nullable();
            $table->integer('status_waps')->nullable();
            $table->string('email', 100)->nullable();
            $table->string('fullname', 100)->nullable();
            $table->string('mo_id', 100)->nullable();
            $table->integer('count_reload')->nullable();
            $table->integer('anti_attrition_flag')->nullable();
            $table->integer('count_tac')->nullable();
            $table->string('assign_to',100)->nullable();
            $table->string('assign_by',100)->nullable();
            $table->timestamp('submitted_date')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('term');
    }
}
