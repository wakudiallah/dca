<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spouse', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 70)->nullable();
            $table->string('homephone', 20)->nullable();
            $table->string('mobilephone', 20)->nullable();
            $table->string('emptype', 80)->nullable();
            $table->string('emptype_others', 150)->nullable();
            $table->string('id_praapplication', 32)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spouses');
    }
}
