<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanammountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanammount', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('package', 70)->nullable();
            $table->integer('package_id')->nullable();
            $table->float('loanammount', 18, 2)->nullable();
            $table->integer('new_tenure')->nullable();
            $table->float('maxloan', 18, 2)->nullable();
            $table->string('id_praapplication', 32)->nullable();
            
            
            $table->string('id_tenure', 30)->nullable();
            $table->float('installment', 18, 2)->nullable();
            $table->float('rate', 18, 2)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanammounts');
    }
}
