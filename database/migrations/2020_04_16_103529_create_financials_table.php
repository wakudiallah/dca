<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->float('income', 8,2)->nullable();
            $table->float('monthly_income', 90)->nullable();
            $table->float('other_income', 90)->nullable();
            $table->float('total_income', 99)->nullable();
            $table->float('spouse_income', 50)->nullable();
            $table->float('cost_living', 3)->nullable();
            $table->float('monthly_payment', 7, 2)->nullable();
            $table->float('house_rental', 7, 2)->nullable();
            $table->float('fn_monthly_basic', 7, 2)->nullable();
            $table->float('fn_govt_service', 7, 2)->nullable();

            $table->float('fn_financial_incentive', 7, 2)->nullable();
            $table->float('fn_fixed_allowance', 7, 2)->nullable();
            $table->float('fn_cost_of', 7, 2)->nullable();
            $table->float('fn_housing_allowance', 7, 2)->nullable();
            $table->float('fn_special_allowance', 7, 2)->nullable();
            $table->float('fn_gross_income', 7, 2)->nullable();

            $table->float('fn_overtime_allowance', 7, 2)->nullable();
            $table->float('fn_travel', 7, 2)->nullable();
            $table->float('fn_commission', 7, 2)->nullable();
            $table->float('fn_business_income', 7, 2)->nullable();

            $table->integer('fn_yearly_devidend')->nullable();

            $table->float('fn_food_allowance', 7, 2)->nullable();
            $table->float('fn_yearly_contractual', 7, 2)->nullable();
            $table->float('fn_housing_building', 7, 2)->nullable();
            $table->float('fn_non_fixed', 7, 2)->nullable();
            $table->float('fn_share_income', 7, 2)->nullable();
            $table->float('fn_total_income', 7, 2)->nullable();
            $table->float('fn_knwsp', 7, 2)->nullable();
            $table->float('fn_income_tax', 7, 2)->nullable();
            $table->float('fn_cp38', 7, 2)->nullable();
            $table->float('fn_house_financing', 7, 2)->nullable();
            $table->float('fn_others', 7, 2)->nullable();
            $table->float('fn_lembaga_tabung', 7, 2)->nullable();
            $table->float('fn_insurance', 7, 2)->nullable();
            $table->float('fn_perkeso', 7, 2)->nullable();
            $table->float('fn_zakat', 7, 2)->nullable();
            $table->float('fn_bpa', 75)->nullable();
            $table->float('fn_asb')->nullable();
            $table->float('fn_total_payslip', 7, 2)->nullable();
            $table->float('fn_cost_of2', 7, 2)->nullable();

            $table->float('fn_other_non', 7, 2)->nullable();
            $table->float('fn_total_non', 7, 2)->nullable();
            $table->float('fn_total_deductions', 7, 2)->nullable();

            $table->float('fn_total_net', 7, 2)->nullable();

            
            $table->string('fn_location', 125)->nullable();

            $table->double('pemrumah1', 8, 2)->nullable();

            $table->double('pemrumah2', 8, 2)->nullable();

            $table->double('pemrumah3', 8, 2)->nullable();

            $table->double('kadkredit1', 8, 2)->nullable();
            $table->double('kadkredit2', 8, 2)->nullable();
            $table->double('kadkredit3', 8, 2)->nullable();

            $table->double('pempribadi', 8, 2)->nullable();
            $table->double('pemperabot', 8, 2)->nullable();
            $table->double('pemkenderaan', 8, 2)->nullable();
            $table->double('pemlain', 8, 2)->nullable();

            $table->text('pemlain_nyatakan')->nullable();
            $table->double('jumlahpembiayaan', 8, 2)->nullable();
            $table->double('lainlain2', 8, 2)->nullable();

            $table->double('jumlah_pendapatan', 8, 2)->nullable();
            $table->double('l2_jumlahbayaran', 8, 2)->nullable();
            $table->double('l3_bkr', 8, 2)->nullable();

            $table->double('l3_koperasi', 8, 2)->nullable();
            $table->double('l3_bank', 8, 2)->nullable();

            $table->double('l3_lainlain', 8, 2)->nullable();
            $table->double('l3_jumlah', 8, 2)->nullable();
            $table->double('l4_jumlahbayaran', 8, 2)->nullable();


            $table->double('l4_jumlah_pendapatan', 8, 2)->nullable();
            $table->double('l4_dsr', 8, 2)->nullable();
            $table->double('l4_jumlahpinjam', 8, 2)->nullable();

            $table->double('l4_jumlahpinjam2', 8, 2)->nullable();
            $table->double('l4_tempoh', 8, 2)->nullable();
            $table->double('l4_kadar', 8, 2)->nullable();


            $table->double('l4_tempohbulan', 8, 2)->nullable();
            $table->double('l4_ansuran', 8, 2)->nullable();
            $table->double('l4_bayaranbalik', 8, 2)->nullable();

            $table->double('l4_jumlah_pendapatan2', 8, 2)->nullable();
            $table->double('l4_dsr2', 8, 2)->nullable();

            $table->string('note1', 255)->nullable();
            $table->string('note2', 255)->nullable();
            $table->string('note3', 255)->nullable();
            $table->string('note4', 255)->nullable();
            $table->string('note5', 255)->nullable();
            $table->string('note6', 255)->nullable();
            $table->string('note7', 255)->nullable();
            $table->string('note8', 255)->nullable();
            $table->string('note9', 255)->nullable();
            $table->string('note10', 255)->nullable();


            $table->datetime('cal_bulan')->nullable();
            $table->integer('debt_consolidation')->nullable();
            $table->text('remark')->nullable();

            $table->string('product_bundling', 1)->nullable();
            $table->string('product_bundling_specify', 100)->nullable();
            $table->string('cross_selling', 1)->nullable();
            $table->string('cross_selling_specify', 100)->nullable();
            $table->string('takaful_coverage', 1)->nullable();
            $table->string('takaful_coverage_specify', 100)->nullable();
            $table->string('purpose_facility', 100)->nullable();
            $table->string('type_customer', 100)->nullable();

            $table->string('payment_mode', 100)->nullable();
            $table->string('bank_name', 100)->nullable();
            $table->string('account_no', 50)->nullable();
            $table->string('bank1', 100)->nullable();
            $table->string('bank2', 100)->nullable();
            $table->string('bank3', 100)->nullable();
            $table->string('bank4', 100)->nullable();
            $table->string('bank5', 100)->nullable();
            $table->string('bank6', 100)->nullable();

            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financials');
    }
}
