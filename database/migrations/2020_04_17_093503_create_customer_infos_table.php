<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_infos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('ApplRefno', 70)->nullable();
            $table->string('Acid', 20)->nullable();

            $table->string('CustName', 250)->nullable();
            $table->string('CustNewId', 20)->nullable();
            
            $table->string('CustHp', 15)->nullable();

            $table->string('Custemail', 100)->nullable();
            $table->datetime('CustDob')->nullable();

            $table->string('CustMarital', 10)->nullable();
            $table->string('CustReligion', 10)->nullable();
            
            $table->string('CustProfession', 10)->nullable();

            $table->string('CustEmployer', 250)->nullable();
            $table->string('CustNOB', 10)->nullable();

            $table->string('CustDesignation', 10)->nullable();

            $table->datetime('CustEmployDt')->nullable();
            
            $table->string('CustIncomeTy', 10)->nullable();

            $table->decimal('CustIncomeRange', 18,2)->nullable();

            $table->decimal('CustActualIncome', 18,2)->nullable();

            $table->string('CustJobStat', 10)->nullable();

            $table->string('CustEduLvl', 60)->nullable();
            $table->string('CustIncmTyp', 100)->nullable();
            $table->string('CustWrkSec', 60)->nullable();
            $table->decimal('CustFixedIncome', 18,2)->nullable();
            $table->decimal('CustOtherIncome', 18,2)->nullable();
            
            $table->string('PersContact', 60)->nullable();

            $table->string('DepartmentName', 60)->nullable();

            $table->string('PhoneNo', 15)->nullable();

            $table->decimal('DSRRate', 10, 2)->nullable();
            $table->decimal('AppvAmt', 15, 2)->nullable();
            $table->integer('AppvTenure')->nullable();

            $table->decimal('AppvRate', 10, 2)->nullable();
            $table->decimal('AppvInst', 10, 2)->nullable();

            $table->datetime('AcceptDt')->nullable();
            $table->integer('AppvDeferment')->nullable();
            $table->decimal('AppvAddInst', 10,2)->nullable();
            $table->datetime('TransDtTm')->nullable();
            $table->datetime('LoadDT')->nullable();
            $table->string('Act', 10)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_infos');
    }
}
