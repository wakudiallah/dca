<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmploymentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',50)->nullable();
            $table->string('employment_type',100)->nullable();
            $table->string('is_active',10)->nullable();
            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->string('deleted_by', 10)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employment_types');
    }
}
