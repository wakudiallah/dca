<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesettleptiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codesettlepties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('setptycode', 5)->nullable();
            $table->string('SetPtyAbbrv', 3)->nullable();
            $table->string('setptydesc', 100)->nullable();
            $table->string('SetPtyBank_Name', 100)->nullable();
            $table->string('SetPtyRedemptCode', 50)->nullable();
            $table->string('SetPtybene_Name', 100)->nullable();
            $table->string('SetPtyPaymentDesc', 50)->nullable();
            $table->string('SetPtyMBSMOP_Code', 20)->nullable();
            $table->string('SetPtyMBSGL_Code', 20)->nullable();
            $table->string('SetPtyCMSMOP_Code', 20)->nullable();
            $table->string('SetPtyACT', 1)->nullable();
            $table->string('SetPtyAdd1', 50)->nullable();
            $table->string('SetPtyAdd2', 50)->nullable();
            $table->string('SetPtyAdd3', 50)->nullable();
            $table->string('SetPtyAdd4', 50)->nullable();

            $table->string('SetptyPkd', 5)->nullable();
            $table->string('SetRegNo', 12)->nullable();
            $table->string('SetPtyBankAcc', 50)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codesettlepties');
    }
}
