<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDsrBsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsr_b', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->double('financing_amount', 7, 2)->nullable();
            $table->integer('tenure')->nullable();
            $table->double('rate', 7, 2)->nullable();
            $table->double('monthly_repayment',7,2)->nullable();
            $table->double('existing_loan',7,2)->nullable();
            $table->double('total_group_exposure',7,2)->nullable();
            $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsr_bs');
    }
}
