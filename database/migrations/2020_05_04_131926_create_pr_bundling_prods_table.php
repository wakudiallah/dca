<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrBundlingProdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_bundling_prods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 10)->nullable();
            $table->string('desc', 100)->nullable();
            $table->integer('status')->nullable();
            $table->string('updated_by',50)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_bundling_prods');
    }
}
