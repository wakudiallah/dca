<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDsrCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsr_c', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->double('mbsb_housing', 7, 2)->nullable();
            $table->double('mbsb_hire', 7, 2)->nullable();
            $table->double('mbsb_personal', 7, 2)->nullable();
            $table->double('de_angkasa', 7, 2)->nullable();
            $table->double('de_govhousing', 7, 2)->nullable();
            $table->double('de_koperasi', 7, 2)->nullable();
            $table->double('de_nonbiro', 7, 2)->nullable();
            $table->double('de_otherloan', 7, 2)->nullable();
            $table->double('de_other', 7, 2)->nullable();
            $table->double('ccris_housing', 7, 2)->nullable();
            $table->double('ccris_creditcard', 7, 2)->nullable();
            $table->double('ccris_hire', 7, 2)->nullable();
            $table->double('ccris_otherdraft', 7, 2)->nullable();
            $table->double('ccris_other', 7, 2)->nullable();
            $table->double('total_commitment', 10, 2)->nullable();
            $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsr_cs');
    }
}
