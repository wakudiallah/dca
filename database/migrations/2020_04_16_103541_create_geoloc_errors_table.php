<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeolocErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geoloc_errors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('error_status')->nullable();
            $table->string('page_title', 60)->nullable();
            $table->string('header_content', 80)->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geoloc_errors');
    }
}
