<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name1', 66)->nullable();
            $table->string('mobilephone1', 15)->nullable();
            $table->string('home_phone1', 50)->nullable();
            $table->string('relationship1', 100)->nullable();
            $table->string('name2', 66)->nullable();
            $table->string('mobilephone2', 15)->nullable();
            $table->string('home_phone2', 50)->nullable();
            $table->string('relationship2', 100)->nullable();
            $table->string('id_praapplication', 32)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
