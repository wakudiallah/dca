<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDsrDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsr_d', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->string('sec2_verified_by', 75)->nullable();
            $table->timestamp('sec2_verified_time')->nullable();
            $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsr_ds');
    }
}
