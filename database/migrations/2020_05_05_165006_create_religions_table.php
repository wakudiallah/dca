<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReligionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         if(!Schema::hasTable('religions'))
        {

        Schema::create('religions', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('code',100)->nullable();
            $table->string('religion', 255)->nullable();
            $table->string('is_active', 255)->nullable();
            $table->string('created_by', 10)->nullable();

            $table->string('updated_by', 10)->nullable();
            $table->string('deleted_by', 10)->nullable();
            $table->timestamps();

             $table->SoftDeletes();
        });

    }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('religions');
    }
}
