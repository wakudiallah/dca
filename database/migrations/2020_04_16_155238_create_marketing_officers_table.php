<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingOfficersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_officers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employer_id', 32)->nullable();
            $table->string('phoneno', 17)->nullable();
            $table->string('mo_id', 100)->nullable();
            $table->string('manager_id', 100)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_officers');
    }
}
