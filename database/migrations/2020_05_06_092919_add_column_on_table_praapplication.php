<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnTablePraapplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

           Schema::table('praapplications', function (Blueprint $table) {
            $table->integer('age')->nullable();
             $table->string('id_type',10)->nullable();
              $table->string('gender',10)->nullable();
                $table->datetime('dob')->nullable();
                  $table->string('employer',10)->nullable();
                    $table->string('sector',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('praapplications', function (Blueprint $table) {
           
        });
    }
}
