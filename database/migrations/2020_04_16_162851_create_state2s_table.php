<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateState2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state2s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('state_code', 3)->nullable();
            $table->string('state_name', 35)->nullable();
            $table->string('alias_name', 200)->nullable();
            $table->string('clrt_name', 50)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state2s');
    }
}
