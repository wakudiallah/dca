<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskratingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riskrating', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->string('sector', 50)->nullable();
            $table->integer('dsr')->nullable();
            $table->integer('ndi')->nullable();
            $table->integer('salary_deduction')->nullable();
            $table->integer('age')->nullable();
            $table->integer('education_level')->nullable();
            $table->integer('employment')->nullable();
            $table->integer('position')->nullable();
            $table->integer('marital_status')->nullable();
            $table->integer('spouse_employment')->nullable();
            $table->integer('property_loan')->nullable();
            $table->integer('adverse_ccris')->nullable();
            $table->double('total_score', 7, 2)->nullable();
            $table->string('grading', 255)->nullable();
            $table->string('decision', 100)->nullable();
            $table->string('scoring_by', 255)->nullable();
            $table->datetime('date_scoring')->nullable();
            $table->string('reviewed_by', 255)->nullable();
            $table->datetime('date_reviewed')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riskratings');
    }
}
