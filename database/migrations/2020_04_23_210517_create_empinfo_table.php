<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grade', 5)->nullable();
            $table->string('empname',100)->nullable();
            $table->string('emptype',100)->nullable();
            $table->string('emptype_others',200)->nullable();
            $table->integer('employment_id')->nullable();
            $table->integer('employer_id')->nullable();
            $table->string('position', 32)->nullable();
            $table->string('address',250)->nullable();
            $table->string('address2', 250)->nullable();
            $table->string('address3',250)->nullable();
            $table->string('state', 100)->nullable();
            $table->string('state_code',10)->nullable();
            $table->string('postcode', 10)->nullable();
            $table->datetime('joined')->nullable();
            $table->float('salary', 10,2)->nullable();

            $table->float('allowance',10,2)->nullable();
            $table->float('deduction',10,2)->nullable();
            $table->string('id_praapplication',32)->nullable();
            $table->string('occupation', 100)->nullable();
            $table->string('occupation_sector',50)->nullable();
            $table->string('range_of_income', 100)->nullable();
            $table->string('empstatus',50)->nullable();
            $table->string('dept_name', 100)->nullable();
            $table->string('division',70)->nullable();
            $table->string('nature_business', 100)->nullable();
            $table->string('office_phone',20)->nullable();
            $table->string('office_fax', 20)->nullable();
            $table->string('working_exp',10)->nullable();
            $table->string('mbsb_staff', 10)->nullable();
            $table->string('staff_no', 20)->nullable();

            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empinfo');
    }
}
