<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancingPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('financing_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 60)->nullable();
            $table->string('name', 60)->nullable();
            $table->string('category', 60)->nullable();
            $table->decimal('flat_rate', 4,2)->nullable();
            $table->decimal('effective_rate', 4,2)->nullable();

            $table->decimal('min_financing', 10,2)->nullable();
            $table->decimal('max_financing', 10,2)->nullable();
            $table->decimal('min_salary', 10,2)->nullable();
            $table->integer('min_age')->nullable();
            $table->integer('max_age')->nullable();

            $table->decimal('rbp_value', 4,2)->nullable();
            $table->decimal('base_rate', 4,2)->nullable();

            $table->integer('is_time_salary')->nullable();
            $table->integer('time_salary')->nullable();
            $table->string('create_by', 32)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financing_packages');
    }
}
