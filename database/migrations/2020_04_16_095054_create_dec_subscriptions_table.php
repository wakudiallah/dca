<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDecSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dec_subscription', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sub_wealth', 70)->nullable();
            $table->string('gpa', 60)->nullable();
            $table->string('annur', 60)->nullable();
            $table->string('hasanah', 100)->nullable();
            $table->string('wasiat', 100)->nullable();
            $table->string('id_praapplication', 50)->nullable();
            $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dec_subscription');
    }
}
