<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendToWapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_to_waps', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('id_praapplication',32)->nullable();
                $table->integer('ic')->nullable();
            $table->integer('name')->nullable();
            $table->float('AppvAmt',10,2)->nullable();
            $table->float('AppvInst',10,2)->nullable();
            $table->float('AppvRate',10,2)->nullable();
            $table->datetime('AppvDt')->nullable(); 
            $table->integer('AppvTen')->nullable();
             $table->string('AppvRefno',100)->nullable();
              $table->string('AppvStat',100)->nullable();
               $table->string('messages',100)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_to_waps');
    }
}
