<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbpRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('rbp_rates'))
        {

            Schema::create('rbp_rates', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('score_grade', 20)->nullable();
                $table->string('takaful_coverage', 50)->nullable();
                $table->string('product_bundling', 50)->nullable();
                $table->string('payment_method', 50)->nullable();
                $table->string('packge', 50)->nullable();
                $table->string('tenure', 50)->nullable();
                $table->string('type_app', 50)->nullable();
                 $table->float('base_rate', 4,2)->nullable();
                $table->float('effective_profit_rate', 4,2)->nullable();
                $table->string('created_by', 10)->nullable();

                $table->string('updated_by', 10)->nullable();
                $table->string('deleted_by', 10)->nullable();
                $table->timestamps();

                 $table->SoftDeletes();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbp_rates');
    }
}
