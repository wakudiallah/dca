<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDsrAsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsr_a', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->string('sector', 255)->nullable();
            $table->string('gender', 90)->nullable();
            $table->string('category', 90)->nullable();
            $table->string('existing_mbsb', 99)->nullable();
            $table->string('cif_no', 50)->nullable();
            $table->integer('age')->nullable();
            $table->double('basic_salary1', 7, 2)->nullable();
            $table->double('basic_salary2', 7, 2)->nullable();
            $table->double('basic_salary3', 7, 2)->nullable();
            $table->double('fixed_allowances1', 7, 2)->nullable();
            $table->double('fixed_allowances2', 7, 2)->nullable();
            $table->double('fixed_allowances3', 7, 2)->nullable();
            $table->double('total_earnings1', 7, 2)->nullable();
            $table->double('total_earnings2', 7, 2)->nullable();
            $table->double('total_earnings3', 7, 2)->nullable();
            $table->double('variable_income', 7, 2)->nullable();
            $table->double('gmi', 7, 2)->nullable();
            $table->double('epf1', 7, 2)->nullable();
            $table->double('epf2', 7, 2)->nullable();
            $table->double('epf3', 7, 2)->nullable();
            $table->double('socso1', 7, 2)->nullable();
            $table->double('socso2', 7, 2)->nullable();
            $table->double('socso3', 7, 2)->nullable();
            $table->double('income_tax1', 7, 2)->nullable();
            $table->double('income_tax2', 7, 2)->nullable();
            $table->double('income_tax3', 7, 2)->nullable();
            $table->double('other_contribution1', 7, 2)->nullable();
            $table->double('other_contribution2', 7, 2)->nullable();
            $table->double('other_contribution3', 7, 2)->nullable();
            $table->double('total_deduction1', 7, 2)->nullable();
            $table->double('total_deduction2', 7, 2)->nullable();
            $table->double('total_deduction3', 7, 2)->nullable();
            $table->double('nmi', 7, 2)->nullable();
            $table->double('additional_income', 7, 2)->nullable();
            $table->double('ite', 7, 2)->nullable();
            $table->string('sec1_verified_by', 75)->nullable();
            $table->dateTime('sec1_verified_time')->nullable();
            $table->double('total_gross_income', 7, 2)->nullable();
            $table->double('total_net_income', 7, 2)->nullable();
           
            $table->double('basic_salary1a', 200)->nullable();
              $table->float('other_variable_income',7,2)->nullable();
            $table->float('total_variable_income',7,2)->nullable();
            $table->float('total_variable_income_recognised',7,2)->nullable();
             $table->timestamps();
             $table->SoftDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsr_as');
    }
}
