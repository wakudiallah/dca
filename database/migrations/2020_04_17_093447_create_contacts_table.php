<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('address')->nullable();
            $table->string('postcode', 12)->nullable();

            $table->string('city', 32)->nullable();
            $table->string('state', 32)->nullable();

            $table->string('ownership', 100)->nullable();
            $table->string('handphone', 20)->nullable();

            $table->string('fax', 20)->nullable();
            $table->string('id_praapplication', 32)->nullable();


            $table->string('officephone', 32)->nullable();
            $table->text('address2')->nullable();

            $table->string('postcode2', 12)->nullable();
            $table->string('city2', 32)->nullable();

            $table->string('state2', 32)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
