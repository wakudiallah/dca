<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDsrEsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsr_e', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_praapplication', 255)->nullable();
            $table->double('total_debt', 7, 2)->nullable();
            $table->double('net_income', 7, 2)->nullable();
            $table->double('dsr', 10,4)->nullable();
            $table->double('dsr_bnm', 10, 4)->nullable();

            $table->double('ndi', 7, 2)->nullable();

            $table->string('decision', 100)->nullable();

            $table->datetime('data_save')->nullable();
           $table->timestamps();
            $table->SoftDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsr_e');
    }
}
