<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFillUpOnTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('term', function (Blueprint $table) {
            $table->datetime('date_assign_fa')->nullable();
            $table->datetime('date_check_dsr')->nullable();
            $table->string('fill_by',50)->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('term', function (Blueprint $table) {
          
        });
    }
}
