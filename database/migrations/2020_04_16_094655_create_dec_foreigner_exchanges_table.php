<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDecForeignerExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dec_foreigner_exchanges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('foreign1', 70)->nullable();
            $table->string('foreign2', 60)->nullable();
            $table->string('foreign3', 60)->nullable();
            $table->string('foreign4', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('id_praapplication', 50)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dec_foreigner_exchanges');
    }
}
