<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettlementInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlement_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_attrition', 250)->nullable();
            $table->double('id_settlement',20)->nullable();
            $table->string('ACID2',20)->nullable();

            $table->string('CustIDNo2', 50)->nullable();
            $table->integer('MaxAge')->nullable();

            $table->string('DueDate', 200)->nullable();

            $table->decimal('BalOutStanding', 15, 2)->nullable();
            $table->decimal('FullSettlement', 15, 2)->nullable();
            $table->decimal('ProfitEarned', 15, 2)->nullable();
            $table->decimal('Rebate', 15, 2)->nullable();

            $table->integer('PaidInstallment')->nullable();
            $table->integer('RemInstallment')->nullable();

            $table->decimal('DSRUtilise', 10, 2)->nullable();

            $table->integer('DSRUnutilise')->nullable();

            $table->string('CalculateDate', 200)->nullable();

            $table->integer('TaskIDNo')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlement_infos');
    }
}
