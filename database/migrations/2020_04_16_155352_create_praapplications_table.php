<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praapplications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pra',100);

            $table->string('fullname', 255)->nullable();
            $table->string('icnumber', 255)->nullable();
            $table->string('phone', 60)->nullable();

            $table->double('basicsalary', 15, 2)->nullable();
            $table->double('allowance', 15,2)->nullable();
            $table->double('deduction', 15, 2)->nullable();

            $table->integer('id_package')->nullable();
            $table->integer('id_employer')->nullable();
            $table->integer('status')->nullable();
            $table->double('loanamount',15,2)->nullable();

            

            $table->string('email', 200)->nullable();
            $table->string('majikan', 120)->nullable();

            $table->integer('mo_stage')->nullable();
            $table->integer('email_exists')->nullable();


            $table->string('acus_email', 200)->nullable();
            $table->string('referral_id', 32)->nullable();


            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();

            $table->string('location',200)->nullable();
            $table->datetime('date_registration')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praapplications');
    }
}
