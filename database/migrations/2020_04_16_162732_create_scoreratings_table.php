<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreratingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scoreratings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sector', 255)->nullable();
            $table->string('variable', 70)->nullable();
            $table->string('value', 200)->nullable();
            $table->float('float_value', 10, 4)->nullable();
            $table->float('float_value2', 10, 4)->nullable();
            $table->string('score', 32)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scoreratings');
    }
}
