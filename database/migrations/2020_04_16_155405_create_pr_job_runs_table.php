<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrJobRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_job_runs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('JobDate')->nullable();
            $table->string('JobDesc', 200)->nullable();
            $table->string('CreatedBy', 30)->nullable();
            $table->string('Act', 10)->nullable();


            $table->integer('SalIncrement')->nullable();
            $table->decimal('InterestRate', 10,2)->nullable();
            $table->integer('MaxDsr')->nullable();
            $table->integer('MaxAge')->nullable();
            $table->integer('MaxTenor')->nullable();

            $table->string('VarianceRate', 10)->nullable();
            $table->integer('PayOut')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_job_runs');
    }
}
