<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('basics'))
        {
            Schema::create('basics', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name',250)->nullable();
                $table->string('name_prefed',250)->nullable();
                $table->string('id_type',10)->nullable();
                $table->string('new_ic',12)->nullable();
                $table->string('old_ic',12)->nullable();
                $table->string('police_number',20)->nullable();
                $table->datetime('dob')->nullable();
                $table->string('country_dob',100)->nullable();
                $table->string('state_dob',250)->nullable();
                $table->string('gender',10)->nullable();
                $table->string('resident',10)->nullable();
                $table->string('nationality',10)->nullable();
                $table->string('country_origin',10)->nullable();
                $table->string('title',10)->nullable();
                $table->string('title_others',50)->nullable();
                $table->string('marital',10)->nullable();
                $table->string('dependents',10)->nullable();
                $table->string('ownership',10)->nullable();
                $table->string('race',10)->nullable();
                $table->string('bumiputera',10)->nullable();
                $table->string('religion',10)->nullable();
                $table->string('education',10)->nullable();
                $table->string('race_others',50)->nullable();
                $table->string('country_others',250)->nullable();
                $table->string('religion_others',50)->nullable();
                $table->string('anti_attrition_flag',10)->nullable();
                $table->string('acid',250)->nullable();
                $table->string('count_tac',10)->nullable();
                $table->string('otp',10)->nullable();
                $table->boolean('address_correspondence')->nullable();
                $table->longtext('address')->nullable();
                $table->longtext('address2')->nullable();
                $table->longtext('address3')->nullable();
                $table->string('postcode',50)->nullable();
                $table->string('state',50)->nullable();
                $table->string('state_code',50)->nullable();
                $table->longtext('corres_address1')->nullable();
                $table->longtext('corres_address2')->nullable();
                $table->longtext('corres_address3')->nullable();
                $table->string('corres_postcode',50)->nullable();
                $table->string('corres_state',50)->nullable();
                $table->string('corres_state1',50)->nullable();
                $table->string('corres_homephone',50)->nullable();
                $table->string('corres_mobilephone',50)->nullable();
                $table->string('corres_email',250)->nullable();
                $table->string('id_praapplication',50)->nullable();
                $table->string('age',10)->nullable();
                $table->boolean('status')->default('1')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basics');
    }
}
