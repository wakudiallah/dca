<?php
/*
 |--------------------------------------------------------------------------
 | 
 |--------------------------------------------------------------------------
*/
return [   
    /* side menu tab */
    'max_day' => '3',
    'moaqs' => 'http://192.168.0.123/GLOBE/AuthLog.aspx',
    'agent' => 'GLOBAL',
    'submission_p1' =>'- For pending cases (where PFD cannot contact the customer), PFD will e-mail the status of the application for branches to follow-up as indicated at pending column(**)',
    'submission_p2' =>'- If customer cannot be contacted please inform PFD Dept via e-mail immediately.',
    'submission_p3' =>'- For follow-up purposes, Branches within the Regional Office area can get assistance from their Marketing Staff.',
    'submission_p4' =>'- Branches to respond within 1 working day from the date you received the email from Personal Financing Dept to avoid delay in processing the application.',
    
];