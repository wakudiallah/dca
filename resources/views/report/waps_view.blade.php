<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Calculated Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->

<div id="main" role="main">
  	<?php
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
    	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
       		<div class="jarviswidget well" id="wid-id-0">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                    data-widget-colorbutton="false" 
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true" 
                    data-widget-sortable="false"
                -->
	            <header>
		            <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
		            <h2>Widget Title </h2>    
		        </header>
	        	<div>
	        		<div class="jarviswidget-editbox"></div>
	        		<div class="widget-body no-padding">
					{!! Form::open(['url' => 'report/calculated','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                        <fieldset>
							<section>
                                <label class="label">Status </label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='status'>
                                        <option value="6">GLAN</option>
                                        <option value="2">Rejected</option>
                                        <option value="3">Pending Approval</option>
                                    </select>
                                </label>
                            </section>
						  <section >
                              <label class="label">Select Month </label>
                                 <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="bulan" placeholder="Select Month"  class="form-control startmonth"  required>
                                    <b class="tooltip tooltip-bottom-right">Select Month </b>
                                </label>
                            </section>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </fieldset>
                        <div class="modal-footer">
                            <button type="submit" name="submit" class="btn btn-primary">
                                Generate
                            </button>
                        </div>
                    {!! Form::close() !!} <br><br><br>
				</div>
			</div>
		</article>
									
										
								
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div align='center'>
				<div align='right'></div>
                    <br>
                <div class="jarviswidget well" id="wid-id-0">
                    <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                        
                        data-widget-colorbutton="false" 
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true" 
                        data-widget-sortable="false"
                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Widget Title </h2> 
                    </header>
                    <div>
                    	<div class="jarviswidget-editbox">
                    	</div>
                    	<div class="widget-body no-padding">
                    		<table id="example" class="table table-striped table-bordered table-hover" width="100%">
								<thead>			                
									<tr>
										<th>No.</th>
                                        <th>IC Number</th>
                                        <th>Name</th>
										<th>Phone</th>
										<th>Email</th>
                                        <th>Employment</th>
										<th>Employer</th>
                                        <th>Basic Sallary</th>
                                        <th>Laon Amount</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; ?>
                                     @foreach($pra as $pra)
                                     <tr>
										<td>
										   {{ $i }}                              
										</td>
										<td>{{ $pra->status }}</td>
										<td>{{ $pra->status }}</td>
										<td>{{ $pra->phone }}</td>
										<td>{{$pra->email }}</td>
										<td>{{ $pra->status }}</td>
										<td>{{ $pra->status }}</td>
										<td>RM {{ $pra->basicsalary }}</td>
										<td>RM {{ $pra->loanamount }}</td>
									</tr>
									<?php $i++; ?>
									@endforeach
								</tbody>
							</table>
						</div>
							<br><br><br><br>
					</div>
				</div>
			</div>
		</article>
    </div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->


<!-- END PAGE FOOTER -->
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */

	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
				$(document).ready(function() {
					var t = $('#example').DataTable( {
						"columnDefs": [ {
							"searchable": false,
							"orderable": false,
							"targets": 0
						} ],
						"order": [[ 1, 'asc' ]]
					} );
				 
					t.on( 'order.dt search.dt', function () {
						t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = i+1;
						} );
					} ).draw();
				} );
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
 <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>




          
 <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

				$('.startmonth').datepicker({
					changeMonth: true,
					changeYear: true,
				
					dateFormat: 'MM yy',
					onClose: function(dateText, inst) { 
						var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						$(this).datepicker('setDate', new Date(year, month, 1));
					}
				});

        </script>

