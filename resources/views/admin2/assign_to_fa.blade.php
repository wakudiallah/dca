<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "DCA Administrator";

$page_css[] = "your_style.css";
include("asset/inc/header.php");

include("asset/inc/nav.php");

?>



<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<script type="text/javascript" src=""></script>
 <!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bin/push.min.js") }}"></script>

<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @if (Session::has('error'))
            <div class="alert adjusted alert-danger fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('error') }}</strong> 
            </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('success') }}</strong> 
            </div>
        @endif
      
       
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Assign to FA</h2>
                        </header><!-- widget div-->
                        
                        <div>

                            


                            <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/post-assign-to-fa')}}" >
                            

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>                         
                                        <tr>
                                            
                                            <th width="10%">Assign</th>
                                            <th>IC Number</th>
                                            <th>Name</th>
                                            <th>Submit Date</th>
                                            <th>Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; ?>
                                        @foreach ($terma as $term)
                                        <tr>
                                            <td align="center">
                                                <div class="checkbox">

                                                  <input type="hidden"  name="ci[]" value="{{$term->id}}">
                                                  
                                                  <input type="hidden"  name="id2[]" value="{{$term->id}}">

                                                  <input type="hidden"  name="id3[]" value="{{$term->id_prapplication}}">

                                                    <label><input type="checkbox" value="{{$term->id}}" name="id[]" class="friends"></label>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $term->Basic->new_ic }}
                                            </td>
                                            <td>
                                                <?php $name = strtoupper($term->Basic->name ); ?>{{$name}}  
                                            </td>
                                             <td>{{$term->file_created }}</td>
                                              <td><div align='center'>
                                                    @if($term->verification_result ==0)
                                                        @if($term->status==1)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                        @elseif($term->status==77)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-default'><b>Customer Ready Fill Up Form</b></span>
                                                        @elseif($term->status==88)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'><b>Documents Rejected</b></span>
                                                        @else
                                                        @if($term->referral_id!=0)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>Submitted to WAPS</span>
                                                            @elseif($term->referral_id==0)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-info'>Pending Verification</span>
                                                            @endif
                                                        @endif
                                                    @elseif($term->verification_result ==1)                                                
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-success'>Ready for 2nd Verification</span>
                                                    @elseif($term->verification_result ==2)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-primary'>Submitted to Processor/WAPS</span>
                                                        <br>
                                                        @if($term->id_branch=='0')
                                                            <div align='center'> - </div> 
                                                        @else                                            
                                                            <i>{{ $term->Branch->branchname }}</i>
                                                        @endif 
                                                        <br>
                                                        @if($term->financial->l3_jumlah > 0)
                                                            <font color="red"><i>Overlapping Case</i></font>
                                                        @endif
                                                    @elseif($term->verification_result ==3)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Rejected</span>
                                                    @elseif($term->verification_result ==4)
                                                      <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Route Back to User</span>
                                                    @endif
                                                </div>
                                            </td>
                                        @endforeach


                                        </tr>
                                         <?php
                                        $i++; ?>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="col-md-5"></div>

                                                <div class="col-md-7">
                                                <select id="fa" class="form-control" data-live-search="true" data-live-search-style="startsWith" class="select2" name="fa">
                                                  @foreach ($fa as $data)
                                                  <option value="{{$data->id}}" >{{$data->name}} </option>
                                                  @endforeach
                                                </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <button type="submit" name="submit" class="btn btn-primary" onclick='return confirm("Are you sure to assign?")'>
                                                   Assign
                                                </button>

                                            </div>
                                        </div>
                        
                                    </tbody>


                                </table>

                               {!! Form::close() !!} 

                                 

                            </div>
                        </div>

                        
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
          
            "scrollX": true,
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });
                
</script>



<script type="text/javascript">
    $( '#popup-validation' ).on('submit', function(e) {
       if($( 'input[class^="friends"]:checked' ).length === 0) {
          alert( 'Please Select the applican' );
          e.preventDefault();
       }
    });
</script>
<!-- End not select -->











          
