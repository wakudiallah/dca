<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Administrator [First Level Verification]";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	@if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif
	  <div class='col-md-6'>
	  <?php $k=1; ?>
		@foreach ($terma as $termz)
		{{ $termz->PraApplication->email }} - {{ $termz->created_at }}<br>
		
		@endforeach
		na<br>
		</div>
		<div class='col-md-6'>
			@foreach ($pickup as $p)
				
					{{ $p->email }} - {{ $p->created_at }}<br>
			
			@endforeach
		</div>
	  <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Incomplated Application</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
							              <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th class="hidden-xs">IC Number</th>
                                                    <th>Name</th>
													<th class="hidden-xs">Phone</th> 
													<th class="hidden-xs">Email</th> 													
                                                    <th class="hidden-xs">Reg Date</th>
                                                    <th class="hidden-xs">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                 @foreach ($terma as $term)
												
												
                                              <tr>
                                              
                                                <td>
                                                   {{ $i }}                              
                                                </td>
                                                  
                                                  <td class="hidden-xs">
                                              {{ $term->Basic->new_ic }}
                                              <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                                <div id='block{{$i}}'></div>
                                                <script type="text/javascript">

                                                    $(document).ready(function() {
                                                    
                                                    var old_icx = $('#ic99{{$i}}').val();
                                                    
                                                       
                                                    if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                    {
                                                    $('#block{{$i}}').html("<span class='label label-danger'>IC Blocked </span>");
                                                    
                                                    }
                                                    });
                                                  </script>

                                                  
                                                  
                                                  </td>
                                                <td>{{ $term->Basic->name }}</td>
												<td class="hidden-xs">@if(!empty($term->PraApplication->phone)) {{ $term->PraApplication->phone }} @endif</td>
                                                <td>@if(!empty($term->PraApplication->email))  {{ $term->PraApplication->email }} @endif</td>
												<td class="hidden-xs">{{$term->created_at }}</td>
                                               
                                                <td class="hidden-xs" align='center'>
													 <a href="{{ url('/admin/user_detail/'.$term->id_praapplication.'/incomplated_view') }}" class='btn btn-sm btn-default'>View Application</a>  
                                                </td>
    
                                            </tr>
                                              <?php
                                              $i++; ?>
                                            @endforeach
											</tbody>
										</table>
                                          
   

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
  
                
                    </div>
                
                    <!-- end row -->
                
        </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>




          
