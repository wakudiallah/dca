<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Pinjaman Peribadi MBSB ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>

<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
     
     
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	@if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif
	  
	<section id="widget-grid" class="">
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-oceanblue" >
                    <?php $j=1; foreach($terma as $termxz) { { ?>      
                     <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>		
                                <b>{{ $termxz->Basic->name }}</b>
                            </h2>
                        </header>

                    <div class="widget-body">
                        <table id="dt_basic" class='table table-striped table-bordered table-hover'>
							<thead>
							    <tr>
								    <th>Date</th>
							        <th>User</th>
	                                <th>Activity</th>
                                    <!--<th>Location</th>-->
	                                <th>Remark</th>
								    <th>Reason</th>
							    </tr>
                            </thead>
							
                            <tbody>
                                <tr>
                                    <td> {{  $termxz->PraApp->created_at  }}</td>
                                    <td>{{ $termxz->PraApp->fullname }}</td>
                                    @if($termxz->referral_id!='0')
                                    <td> Registered By Marketing Officer</td>
                                        <td>MO Email : {{$termxz->PraApp->email}}<br> Cust. Email : {{$termxz->PraApp->acus_email}}</td>
                                    @else
                                        <td> Customer Registered</td>
                                        <td>Email : {{$termxz->PraApp->email}} </td>
                                    @endif
                                        <td> &nbsp; </td>
                                </tr>
							     @if(!empty($termxz->Log_download->first()->id))
                                    @foreach ($termxz->Log_download as $logfull)
                                    <tr>
                                        <td>{{$logfull->downloaded_at }}</td>
                                            @if($logfull->user->role=='0')
										<td>{{$logfull->user->name}}</td>
                                            @else
                                            <td>MBSB Personal Financing -i</td>
                                        @endif
                                        <td>{{ $logfull->activity }}</td>
                                        <!--<td> 
                                            <a href="{{url('/geolocation/map/'.$logfull->lat.'/'.$logfull->lng.'/'.$logfull->location)}}">
                                                {{  $logfull->location  }} 
                                                @if( $logfull->location!="")
                                                <b>(Click to View Location)</b>
                                                @endif
                                            </a>
                                        </td>-->
                                         <td> {{ $logfull->log_remark }}</td>
										  @if ($logfull->log_reason=="")
    										<td> &nbsp; </td>
    										@else
    										<td> {{ $logfull->log_reason }}</td>
    										@endif
									</tr>
								 @endforeach
							</tbody>
								
							 
							  @endif
							  
												  
						</table>							  
                            <?php  } } ?>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
						
						
		
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
</div>
                        </div></div>
                      
                        </article>
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->


<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					  
						"scrollX": false,
				
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>



