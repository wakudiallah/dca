<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing Officer Page";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<script type="text/javascript" src=""></script>
 <!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bin/push.min.js") }}"></script>

<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @if (Session::has('error'))
            <div class="alert adjusted alert-danger fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('error') }}</strong> 
            </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('success') }}</strong> 
            </div>
        @endif
      
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics Application Under <b><?php print $user->name ; ?></b></h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <?php   
                                    function ringgit($nilai, $pecahan = 0) 
                                        { 
                                            return number_format($nilai, $pecahan, '.', ','); 
                                        }
                                    
                                    $sumloan= 0;
                                    $sumapp= 0;                     
                            
                                    $sumapproved= 0;
                                    $sumloan_approved= 0; 
                                    
                                    $sumrejected= 0; 
                                    $sumloan_rejected= 0; 
                                    
                                    $sumprocess= 0;
                                    $sumloan_process= 0; 
                                    
                                    $sumpendingapproval= 0; 
                                    $sumloan_pendingapproval= 0;

                                    $sumpending2ndadmin= 0; 
                                    $sumloan_pending2ndadmin= 0; 

                                    $sumpendingadmin= 0; 
                                    $sumloan_pendingadmin= 0; 

                                    $sumpendingtenure= 0; 
                                    $sumloan_pendingtenure= 0; 
                                ?>
                                        
                                @foreach ($terma as $termb)
                                    <?php $sumloan = $sumloan + $termb->PraApp->loanamount;
                                        $sumapp = $sumapp + 1; ?>
                                        @if($termb->verification_result_by_bank ==1 AND $termb->status_waps ==1)
                                            <?php   
                                                $sumapproved = $sumapproved + 1; 
                                                $sumloan_approved = $sumloan_approved + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if(($termb->verification_result_by_bank ==2) OR ($termb->verification_result ==3) OR ($termb->status ==88))
                                            <?php   
                                                $sumrejected = $sumrejected + 1; 
                                                $sumloan_rejected = $sumloan_rejected + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if($termb->status_waps ==0 AND $termb->verification_result ==2 AND $termb->verification_status ==1)
                                            <?php  
                                                $sumpendingapproval = $sumpendingapproval + 1;
                                                $sumloan_pendingapproval = $sumloan_pendingapproval + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if($termb->verification_result_by_bank ==0 AND $termb->verification_result ==0 AND $termb->status ==77)
                                            <?php   
                                                $sumprocess = $sumprocess + 1; 
                                                $sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if($termb->verification_result ==1 )
                                            <?php   
                                                $sumpending2ndadmin = $sumpending2ndadmin + 1; 
                                                $sumloan_pending2ndadmin = $sumloan_pending2ndadmin + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if($termb->verification_result ==0 AND $termb->status ==1)
                                            <?php   
                                                $sumpendingadmin = $sumpendingadmin + 1; 
                                                $sumloan_pendingadmin = $sumloan_pendingadmin + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                        @if($termb->status ==0)
                                            <?php   
                                                $sumpendingtenure = $sumpendingtenure + 1; 
                                                $sumloan_pendingtenure = $sumloan_pendingtenure + $termb->PraApp->loanamount; 
                                            ?>
                                        @endif
                                @endforeach
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Total Application</th>
                                            <th>Total Loan Amount Request</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total Approved by Branch </td>
                                            <td>{{ $sumapproved }}</td>
                                            <td>RM {{ ringgit($sumloan_approved) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Rejected by Branch or Admin </td>
                                            <td>{{ $sumrejected }}</td>
                                            <td>RM {{ ringgit($sumloan_rejected) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending WAPS </td>
                                            <td>{{ $sumpendingapproval }}</td>
                                            <td>RM {{ ringgit($sumloan_pendingapproval) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending Verifiction </td>
                                            <td>{{ $sumprocess }}</td>
                                            <td>RM {{ ringgit($sumloan_process) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending DSR  </td>
                                            <td>{{ $sumpendingadmin }}</td>
                                            <td>RM {{ ringgit($sumloan_pendingadmin) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending Financing Eligibility  </td>
                                            <td>{{ $sumpendingtenure }}</td>
                                            <td>RM {{ ringgit($sumloan_pendingtenure) }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>All</b></td>
                                            <td><b>{{ $sumapp }}</b></td>
                                            <td><b>RM {{ ringgit($sumloan) }}</b></td>
                                        </tr>                                     
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </article> 
            </div>
        </section>

        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application List</h2>
                        </header><!-- widget div-->
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>                         
                                        <tr>
                                            <th>No.</th>
                                            <th class="hidden-xs">IC Number</th>
                                            <th>Name</th>
                                            <th class="hidden-xs">Phone</th>
                                            <th>Submit Date</th>
                                            <th>Status</th>
                                            <th>Branch Status</th>
                                            <th class="hidden-xs">Activity</th>
                                            <th>Action</th>
                                            <th class="hidden-xs">Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; ?>
                                         @foreach ($terma as $term)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td class="hidden-xs">{{ $term->Basic->new_ic }}
                                                <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                                    <div id='block{{$i}}'></div>
                                                        <script type="text/javascript">
                                                            $(document).ready(function() {
                                                                var old_icx = $('#ic99{{$i}}').val();
                                                                if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                                    {
                                                                        $('#block{{$i}}').html("<span class='label label-danger'>IC Blocked </span>");
                                                                    }
                                                            });
                                                        </script>
                                            </td>
                                            <td><?php $name = strtoupper($term->Basic->name ); ?>{{$name}}</td>
                                            <td class="hidden-xs">{{ $term->PraApplication->phone }}</td>
                                            <td>{{$term->file_created }}</td>
                                            <td><div align='center'>
                                                    @if($term->verification_result ==0)
                                                        @if($term->status==1)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                        @elseif($term->status==77)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-default'><b>Customer Ready Fill Up Form</b></span>
                                                        @elseif($term->status==88)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'><b>Documents Rejected</b></span>
                                                        @else
                                                        @if($term->referral_id!=0)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>Submitted to WAPS</span>
                                                            @elseif($term->referral_id==0)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-info'>Pending Verification</span>
                                                            @endif
                                                        @endif
                                                    @elseif($term->verification_result ==1)                                                
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-success'>Ready for 2nd Verification</span>
                                                    @elseif($term->verification_result ==2)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-primary'>Submitted to Processor/WAPS</span>
                                                        <br>
                                                        @if($term->id_branch=='0')
                                                            <div align='center'> - </div> 
                                                        @else                                            
                                                            <i>{{ $term->Branch->branchname }}</i>
                                                        @endif 
                                                        <br>
                                                        @if($term->financial->l3_jumlah > 0)
                                                            <font color="red"><i>Overlapping Case</i></font>
                                                        @endif
                                                    @elseif($term->verification_result ==3)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Rejected</span>
                                                    @elseif($term->verification_result ==4)
                                                      <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Route Back to User</span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div align='center'>
                                                    @if($term->verification_result_by_bank >=0 AND $term->verification_result ==2)
                                                        <?php
                                                            $this->soapWrapper = $soap_Wrapper;
                                                            $this->soapWrapper->add('cds', function ($service) {
                                                            $service
                                                                ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                                                            });

                                                            $param=array(
                                                                'AppNo'   => $term->id_praapplication,
                                                                'txnCode' => 'ST003',
                                                                'ActCode' => 'R',
                                                                'usr' => 'momwapsuat',
                                                                'pwd' => 'J@ctL71N$#'
                                                            );

                                                            $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));
                                                            $smsMessages = is_array( $response->MOMCallStatResult)
                                                            ? $response->MOMCallStatResult
                                                            : array( $response->MOMCallStatResult );

                                                            foreach ( $smsMessages as $smsMessage )
                                                                {
                                                                  $result=$smsMessage->ApvStat;
                                                                  //echo $result;
                                                                }
                                                        ?>
                                                        @if($result=='GLAN')
                                                            <span data-toggle="tooltip" title=""  class='label label-success'>Pending WAPS</span>
                                                        @elseif(($result=='DECL'))
                                                            <span data-toggle="tooltip" title=""  class='label label-warning'>Decline</span>
                                                            <script type="text/javascript">
                                                                setTimeout(function(){
                                                                   location.reload();
                                                                },15000000);
                                                            </script>
                                                                 <!--<script type="text/javascript">
                                                                    Push.create("New Process Waps");
                                                                  </script>-->
                                                         @elseif($result=='APVA')
                                                            <span data-toggle="tooltip" title=""  class='label label-info'>Approved with </span>
                                                        @elseif($result=='REJM')
                                                            <span data-toggle="tooltip" title=""  class='label label-danger'>Rejected</span>
                                                        @elseif($result=='ACCP')
                                                            <span data-toggle="tooltip" title=""  class='label label-primary'>Acepted</span>
                                                        @endif
                                                    @endif
                                                </div>  
                                            </td>
                                            <td class="hidden-xs">
                                                <a href="JavaScript:newPopup('{{url('/')}}/activities/{{$term->id_praapplication}}');" class='btn btn-sm btn-default'><i class="fa fa-search"></i></a>
                                            </td>
                                            <td align='center'>
                                                @if(($term->status==1) AND ($term->verification_result==0) )
                                                    <a href="{{ url('/admin/step1/'.$term->id_praapplication.'/verify') }}" class='btn btn-sm btn-warning' ><i class='fa fa-file'></i> Verify Docs</a>
                                                @elseif($term->status==77)
                                                    <a href="{{ url('/admin/step1/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                @elseif(($term->status==1) AND ($term->verification_result==2) )
                                                    <a href="{{ url('/admin/step1/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a><br>
                                                    <a href="{{ url('/admin/user_detail/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                @else
                                                @if($term->status=='99')
                                                    <a href="{{ url('/admin/user_detail/'.$term->id_praapplication.'/verify') }}" class='btn btn-sm btn-success fa fa-book' >Verify Form</a>
                                                @endif 
                                                @if($term->edit=='1')
                                                    @if($term->verification_result !=3)
                                                        @if($term->verification_result !=2)
                                                            <a href="{{url('/')}}/form/approveedit/{{$term->id_praapplication}}" 
                                                            onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                        @endif 
                                                    @endif 
                                                @endif 
                                                @if($term->status=='1')
                                                    @if($term->verification_result !=3)
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="amyModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="amyModal{{$i}}" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                &times;
                                                                            </button>
                                                                            <h4 class="modal-title" id="addBranch">Route Back Application to User</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                         {!! Form::open(['url' => 'form/routeback','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                                                          <fieldset>
                                                                                    <section >
                                                                                          <label class="label">Customer Name</label>
                                                                                            <label class="input">
                                                                                            <input type='text' value='{{ $term->Basic->name}}' readonly disabled/>
                                                                                            <b class="tooltip tooltip-bottom-right">Customer Name</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                                    <section >
                                                                                          <label class="label">IC Number</label>
                                                                                            <label class="input">
                                                                                                <input type='text' value='{{ $term->Basic->new_ic}}' readonly disabled/>
                                                                                            <b class="tooltip tooltip-bottom-right">IC Number</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                                    <section >
                                                                                          <label class="label">Reason</label>
                                                                                            <label class="input">
                                                                                                <textarea id='reason' name='reason' class='form-control' rows="4" cols="73"></textarea>
                                                                                            <b class="tooltip tooltip-bottom-right">Reason</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                        
                                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                  <input type="hidden" name="id_praapplication" value="{{ $term->id_praapplication }}">
                                                                                  <input type="hidden" name="cus_ic" value="{{ $term->Basic->new_ic}}">
                                                                                  <input type="hidden" name="cus_name" value="{{ $term->Basic->name }}">
                                                                                  <input type="hidden" name="cus_email" value="{{ $term->PraApplication->email }}">
                                                                             </fieldset>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                                Cancel
                                                                            </button>
                                                                            <button type="submit" name="submit" class="btn btn-lg txt-color-darken">
                                                                                           Submit
                                                                                        </button>
                                                                                       
                                                                               {!! Form::close() !!}   
                                                                        </div>
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                    @endif 
                                                @endif  
                                            @endif 
                                            </td>
                                            <td class="hidden-xs"> <a href="{{url('/')}}/admin/downloadzip/{{$term->id_praapplication}}">
                                                <img width='24' height='24' src="{{ url('/') }}/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                @if($term->status!=1 AND $term->status!=88 AND $term->status!=77 AND $term->status!=99)
                                                    <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                @endif
                                                @if($term->verification_result_by_bank >=0 AND $term->verification_result ==2)
                                                    <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php
                                        $i++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                                 

                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
        /* // DOM Position key index //
    
            l - Length changing (dropdown)
            f - Filtering input (search)
            t - The Table! (datatable)
            i - Information (records)
            p - Pagination (paging)
            r - pRocessing 
            < and > - div elements
            <"#id" and > - div with an id
            <"class" and > - div with a class
            <"#id.class" and > - div with an id and class
            
            Also see: http://legacy.datatables.net/usage/features
            */  

            /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };
    /* END BASIC */
                
    /* COLUMN SHOW - HIDE */
        $('#datatable_col_reorder').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }           
        });
        /* END COLUMN SHOW - HIDE */
     })
</script>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
          
            "scrollX": true,
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });
                
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 800);
 
});
</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>





          
