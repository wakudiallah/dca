<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submit Application";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
</style>

    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <div id="content">
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>Application form </h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="form-bootstrapWizard">
                                                        <ul class="bootstrapWizard form-wizard">
                                                            <li data-target="#step1">
                                                                <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Maklumat Pelanggan   <i> Customer Information </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step2" class="not-active">
                                                                <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Maklumat Kontak <i> Contact Details </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step3" class="not-active">
                                                                <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Maklumat Pekerjaan <i> Information Of Employment </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step4" class="not-active">
                                                                <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Jumlah Pembiayaan <i> Loan Amount </i></span> </a>
                                                            </li>
                                                            
                                                            </li>
                                                            <li data-target="#step5" class="not-active">
                                                                <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title">Maklumat Pasangan <i> Spouse Information </i></span> </a>
                                                            </li>
                                                            <li data-target="#step6" class="not-active">
                                                                <a href="#tab6" data-toggle="tab"> <span class="step">6</span> <span class="title">Perujuk <i> Reference </i></span> </a>
                                                            </li>
                                                             <li data-target="#step7" class="not-active" >
                                                                <a href="#tab7" data-toggle="tab"> <span class="step">7</span> <span class="title">Latar Belakang Kewangan <i> Financial Background </i></span> </a>
                                                            </li>
                                                            <li data-target="#step8"class="not-active">
                                                                <a href="#tab8" data-toggle="tab"> <span class="step">8</span> <span class="title">Muat Naik Dokumen <i>  Upload Document </i></span> </a>
                                                            </li>
                                                             <li data-target="#step9" class="not-active">
                                                                <a href="#tab9" data-toggle="tab"> <span class="step">9</span> <span class="title">Pengisytiharan <i> Declaration </i></span> </a>
                                                            </li>
                                                             
                                                             
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                        <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Seterusnya / <i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab1">
                                                         @foreach($data as $data)
                                                            <br>
                                                            <h3><strong>Step 1 </strong> - Maklumat Pelanggan   / <i> Customer Information </i></h3>
                                                            <div class="col-md-6">
                                                                <br>
                                                      <div class="form-group">
                                                    <label>1. Negara Asal / <i>Country Of Origin </i> :</label>
                                                     <select class="form-control" id="country" name="country">
                                                     @if(!empty($data->country))
                                                      <option >{{$data->country}} </option>
                                                      @endif
                                                    <option >My Malaysia /<i>Malaysian</i></option>
                                                  
                                                   
                                                    </select>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}" >
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Jenis Pengenalan Diri <i>/ ID Type </i> :</label>
                                                    <select class="form-control" id="card" name="card">
                                                    @if(!empty($data->card))
                                                      <option >{{$data->card}} </option>
                                                      @endif
                                                    <option >Kad Pengenalan Baru / <i>New IC</i></option>
                                                   
                                                    <option >Kad Pengenalan Lama / <i> Old IC</i></option>
                                                    <option >KP Tentera / <i> Army Identity Card</i></option>
                                                    <option >KP Polis / <i> Police Identity Card</i></option>
                                                    </select>
                                                    </div>
                                                    <div class="form-group">
                                                     <label>3. Kewarganegaraan / <i>Citizen </i> :</label>
                                                    <select class="form-control" id="citizen" name="citizen">
                                                    @if(!empty($data->citizen))
                                                      <option>{{$data->citizen}}</option>
                                                      @endif
                                                    <option>Warganegara / <i>Citizen</i></option>
                                               
                                                    </select>
                                                    </div>
                                                    <div class="form-group">
                                                    <label>4. No. Kad Pengenalan Baru / <i>New IC Number </i> :</label>
                                                    <input name="new_ic"  id="icnumber"  type="text" maxlength="12" id="text" value="{{$data->new_ic}}"class="form-control" onkeypress="return isNumberKey(event)" readonly="readonly">

                                                    
                                                    
                                                    </div>
                                                    <div class="form-group">
                                                    <label>5. No. Kad Pengenalan Lama / <i>Old IC Number </i> :</label>
                                                    <input name="old_ic"type="text" value="{{$data->old_ic}}" maxlength="8" id="old_ic" class="form-control"   />
                                                    </div>
                                                    <div class="form-group">
                                                    <label>6. Tarikh Lahir <i>/ Date Of Birth </i> :</label>
                                                    <?php 

                                                   $lahir =  date('d/m/Y', strtotime($data->dob));
                                                            ?>
                                                    <input type="text" maxlength="14" name="dob" value="{{ $lahir  }}"  class="form-control " name="tanggal" readonly="readonly" />
                                                
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                    <label>7. Nama (seperti dalam KP atau Pasport) / <i>Name (As in IC or Passport) </i> :</label>
                                                    <input type="text" maxlength="66" id="text" name="name" value="{{$data->name}}" class="form-control">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                    <label>8. Nama Pilihan (jika ada) / <i>Preferred Name (if any) </i> :</label>
                                                    <input type="text" maxlength="44" id="text" name="name_prefed" value="{{$data->name_prefed}}"class="form-control">
                                                    </div>

                                                     <div class="form-group">
                                            <label>9. Gelaran / <i>Title</i>:</label>
                                           <select class="form-control" name="title">
                                            @if(!empty($data->title))
                                                      <option  value="{{$data->title}}">{{$data->title}} </option>
                                                      @endif
                                                <option> En /<i> Mr </i> </option>
                                                <option > Datuk /<i> Dato'</i></option>
                                                <option >Tan Sri</option>
                                                <option >Puan /<i> Mrs </i></option>
                                                <option >Datin</option>
                                                <option >Puan Sri</option>
                                                 <option >Cik /<i> Miss </i></option>
                                                <option >Dr</option>
                                                <option >Profesor /<i> Professor </i></option>
                                                <option >Yang Berhormat</option>
                                                <option >Lain-lain...</option>
                                          
                                        </select>
                                           
                                        </div>
                                        

                                            
                                         </div>



                                         <div class="col-md-6">
                                                                <br>
                                                                <div class="form-group">
                                            <label>10. Jantina / <i>Gender </i> :</label>
                                            <select class="form-control" name="gender">
                                             @if(!empty($data->gender))
                                                      <option  >{{$data->gender}} </option>
                                                      @endif
                                                <option >Lelaki / <i>Male</i></option>
                                                <option >Perempuan / <i>Female</i></option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>11. Taraf Perkahwinan /<i>Marital Status </i> :</label>
                                             <select class="form-control"  name="marital" id="status">
                                              @if(!empty($data->gender))
                                                      <option  >{{$data->marital}} </option>
                                                      @endif
                                              
                                                <option >Bujang / <i>Single</option>
                                                  <option>Berkahwin / <i>Married</i></option>
                                                <option>Balu / <i>Widowed</i></option>
                                                <option >Bercerai / <i>Divorced</i></option>
                                               
                                            </select>
                                           
                                            
                                            </div>

                                        <div class="form-group">
                                        
                                                <label>
                                                    12. Bilangan Tanggungan / <i>No. of dependants</i>
                                                    <input type="text" class="form-control" maxlength="2" value="{{$data->dependents}}" name="dependents" onkeypress="return isNumberKey(event)">
                                                </label>
                                                
                                           
                                        </div>
                                        <div class="form-group">
                                            <label>13. Bangsa / <i>Race </i> :</label>
                                            <select class="form-control" name="race">
                                            @if(!empty($data->race))
                                                      <option >{{$data->race}} </option>
                                                      @endif
                                                <option >Bumiputera / <i>Melayu</i></option>
                                                <option >India /<i> Indian</i></option>
                                                <option >Cina /<i> Chinese</i></option>
                                                <option >Lain-lain / <i> Others</i></option>
                                            </select>
                                           
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>14. Agama / <i>Religion </i> :</label>
                                             <select class="form-control" name="relegion">
                                             @if(!empty($data->relegion))
                                                      <option >{{$data->relegion}} </option>
                                                      @endif
                                                    <option>Islam / <i>Muslim</i></option>
                                                    <option>Kristian /<i> Christian</option>
                                                    <option >Buddha /<i> Buddhist</i></option>
                                                     <option >Hindu /<i> Hinduism</i></option>
                                                    <option >Lain-lain / <i> Others</i></option>
                                                    
                                                    </select>

                                        </div>
                                        <div class="form-group">
                                            <label>15. Pendidikan Tertinggi Diperolehi / <i>Education</i> :</label>
                                              <select class="form-control" name="education">
                                              @if(!empty($data->education))
                                                      <option  >{{$data->education}} </option>
                                                      @endif
                                                    <option>Rendah / <i>Primary</i></option>
                                                    <option >Menengah /<i> Secondary</i></option>
                                                    <option >Profesional /<i> Professional</i></i></option>
                                                     <option >Tinggi /<i> Tertiary</i></option>
                                                    <option >Lain-lain / <i> Others</i></option>
                                                    
                                                    </select>
                                           
                                        </div>
                                        <div class="form-group">
                                            <label>16. Kategori Pelanggan / <i>Customer Category</i> :</label>
                                            <select class="form-control" name="category">
                                            @if(!empty($data->category))
                                                      <option  >{{$data->category}} </option>
                                                      @endif
                                                <option>Anggota Bank Persatuan/ <i>Member</i></option>
                                                <option >Bukan Anggota Bank Persatuan / <i>Non Member</i></option>
                                               
                                               
                                            </select>
                                        </div>
                                        <div class="form-group">
                                                    <label>17. Nama Ibu / <i>Mother's Malden Name </i> :</label>
                                                     <input type="text" maxlength="44" name="mother_name" 
                                                     value="{{$data->mother_name}}"class="form-control">
                                                
                                                </div>

                                                </div>
                                                        </div>
                                                        @endforeach
                                                        <div class="tab-pane" id="tab2">
                                                         @foreach($contact as $contact)
                                                            <br>
                                                            <h3><strong>Step 2</strong> - Maklumat Kontak / <i> Contact Details </i> </h3>
                                                    
                                                     <div class="col-md-6">
                                                                <br>
                                                                 <div class="form-group">
                                                                <label>1. Alamat Kediaman /<i> Residential Address </i></label>
                                                             <br>
                                                             <textarea class="form-control" maxlength="66" rows="3" name="address">{{$contact->address}}</textarea>
                                                                </div>
                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$contact->id_praapplication}}" >

                                                                <div class="form-group">
                                                                <label>2. Poskod/ <i>Postcode :</i></label>
                                                             <br><input type="text" maxlength="5" id="postcode" value="{{$contact->postcode}}" name="postcode" class="form-control" onkeypress="return isNumberKey(event)"/>
                                                                  </div>
                                                                    <div class="form-group">
                                                    <label>3. Bandar / <i>City :</i></label><br>
                                                      <input type="text" maxlength="15"  value="{{$contact->city}}"  id="city" name="city" class="form-control"></br>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>4. Negeri /<i>State</i> : </label>
                                                     <br><input type="text" maxlength="50" value="{{$contact->state}}" id="state" name="state" class="form-control"></br>
                                                            </div>

                                                       </div>     

                                                        <div class="col-md-6">
                                                                <br>
                                                    <div class="form-group">
                                                    <label>5. Jenis Pemilikan /<i> Type Of Ownership </i> : </label>
                                                   
                                                      <select class="form-control" name="ownership">
                                                      @if(!empty($contact->ownership))
                                                      <option >{{$contact->ownership}} </option>
                                                      @endif
                                                    <option >Sendiri /<i>Own</i></option>
                                                    <option>Sewa /<i>Rental</i></option>
                                                    <option>Lain-lain / <i>Other..</i></option>
                                                  
                                                 
                                                    </select>
                                                    </div>
                                                    <div class="form-group">
                                                    <label>6. No. Untuk Dihubungi / <i>Contact No.</i> :</label>
                                                    <ul>
                                                    <li>Kediaman / <i>House </i> : <br><input type="text" maxlength="12" value="{{$contact->houseno}}"
                                                    name="houseno" onkeypress="return isNumberKey(event)">
                                                    </li>
                                                    <li>Tel.Bimbit / <i>Handphone</i> : <br><input required="required" type="text" maxlength="12" value="{{$contact->handphone}}" 
                                                    name="handphone" readonly="readonly" value="{{$data->phone}}" onkeypress="return isNumberKey(event)"></li>
                                                   <li>No. Tel. Pejabat  /<i>  Office Phone No.   </i>: <br><input type="text" maxlength="12" value="{{$contact->officephone}}" name="officephone" onkeypress="return isNumberKey(event)"  required="required" />
                                                     </li> 
                                                    <li>Faks / <i>Fax</i> : <br><input type="text" maxlength="12" name="fax" value="{{$contact->fax}}" onkeypress="return isNumberKey(event)"></li>
                                                    </li>
                                                      <li>Alamat E-mel / <i>E-mail Address</i> : <br><input type="text" maxlength="50"  name="emel" value="{{$user->email}}" disabled>
                                                      </li>
                                                    </ul>
            
            
                                                    </div>
                                                        </div>
                                                           @endforeach
                                                        </div>
                                                       
                                                        <div class="tab-pane" id="tab3">
                                                            <br>
                                                            <h3><strong>Step 3</strong> - Maklumat Pekerjaan / <i> Information Of Employment </i> </h3>
                                                            <div class="col-md-6">
                                                                <br>
                                                                @foreach($empinfo as $empinfo)
                                                      <!--<div class="form-group">
                                            <label>1. Jenis Pekerjaan / <i> Occupation Sector : </i> </label>

                                           <select name="employment_id" id="Employment" class="form-control" >
                                                        <option value="{{$empinfo->employment_id}}">{{$empinfo->employment->name}}</option>
                                                         @foreach ($employment as $employment)
                                                    <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                                                            @endforeach
                                                    </select>

                                                    
                                            
                                        </div>!-->


<script type="text/javascript">
   $(document).ready(function() {
    @if($empinfo->employment_id ==  '3' )
   
 
     $("#empname2").show();
       $("#empname").hide();
       @else 

          $("#empname2").hide();
       $("#empname").show();
   @endif
   });

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();
   

     $("#Employer").html(" ");
     if(Employment == '3') {
      $("#empname2").show();
       $("#empname").hide();

     }
     else {
           $("#empname2").hide();
            $("#empname").show();

  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });
}

   
});
</script>
                            
                                 <!--<div class="form-group">
                                            <label>2. Majikan / <i> Employers </i>:</label>
                                          
                                              <div id="empname">
                                            <select name="employer_id" id="Employer" class="form-control" > 
                                                  <option value="{{$empinfo->employer_id}}">{{$empinfo->employment->name}}</option>
                                             </select> 
                                             </div>
                                            
                                             <div id="empname2">
                                             <input  type="text" name="empname" class="form-control" value="{{$empinfo->empname}}">
                                             </div>
                                       
                                          
                                        
                                        </div>!-->
                                                    <div class="form-group">
                                                    <label>1. Nama Majikan <i>/ Employer's Name </i> :</label>
                                                    <input type="text" name="emp_name" maxlength="44"  
                                                    value=" @if(!empty($empinfo->empname)) {{$empinfo->empname}}
                                                    @else {{$pra->employer->name}}  @endif " class="form-control">
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$empinfo->id_praapplication}}" >
                                                    </div>

                                                    <div class="form-group">
                                                    <label>2. Alamat Majikan <i>/ Employer's Address </i> :</label>
                                                    <input type="text" name="address" maxlength="120"  value="{{$empinfo->address}}" class="form-control">
                                                    </div>
                                                   <div class="form-group">
                                
                                <label>3. Tarikh Mula Berkhidmat / <i> Date Of Joined </i>:</label>
                                    <input type="text" maxlength="10" id="tanggall" name="joined"  
                                    
                                     @if($empinfo->joined != '0000-00-00')
                                    value="{{$empinfo->joined}}" 
                                    @endif


                                      class="form-control startdate"/>
                                </div> 
                                
                                
                                
                               
                                       </div> 
                                        <div class="col-md-6">
                                            <br>
                               
                                      <div class="form-group">
                                
                                <label>4. Pangkat / <i> Grade </i>:</label>
                                <br><input type="text" id="" maxlength="8"  name="grade" value="{{$empinfo->grade}}" class="form-control">           
                                </div>    
                                <div class="form-group">
                                
                                <label>5. Taraf Jawatan (Tetap/Sementara/Kontrak) / <i> Employment Status (Permanent/Temporary/Contract) </i>:</label>
                                <br><input type="text"  maxlength="22" id="" name="empstatus" value="{{$empinfo->empstatus}}" class="form-control">           
                                </div>   
                                <!--<div class="form-group">
                                            <label>7. Gaji Bulanan  /<i> Basic Salary </i> (RM)  :</label>
                                         
                                           <br><input type="text" id="basicsalary" value="{{$empinfo->salary}}" name="salary" class="form-control">  
                                            </div>

                                            <div class="form-group">
                                            <label>8. Elaun /<i> Allowance </i>   (RM):</label>
                                         
                                           <br><input type="text" id="allowance"  name="allowance" value="{{$empinfo->allowance}}" class="form-control">  
                                            </div>

                                            <div class="form-group">
                                            <label>9. Jumlah Potongan Bulanan Semasa /<i> Deduction </i>   (RM)   :</label>
                                         
                                           <br><input type="text" id="deduction" value="{{$empinfo->deduction}}" name="deduction" class="form-control">  
                                            </div>!-->
                                         
                                         
                                         </div>
                                                        @endforeach  

                                                        </div>

                                                        <div class="tab-pane" id="tab4">
                                                            <br>
                                                            <h3><strong>Step 4</strong> - Jumlah Pembiayaan / <i> Loan Amount </i></h3>
                                                          
                                                               <div class="col-md-6">
                                                                <br>
                                                                  @foreach($loan as $loan)
                                                   <?php
                                            $salary_dsr = $loan->dsr * $total_salary / 100;

                                            $max  =  $salary_dsr * 12 * 10 ;
                                             ?>
                                                
                                                @endforeach
                                                     <div class="form-group">
                                                    <label>1. Pakej   /<i> Package</i>:  </label>
                                                      <input type="text" class="form-control" name="package" placeholder="RM" value="{{$pra->package->name}}" readonly>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                   <input name="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Jumlah Pembiayaan  <i>/ Loan Amount  </i> :</label>
                                                    <input type="text" class="form-control" name="loanammount" placeholder="RM" value="{{$loanammount->loanammount}}" readonly >
                                                    </div>
                                 
                                         <div class="form-group">
                                                    <label>3. Pinjaman Maksima <i>/ Max Loan Eligibility  </i>  (RM):</label>
                                                    <input type="text" class="form-control" name="maxloan" placeholder="RM" readonly 
                                                  @if(!empty($loanammount->maxloan))
                                                    value="{{$loanammount->maxloan}}"
                                                    @else 
                                                     value="{{$max}}"
                                                    @endif
                                                    />
                                                    </div>
                                 

                                     <div class="form-group">
                                            <label>4. Cara Bayaran Balik / <i> Payment Method   </i>  : </label>

                                                    <select class="form-control" name="method">
                                                    @if(!empty($loanammount->method))
                                                      
                                                       <option >{{$loanammount->method}} </option>
                                                      @endif
                                                   
                                                    <option>Biro </option>
                                                    <option>Gajian <i>/ Salary</i></option>
                                                     <option>Pindahan Gaji Majikan (PGM)</option>
                                                     <option>Post Dated  Cheque (PDC)</option>
                                                        <option>Standing Instruction (SI) / Arahan Tetap (AT)</option>

                                                
                                                    </select>

                                        </div>
                                         <div class="form-group">
                                                    <label>5. No Anggota / <i>Membership No.</i> :</label>
                                                    <input type="text" maxlength="14" class="form-control" data-mask="99-999-9999999" name="memberno" value="{{$loanammount->memberno}}" >
                                                    </div>
                                 </div>
                                        <div class="col-md-6">
                                                                <br> <br>
                                        
                                          <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Tempoh</th>
                                                <th>DSR</th>
                                                <th>Ansuran Minima</th>
                                                 <th>Kadar Keuntungan</th>
                                                   <th>Pilih</th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            @foreach($tenure as $tenure)
                                            <?php 

                                                   $bunga2 =  $pra->loanamount * $tenure->rate /100   ;
                                                   $bunga = $bunga2 * $tenure->years;
                                                   $total = $pra->loanamount + $bunga ;
                                                   $bulan = $tenure->years * 12 ;
                                                   $installment =  $total / $bulan ;

                                                   

                                                   if($installment  < $salary_dsr ) {

                                                ?>
                                            <tr>
                                                <td>{{$tenure->years}} years</td>
                                                <td>{{$tenure->loan->dsr}} % </td>
                                                
                                                <td>MYR {{ number_format($installment, 0 , ',' , '.' )  }}   / month </td>
                                              
                                                <td>{{$tenure->rate}} %</td>
                                                <td> 
                                                 <input type="radio" name="id_tenure" value="{{$tenure->id}}" <?php if($tenure->id == $loanammount->id_tenure) {echo 'checked' ; }  ?> >
                                                  </td>
                                            </tr>
                                            <?php } ?>
                                            @endforeach

                                        </tbody>
                                    </table>


                                                            </div>
                                                        </div>

                                                          <div class="tab-pane" id="tab5">
                                                            <br>
                                                            <h3><strong>Step 5</strong> - Maklumat Pasangan / <i> Spouse Information </i> <span id="statuskawin"> (Not Applicable) </span></h3>  
                                                          <div class="col-md-6"  id="tab99">
                                                                <br>
                                                      <div class="form-group"  >
                                                       @foreach($spouse as $spouse)
                                                    <label>1.  Nama Suami/Isteri (Seperti dalam KP)   / <i>Spouse Name (As in IC)   :  </i>  </label>
                                                 <input type="text" maxlength="66" class="form-control" value="{{$spouse->name}}" name="name">
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Tarikh Lahir /<i>Date Of Birth:  </i> </label>
                                                    <input type="text"  maxlength="10"  class="form-control startdate" 
                                                   form-control startdate" 
                                                @if($spouse->dob != '0000-00-00') 
                                                     value="{{$spouse->dob}}" 
                                                     @endif
                                                      name="dob" >
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                    </div>
                                                   <div class="form-group">
                                
                                <label>3. No. Kad Pengenalan Baru /<i>  New IC  NO.</i>:</label>
                                    <input type="text" maxlength="12" value="{{$spouse->new_ic}}" maxlength="12" name="new_ic" class="form-control" onkeypress="return isNumberKey(event)"/>
                                </div> 
                                
                                
                                         <div class="form-group">
                                
                                <label>4. No. Kad Pengenalan Lama /<i>  Old IC No.</i>:</label>
                                    <input type="text" maxlength="8" value="{{$spouse->old_ic}}" name="old_ic" class="form-control" />
                                </div> 
                                  <div class="form-group">
                                
                                <label>5.  No. Tel. / Contact No. </i>:</label>
                                    <input type="text" maxlength="66" value="{{$spouse->handphone}}" id="handphone" name="handphone" class="form-control"/>
                                </div> 
                                
                                
                                 
                                       </div> 
                                        <div class="col-md-6" id="tab98">
                                            <br>
                                <div class="form-group">
                                
                                <label>6.  Nama & Alamat Majikan /<i>  Name & Employer's Address </i>:</label>
                                    <input type="text" maxlength="120" value="{{$spouse->emp_name}}" id="tanggall" name="emp_name" class="form-control"/>
                                </div> 
                                
                            
                                 <div class="form-group">
                                
                                <label>7.  Poskod  /<i>  Postcode  </i>:</label>
                                    <input type="text" maxlength="5" value="{{$spouse->postcode}}" id="postcode2" name="postcode" class="form-control" onkeypress="return isNumberKey(event)"/>
                                </div> 
                                
                             <div class="form-group">
                                
                                <label>8.  Bandar / Negeri  /<i>  City / State   </i>:</label>
                                    <input type="text"  maxlength="50" value="{{$spouse->city}}" id="city2" name="city" class="form-control"/>
                                </div> 
                                    <div class="form-group">
                                
                                <label>9.  No. Tel. Pejabat  /<i>  Office Phone No.   </i>:</label>
                                    <input type="text" maxlength="12" value="{{$spouse->phone}}"id="tanggall" name="phone" class="form-control" onkeypress="return isNumberKey(event)"/>
                                </div> 
                                     
                            
                                @endforeach
                                        
                                         

                                            
                                         </div>
                                                          
                                                        </div>

                                                          <div class="tab-pane" id="tab6">
                                                            <br>
                                                            <h3><strong>Step 6</strong> - Perujuk / <i> Reference </i> </h3>
                                                          @foreach($reference as $reference)
                                                          <div class="col-md-6" >
                                                                <br>
                                                                 <div class="form-group" >
                                                                <label>1.  Nama Perujuk / <i> Reference Name  </i> : </label>
                                                             <input type="text" maxlength="66" name="name" value="{{$reference->name}}" class="form-control">
                                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                                </div>

                                                                <div class="form-group">
                                                                <label>2.   Alamat Kediaman  /<i> Residential Address  </i> : </label>
                                                             <br><textarea maxlength="66" class="form-control"  name="address" rows="3" >{{$reference->address}}</textarea>
                                                                </div>
                                                                    <div class="form-group">
                                                    <label>3. Poskod  / <i>Postcode  :</i></label><br>
                                                      <input type="text" maxlength="5"  name="postcode" id="postcode3" value="{{$reference->postcode}}" class="form-control" onkeypress="return isNumberKey(event)">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>4. Bandar / Negeri /<i> City / State</i> : </label>
                                                     <input type="text" maxlength="50" id="city3" name="state" class="form-control" value="{{$reference->state}}">
                                                            </div>

                                                       </div>     

                                                        <div class="col-md-6">
                                                                <br>
                                                    <div class="form-group">
                                                    <label>5. No. Kad Pengenalan /<i> IC No  </i> : </label>
                                                    <input type="text" maxlength="12"  name="new_ic" value="{{$reference->new_ic}}" class="form-control" onkeypress="return isNumberKey(event)">
                                                    </div>
                                                     <div class="form-group">
                                                   <label>6. Pekerjaan  / <i> Occupation</i> :</label>
                                                    <input type="text" maxlength="22" name="occupation"  class="form-control" value="{{$reference->occupation}}" >
                                                    </div>
                                                    <div class="form-group">
                                                   <label>7. Hubungan   / <i> Relationship</i> :</label>
                                                    <input type="text" maxlength="22" name="relationship"  class="form-control" value="{{$reference->relationship}}" >
                                                    </div>

                                                    <div class="form-group">
                                                    <label>8. No. Untuk Dihubungi / <i>Contact No.</i> :</label>
                                                    <ul>
                                                    <li>Kediaman / <i>House </i> : <br><input type="text" maxlength="12" name="houseno"  value="{{$reference->houseno}}"  onkeypress="return isNumberKey(event)">
                                                    </li>
                                                    <li>Tel.Bimbit / <i>Handphone</i> : <br><input type="text" maxlength="12" name="handphone"  value="{{$reference->handphone}}" required="required" onkeypress="return isNumberKey(event)"></li>
                                                    <li>Pejabat / <i>Office</i> :<br><input type="text" maxlength="12" name="officeno" value="{{$reference->officeno}}" onkeypress="return isNumberKey(event)"></li>
                                                    </ul>
            
            
                                                    </div>
                                                        </div>
                                                        @endforeach
                                                        </div>

                                                          <div class="tab-pane" id="tab7">
                                                            <br>
                                                            <h3><strong>Step 7</strong> - Latar Belakang Kewangan /<i> Financial Background </i> </h3>
                                                          
                                                        @foreach($financial  as $financial)
                                                          <div class="col-md-6">
                                                          
                                                                <br>
                                                                PENDAPATAN (A) /<i> INCOME (A)</i> (RM)
                                                                <br>
                                                                 <div class="form-group">
                                                                <label>1. Gaji Bulanan : Asas + Elaun Tetap / <i> Monthly Income : Basic + Fixed Allowance </i>  </label>
                                                             <input type="text" maxlength="10" name="income"  id="income1" class="form-control income" placeholder="RM" onchange="toFloat('income1')" value="{{number_format((float)$financial->income, 2, '.', '')}}" onkeypress="return isNumberKey(event)">

                                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">                                                      
                                                                </div>

                                                                <div class="form-group">
                                                                <label>2.   Lain-lain Pendapatan  /<i> Other Income </i> : </label>
                                                             <input type="text" maxlength="10" required="required" class="form-control income" name="other_income" placeholder="RM" value="{{number_format((float)$financial->other_income, 2, '.', '')}}" id="income2" onchange="toFloat('income2')"   onkeypress="return isNumberKey(event)">
                                                                </div>
                                                                    <div class="form-group">
                                                    <label>3. Pendapatan Suami / Isteri /<i> Husband / Wife Income   :</i></label><br>
                                                      <input type="text" onchange="toFloat('income3')"   maxlength="10" required="required" class="form-control income" name="spouse_income" placeholder="RM" value="{{number_format((float)$financial->spouse_income, 2, '.', '')}}" id="income3" onkeypress="return isNumberKey(event)"> 
                                                        </div>

                                                        <div class="form-group">
                                                            <label>4. Jumlah Pendapatan / <i>Total Income </i> : </label>
                                                     <input   type="text" maxlength="10" required="required" class="form-control" placeholder="RM" name="total_income" value="{{number_format((float)$financial->income + $financial->other_income +  $financial->spouse_income  , 2, '.', '')}}" id="income4" readonly >
                                                            </div>

                                                       </div>     

                                                      <div class="col-md-6">
                                                           <br>
                                                                PERBELANJAAN (B) /<i> EXPENSES (B)</i> (RM)
                                                                
                                                                <br>
                                                             

                                                                <div class="form-group">
                                                                <label>1.   Lain-lain Perbelanjaan / <i> Other Expenses </i> : </label>
                                                             <input onchange="toFloat('outcome2')"  type="text" maxlength="10" required="required" class="form-control income" placeholder="RM" name="expenses" value="{{number_format((float)$financial->expenses, 2, '.', '')}}" id="outcome2" onkeypress="return isNumberKey(event)">
                                                                </div>
                                                                    <div class="form-group">
                                                    <label>2. Jumlah Ansuran Bulanan /<i> Monthly Payment  :</i></label><br>
                                                      <input onchange="toFloat('outcome3')"  type="text" maxlength="10" required="required" class="form-control income" placeholder="RM" name="monthly_payment" value="{{number_format((float)$financial->monthly_payment, 2, '.', '')}}" id="outcome3" onkeypress="return isNumberKey(event)">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>3. Sewa Rumah / <i> House Rental </i> : </label>
                                                     <input onchange="toFloat('outcome4')"  type="text" maxlength="10" required="required" class="form-control income" placeholder="RM" name="house_rental" value="{{number_format((float)$financial->house_rental, 2, '.', '')}}" id="outcome4" onkeypress="return isNumberKey(event)">
                                                            </div>

                                                            <div class="form-group">
                                                            <label>4. Jumlah Perbelanjaan  /<i> Total Expenses </i> : </label>
                                                     <input type="text" maxlength="10" required="required" class="form-control " placeholder="RM" name="total_expenses" value="{{number_format((float)$financial->cost_living + $financial->expenses + $financial->monthly_payment + $financial->house_rental, 2, '.', '')}} " id="outcome5" readonly>
                                                            </div>
                                                            </div> 
                                                               <div class="col-md-4"></div>
                                                                <div class="col-md-4">
                                                            <div class="form-group ">
                                                            <label> <b> PENDAPATAN BERSIH/<i> NET INCOME </i> (A - B) (RM) : </b></label>
                                                     <input type="text" maxlength="10" required="required" class="form-control md-5" placeholder="RM" name="net_income" value="{{number_format((float)($financial->income + $financial->other_income +  $financial->spouse_income) - ( $financial->expenses + $financial->monthly_payment + $financial->house_rental), 2, '.', '')}}
                                                    " id="totalincome" readonly >
                                                            </div>
                                                            </div>

                                                       
                                                        </div>
                                                         @endforeach
                                                          <div class="tab-pane" id="tab8">
                                                            <br>
                                                            <h3><strong>Step 8</strong> - Muat Naik Dokumen / <i>  Upload Document </i>   (<i>File type must .pdf </i>) </h3>
                                                            <fieldset>
                                            
                                            
                                                  <!--<div class="form-group">
                                                    <label class="col-md-4 control-label">Borang Permohonan  Lengkap diisi dan ditandatangani <a href="<?php print url('/'); ?>/document/1.doc">download</a></label>
                                                    <div class="col-md-8">
      
      <input id="fileupload1" class="btn btn-default" type="file" name="file1">
        <input type="hidden" name="document1" id="documentx1" value="Borang Permohonan Lengkap ">
    &nbsp; <span id="document1"> </span> 
  
@if(!empty($document1->name))
   <span id="document1a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document1->upload}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
       
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-4 control-label">Borang Maklumat Tambahan Permohonan Pembiayaan diisi dan ditandatangani <a href="<?php print url('/'); ?>/document/1.doc">download</a></label>
                                                    <div class="col-md-8">
           <input id="fileupload2" class="btn btn-default" type="file" name="file2" >
        <input type="hidden" name="document2"  id="documentx2"  value="Borang Maklumat Tambahan Permohonan Pembiayaan ">
    &nbsp; <span id="document2"> </span> 
@if(!empty($document2->name))
   <span id="document2a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document2->upload}}"> {{$document2->name}}</a> <i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
                                                    </div>
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <label class="col-md-4 control-label">Borang Maklumat Tambahan Permohonan Pembiayaan diisi dan ditandatangani (Penjamin JIka Ada) <a href="<?php print url('/'); ?>/document/1.doc">download</a></label>
                                                    <div class="col-md-8">
                  <input id="fileupload3" class="btn btn-default" type="file" name="file3" >
        <input type="hidden"   id="documentx3"  name="document3" value="Borang Maklumat Tambahan Permohonan Pembiayaan ">
    &nbsp; <span id="document3"> </span> 
@if(!empty($document3->name))
   <span id="document3a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document3->upload}}"> {{$document3->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
                           
                                                    </div>
                                                </div>
                                             

                                                 <div class="form-group">
                                                    <label class="col-md-4 control-label">Borang Biro Perkhidmatan Angkasa <a href="<?php print url('/'); ?>/document/1.doc">download</a></label>
                                                    <div class="col-md-8">
          <input id="fileupload4"  id="document4"  class="btn btn-default" type="file" name="file4" >
        <input type="hidden" name="document4" id="documentx4"  value="Borang Biro Perkhidmatan Angkasa">
    &nbsp; <span id="document4"> </span> 
@if(!empty($document4->name))
   <span id="document4a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document4->upload}}"> {{$document4->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
             
                                                    </div>
                                                </div>!-->
                                                
                                                <!--<div class="form-group">
                                                    <label class="col-md-4 control-label">Surat Kebenaran Potongan Gaji</label>
                                                    <div class="col-md-8">
                <input id="fileupload5" class="btn btn-default" type="file" name="file5" >
        <input type="hidden" name="document5"  id="documentx5"   value="Surat Kebenaran Potongan Gaji">
    &nbsp; <span id="document5"> </span> 
@if(!empty($document5->name))
   <span id="document5a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document5->upload}}"> {{$document5->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
             
                                                    </div>
                                                </div>-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Salinan Kad Pengenalan</label>
                                                    <div class="col-md-8">
                                                         <input id="fileupload6" class="btn btn-default" type="file" name="file6" >
        <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenalan">
    &nbsp; <span id="document6"> </span> 
   

@if(!empty($document6->name))

   <span id="document6a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document6->upload}}"> {{$document6->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif


             
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Salinan Penyata Gaji Untuk 3 Bulan Terkini</label>
                                                    <div class="col-md-8">
                                                         <input id="fileupload7" class="btn btn-default" type="file" name="file7" >
        <input type="hidden" name="document7"  id="documentx7"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini">
    &nbsp; <span id="document7"> </span> 
    
@if(!empty($document7->name))

   <span id="document7a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif


      @if(!empty($document10->name))
      <br>

   <span id="document6a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document6->upload}}"> {{$document10->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif

   @if(!empty($document11->name))
    <br>
   <span id="document6a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document6->upload}}"> {{$document11->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif

             
                                                    </div>
                                                </div>
                                                <!--<div class="form-group">
                                                    <label class="col-md-4 control-label">Penyata Gaji Asal Terkini</label>
                                                    <div class="col-md-8">
                                                         <input id="fileupload8" class="btn btn-default" type="file" name="file8" >
        <input type="hidden" name="document8"   id="documentx8"  value="Penyata Gaji Asal Terkini">
    &nbsp; <span id="document8"> </span> 
@if(!empty($document8->name))
   <span id="document8a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document8->upload}}"> {{$document8->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
             
                                                    </div>
                                                </div>!-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Surat Pengesahan Majikan</label>
                                                    <div class="col-md-8">
                                                         <input id="fileupload9" class="btn btn-default" type="file" name="file9"  >
        <input type="hidden" name="document9"   id="documentx9"  value="Surat Pengesahan Majikan">
    &nbsp; <span id="document9"> </span> 
@if(!empty($document9->name))
   <span id="document9a"><a target='_blank' href="{{url('/uploads/file/')}}/{{$user->name}}/{{$document9->upload}}"> {{$document9->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
   @endif
            
                                                    </div>
                                                </div>
                                            </fieldset>
                                                          
                                                        </div>
                                                         <div class="tab-pane" id="tab9">

                                                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}" >
                                                            <br>
                                                            <h3><strong>Step 9</strong> - Pengisytiharan / <i> Declaration </i> </h3>
                                                          Saya/kami dengan ini mengaku dan mengisytiharkan bahawa : <br>
                                                          <i>  I/we hereby agreed and declare that: <br></i> <br>

                                                          i) Maklumat yang diberikan di dalam borang permohonan pembiayaan ini dan dokumen-dokumen lain adalah benar tanpa menyembunyikan maklumat yang mungkin mempengaruhi permohonan saya/ kami dan pihak Co-opbank Persatuan berhak menolak / menarik balik sekiranya maklumat tersebut tidak benar dan palsu. <br>
                                                          <i> All the information stated in the application form and the relevant supporting documents are true/genuine. Co-opbank Persatuan reserve the right to reject/withdraw this application in any case of fraud/forged documents. <br></i>
                                                          ii) Segala transaksi yang akan dilaksanakan tidak berkaitan apa jua perkara yang tidak dibenarkan dibawah 'AMLATFA 2001' (Akta Pencegahan Pengubahan Wang Haram, Pencegahan Keganasan dan Hasil daripada Akta Haram 2001).<br>
                                                          <i>All the related transaction hereafter is not connected to Anti Money Laundering, Anti-Terrorism Financing and Proceed of Unlawful Activities Act 2001.<br></i>
                                                          iii) Tidak dikenakan tindakan kebankrapan dan bukanlah seorang yang muflis seperti yang tertakluk dibawah Seksyen 3 Akta Kebankrapan 1967. Saya / kami akan memaklumkan kepada pihak  Co-opbank  Persatuan dalam masa 7 hari sekiranya saya / kami berada dalam proses kebankrapan.<br><i>No bankruptcy proceeding are being taken against me/us and I/we are not a bankrupt under Section 3 of the Bankruptcy Act 1967, I undertake to notify Co-opbank Persatuan within 7 days should there be any bankruptcy proceeding is taken against me/us. <br></i>
                                                          iv) Membenarkan pihak  Co-opbank Persatuan menghubungi majikan  atau mana-mana pihak untuk mendapatkan sebarang keterangan mengenai saya / kami dan seterusnya membenarkan mereka  memberi keterangan yang diperlukan oleh pihak  Co-opbank Persatuan.<br>
                                                          <i>Allow Co-opbank Persatuan to contact my employer or any related parties to obtain the relevant information on me / us and thereafter allow them to provide the necessary information. <br></i>
                                                          v) Tiada sebarang permohonan pembiayaan di institusi kewangan lain serentak/dalam tempoh masa yang terdekat ketika borang permohonan ini dihantar ke  Co-opbank Persatuan. <br> <i>I/we declare that I/we did not submit any other financing request to other financial institution simultaneously. <br></i>
                                                          <br>
Saya/kami dengan ini memberi kebenaran kepada Co-opbank Persatuan yang tidak boleh dibatal tanpa syarat untuk mendedahkan maklumat berkaitan dengan kemudahan kewangan saya / kami di Co-opbank Persatuan bagi apa-apa tujuan sekalipun kepada Cawangan Co-opbank Persatuan , Subsidiari, Bank Negara Malaysia (BNM), Unit Kredit Pusat (Central Credit Unit), Biro Maklumat Guaman dan agensi lain yang diluluskan oleh BNM atau mana-mana pihak lain yang mempunyai bidang kuasa ke atas Co-opbank Persatuan, dan pemegang serahan hak yang dibenarkan dan diberikuasa oleh Co-opbank Persatuan. Saya / kami membenarkan pihak Co-opbank Persatuan mendapatkan maklumat peribadi kredit saya / kami dari CTOS, RAM dan mana-mana pihak berkuasa lain yang berkaitan. Maklumat yang diperolehi hanya boleh digunakan bagi tujuan permohonan pembiayaan sahaja.  <br> <i> I/we agreed to allow Co-opbank Persatuan to reveal /forward all relevant information relating to this financing to any Co-opbank Persatuan Branch, Subsidiaries, Central Bank of Malaysia (BNM), Central Credit Bureau and any other agencies approved by Central Bank of Malaysia (BNM) or any other supervising Co-opbank Persatuan. I/we hereby agree to allow Co-opbank Persatuan to obtain the relevant information about me/us from CTOS, RAM or any other parties. All relevant information obtained are meant to be used for this specific financing only.  <br></i>
<br>
                                                        
                                                          <table align="center">
                                                          <tr>
                                                          <td valign="center" width="30"> 
                                                          <input type="checkbox" id="pertama" name="pertama" value="1"  checked > 
                                                          </td>
                                                        
                                                          <td>
                                                          Saya/kami bersetuju dengan terma dan syarat di atas / <br>  <i> I/we agreed with the above term and conditions </i> </td>
                                                          </tr>
                                                          </table>

                                                           <br>
                                                          <table border="1">
                                                          <tr>
                                                          <td align="center">  <b>AKTA PERLINDUNGAN DATA PERIBADI 2010 (APDP)</b> <br> <b>NOTIS PRIVASI  </b></td>
                                                          </tr>
                                                        
                                                          <td>
                                                          Pelanggan yang dihormati, <br>

                                                          <br> Selaras dengan peguatkuasaan APDP pada 15 November 2013,  Co-opbank Persatuan komited untuk mematuhi semua peruntukan APDP yang bertujuan untuk melindungi data peribadi anda. Mengikut peruntukan di bawah APDP  Co-opbank Persatuan perlu mendapatkan  persetujuan anda apabila mengumpul dan memproses data peribadi anda. Sila ambil maklum, maklumat berkenaan adalah penting untuk   Co-opbank Persatuan menilai permohonan anda sebelum permohonan anda diproses ke peringkat selanjutnya. Maklumat  berkenaan juga dapat membantu  Co-opbank  Persatuan meningkatkan perkhidmatan. <br> <br>
                                                          <b>Memproses Data</b> <br><br>

                                                          Data peribadi anda boleh diakses oleh  Co-opbank Persatuan dan/atau ejen sah  Co-opbank Persatuan untuk tujuan berikut  : 
                                                          <ol type="i">
                                                          <li> Berkomunikasi dengan anda</li>
                                                          <li> Menyediakan perkhidmatan kepada anda termasuk anak syarikat  Co-opbank Persatuan (jika ada)</li>
                                                     
                                                          <li> Menilai permohonan anda bagi apa-apa produk  Co-opbank Persatuan</li>
                                                          <li> Memproses apa-apa transaksi pembayaran</li>
                                                          <li> Menguruskan dan mengekalkan akaun dan kemudahan</li>
                                                          <li> Mengesahkan kedudukan kewangan menerusi pemeriksaan rujukan kredit</li>
                                                          <li> Menjawab pertanyaan dan aduan anda serta menyelesaikan pertikaian secara am</li>
                                                           <li> Menguruskan pembabitan anda dalam sebarang peraduan dan/atau kempen</li>
                                                          <li> Melaksanakan aktiviti dalaman</li>
                                                          <li>Melakukan kaji selidik pasaran dan analisis bagi meningkatkan produk dan perkhidmatan  Co-opbank Persatuan</li>
                                                          <li> Memenuhi keperluan pihak berkuasa/keperluan perundangan atau mana-mana Undang-Undang</li>
                                                          <li>Menguruskan faedah dan manfaat anda</li>
                                                          <li> Menguruskan pembayaran dermasiswa, kematian atau takaful yang berkaitan</li>
                                                          <li> Menguruskan pemulihan hutang dan kutipan</li>
                                                         
                                                          </ol>
                                                          

                                                        
                                                        <b>Penzahiran Data Peribadi  </b> <br>
                                                        <br>
                                                        Adalah dimaklumkan dalam sebarang situasi dan jika diperlukan ketika melaksanakan tujuan-tujuan yang dinyatakan di atas,  Co-opbank Persatuan boleh menzahirkan maklumat peribadi anda kepada pihak berikut :
                                                         <ol type="i">
                                                         <li>Mana-mana organisasi yang dilantik untuk berkhidmat sebagai ejen  Co-opbank Persatuan</li>
                                                         <li>Mana-mana organisasi untuk melengkapkan transaksi yang diminta oleh anda</li>
                                                         <li>Mana-mana penyedia perkhidmatan kewangan yang diminta oleh anda</li>
                                                         <li>Mana-mana penyedia perkhidmatan  Co-opbank Persatuan</li>
                                                         <li>Mana-mana pihak  yang dilantik oleh  Co-opbank Persatuan</li>
                                                         <li>Anak-anak syarikat  Co-opbank Persatuan</li>
                                                         <li>Penasihat profesional</li>
                                                         <li>Mana-mana pihak yang memberi jaminan atas permohonan anda</li>
                                                         <li>Mana-mana pihak yang dibenarkan di dalam peruntukan Undang-Undang, Ketetapan mahkamah atau pihak berkuasa</li>
                                                         <li>Mana-mana agensi/syarikat pelaporan kredit yang berkhidmat dengan kami</li>
                                                         <li>Agensi penguatkuasaan atau pengawalseliaan</li>


                                                         </ol>
                                                         </td>
                                                          </tr>
                                                           <tr>
                                                          <td align="center"> <b>PERAKUAN </b></td> 
                                                          </tr>
                                                         <tr>
                                                         <td>

                                                         <br>
                                                          Dengan ini, saya/kami mengesahkan bahawa :
                                                          <ul>
                                                          <li>  <input type="checkbox" id="satu" name="satu" value="1"  checked>  Saya/kami bersetuju tertakluk di bawah nota privasi di atas  </li>
                                                          <li> <input type="checkbox" id="dua" name="dua" value="1"  checked >Saya/kami dengan ini bersetuju memberi persetujuan secara nyata ("explicit consent") kepada  Co-opbank Persatuan untuk mengumpul, memproses dan/atau menzahirkan data peribadi saya (termasuk data sensitif) seperti di dalam Nota Privasi di atas</li>
                                                          <li> <input type="checkbox" id="tiga" name="tiga" value="1" checked > Data peribadi yang diberikan oleh saya/kami adalah tepat, lengkap, terkini dan benar. Saya/kami berjanji akan memaklumkan kepada  Co-opbank Persatuan secepat mungkin jika terdapat perubahan data peribadi saya/kami 
                                                          </li>

                                                          </ul>

                                                          <br>
                                                          Pada masa tertentu  Co-opbank Persatuan mungkin akan menghantar apa-apa bentuk promosi dan pemasaran yang mungkin menarik minat anda. Jika anda mahu menerima komunikasi tersebut, sila lengkapkan ruangan di bawah dengan menandakan diruangan pilihan anda.
                                                          <br>
                                                          <input type="radio" name="pernyataan" value="1" @if($term->promosi > 0) {{'checked'}} @endif > Ya, Saya/kami bersetuju 
                                                          <br>
                                                          <input type="radio" name="pernyataan" value="0"  @if($term->promosi < 1) {{'checked'}} @endif  > Tidak, Saya/kami tidak bersetuju

                                                          </td>
                                                          </tr>
                                                          </table>



                                                        
                                                         
                                                        

                                                          
                                                        </div>
                                                      

                                                     
                
                                                       <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                        <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Seterusnya / <i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET ENffD -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
                <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  city: {
                    required: false
                  },
                  postcode: {
                    required: false,
                    minlength: 4
                  },
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {

                   

                      $.ajax({

                            type: "PUT",
                           
                            url: '{{ url('/form/') }}'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),

                            cache: false,
                            beforeSend: function () {
                               
                            },

                            success: function () {
                             
                            
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

      <script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>

  <script type="text/javascript">

$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city2").val(data[k].post_office+' / '+data[k].state.state_name );
                  
                    });

                }
            });

   
});
</script>
  <script type="text/javascript">

$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city3").val(data[k].post_office+' / '+data[k].state.state_name );
                  
                    });

                }
            });

   
});
</script>
@if($data->marital != 'Berkahwin / Married')
  <script type="text/javascript">

  $(document).ready(function() {
    

        $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").show();
   
});
  </script>
  @else 
  <script type="text/javascript">

  $(document).ready(function() {
    

    $("#tab5 :input").attr("disabled", false);
            $("#statuskawin").hide();
   
});
  </script>

@endif 

  <script type="text/javascript">

$( "#status" ).change(function() {
    var status = $('#status').val();
     
    if(status  != 'Berkahwin / Married' ) {
       
        $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").show();
    }
    else {
   
      $("#tab5 :input").attr("disabled", false);
            $("#statuskawin").hide();
    }

   
});
</script>

<?php for ($x = 1; $x <= 10; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/uploads/file/')}}/{{$user->name}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>


<script type="text/javascript">

$( "#agree" ).click(function() {
     var icnumber = $('#icnumber').val();
var satu = $('#satu:checked').val();
var dua = $('#dua:checked').val();
var tiga = $('#tiga:checked').val();
var pertama = $('#pertama:checked').val();
    var r = prompt("Please enter your ic number" );
        var pernyataan = $('input[name="pernyataan"]:checked').val();


if (r == icnumber) {
    if(satu > 0 && dua > 0 &&  tiga > 0  &&  pertama > 0   ) {
  
                      $.ajax({

                            type: "POST",
                           
                            url: '{{ url('/form/term/') }}',
                            data: $('#tab9 :input').serialize(),

                            cache: false,
                            beforeSend: function () {
                               
                            },

                            success: function () {
                             
                                alert("success");
                                location.reload();
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });

                  }
                  else {

                     alert("You must agree to the terms and conditions");
                    
                  }
              }
                  else {

                        alert("Incorrect, Please Retype Your IC Number !");

                  }
   
});
</script>


 <script type="text/javascript">

$( ".income" ).change(function() {


if(  $('#income1').val() ) {
    var income1 = $('#income1').val();
}
else {
    var income1 = 0;
}
if(  $('#income2').val() ) {
    var income2 = $('#income2').val();
}
else {
    var income2 = 0;
}

if(  $('#income3').val() ) {
    var income3 = $('#income3').val();
}
else {
    var income3 = 0;
}

if(  $('#outcome2').val() ) {
    var outcome2 = $('#outcome2').val();
}
else {
    var outcome2 = 0;
}

if(  $('#outcome3').val() ) {
    var outcome3 = $('#outcome3').val();
}
else {
    var outcome3 = 0;
}
if(  $('#outcome4').val() ) {
    var outcome4 = $('#outcome4').val();
}
else {
    var outcome4 = 0;
}



var total = parseFloat(income1) + parseFloat(income2)  + parseFloat(income3) ;
    $("#income4").val(parseFloat(total).toFixed(2));

var total2 = parseFloat(outcome2)  + parseFloat(outcome3) + parseFloat(outcome4)   ;
    $("#outcome5").val( parseFloat(total2).toFixed(2));

   ;
    $("#totalincome").val( parseFloat(total - total2).toFixed(2));

   
});
</script>
<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("select option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });

</script>

<script type="text/javascript">

$( "#old_ic" ).change(function() {

     var old_ic = $('#old_ic').val();
  
      
if(old_ic == 'A1340304' | old_ic =='A2124626'  | old_ic =='4999546' | old_ic== 'A1962144'  | old_ic== 'A3563969')
{

alert("sorry, your olc ic blocked by system");
$('#old_ic').val("");

} 





});
</script>



<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>


<script>
function alertWithoutNotice(message){
    setTimeout(function(){
        alert(message);
    }, 1000);
}
</script>






<script type="text/javascript">

        $(document).ready(function() {
          
        
           
           var form = document.getElementById("wizard-1");
var elements = form.elements;
for (var i = 0, len = elements.length; i < len; ++i) {
    elements[i].disabled = true;
}
           

            });
    </script>