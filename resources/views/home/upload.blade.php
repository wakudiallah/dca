@extends('layouts.dca.template')
@section('content')
<style type="text/css">
    
</style>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
@include('layouts.dca.menu')
 
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
            @include('sweetalert::alert')  
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                    <div class="front-left">
                        
                        @include('layouts.dca.left_front')
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">
                    <h3 class="mb30">Financing Detail </h3>
                        <fieldset>
                            <table width="100%" style="line-height: 30px !important">
                                <tr>
                                    <td>
                                        <label class="try control-label" for="name" >Name</label>
                                    </td>
                                    <td><b>: </b></td>
                                    <td> <b>{{$pra->fullname}}</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="try control-label" for="name">IC Number</label>
                                    
                                    </td>
                                    <td><b>: </b></td>
                                    <td><b> {{$pra->icnumber}}</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="try control-label" for="name">Financing Amount</label>
                                    
                                    </td>
                                    <td><b>: </b></td>
                                    <td><b> RM {{number_format($loanamount->loanammount)}}</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="control-label" for="name">Tenures</label>
                                        
                                    </td>
                                    <td><b>: </b></td>
                                    <td> <b>{{$loanamount->Tenure->years}} Years </b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="control-label" for="name">Package</label>
                                    
                                    </td>
                                    <td><b>: </b></td>
                                    <td><b>
                                        @if($pra->package->id=="1")
                                            Mumtaz-<i><font size="2">i</font></i>
                                        @elseif($pra->package->id=="2")
                                             Afdhal-<i><font size="2">i</font></i>
                                        @else
                                           Private Sector PF-<i><font size="2">i</font></i>
                                        @endif
                                        </b>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>
                        </fieldset>

                        
                        <div class="col-sm-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label class="control-label" for="name">Front IC <span>*</span></label>
                                    <form id="upload1" action="{{url('save/upload/1')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <input type="file" name="file1" id="changePicture1" class="hidden-input test1 form-control" onchange='this.form.submit()' value="Front IC">
                                        <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">
                                          <input type="hidden" name="document1"  id="documentx1"  value="Front IC">
                                    </form>
                                </div>

                                <div class="col-sm-4">
                                    @if(empty($document1->upload))
                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                    @else
                                    <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document1->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
                                    @endif
                                </div>

                                
                            </div>
                        </div>
                             
                        <div class="col-sm-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label class="control-label" for="name">Back IC <span>*</span></label>
                                    <form id="upload2" action="{{url('save/upload/2')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <input type="file" name="file2" id="changePicture2" class="hidden-input test1 form-control" onchange='this.form.submit()' value="Back IC">
                                        <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">
                                         <input type="hidden" name="document2"  id="documentx2"  value="Back IC">
                                    </form>
                                </div>

                                <div class="col-sm-4">
                                    @if(empty($document2->upload))
                                    <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                    @else
                                    <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document2->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
                                    @endif
                                </div>
                            </div>

                            
                        </div>
                        <!--<div class="col-sm-12 col-md-12">
                            <div class="">
                                 @if(empty($document3->upload))
                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                @else
                                <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document3->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
                                @endif
                                <div class="caption">
                                <h4 style="color: red !important">Latest Payslip*</h4>
                                <p>
                                    <form id="upload3" action="{{url('save/upload/3')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <input type="file" name="file3" id="changePicture3" class="hidden-input test1 form-control" onchange='this.form.submit()' value="Latest Payslip">
                                        <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">
                                         <input type="hidden" name="document3"  id="documentx3"  value="Latest Payslip">
                                    </form>
                                </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="">
                                @if(empty($document4->upload))
                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                @else
                                <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document4->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
                                @endif
                                <div class="caption">
                                <h4 style="color: red !important">Payslip (Optional 1)</h4>
                                <p>
                                    <form id="upload4" action="{{url('save/upload/4')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <input type="file" name="file4" id="changePicture4" class="hidden-input test1 form-control" onchange='this.form.submit()' value="Payslip (Optional 1)">
                                        <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">
                                         <input type="hidden" name="document4"  id="documentx4"  value="Payslip otional 1">
                                    </form>
                                </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="">
                                 @if(empty($document5->upload))
                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                @else
                                 <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document5->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>
                                @endif
                                <div class="caption">
                                    <h4 style="color: red !important">Payslip (Optional 1)</h4>
                                    <p>
                                        <form id="upload5" action="{{url('save/upload/5')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <input type="file" name="file5" id="changePicture5" class="hidden-input test1 form-control" onchange='this.form.submit()' value="Payslip Optional 2">
                                            <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">
                                             <input type="hidden" name="document5"  id="documentx5"  value="Payslip optional 2">
                                        </form>
                                    </p>
                                </div>
                            </div>
                        </div>-->
                        
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label class="control-label" for="name">State<span>*</span></label>
                                <select name="state" id="state" class="form-control">
                                    <option value="0" selected="">--- SELECT STATE ---</option>
                                    @foreach($state as $state)
                                        <?php 
                                            if(!empty($state->state_code)==$term->state_id) {
                                            $selected = "selected";
                                            }
                                            else {
                                                $selected = "";
                                            } 
                                        ?>
                                    <option {{$selected}} value="{{$state->state_code}}">{{$state->state_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Branch <span>*</span></label>
                                
                                <select name="city" id="branch" class="form-control" >
                                    <option value="0" selected="">--- SELECT BRANCH ---</option> 
                                    @foreach($branch as $branch)
                                      <?php 
                                      if(!empty($branch->branch_code)==$term->id_branch) {
                                          $selected = "selected";
                                      }
                                      else {
                                        $selected = "";
                                      } 
                                      ?>
                                      <option {{$selected}} value="{{$branch->branch_code}}">{{$branch->branchname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <input type='checkbox' id='terms'  name='terms' value='1' required="required" ><b> I agree with <a href="" data-toggle="modal" data-target="#termModal">Term and condition</a></b><br>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                             <div class="form-group">
                                @if(empty($document2->upload) || empty($document1->upload))
                                <a href="#" class="btn btn-primary btn-lg" disabled onclick="myFunction()">Submit</a>
                                @else
                                   <a align='right' id="submit_upload" class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Account Verification</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => 'form/uploaddoc', 'enctype' => 'multipart/form-data']) !!}
                        <fieldset>
                            <div class="form-body">   
                                <div class="form-group">
                                    <font color='black'>IC Number</font>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                     <input type="hidden" name="id_branch" id="id_branch" onClick="checkBranch()">
                                    <input type="icnumber" id="icnumber" maxlength='12' minlength='6' class="form-control" name="icnumber" value=""  required>
                                    <input type="hidden" id="fullname" name="fullname" value="{{$pra->fullname}}"  readonly >
                                    <input type="hidden" id="id_praapplication" name="id_praapplication" value="{{$pra->id_pra}}" readonly >
                                    <input type='hidden' id='status_penyelesaian' name="debt_consolidation" value='0'/>
                                   
                                </div>
                            </div>
                        </fieldset>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cancel
                                </button>
                                <button type="submit" name="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        {!! Form::close() !!}   
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>

 @include('praapplication.upload_js')

<script>
$(document).ready(function(){
    $("#submit_upload").click(function(){
        var branch = $('#branch').val();
        var state = $('#state').val();
        var terms = $('input[name="terms"]:checked').val();
        if ((state==0)){
            bootbox.alert("You must select state");
        }
        //else if (branch==0){
             //bootbox.alert("You must select branch");
        //}
        else if(terms != 1   ) {
            bootbox.alert('You must agree to the Terms and Conditions'); 
        }
        else{
            $('#myModal').modal('show');
        }
    });
});
</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="state"]').on('change', function() {
          
            var stateID = $(this).val();
             var wilayah    = $('#wilayah').val();
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/branch/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        $('select[name="city"]').empty();
                        
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                          //  $("#id_branch").val(data[k].state.state_code );
                        });


                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>
<script type="text/javascript">
    var uploadField = document.getElementById("changePicture1");
    uploadField.onchange = function() {
        if(this.files[0].size > 6000000){
           alert("Please upload file below 5 MB");
           this.value = "";
        }
        else{
            var fileExtension = ['pdf','png','jpeg','jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.pdf' formats are allowed.");
            }
            else{
                var form = document.getElementById('upload1');
                form.submit();
            }
        }           
    };
</script>

<script type="text/javascript">
    var uploadField = document.getElementById("changePicture2");
    uploadField.onchange = function() {
        if(this.files[0].size > 6000000){
           alert("Please upload file below 5 MB");
           this.value = "";
        }
        else{
            var fileExtension = ['pdf','png','jpeg','jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.pdf' formats are allowed.");
            }
            else{
                var form = document.getElementById('upload2');
                form.submit();
            }
        }           
    };
</script>

<script type="text/javascript">
    var uploadField = document.getElementById("changePicture3");
    uploadField.onchange = function() {
        if(this.files[0].size > 6000000){
           alert("Please upload file below 5 MB");
           this.value = "";
        }
        else{
            var fileExtension = ['pdf','png','jpeg','jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.pdf' formats are allowed.");
            }
            else{
                var form = document.getElementById('upload3');
                form.submit();
            }
        }           
    };
</script>

<script type="text/javascript">
    var uploadField = document.getElementById("changePicture4");
    uploadField.onchange = function() {
        if(this.files[0].size > 6000000){
           alert("Please upload file below 5 MB");
           this.value = "";
        }
        else{
            var fileExtension = ['pdf','png','jpeg','jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.pdf' formats are allowed.");
            }
            else{
                var form = document.getElementById('upload4');
                form.submit();
            }
        }           
    };
</script>

<script type="text/javascript">
    var uploadField = document.getElementById("changePicture5");
    uploadField.onchange = function() {
        if(this.files[0].size > 6000000){
           alert("Please upload file below 5 MB");
           this.value = "";
        }
        else{
            var fileExtension = ['pdf','png','jpeg','jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.pdf' formats are allowed.");
            }
            else{
                var form = document.getElementById('upload5');
                form.submit();
            }
        }           
    };
</script>
 <script>
        function myFunction() {
            alert("Please Upload Mandatory Documents");
        }
    </script>
<script type="text/javascript">
    var select = document.getElementById('branch');
var input = document.getElementById('id_branch');
select.onchange = function() {
    input.value = select.value;
}
</script>

 @endsection




 @push('addjs')

<script>
$(document).ready(function(){
    $("#login").click(function(){
     
        $('#termModal').modal('show');
       
    });
});
</script>

 @endpush
