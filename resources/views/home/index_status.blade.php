@extends('layouts.dca.template')
@section('content')
<style type="text/css">
    .img-mbsb{
        margin-bottom: 50px !important;
        width: 300px !important;
        height: 100px !important;

        display: block;
      margin-left: auto;
      margin-right: auto;
      width: 50%;
    }
    .left{
        color: #ffffff !important;     
        z-index: 1;
    }
    .content {
        padding-top: 0px !important;
        padding-bottom: 80px;
    }
    .front-right{
        margin-top: 24px !important;

    }
    .front-left{
        margin-top: 180px !important;
        padding-right: 100px !important;
    }

    .consultantion-form{
        padding:  20px !important;
        margin:  20px !important;
    }
</style>

<link href="{{asset('new/temp/css/update.css')}}" rel="stylesheet')}}">

@include('layouts.dca.menu')
 @include('sweetalert::alert')  
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                        <div class="front-left">
                            
                            @include('layouts.dca.left_front')
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 front-right">
                        
                        <div class="consultantion-form">
                            <h2 class="mb30">Application Status</h2>

                            <div class="consultantion-form" style="background-color: #f2f2f7 !important">
                            
                                <h3 style="color: #0d0d0d !important">Personal Details</h3>
                                
                                
                                <div class="detail-info" style="line-height: 30px !important">
                                    
                                    <p>
                                        WAKUDIALLAH (93052403333)
                                        <br>
                                        016-23444-333 - wakudiallah05@gmail.com
                                    </p>

                                    <p>
                                        Submit : 15-Mei-2020<br>
                                        BRANC = KUALA LUMPUR
                                    </p>
                                </div>
                                


                            </div>

                            <table id="datatable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Submit Date</th>
                                            <th>Current Status</th>
                                            <th>Remark</th>
                                            <th>Activities</th>
                                              @if($term->verification_status ==1 AND $term->verification_result ==2)
                                             <th>Action</th>
                                             @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$term->Basic->name}}</td>
                                            <td>{{$term->file_created}}</td>
                                            <td>
                                              
                                                    @if(($term->status ==3)  OR  ($term->status ==2)) 
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>Pending Documents Verification</span>
                                                    @elseif(($term->status ==4)  OR  ($term->fill_by ==1)) 
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>fill up by FA</span>
                                                    @elseif($term->status ==99)  
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'>Documents Rejected</span>
                                                     @elseif($term->status ==5)  
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'>Pending Verification</span>
                                                     @elseif($term->status ==6)  
                                              
                                          
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Application in process to Branch</span>
                                                   
                                                    @endif  
                                            
                                            </td>
                                             <td>
                                              @if($term->status ==88)
                                                   {{ $term->verification_remark }}
                                              @endif
                                              @if($term->verification_status ==1 AND $term->verification_result_by_bank ==0)
                                                  {{ $term->verification_remark }}
                                             @elseif($term->verification_status ==1 AND $term->verification_result_by_bank !=0)
                                                  {{ $term->remark_bank }}
                                               @endif
                                             </td>

                                             <td>
                                                 <a href="JavaScript:newPopup('{{url('/')}}/activities_user/{{$term->id_praapplication}}');" class='btn btn-sm btn-primary' ><i class='fa fa-history'></i>  History</a>

                                             </td>


                                             @if($term->verification_result ==3)
                                               <td> - </td>
                                             @else
                         
                                                  
                         
                           @if($term->verification_status ==1 AND $term->verification_result ==2)
                            <td> 
                                                    @if($term->verification_result_by_bank ==0)
                                                        <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank" alt="download application form"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png" alt="download application form"></img></a>
                                                    @elseif($term->verification_result_by_bank ==1)                                                
                                                        <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @elseif($term->verification_result_by_bank ==2)
                                                       <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @elseif($term->verification_result_by_bank ==3)
                                                    <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @endif  
                                              @endif
                                                 </td>
                          
                                             @endif
                                        </tr>
                                       
                                    </tbody>
                                </table>
                                
                            <div class="table-responsive" style="margin-top: 40px !important">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col">Name</th>
                                      <th scope="col">Submit Date</th>
                                      <th scope="col">Current Status</th>
                                      <th scope="col">Remark</th>
                                      <th scope="col">Activities</th>
                                      <th scope="col">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row">1</th>
                                      <td>Mark</td>
                                      <td>Otto</td>
                                      <td>@mdo</td>
                                      <td>Otto</td>
                                      <td>@mdo</td>
                                    </tr>
                                    
                                  </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
        </div>
    </div>
 @include('praapplication.js')
 @endsection
