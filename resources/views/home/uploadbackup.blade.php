    @extends('layouts.dca.template')

    @section('content')

   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <style type="text/css">
      body{
        font-family: 'Helvetica', 'Arial', sans-serif !important;
    }

    .login100-more::before {
        content: "" !important;
        display: block !important;
        position: absolute !important;
        z-index: -1 !important;
        width: 100% !important;
        height: 100% !important;
        top: 0 !important;
        left: 0 !important;
        background: #e8519e !important;
        background: -webkit-linear-gradient(bottom, #17a2b8, #007bff) !important;
        background: -o-linear-gradient(bottom, #e8519e, #c77ff2) !important;
        background: -moz-linear-gradient(bottom, #e8519e, #c77ff2) !important;
        background: linear-gradient(bottom, #e8519e, #c77ff2) !important;
        opacity: 0.8 !important;
        background-image: url('http://localhost:7777/dca_uat/dca/public/dca/images/background-blue.jpg'); 
    }

</style>


 <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
                    <ul>
                        <li>+ 180-123-4567 / 8910</li>
                        <li>|</li>
                        <li>info@demo.com</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="#">Help Center</a> </li>
                        <li>|</li>
                        <li><a href="#">Job Listing</a> </li>
                        <li>|</li>
                        <li><a href="#">Clients</a></li>
                        <li>|</li>
                        <li><a href="#">Client Reviews</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-toggel">
        <a href="#" id="dots" class="dots-icon"><i class="fa fa-ellipsis-v visible-xs"></i></a>
    </div>
   <!-- /.top-header-section-->
    <!-- header-section-->
    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <div class="logo">
                       <!-- <a href="index.html"><img src="./images/logo.png" alt=""> </a>-->
                    </div>
                </div>
                <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                    <!-- navigations-->
                    <div class="navigation">
                        <div id="navigation">
                            <ul>
                                <li class="active"><a href="index.html">Home</a></li>
                                
                                <li class="has-sub"><a href="#">Blog</a>
                                    <ul>
                                        <li><a href="blog-default.html">Blog Default</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <!-- /.navigations-->
                </div>
                <div class="col-lg-3 hidden-md col-sm-6 hidden-xs pull-right">
                    <a href="#" class="btn btn-outline head-btn">Request A Consultation</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /. header-section-->
      <!-- services -->
     <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 login100-more" >
                    <div class="title-left hidden-xs" >
                        <h1 style="color: white !important">Hello!</h1>
                        <h3 style="color: #ffffff !important">Welcome to MBSB Bank Online Application Form</h3>
                        <p>Now its very easy to apply for a Prime Rich Current Account-i</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb30">
                    <div class="consultantion-form">
                        <h3 class="mb30">Request A Consultation </h3>
                          <fieldset>
                        <form id="wizard-1" >
                            <table class=" table-hover" width="50%">
                                <tr>
                                    <td width="50%">Name</td>
                                    <td width="5%">: </td>
                                    <td width="45%"> {{$pra->fullname}}</td>
                                </tr>
                                <tr>
                                    <td>IC Number</td>
                                    <td>: </td>
                                    <td> {{$pra->icnumber}}</td>
                                </tr>
                                <tr>
                                    <td>Financing Amount</td>
                                    <td>: </td>
                                    <td> RM {{number_format($loanamount->loanammount)}}</td>
                                </tr>
                                <tr>
                                    <td>Tenures</td>
                                    <td>: </td>
                                    <td> {{$loanamount->Tenure->years}} tahun</td>
                                </tr>
                                <tr>
                                    <td>Package</td>
                                    <td>: </td>
                                    <td>
                                        @if($pra->package->id=="1")
                                            Mumtaz-<i><font size="2">i</font></i>
                                        @elseif($pra->package->id=="2")
                                             Afdhal-<i><font size="2">i</font></i>
                                        @else
                                           Private Sector PF-<i><font size="2">i</font></i>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>

                            <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label"><b style="font-size: 13px!important">State</b></label>
                                        <select name="state" class="form-control" style="width:350px">
                                                 <option>--- Select State ---</option>
                                                @foreach($state as $state)
                                                  <?php 
                                                  if(!empty($state->state_code)==$term->state_id) {
                                                      $selected = "selected";

                                                  }
                                                  else {
                                                    $selected = "";
                                                  } 
                                                  ?>
                                                  <option {{$selected}} value="{{$state->state_code}}">{{$state->state_name}}</option>
                                                @endforeach
                                        </select>
                                </div>
                            </div>
                   <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                            <label class="control-label"><b style="font-size: 13px!important">Branch </b></label>
                             <select name="city" class="form-control" style="width:350px">
                           
                                <option>--- Select MO ---</option>
                                @foreach($branch as $branch)
                                  <?php 
                                  if(!empty($branch->branch_code)==$term->id_branch) {
                                      $selected = "selected";

                                  }
                                  else {
                                    $selected = "";
                                  } 
                                  ?>
                                  <option {{$selected}} value="{{$branch->branch_code}}">{{$branch->branchname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                         
                                        
                                       

                                        <h4 style="color: red !important; margin-bottom: 30px !important ">*Documents must be in PDF format & File must be less than 500 kb </h4>

                                      

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label sr-only" for="name">Latest Payslip<sup>*</sup></label>  
                                      @if(empty($document7->doc_pdf))
                                                <a href="#" class="btn bg-red"><i class="material-icons disabled">do_not_disturb_alt</i> </a>
                                                @else
                                                <a href="{{asset('/documents/user_doc/'.$pra->id_pra.'/'.$document7->doc_pdf)}}" class="btn bg-light-blue btn-lg" target='_blank'><i class="material-icons" >library_books</i></a>

                                                <span id="document7a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span>
                                                @endif

                                                 <label class="input ">
                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">   
                                         <input type="hidden" id="id_praapplication" name="id_praapplication" value="{{$pra->id_pra}}"/>    
                                         <input id="fileupload7"  @if(empty($document7->name)) required @endif   class="btn btn-default" type="file" name="file7"/>
                                        <input type="hidden" name="document7"  id="documentx7"  value="Payslip">
                                        &nbsp; <span id="document7"> </span>
                                        <input type='hidden' value='@if(!empty($document7->name)){{$document7->upload}}@endif' id='a7' name='a7'/>                        
                                        @if(!empty($document7->name))

                                        <!--<img src="{{asset('/')}}/storage/uploads/file/{{$pra->id}}/{{$document7->upload}}" width="110"/>-->

                                            <span id="document7a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span>
                                            <!--<img src="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->icnumber)}}/{{$document7->upload}}"/> -->
                                        @else
                                        @endif
                                    </label>                 
                            
                            </div>
                            
                         

                                </fieldset>
                   
                    
                    
                      
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Global site tag (gtag.js) - Google Analytics -->


 @include('praapplication.result_js')
<?php for ($x = 1; $x <= 15; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="state"]').on('change', function() {
          
            var stateID = $(this).val();
             var wilayah    = $('#wilayah').val();
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/branch/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="city"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>


@stop