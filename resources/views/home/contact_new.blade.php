@extends('layouts.dca.template')
@section('content')
<style type="text/css">
    .img-mbsb{
        margin-bottom: 50px !important;
        width: 300px !important;
        height: 100px !important;

        display: block;
      margin-left: auto;
      margin-right: auto;
      width: 50%;
    }
    .left{
        color: #ffffff !important;     
        z-index: 1;
    }
    .content {
        padding-top: 0px !important;
        padding-bottom: 80px;
    }
    .front-right{
        margin-top: 24px !important;

    }
    .front-left{
        margin-top: 180px !important;
        padding-right: 100px !important;
    }

    .consultantion-form{
        padding:  20px !important;
        margin:  20px !important;
    }
</style>

<link href="{{asset('new/temp/css/update.css')}}" rel="stylesheet')}}">

    @include('layouts.dca.menu')
    @include('sweetalert::alert')  
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                        <div class="front-left">
                            
                            @include('layouts.dca.left_front')
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 front-right">
                        
                        <div class="consultantion-form">
                            <h2 class="mb30">Application Status</h2>

                            <div class="consultantion-form" style="background-color: #f2f2f7 !important">
                            
                                <h3 style="color: #0d0d0d !important">Personal Details</h3>
                                
                                
                                <div class="detail-info" style="line-height: 30px !important">
                                    
                                    <p>
                                        WAKUDIALLAH (93052403333)
                                        <br>
                                        016-23444-333 - wakudiallah05@gmail.com
                                    </p>

                                    <p>
                                       Telephone:+(603) 2096 3000 <br>Address:Ground Floor, Wisma Mbsb, 48, Jalan Dungun, Bukit Damansara, 50490 Kuala Lumpur, <br>Federal Territory of Kuala Lumpur
                                    </p>
                                </div>
                                


                            </div>


                            
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
        </div>
    </div>
 @include('praapplication.js')

 
 @endsection
