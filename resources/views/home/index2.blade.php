<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Application Status";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}


.page-footer {
    height: 63px;
    padding: 15px 13px 0;
    padding-bottom: -33px;
    border-top: 1px solid #CECECE;
    background: #2a2725;
    width: 100%;
    position: absolute !important;
    position: block;
    display: block;
    bottom: 0;
}

.inline-block img{
  float: left !important;
}

</style>

    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <div id="content">
       @include('sweetalert::alert')  
     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
   
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
            
                        -->
                    
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application Status</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                     
              
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Submit Date</th>
                                            <th>Current Status</th>
                                            <th>Remark</th>
                                            <th>Activities</th>
                                              @if($term->verification_status ==1 AND $term->verification_result ==2)
                                             <th>Action</th>
                                             @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$term->Basic->name}}</td>
                                            <td>{{$term->file_created}}</td>
                                            <td>
                                              
                                                    @if(($term->status ==3)  OR  ($term->status ==2)) 
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>Pending Documents Verification</span>
                                                    @elseif(($term->status ==4)  OR  ($term->fill_by ==1)) 
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>fill up by FA</span>
                                                    @elseif($term->status ==99)  
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'>Documents Rejected</span>
                                                     @elseif($term->status ==5)  
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'>Pending Verification</span>
                                                     @elseif($term->status ==6)  
                                              
                                          
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Application in process to Branch</span>
                                                   
                                                    @endif  
                                            
                                            </td>
                                             <td>
                                              @if($term->status ==88)
                                                   {{ $term->verification_remark }}
                                              @endif
                                              @if($term->verification_status ==1 AND $term->verification_result_by_bank ==0)
                                                  {{ $term->verification_remark }}
                                             @elseif($term->verification_status ==1 AND $term->verification_result_by_bank !=0)
                                                  {{ $term->remark_bank }}
                                               @endif
                                             </td>

                                             <td>
                                                 <a href="JavaScript:newPopup('{{url('/')}}/activities_user/{{$term->id_praapplication}}');" class='btn btn-sm btn-primary' ><i class='fa fa-history'></i>  History</a>

                                             </td>


                                             @if($term->verification_result ==3)
                                               <td> - </td>
                                             @else
                         
                                                  
                         
                           @if($term->verification_status ==1 AND $term->verification_result ==2)
                            <td> 
                                                    @if($term->verification_result_by_bank ==0)
                                                        <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank" alt="download application form"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png" alt="download application form"></img></a>
                                                    @elseif($term->verification_result_by_bank ==1)                                                
                                                        <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @elseif($term->verification_result_by_bank ==2)
                                                       <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @elseif($term->verification_result_by_bank ==3)
                                                    <a href="{{url('/')}}/downloadpdf/{{$term->id_praapplication}}" target="_blank"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @endif  
                                              @endif
                                                 </td>
                          
                                             @endif
                                        </tr>
                                       
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
               
        </div>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
      <?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
  
?>


<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>

