<article class="col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics Application Under <b><?php print $user->name ; ?></b></h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <?php 	
    								function ringgit($nilai, $pecahan = 0) 
    									{ 
    										return number_format($nilai, $pecahan, '.', ','); 
    									}
    								
    								$sumloan= 0;
    								$sumapp= 0;						
    						
    								$sumapproved= 0;
    								$sumloan_approved= 0; 
    								
    								$sumrejected= 0; 
    								$sumloan_rejected= 0; 
    								
    								$sumprocess= 0;
    								$sumloan_process= 0; 
    								
    								$sumpendingapproval= 0; 
    								$sumloan_pendingapproval= 0;

    								$sumpending2ndadmin= 0; 
    								$sumloan_pending2ndadmin= 0; 

    								$sumpendingadmin= 0; 
    								$sumloan_pendingadmin= 0; 

                                    $sumpendingtenure= 0; 
                                    $sumloan_pendingtenure= 0; ?>
    								
                                    @foreach ($terma as $termb)
    							        <?php $sumloan = $sumloan + $termb->PraApp->loanamount;
    									    $sumapp = $sumapp + 1; ?>
    							            @if($termb->verification_result_by_bank ==1 AND $termb->status_waps ==1)
            									<?php 	$sumapproved = $sumapproved + 1; 
            											$sumloan_approved = $sumloan_approved + $termb->PraApp->loanamount; 
            									?>
    							             @endif
                							@if(($termb->verification_result_by_bank ==2) OR ($termb->verification_result ==3) OR ($termb->status ==88))
            									<?php 	$sumrejected = $sumrejected + 1; 
            											$sumloan_rejected = $sumloan_rejected + $termb->PraApp->loanamount; 
            									?>
                							@endif
                							@if($termb->status ==6)
            									<?php 	$sumpendingapproval = $sumpendingapproval + 1;
            											$sumloan_pendingapproval = $sumloan_pendingapproval + $termb->PraApp->loanamount; 
            									?>
                							@endif
                							 @if($termb->verification_result_by_bank ==0 AND $termb->verification_result ==0 AND $termb->status ==77)
            									<?php 	$sumprocess = $sumprocess + 1; 
            											$sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
            									?>
                							@endif
                							@if(($termb->status ==4 ) OR ($termb->status ==5 ))
            									<?php  $sumprocess = $sumprocess + 1; 
                                                        $sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
                                                ?>
                							@endif
                							@if($termb->status ==3)
            									<?php 	$sumpendingadmin = $sumpendingadmin + 1; 
            											$sumloan_pendingadmin = $sumloan_pendingadmin + $termb->PraApp->loanamount; 
            									?>
                							@endif
                                            @if($termb->status ==0)
                                                <?php   $sumpendingtenure = $sumpendingtenure + 1; 
                                                        $sumloan_pendingtenure = $sumloan_pendingtenure + $termb->PraApp->loanamount; 
                                                ?>
                                            @endif
    						      @endforeach
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
    											<th>Status</th>
    											<th>Total Application</th>
    											<th>Total Loan Amount Request</th>
                                            </tr>
                                        </thead>
                                        <tbody>
    										<tr>
    											<td>Total Pending DSR  </td>
    											<td>{{ $sumpendingadmin }}</td>
    											<td>RM {{ ringgit($sumloan_pendingadmin) }}</td>
    										</tr>

                                            <tr>
                                                <td>Total Pending Verification </td>
                                                <td>{{ $sumprocess }}</td>
                                                <td>RM {{ ringgit($sumloan_process) }}</td>
                                            </tr> 
                                            <tr>
                                                <td>Total Approved by FA / Branch  </td>
                                                <td>{{ $sumapproved }}</td>
                                                <td>RM {{ ringgit($sumloan_approved) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Total Rejected by FA / Branch </td>
                                                <td>{{ $sumrejected }}</td>
                                                <td>RM {{ ringgit($sumloan_rejected) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Total Pending WAPS </td>
                                                <td>{{ $sumpendingapproval }}</td>
                                                <td>RM {{ ringgit($sumloan_pendingapproval) }}</td>
                                            </tr>                                       <!--<tr>
                                                <td>Total Pending Financing Eligibility  </td>
                                                <td>{{ $sumpendingtenure }}</td>
                                                <td>RM {{ ringgit($sumloan_pendingtenure) }}</td>
                                            </tr>-->
    										<tr>
    											<td><b>All</b></td>
    											<td><b>{{ $sumapp }}</b></td>
    											<td><b>RM {{ ringgit($sumloan) }}</b></td>
    										</tr>                                     
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </article>