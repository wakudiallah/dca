<style type="text/css">
	
th {
    font-size: 14px;
  

}

td {
    font-size: 10px;
    line-height: 170%;

}
p {
    
    line-height: 130%;

}

ul
{
    list-style-type: none;
}

</style>

<table  width="530" >
	<tr>
	<td colspan="2" align="center">	<img src="{{url('/')}}/asset/img/logo.png" ></td>
	</tr>
	<tr>
	 <th width="280" valign="top">
			
				<b> BORANG MAKLUMAT PELANGGAN (INDIVIDU) / <i> CUSTOMER INFORMATION FORM(INDIVIUAL) </i></b> 
	 </th>
	 <td align="right">
			 <table >
				<tr>
				<td colspan="2">
				<b>UNTUK KEGUNAAN BANK / <i>FOR BANK'S USE </i></b> </td>
				</tr>
				<tr>
				<td> Dokument Diterima Oleh </td>  <td> :   </td>
				</tr>
				<tr>
				<td> Tarikh </td>  <td> :   </td>
				</tr>
				<tr>
				<td> Cawangan </td>  <td> :   </td>
				</tr>
				<tr>
				<td><b>  NO.  CIF </b> </td>  <td> :  </td>
				</tr>

			</table>
</td>
	</tr>
	<tr>
	<td valign="top" rowspan="2">
	
			<p  align="center"><b>SEKSYEN A - MAKLUMAT PELANGGAN / <i> SECTION A - CUSTOMER INFORMATION </i></b></p>
						<ul>
						  <li>1. Negara Asal / <i> Country of origin </i> :  </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->country}}</li></ul>
						  <li>2.Jenis Pengenalan Diri / <i>ID Type </i> : </li> 
						  	<ul style=" margin-left: -20px;"> <li >{{$data->card}}</li></ul>
						  <li>3. Kewarganegaraan / <i>Citizen</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->citizen}}</li></ul>
						  <li>4. No. Kad Pengenalan Baru /  <i>New IC Number</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->new_ic}}</li></ul>
						  <li>5. No. Kad Pengenalan Lama / <i>Old IC Number</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->old_ic}}   	 <?php if(empty($data->old_ic)) { echo"-";} ?> </li></ul>
						  <li>6. Tarikh Lahir / <i>Date Of Birth</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{date('d/m/Y', strtotime($data->dob))}} </li></ul>
						  <li>7. Nama (seperti dalam KP atau Pasport) / <i> Name (As in IC or Passport)</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->name}}</li></ul>
						  <li>8.  Nama Pilihan (jika ada)  / <i>Preferred Name (if any)</i> :</li>
						  	<ul style=" margin-left: -20px;"> <li > {{$data->name_prefered}}
						  	 <?php if(empty($data->name_prefered)) { echo"-";} ?> </li></ul>
						  <li>9. Gelaran / <i>Gender </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->title}}</li></ul>
						  <li>10. Jantina / <i>Title</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->gender}}</li></ul>
						  <li>11. Taraf Perkahwinan / <i>Marital Status</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->marital}}</li></ul>
						 
						  <li>12. Bangsa  / <i>Race </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->race}}</li></ul>
						  <li>13. Agama  / <i>Religion </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->relegion}}</li></ul>
						  <li>14. Pendidikan Tertinggi Diperolehi / <i>Education </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->education}}</li></ul>
						  <li>15. Kategori Pelanggan / <i>Customer Category</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->category}} <?php if(empty($data->category)) { echo"-";} ?>  </li></ul>
						  <li>16. Nama Ibu / <i>Mother's Malden Name </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$data->mother_name}}</li></ul>


						</ul>
						</td>
						<td valign="top">
					
						<p align="center"><b> SEKSYEN B - MAKLUMAT KONTAK / <i> SECTION B - CONTACT DETAIL </i> </b></p>
						
						<ul>

						  <li>1. Alamat Kediaman / <i> Residential Address </i> :  </li>
						  <ul style=" margin-left: -20px;"> <li >{{$contact->address}} </li></ul>
						  <li>2. Poskod  / <i>Postcode  </i> : </li> 
						  <ul style=" margin-left: -20px;"> <li >{{$contact->postcode}}</li></ul>
						  <li>3. Bandar  / <i>City </i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$contact->city}}</li></ul>
						  <li>4. Negeri /  <i>State </i> :</li>
						  <ul style=" margin-left: -20px;"> <li > {{$contact->state}} </li></ul>
						  <li>5. Jenis Pemilikan / <i> Type Of Ownership</i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$contact->ownership}} </li></ul>
						 <li>6. Alamat Surat Menyurat /  <i>Correspondence Address </i> : </li>
						 <ul style=" margin-left: -20px;"> <li >{{$contact->address2}}</li></ul>
						  <li>7. Poskod  / <i>Postcode  </i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$contact->postcode2}} </li></ul> 
						 <li>8. Bandar  / <i>City </i> : </li>
						 <ul style=" margin-left: -20px;"> <li >{{$contact->city2}} </li></ul>
						  <li>9. Negeri /  <i>State </i> :</li>
						  <ul style=" margin-left: -20px;"> <li > {{$contact->state2}}</li></ul>
						  <li>10. No. Untuk Dihubungi / <i>Contact No.</i>  : 
										  	<ul>
										  		 <li>Kediaman   / <i>House   </i> : {{$contact->handphone}}</li>
										  		 <li>Tel.Bimbit  / <i>Handphone   </i> : {{$contact->handphone}}</li>
										  		 <li>No. Tel. Pejabat  / <i>Office Phone No.  </i> : {{$contact->officephone}}</li>
										  		 <li>Faks   / <i>Fax   </i> : {{$contact->fax}}</li>
										  		 <li>Alamat E-mel  / <i>E-mail Address   </i> : {{$pra->email}}</li>

										  	</ul>
										  	</li>

						</ul>
					
						</td>

						</tr>
					<tr>

					<td valign="top">

						<p align="center"><b>SEKSYEN C - MAKLUMAT PEKERJAAN / <i> SECTION C - INFORMATION OF EMPLOYEMENT </i></b></p>
						<ul>
						  <li>1. Nama Majikan / <i> Employer's Name </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->empname}} </li></ul>
						  <li>2. Alamat Majikan /   <i>Employer's Address </i> : </li> 
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->address}}</li></ul>
						  <li>3. Tarikh Mula Berkhidmat / <i>Date Of Joined</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{date('d/m/Y', strtotime($empinfo->joined))}}</li></ul>
						  <li>4. Pangkat  /  <i>Grade </i> :</li>
						  	<ul style=" margin-left: -20px;"> <li > {{$empinfo->grade}}</li></ul>
						  <li>5. Taraf Jawatan (Tetap/Sementara/Kontrak) / <i>Employment Status (Permanent/Temporary/Contract) </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->empstatus}}</li></ul>
						  <li>6. Sektor Pekrjaan / <i>Occupation Sector</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->occupation_sector}}</li></ul>
						  <li>7. Pekerjaan / <i> Occupation</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->occupation}}</li></ul>
						  <li>8. Julat Pendapatan  / <i> Range Of Income</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$empinfo->range_of_income}}</li></ul>
						  


						</ul>

	</td>


	</tr> 

</table>

<table  style="page-break-before: always" width="530">

	<tr>
	 <th colspan="2"> 
										 <table>
										 <tr>
														 <th>
													 	<img src="{{url('/')}}/asset/img/logo.png" > <br>
														</th>
														<th>

												<b> Borang Permohonan Pembiayaan / <br><i> Personal Financing Application Form</i></b> 
														</th>
										</tr>
										</table>
	 </th>
	 
	</tr>
	<tr>
				<td valign="top">
				
				<ul>
					<li> Jumlah Pembiayaan /  <i>Loan Amount  </i> : RM {{number_format($loanammount->loanammount, 0 , '.' , ',' )}}</li>
					<li>Tempoh (bulan) /  <i>Tenure (month) </i> : {{$loanammount->tenure->years * 12 }} Bulan</li>
					<li>No. Anggota /  <i> Membership No. </i> : {{$loanammount->memberno}}</li>
					</ul>
					</td>
					<td valign="top">
					<ul>
					<li> Cara Bayaran Balik /  <i>Payment Method </i> : {{$loanammount->method}}</li>
					<li>Jenis Pembiayaan /  <i>Financing Type </i> : {{$loanammount->package}}</li>
					

				</ul>
				</td>
	</tr>
	<tr>
	<td valign="top">
	
			<p  align="center"><b>SEKSYEN D - MAKLUMAT PASANGAN / <i> SECTION D - SPOUSE INFORMATION </i></b></p>
						<ul>
						  <li> Nama Suami/Isteri (Seperti dalam KP) / <i> Spouse Name (As in IC) </i> :  </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$spouse->name}} </li></ul>
						  <li> Tarikh Lahir / <i>Date Of Birth</i> : </li> 
						  	<ul style=" margin-left: -20px;"> <li >{{date('d/m/Y', strtotime($spouse->dob))}} </li></ul>
						  
						  <li> No. Kad Pengenalan Baru /  <i>New IC No.</i> :</li>
						  	<ul style=" margin-left: -20px;"> <li > {{$spouse->new_ic}}</li></ul>

						  <li> No. Kad Pengenalan Lama / <i>Old IC No.</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$spouse->old_ic}}</li></ul>

						  <li> No. Tel / <i>Contact No.</i> :  </li>
						  	<ul style=" margin-left: -20px;"> <li > {{$spouse->handphone}}</li></ul>
						  <li>  Nama & Alamat Majikan  / <i> Name & Employer's Address </i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$spouse->empname}}</li></ul>
						  <li>  Poskod   / <i>Postcode</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$spouse->postcode}}</li></ul>
						  <li>No. Tel. Pejabat / <i>Office Phone No. </i> :</li>
						  	<ul style=" margin-left: -20px;"> <li > {{$spouse->phone}}</li></ul>
						  <li>Bandar / Negeri  / <i>City / State</i> : </li>
						  	<ul style=" margin-left: -20px;"> <li >{{$spouse->city}}</li></ul>
						 


						</ul>
						</td>
						<td valign="top">
					
						<p align="center"><b> SEKSYEN E - PERUJUK / <i> SECTION B - REFERENCE </i> </b></p>
						
						<ul>

						  <li>Nama Perujuk / <i> Reference Name </i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$reference->name}} </li></ul>
						  <li>Alamat Kediaman  / <i>Residential Address  </i> : </li> 
						  <ul style=" margin-left: -20px;"> <li >{{$reference->address}}</li></ul>
						  <li> Poskod   / <i>Postcode  </i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$reference->postcode}}</li></ul>
						  <li>Bandar / Negeri /  <i>City / State </i> :</li>
						  <ul style=" margin-left: -20px;"> <li > {{$reference->state}} </li></ul>
						  <li> No. Kad Pengenalan / <i> IC No. </i> : </li>
						  <ul style=" margin-left: -20px;"> <li >{{$reference->new_ic}}</li></ul>
						 <li>Pekerjaan  /  <i>Occupation  </i> : </li>
						 <ul style=" margin-left: -20px;"> <li >{{$reference->occupation}} </li></ul>
						  <li>Hubungan / <i>Relationship   </i> : </li> 
						   <ul style=" margin-left: -20px;"> <li >{{$reference->relationship}} </li></ul>
						  <li>No. Untuk Dihubungi / <i>Contact No.</i>  : 
										  	<ul>
										  		 <li>Kediaman   / <i>House   </i> : {{$reference->houseno}}</li>
										  		 <li>Tel.Bimbit  / <i>Handphone   </i> : {{$reference->handphone}}</li>
										  		 <li>No. Tel. Pejabat  / <i>Office Phone No.  </i> : {{$reference->officeno}}</li>
										  		
										  	</ul>
										  	</li>

						</ul>
					
						</td>

						</tr>
						<tr>
						<td colspan="2"><p  align="center"><b>SEKSYEN F - LATAR BELAKANG KEWANGAN/ <i> SECTION F - FINANCIAL BACKGROUND</b></p></td>
						</tr>

						<tr>
	<td valign="top">
	
			<p  align="center"><b>PENDAPATAN (A)/ <i> INCOME (A) </i></b></p>
						<ul>
						  <li>  Gaji Bulanan : Asas + Elaun Tetap <i> Monthly Income : Basic + Fixed Allowance </i> : RM {{number_format($financial->income, 2 , '.' , ',' )}} </li>
						  <li>  Lain-lain Pendapatan / <i>Other Income</i> : RM  {{number_format($financial->other_income, 2 , '.' , ',' )}}</li> 
						  
						  <li> Pendapatan Suami / Isteri /  <i>Husband / Wife Income </i> : RM {{number_format($financial->spouse_income, 0 , '.' , ',' )}}</li>
						  <li> Jumlah Pendapatan / <i>Total Income </i> : RM  {{number_format($financial->income + $financial->other_income + $financial->spouse_income, 2 , '.' , ',' )}}</li>
						  
						 


						</ul>
						</td>
						<td valign="top">
					
						<p align="center"><b> PERBELANJAAN (B) / <i> EXPENSES (B) </i> </b></p>
						
						<ul>
						  <li>  Sara Hidup <i> Cost of Living </i> : RM {{number_format($financial->cost_living, 2 , '.' , ',' )}} </li>
						  <li>  Lain-lain Perbelanjaan / <i>Other Expenses</i> : RM  {{number_format($financial->expenses, 2 , '.' , ',' )}}</li> 
						  
						  <li> Jumlah Ansuran Bulanan  <i>Monthly Payment </i> : RM {{number_format($financial->monthly_payment, 0 , '.' , ',' )}}</li>
						  <li> Sewa Rumah  <i>House Rental </i> : RM {{number_format($financial->house_rental, 0 , '.' , ',' )}}</li>
						  
						  <li> Jumlah Perbelanjaan  / <i>Total Expenses  </i> : RM  
						  {{number_format($financial->cost_living + $financial->expenses + $financial->monthly_payment + $financial->house_rental, 2 , '.' , ',' )}}</li>
						  
						 


						</ul>
					
						</td>

						</tr>
						<tr>
						<td colspan="2"><p  align="center"><b>PENDAPATAN BERSIH / NET INCOME (A -B)  : RM  
						  {{number_format(($financial->income + $financial->other_income + $financial->spouse_income)-($financial->cost_living + $financial->expenses + $financial->monthly_payment + $financial->house_rental), 2 , '.' , ',' )}} </b></p></td>
						</tr>
						</table>
						<table style="page-break-before: always" >
						<tr>
						<td colspan="3" style="line-height: 120%"><p  align="center"><b>SEKSYEN G - PENGISYTIHARAN/ <i> SECTION G - DECLRATAION</b></p>
							 <b>Saya dengan ini mengaku dan mengisytiharkan bahawa : </b><br>
                                                          <i>  I/we hereby agreed and declare that: <br></i> <br>
                                                          <ol type="i">
                                                        <li>  <b>Maklumat yang diberikan di dalam borang permohonan pembiayaan ini dan dokumen-dokumen lain adalah benar tanpa menyembunyikan maklumat yang mungkin mempengaruhi permohonan saya/ kami dan pihak bank berhak menolak / menarik balik sekiranya maklumat tersebut tidak benar dan palsu. </b> <br>
                                                          <i> All the information stated in the application form and the relevant supporting documents are true/genuine. The bank reserve the right to reject/withdraw this application in any case of fraud/forged documents. </i> </li>

                                                           <li>   <b> Segala transaksi yang akan dilaksanakan tidak berkaitan apa jua perkara  yang tidak dibenarkan dibawah 'AMLATFA 2001' (Akta Pencegahan Penggubalan Wang Haram dan Pembiayaan  Keganasan 2001). </b> <br>
                                                          <i>All the related transaction hereafter is not connected to Anti Money Laundering and Terrorist Financing Act 2001. <br></i> </li>

                                                          <li>    <b>Tidak dikenakan tindakan kebankrapan dan bukanlah seorang yang muflis seperti yang tertakluk dibawah Seksyen 3 Akta Kebankrapan 1967. Saya / kami akan memaklumkan kepada pihak Bank Persatuan dalam masa 7 hari sekiranya saya / kami berada dalam proses kebankrapan. </b><br><i>No bankruptcy proceeding are being taken against me/us and I/we are not a bankrupt under Section 3 of the Bankruptcy Act 1967, I undertake to notify the Bank within 7 days should there be any bankruptcy proceeding is taken against me/us. </i> </li>
                                                           <li>   <b>Membenarkan pihak Bank Persatuan menghubungi majikan  atau mana-mana pihak untuk mendapatkan sebarang keterangan mengenai saya / kami dan seterusnya membenarkan mereka  memberi keterangan yang diperlukan oleh pihak Bank Persatuan.</b> <br>
                                                          <i>Allow the Bank to contact my employer or any related parties to obtain the relevant information on me / us and thereafter allow them to provide the necessary information.</i> </li>
                                                          <li>  <b>Tiada sebarang permohonan pembiayaan di institusi kewangan lain serentak/dalam tempoh masa yang terdekat ketika borang permohonan ini dihantar ke Bank Persatuan.</b> <br> <i>I/we declare that I/we did not submit any other financing request to other financial institution simultaneously. 
                                                          </i> </li>
                                                          </ol>
 <b>Saya/kami dengan ini memberi kebenaran kepada Bank Persatuan yang tidak boleh dibatal tanpa syarat untuk mendedahkan maklumat berkaitan dengan kemudahan kewangan saya / kami di Bank Persatuan bagi apa-apa tujuan sekalipun kepada Cawangan Bank Persatuan , Subsidiari, Bank Negara Malaysia (BNM), Unit Kredit Pusat (Central Credit Unit), Biro Maklumat Guaman dan agensi lain yang diluluskan oleh BNM atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas Bank Persatuan, dan pemegang  serahan hak yang dibenarkan dan diberikuasa oleh Bank Persatuan. Saya / kami membenarkan pihak Bank Persatuan mendapatkan maklumat peribadi kredit saya / kami dari CTOS, RAM dan mana-mana pihak berkuasa lain yang berkaitan. Maklumat yang diperolehi hanya boleh digunakan bagi tujuan permohonan pembiayaan sahaja. </b> <br> <i> I/we agreed to allow Bank Persatuan to reveal /forward all relevant information relating to this financing to any Bank Persatuan Branch, Subsidiaries, Central Bank of Malaysia (BNM), Central Credit Bureau and any other agencies approved by Central Bank of Malaysia (BNM) or any other authorities supervising Bank Persatuan. I/we hereby agree to allow Bank Persatuan to obtain the relevant information about me/us from CTOS, RAM or any other parties. All relevant information obtained are meant to be used for this specific financing only. <br></i>
							
						</td>
						</tr>
						
						<tr>
						<td colspan="3"> <br><br><br><br><br><br><br></td>
						</tr>

						<tr>
						<td align="center" style="line-height: 5%"> ---------------------------------- <p> Nama / <i>Name </i>  </p></td>
						<td align="center"  style="line-height: 5%">---------------------------------- <p> Tandatangan Pemohon /<i>Applicant's Signature</i> </p></td>
						<td align="center"  style="line-height: 5%"> ---------------------------------- <p>Tarikh / <i>Date</i> </p> </td>
						</tr>
						</table>




