@inject('request', 'Illuminate\Http\Request')
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">
           
            <li>
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span class="title">Settings</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ $request->segment(1) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="">
                            <i class="fa fa-key"></i>
                            <span class="title">
                                Roles
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
           
        </ul>
    </div>
</div>
{!! Form::open(['url' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}