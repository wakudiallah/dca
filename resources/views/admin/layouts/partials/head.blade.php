<meta charset="utf-8">
<title>
    Autismworld.info
</title>

<meta http-equiv="X-UA-Compatible"
      content="IE=edge">
<meta content="width=device-width, initial-scale=1.0"
      name="viewport"/>
<meta http-equiv="Content-type"
      content="text/html; charset=utf-8">

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
      rel="stylesheet"
      type="text/css"/>
<link rel="stylesheet"
      href="{{ url('quickadmin/css') }}/font-awesome.min.css"/>
<link rel="stylesheet"
      href="{{ url('quickadmin/css') }}/bootstrap.min.css"/>
<link rel="stylesheet"
      href="{{ url('quickadmin/css') }}/components.css"/>
<link rel="stylesheet"
      href="{{ url('quickadmin/css') }}/quickadmin-layout.css"/>
<link rel="stylesheet"
      href="{{ url('quickadmin/css') }}/quickadmin-theme-default.css"/>
<link rel="stylesheet"
      href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
      href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet"
      href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css"/>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.css"/>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.standalone.min.css"/>
<script src="{{asset('ckeditor4/ckeditor.js')}}"></script>
<script src="{{asset('ckeditor4/samples/js/sample.js')}}"></script>

    <style type="text/css">
            /*
         CSS for the main interaction
        */
        .tabset > input[type="radio"] {
          position: absolute;
          left: -200vw;
        }

        .tabset .tab-panel {
          display: none;
        }

        .tabset > input:first-child:checked ~ .tab-panels > .tab-panel:first-child,
        .tabset > input:nth-child(3):checked ~ .tab-panels > .tab-panel:nth-child(2),
        .tabset > input:nth-child(5):checked ~ .tab-panels > .tab-panel:nth-child(3),
        .tabset > input:nth-child(7):checked ~ .tab-panels > .tab-panel:nth-child(4),
        .tabset > input:nth-child(9):checked ~ .tab-panels > .tab-panel:nth-child(5),
        .tabset > input:nth-child(11):checked ~ .tab-panels > .tab-panel:nth-child(6) {
          display: block;
        }

        /*
         Styling
       
        body {
          font: 16px/1.5em "Overpass", "Open Sans", Helvetica, sans-serif;
          color: #333;
          font-weight: 300;
        } */

        .tabset > label {
          position: relative;
          display: inline-block;
          padding: 15px 15px 25px;
          border: 1px solid transparent;
          border-bottom: 0;
          cursor: pointer;
          font-weight: 600;
        }

        .tabset > label::after {
          content: "";
          position: absolute;
          left: 15px;
          bottom: 10px;
          width: 22px;
          height: 4px;
          background: #8d8d8d;
        }

        .tabset > label:hover,
        .tabset > input:focus + label {
          color: #06c;
        }

        .tabset > label:hover::after,
        .tabset > input:focus + label::after,
        .tabset > input:checked + label::after {
          background: #06c;
        }

        .tabset > input:checked + label {
          border-color: #ccc;
          border-bottom: 1px solid #fff;
          margin-bottom: -1px;
        }

        .tab-panel {
          padding: 30px 0;
          border-top: 1px solid #ccc;
        }

        /*
         Demo purposes only
          body {
          padding: 30px;
        }
        */
        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }

       

        .tabset {
          max-width: 65em;
        }
        </style>