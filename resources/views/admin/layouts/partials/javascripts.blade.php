<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('quickadmin/js') }}/bootstrap.min.js"></script>
<script src="{{ url('quickadmin/js') }}/main.js"></script>
<script src="{{ asset('js/pages/todolist.js') }}"></script>
<script src="{{ asset('js/pages/dashboard.js') }}" type="text/javascript"></script>

<script>
    window._token = '{{ csrf_token() }}';
</script>
  <script type="text/javascript">
        CKEDITOR.editorConfig = function (config) {
        config.language = 'en';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('editor_en');
    </script>  

    <script type="text/javascript">
        CKEDITOR.editorConfig = function (config) {
        config.language = 'ms';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('editor_ms');
    </script>


    <script type="text/javascript">
        CKEDITOR.editorConfig = function (config) {
        config.language = 'id';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('editor_id');
    </script> 
 <!--<script type="text/javascript">
      


$('#dataTables').DataTable({
    "dom" : "<f<t>ip>",
        "language": {
        "search": " ",
        "searchPlaceholder" : "Carian",
        "emptyTable": "Data tiada dalam rekod",
        "zeroRecords": "Tiada rekod yang sepadan ditemui",
        "info": "Menunjukkan _START_ ke _END_ dari _TOTAL_ penyertaan",
        "paginate": {
            "first":      "Mula",
            "last":       "Habis",
            "next":       "Selepas",
            "previous":   "Sebelum"
            },
        }
});


    </script>-->
@yield('javascript')