@section('menu')
<nav class="b-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-3">
						<div class="b-nav__logo wow slideInLeft" data-wow-delay="0.3s">
							<h3><a href="{{url('/')}}">Insko<span>.my</span></a></h3>
							<h2><a href="{{url('/')}}"><b>{{trans('menu/menu.title')}}</b></a></h2>
						</div>
					</div>
					<div class="col-sm-8 col-xs-7">
						<div class="b-nav__list wow slideInRight" data-wow-delay="0.3s">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse navbar-main-slide" id="nav">
								<ul class="navbar-nav-menu">
									<li><a href="{{url('/')}}">{{trans('menu/menu.home')}}</a></li>
									<!--<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">{{trans('menu/menu.product')}}<span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{url('/product/car')}}">{{trans('menu/menu.car')}}</a></li>
											<li><a href="{{url('/product/household')}}">{{trans('menu/menu.house')}}</a></li>
											<li><a href="{{url('/product/travel')}}">{{trans('menu/menu.travel')}}</a></li>
											<li><a href="{{url('/product/medical')}}">{{trans('menu/menu.medical')}}</a></li>
											<li><a href="{{url('/product/theft')}}">{{trans('menu/menu.theft')}}</a></li>
											<li><a href="{{url('/product/personal')}}">{{trans('menu/menu.personal')}}</a></li>
										</ul>
									</li>-->
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">{{trans('menu/menu.insurance')}}<span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="{{url('/insurance/zurich')}}">Zurich Takaful</a></li>
											<li><a href="{{url('/insurance/takaful-ikhlas')}}">Takaful Ikhlas</a></li>
											<li><a href="{{url('/insurance/kurnia')}}">Kurnia Insurans</a></li>
											<li><a href="{{url('/insurance/tune-protect')}}">Tune Protect</a></li>
											<li><a href="{{url('/insurance/amassurance')}}">AmAssurance</a></li>
											<li><a href="{{url('/insurance/takaful-malaysia')}}">Takaful Malaysia</a></li>
										</ul>
									</li>
									<li><a href="{{url('/page/about-us')}}">{{trans('menu/menu.about')}}</a></li>
									<!--<li><a href="article.html">Services</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle='dropdown' href="#">Blog <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="blog.html">Blog 1</a></li>
											<li><a href="blogTwo.html">Blog 2</a></li>
											<li><a href="404.html">Page 404</a></li>
										</ul>
									</li>
									<li><a href="{{url('/page/faq')}}">{{trans('menu/menu.faq')}}</a></li>-->
									<li><a href="{{url('/page/contact-us')}}">{{trans('menu/menu.contact')}}</a></li>
									@if (Route::has('login'))
            						@auth
            							@if (!(Auth::user())) 
										@elseif(!empty(Auth::user()->role_id=='0'))
                        				<li><a href="{{ url('profile') }}"><b>{{trans('menu/header.profil')}}</b></a></li>
                        				@endif
                        				@if (!(Auth::user())) 
                        				@elseif(!empty(Auth::user()->role_id!='0'))
                        				<li><a href="{{ url('/admin') }}"><b>Admin</b></a></li>
                        				@endif
                        				<li><a href="{{ route('logout') }}"><b>{{trans('menu/header.out')}}</b></a></li>
                    				@else
										<li><a href="{{ route('register') }}"><b>{{trans('menu/header.register')}}</b></a></li>
										
										<li><a href="{{ route('login') }}"><b>{{trans('menu/header.sign')}}</b></a></li>
								 	@endauth
								@endif	

								</ul>

							</div>

						</div>
					</div>
					<div class="col-md-1 col-xs-1 hidden-xs">
						<div class="b-topBar__lang">
							<div class="dropdown">
								<!--<a href="#" class="dropdown-toggle" data-toggle='dropdown'><b>{{trans('menu/header.language')}}</b></a>-->
								    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								      <img src="{!! asset('img/' .  Config::get('languages')[App::getLocale()]  . '.png') !!}"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
								    </a>
								    <ul class="dropdown-menu">
								        @foreach(Config::get('languages') as $lang => $language)
								            @if($lang != App::getLocale())
								                <li>
								                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/' . $language . '.png') !!}">&nbsp; {{$language}}</a>

								                </li>

								            @endif
								        @endforeach
								    </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav><!--b-nav-->
@stop