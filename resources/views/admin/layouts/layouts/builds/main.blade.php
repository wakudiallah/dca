@extends('layouts.builds.master')

@section('container')    
	
    @include('layouts.builds.menu')
    @yield('menu')

	
    <div class="content">
    	@yield('content')
    </div>

    @include('layouts.builds.footer')
    @yield('footer')


@stop

  