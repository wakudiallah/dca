
<!DOCTYPE html>
<html lang="en">

@include('layouts.builds.header3')
@yield('header3')

<body class="menu-on-top" data-spy="scroll">

    <div id="wrapper">
        @yield('container')    
    </div>
   
  


</body>
</html>
