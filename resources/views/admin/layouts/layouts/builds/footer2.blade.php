@section('footer')
<div class="m-home">
			<div class="b-info">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-xs-12">
							<aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.5s">
								<article class="b-info__aside-article">
									<h3>OPENING HOURS</h3>
									<div class="b-info__aside-article-item">
										<h6>Sales Department</h6>
										<p>Mon-Sat : 8:00am - 5:00pm<span>&middot;</span> 
											Sunday is closed</p>
									</div>
									<div class="b-info__aside-article-item">
										<h6>Service Department</h6>
										<p>Mon-Sat : 8:00am - 5:00pm<span>&middot;</span> 
											Sunday is closed</p>
									</div>
								</article>
								<article class="b-info__aside-article">
									<h3>About us</h3>
									<p>Vestibulum varius od lio eget conseq
										uat blandit, lorem auglue comm lodo 
										nisl non ultricies lectus nibh mas lsa 
										Duis scelerisque aliquet. Ante donec
										libero pede porttitor dacu msan esct
										venenatis quis.</p>
								</article>
							</aside>
						</div>
						<div class="col-md-4 col-xs-12">
							<div class="b-info__latest">
								<h3 class=" wow zoomInUp" data-wow-delay="0.5s">LATEST AUTOS</h3>
								<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-info__latest-article-photo m-audi"></div>
									<div class="b-info__latest-article-info">
										<h6><a href="detail.html">MERCEDES-AMG GT S</a></h6>
										<div class="b-featured__item-links m-auto">
											<a href="#">Used</a>
											<a href="#">2014</a>
											<a href="#">Manual</a>
											<a href="#">Orange</a>
											<a href="#">Petrol</a>
										</div>
										<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
									</div>
								</div>
								<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-info__latest-article-photo m-audiSpyder"></div>
									<div class="b-info__latest-article-info">
										<h6><a href="detail.html">AUDI R8 SPYDER V-8</a></h6>
										<div class="b-featured__item-links m-auto">
											<a href="#">Used</a>
											<a href="#">2014</a>
											<a href="#">Manual</a>
											<a href="#">Orange</a>
											<a href="#">Petrol</a>
										</div>
										<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
									</div>
								</div>
								<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-info__latest-article-photo m-aston"></div>
									<div class="b-info__latest-article-info">
										<h6><a href="detail.html">ASTON MARTIN VANTAGE</a></h6>
										<div class="b-featured__item-links m-auto">
											<a href="#">Used</a>
											<a href="#">2014</a>
											<a href="#">Manual</a>
											<a href="#">Orange</a>
											<a href="#">Petrol</a>
										</div>
										<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-12">
							<address class="b-info__contacts wow zoomInUp" data-wow-delay="0.5s">
								<p>contact us</p>
								<div class="b-info__contacts-item">
									<span class="fa fa-map-marker"></span>
									<em>202 W 7th St, Suite 233 Los Angeles,<br />
										California 90014 USA</em>
								</div>
								<div class="b-info__contacts-item">
									<span class="fa fa-phone"></span>
									<em>Phone:  1-800- 624-5462</em>
								</div>
								<div class="b-info__contacts-item">
									<span class="fa fa-fax"></span>
									<em>FAX:  1-800- 624-5462</em>
								</div>
								<div class="b-info__contacts-item">
									<span class="fa fa-envelope"></span>
									<em>Email:  info@domain.com</em>
								</div>
							</address>
							<address class="b-info__map">
								<a href="contacts.html">Open Location Map</a>
							</address>
						</div>
					</div>
				</div>
			</div><!--b-info-->
<footer class="b-footer">
			<a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
						<div class="b-footer__company wow slideInLeft" data-wow-delay="0.3s">
							<div class="b-nav__logo">
								<h3><a href="home.html">Car<span>Insurance</span></a></h3>
							</div>
							<p>2017 NetXpert Sdn Bhd &copy; All Rights Reserved </p>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="b-footer__content wow slideInRight" data-wow-delay="0.3s">
							<div class="b-footer__content-social">
								<a href="#"><span class="fa fa-facebook-square"></span></a>
								<a href="#"><span class="fa fa-twitter-square"></span></a>
								<a href="#"><span class="fa fa-google-plus-square"></span></a>
								<a href="#"><span class="fa fa-instagram"></span></a>
								<a href="#"><span class="fa fa-youtube-square"></span></a>
								<a href="#"><span class="fa fa-skype"></span></a>
							</div>
							<nav class="b-footer__content-nav">
								<ul>
									<li><a href="home.html">Home</a></li>
									<li><a href="404.html">Pages</a></li>
									<li><a href="listings.html">Inventory</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="404.html">Services</a></li>
									<li><a href="blog.html">Blog</a></li>
									<li><a href="listTable.html">Shop</a></li>
									<li><a href="contacts.html">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer><!--b-footer-->
		<!--Main-->   
		<script src="{{url('/builds/js/jquery-1.11.3.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery-ui.min.js')}}"></script>
		<script src="{{url('/builds/js/bootstrap.min.js')}}"></script>
		<script src="{{url('/builds/js/modernizr.custom.js')}}"></script>

		<script src="{{url('/builds/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
		<script src="{{url('/builds/js/waypoints.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery.easypiechart.min.js')}}"></script>
		<script src="{{url('/builds/js/classie.js')}}"></script>

		<!--Switcher-->
		<script src="{{url('/builds/assets/switcher/js/switcher.js')}}"></script>
		<!--Owl Carousel-->
		<script src="{{url('/builds/assets/owl-carousel/owl.carousel.min.js')}}"></script>
		<!--bxSlider-->
		<script src="{{url('/builds/assets/bxslider/jquery.bxslider.js')}}"></script>
		<!-- jQuery UI Slider -->
		<script src="{{url('/builds/assets/slider/jquery.ui-slider.js')}}"></script>

		<!--Theme-->
		<script src="{{url('/builds/js/jquery.smooth-scroll.js')}}"></script>
		<script src="{{url('/builds/js/wow.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery.placeholder.min.js')}}"></script>
		<script src="{{url('/builds/js/theme.js')}}"></script>
	</body>
</html>
@stop