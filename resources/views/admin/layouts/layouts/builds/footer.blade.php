@section('footer')
<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-3">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">{{trans('menu/footer.easy')}}</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">{{trans('menu/footer.saving')}}</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">{{trans('menu/footer.advice')}}</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features ddd-->


<footer class="b-footer">
			<a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
			<div class="container">
				<div class="row">
					<div class="col-lg-12" align="center">
						<h1 align="center"><font color="#fdc600">{{trans('menu/footer.individual')}}</font></h1>
						<br>
						<h3><font color="white">{{trans('menu/footer.connect')}}</font></h3><br>
						<font color="#fdc600"><i class="fa fa-facebook-square fa-5x"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-google-plus-square fa-5x"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-twitter fa-5x"></i></font>
						
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12" align="justify">
						<p></p><p></p><font size="2" color="white">
						<!--<p>{{trans('menu/footer.footer')}}</p></font>-->
					</div>
				</div>
			</div><br><br>
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
						<div class="b-footer__company wow slideInLeft" data-wow-delay="0.3s">
							<div class="b-nav__logo">
								<h3><a href="{{url('/')}}">Insko<span>.MY</span></a></h3>
								<h2><a href="{{url('/')}}"><b>{{trans('menu/footer.title')}}</b></a></h2>
							</div>
							<p>2018 NetXpert Sdn Bhd &copy; All Rights Reserved </p>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="b-footer__content wow slideInRight" data-wow-delay="0.3s">
							<nav class="b-footer__content-nav">
								<ul>
									<li><a href="{{url('/')}}">{{trans('menu/footer.home')}}</a></li>
									<li><a href="{{url('/page/privacy_notice')}}">Privacy Notice</a></li>
									<li><a href="{{url('/product/car')}}">{{trans('menu/menu.product')}}</a></li>
									<li><a href="{{url('/register')}}">{{trans('menu/footer.renew')}}</a></li>
									<li><a href="{{url('/page/faq')}}">{{trans('menu/menu.faq')}}</a></li>
									<li><a href="{{url('/page/contact-us')}}">{{trans('menu/footer.contact')}}</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer><!--b-footer-->
		<!--Main-->   
		<script src="{{url('/builds/js/jquery-ui.min.js')}}"></script>
		<script src="{{url('/builds/js/bootstrap.min.js')}}"></script>
		<script src="{{url('/builds/js/modernizr.custom.js')}}"></script>

		<script src="{{url('/builds/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
		<script src="{{url('/builds/js/waypoints.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery.easypiechart.min.js')}}"></script>
		<script src="{{url('/builds/js/classie.js')}}"></script>

		<!--Switcher-->
		<script src="{{url('/builds/assets/switcher/js/switcher.js')}}"></script>
		<!--Owl Carousel-->
		<script src="{{url('/builds/assets/owl-carousel/owl.carousel.min.js')}}"></script>
		<!--bxSlider-->
		<script src="{{url('/builds/assets/bxslider/jquery.bxslider.js')}}"></script>
		<!-- jQuery UI Slider -->
		<script src="{{url('/builds/assets/slider/jquery.ui-slider.js')}}"></script>

		<!--Theme-->
		<script src="{{url('/builds/js/jquery.smooth-scroll.js')}}"></script>
		<script src="{{url('/builds/js/wow.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery.placeholder.min.js')}}"></script>
		<script src="{{url('/builds/js/theme.js')}}"></script>
		<script src="{{url('/js/bootbox.min.js')}}"></script>
	</body>
</html>
@stop