@section('header')<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta charset="UTF-8">
<meta name="description" content="car insurance malaysia comparison">
<meta name="keywords" content="Car insurance,car insurance malaysia comparison, car insurance comparison, car insurance malaysia,  Semak sebutharga">
<meta name="author" content="Netxpert Sdn Bhd & Co-op Bank Pertama">


		<title>Insko - car insurance malaysia comparison</title>

		<link rel="shortcut icon" type="image/x-icon" href="{{url('/builds/images/favicon.png')}}" />

		<link href="{{url('/builds/css/master.css')}}" rel="stylesheet">

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="{{url('/builds/assets/switcher/css/switcher.css')}}" media="all"  />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color1.css')}}" title="color1" media="all" data-default-color="true"  />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color2.css')}}" title="color2" media="all"  />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color3.css')}}" title="color3" media="all"  />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color4.css')}}" title="color4" media="all"  />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color5.css')}}" title="color5" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="{{url('/builds/assets/switcher/css/color6.css')}}" title="color6" media="all" />
		<script src="{{url('/builds/js/jquery-1.11.3.min.js')}}"></script>
		<script src="{{url('/builds/js/jquery-ui.min.js')}}"></script>

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="m-index" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<!-- 
		<div class="switcher-wrapper">	
			<div class="demo_changer">
				<div class="demo-icon customBgColor"><i class="fa fa-cog fa-spin fa-2x"></i></div>
				<div class="form_holder">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="predefined_styles">
								<div class="skin-theme-switcher">
									<h4>Color</h4>
									<a href="#" data-switchcolor="color1" class="styleswitch" style="background-color:#f76d2b;"> </a>
									<a href="#" data-switchcolor="color2" class="styleswitch" style="background-color:#de483d;"> </a>
									<a href="#" data-switchcolor="color3" class="styleswitch" style="background-color:#228dcb;"> </a>
									<a href="#" data-switchcolor="color4" class="styleswitch" style="background-color:#00bff3;"> </a>
									<a href="#" data-switchcolor="color5" class="styleswitch" style="background-color:#2dcc70;"> </a>
									<a href="#" data-switchcolor="color6" class="styleswitch" style="background-color:#6054c2;"> </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->

		<header class="b-topBar">
			<div class="container wow slideInDown" data-wow-delay="0.7s">
				<div class="row hidden-md hidden-lg hidden-sm">
					<div class="col-md-4 col-xs-6 hidden-md hidden-lg">
						<div class="b-topBar__addr">
							
						</div>
					</div>
					<div class="col-md-2 col-xs-6 hidden-xs">
						<div class="b-topBar__tel">
							
						</div>
					</div>
					<div class="col-md-4 col-xs-6 hidden-md hidden-lg hidden-sm">
						<nav class="b-topBar__nav">
							<ul>
								@if (Route::has('login'))
            						@auth
            							
                        				<li><a href="{{ route('logout') }}"><b>{{trans('menu/header.out')}}</b></a></li>
                        				<li><a href="{{ url('profile') }}"><b>{{trans('menu/header.profil')}}</b></a></li>
                    			@else
										<li><a href="{{ route('register') }}"><b>{{trans('menu/header.register')}}</b></a></li>
										<li><a href="{{ route('login') }}"><b>{{trans('menu/header.sign')}}</b></a></li>
								 	@endauth
								@endif	
							</ul>
						</nav>
					</div>
					<div class="col-md-2 col-xs-8 hidden-md hidden-lg">
						<div class="b-topBar__lang">
							<div class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle='dropdown'><b>{{trans('menu/header.language')}}</b></a>
								    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								      <img src="{!! asset('img/' .  Config::get('languages')[App::getLocale()]  . '.png') !!}"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
								    </a>
								    <ul class="dropdown-menu">
								        @foreach(Config::get('languages') as $lang => $language)
								            @if($lang != App::getLocale())
								                <li>
								                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/' . $language . '.png') !!}">&nbsp; {{$language}}</a>

								                </li>

								            @endif
								        @endforeach
								    </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header><!--b-topBar-->
@stop

		
		
		