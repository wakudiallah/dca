@section('footer')
<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2 logo">Autismworld<span></span></h2>
                    <p>{{trans('footer.desc')}}</p>
                </div>
                <div class="ftco-footer-widget mb-5">
            	   <h2 class="ftco-heading-2">{{trans('footer.question')}}</h2>
            	   <div class="block-23 mb-3">
    	               <ul>
    	                   <li><span class="icon icon-map-marker"></span><span class="text">Kuala Lumpur</span></li>
    	                   <li><a href="#"><span class="icon icon-phone"></span><span class="text">+(603) </span></a></li>
    	                   <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@autismworld.com</span></a></li>
    	              </ul>
	               </div>
                   <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-5 ml-md-4">
                    <h2 class="ftco-heading-2">{{trans('footer.links')}}</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{url('/')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.home')}}</a></li>
                        <li><a href="{{url('/about-us')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.about')}}</a></li>
                        <li><a href="{{url('/technology')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.technology')}}</a></li>
                        <li><a href="{{url('/resources')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.resources')}}</a></li>
                        <li><a href="{{url('/contact-us')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.contact')}}</a></li>
                    </ul>
                </div>
                <div class="ftco-footer-widget mb-5 ml-md-4">
                    <h2 class="ftco-heading-2">{{trans('menu.therapy')}}</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{url('/therapy/sensory_therapy')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.sensory_therapy')}}</a></li>
                        <li><a href="{{url('/therapy/hippo')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.hippo')}}</a></li>
                        <li><a href="{{url('/therapy/lego')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.lego')}}</a></li>
                        <li><a href="{{url('/therapy/messy-play')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.mossy')}}</a></li>
                        <li><a href="{{url('/therapy/speech')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.speech')}}</a></li>
                        <li><a href="{{url('/therapy/psikomotor')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.psikomotor')}}</a></li>
                        <li><a href="{{url('/therapy/cognitive')}}"><span class="ion-ios-arrow-round-forward mr-2"></span>{{trans('menu.cognitive')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2"><a href="{{url('/')}}/news">{{trans('footer.recent')}}</a></h2>
                    <?php

                        $locale   = \App::getLocale() ?: 'ms';
                        $news = \App\Model\NewsTranslation::where('locale',$locale)->orderBy('created_at', 'desc')->paginate('3');
                    ?>
                    @foreach($news as $news)
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url('{{url('/')}}/images/news/{{$news->Trans->images}}');"></a>
                            <div class="text">
                                <h3 class="heading"><a href="{{url('/')}}/news/{{$news->id_news}}">{{$news->title}}</a></h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span>&nbsp;{{$news->created_at->format('jS F Y h:i:s A')}}</a></div>
                                    <div><a href="#"><span class="icon-person"></span> {{$news->CreatedBy->name}}</a></div>
                                    <!--<div><a href="#"><span class="icon-chat"></span> 19</a></div>-->
                                </div>
                            </div>
                        </div>
                    @endforeach

                   
                </div>
            </div>
            <!--<div class="col-md">
          	    <div class="ftco-footer-widget mb-5">
            	   <h2 class="ftco-heading-2">{{trans('footer.opening')}}</h2>
            	   <h3 class="open-hours pl-4"><span class="ion-ios-time mr-3"></span>{{trans('footer.24hours')}}</h3>
                </div>-->
                <!--<div class="ftco-footer-widget mb-5">
            	    <h2 class="ftco-heading-2">{{trans('footer.subscribe')}}</h2>
                    <form action="#" class="subscribe-form">
                        <div class="form-group">
                            <input type="text" class="form-control mb-2 text-center" placeholder="{{trans('footer.subscribe')}}">
                            <input type="submit" value="{{trans('footer.subscribe')}}" class="form-control submit px-3">
                        </div>
                    </form>
                </div>-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by <a href="https://colorlib.com" target="_blank">Persatuan Rakan Amal Titiwangsa</a></p>
            </div>
        </div>
    </div>
</footer>
    
<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
<script src="{{url('/drcare/js/jquery.min.js')}}"></script>
<script src="{{url('/drcare/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{url('/drcare/js/popper.min.js')}}"></script>
<script src="{{url('/drcare/js/bootstrap.min.js')}}"></script>
<script src="{{url('/drcare/js/jquery.easing.1.3.js')}}"></script>
<script src="{{url('/drcare/js/jquery.waypoints.min.js')}}"></script>
<script src="{{url('/drcare/js/jquery.stellar.min.js')}}"></script>
<script src="{{url('/drcare/js/owl.carousel.min.js')}}"></script>
<script src="{{url('/drcare/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('/drcare/js/aos.js')}}"></script>
<script src="{{url('/drcare/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{url('/drcare/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('/drcare/js/jquery.timepicker.min.js')}}"></script>
<script src="{{url('/drcare/js/scrollax.min.js')}}"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{url('/drcare/js/google-map.js')}}"></script>-->
<script src="{{url('/drcare/js/main.js')}}"></script>
<script type="text/javascript" src="{{url('/medina/js/script.js')}}"></script>
<script src="{{url('/medina/js/owl.carousel/owl.carousel.min.js')}}"></script>
        
 <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
   
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-103790523-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-103790523-2');
</script>

    </body>
</html>
@stop