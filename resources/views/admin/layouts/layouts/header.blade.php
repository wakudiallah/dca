@section('header')
<!DOCTYPE html>
<html lang="en">

  	<head>
	    <title>Autismworld</title>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    
	    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	    <link rel="stylesheet" href="{{url('/drcare/css/open-iconic-bootstrap.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/animate.css')}}">
	    
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.carousel.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.theme.default.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/magnific-popup.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/aos.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/ionicons.min.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/jquery.timepicker.css')}}">


		<!-- Favicon-->
		<link rel="shortcut icon" href="{{asset('favico/fav.ico')}}">

	    
	    <!--<link rel="stylesheet" href="{{url('/drcare/css/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/myicon/font/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/dr/font/flaticon.css')}}">-->
	    <link rel="stylesheet" href="{{url('/new/font/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/icomoon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/style.css')}}">

	    <link rel="stylesheet" href="{{url('/medina/css/medical-icons.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/social-icons.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/icon-font.css')}}">
		<link rel="stylesheet" href="{{url('/medina/js/owl.carousel/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/frontend-grid.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/style.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/mobile.css')}}">

  	</head>
  	<style type="text/css">
  		.autism p{
  			text-align: justify !important;
  			font-size: 22px !important;
  			line-height: 1.5 !important;
  			color: #333333 !important;
  			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; }
  	</style>
 	
  	<body>
  	
  	<header class="header clearfix">
		<!-- Top header -->
		<div class="fw-main-row top-header">
			<div class="fw-container">
				<div class="fl">
					<!-- Social link -->
					<div class="social-link">
						<a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
						<a href="javascript:void(0);"><i class="social-icons icon-twitter-social-logotype"></i></a>
						<a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
					</div><!-- END Social link -->
				</div>
				<div class="fr">
					<!-- Contact item -->
					<span class="contact-item"><i class="icon-font icon-placeholder-1"></i> <span>{{trans('global.address')}}: Kuala Lumpur, Malaysia</span></span>
					<!-- END Contact item -->
					<!-- Contact item -->
					<span class="contact-item"><i class="icon-font icon-clock-2"></i> <span>{{trans('global.monday')}} - {{trans('global.friday')}} 10:00-17:00</span></span>
					<!-- END Contact item -->
					<!-- Contact item -->
					<span class="contact-item"><i class="icon-font icon-telephone-1"></i> <span>{{trans('global.phone')}}: </span></span>
					<!-- END Contact item -->

				</div>
			</div>
		</div>

		<div class="fw-main-row header-wrap">
			<div class="fw-container">
				<div class="fw-row">
					<div class="fw-col-sm-3 fw-col-md-2 logo-area"><a href="/"><img src="{{asset('img/logo.png')}}" alt="Medina template" width="150px" height="350px"></a></div>
					<nav class="fw-col-sm-10 fw-col-md-10 navigation">
						<ul style="width:1060px !important; text-align: justify !important;
  			font-size: 14px !important;
  			line-height: 1.5 !important;
  			color: #333333 !important;
  			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" >
                        	<li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{url('/')}}" >{{trans('menu.home')}}</a></li>
                        	<li class="menu-item-has-children">
                            	<a href="">{{trans('menu.autism')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                      	<li class="{{ Request::is('what-is-autism/type-of-autism') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/type-of-autism')}}">{{trans('menu.type')}}</a></li>
                                        <li class="{{ Request::is('what-is-autism/early-detection') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/early-detection')}}">{{trans('menu.early_detection')}}</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">{{trans('menu.screening')}}</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening')}}</a></li>
                                                    <li><a href="javascript:void(0);">{{trans('menu.screening_ins')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                        <li class="{{ Request::is('what-is-autism/symptom') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/symptom')}}">{{trans('menu.symptom')}}</a></li>
                                    </ul>
                                </div>
                        	</li>
                            <!-- <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.screening')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening_ins')}}</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li class="{{ Request::is('treatment') ? 'current-menu-item' : '' }}"><a href="{{url('/treatment')}}">{{trans('menu.treatment')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="our-team.html">{{trans('menu.therapy')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="{{url('/therapy/sensory')}}">{{trans('menu.sensory_therapy')}}</a></li>
                                        <li><a href="{{url('/therapy/hippo')}}">{{trans('menu.hippo')}}</a></li>
                                        <li><a href="{{url('/therapy/lego')}}">{{trans('menu.lego')}}</a></li>
                                        <li><a href="{{url('/therapy/messy-play')}}">{{trans('menu.mossy')}}</a></li>
                                        <li><a href="{{url('/therapy/speech-therapy')}}">{{trans('menu.speech')}}</a></li>
                                        <li><a href="{{url('/therapy/psikomotor')}}">{{trans('menu.psikomotor')}}</a></li>
                                        <li><a href="{{url('/therapy/cognitive')}}">{{trans('menu.cognitive')}}</a></li>

                                        <li class="menu-item-has-children">
                                           <a href="javascript:void(0);">{{trans('menu.edu_games')}}</a>
                                           <div class="sub-nav">
                                              <ul class="sub-menu">
                                                 <!-- END Menu item has children -->
                                                 <li><a href="{{url('/therapy/edu-games/numbering')}}">{{trans('menu.numbering')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/alphabet')}}">{{trans('menu.alphabet')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/daily-living')}}">{{trans('menu.daily')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                     </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">{{trans('menu.sensory')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/multi-sensory')}}">{{trans('menu.multy_sensory')}}</a></li>
                                        <li class=""><a href="{{url('/sensory-tool')}}">{{trans('menu.sensory_tool')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            @if (Route::has('login'))
                            <li class=" {{ (request()->is('forum-autism')) ? 'current-menu-item' : '' }}"><a href="{{url('/forum-autism')}}">{{trans('menu.forum')}}</a></li>
                            @endif
                            <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.technology')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=" {{ (request()->is('technology')) ? 'current-menu-item' : '' }}"><a href="{{url('/technology')}}">{{trans('menu.technology')}}</a></li>
                                    	<li class=" {{ (request()->is('news')) ? 'current-menu-item' : '' }}"><a href="{{url('/news')}}">{{trans('menu.news')}}</a></li>
                                        <li class=" {{ (request()->is('resources')) ? 'current-menu-item' : '' }}"><a href="{{url('/resources')}}">{{trans('menu.resources')}}</a></li>
                                        <li class=" {{ (request()->is('/directory')) ? 'current-menu-item' : '' }}"><a href="{{url('/directory')}}">{{trans('menu.directory')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                        	<li class=" {{ (request()->is('galleries')) ? 'current-menu-item' : '' }}"><a href="{{url('/galleries')}}">{{trans('menu.galleries')}}</a></li>
                        	<li class="menu-item-has-children">
                            	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                	<img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
                            	</a>
                            	<div class="sub-nav">
                                	<ul class="sub-menu">
                                        @foreach(Config::get('languages') as $lang => $language)
                                            @if($lang != App::getLocale())
                                                <li>
                                                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                	</ul>
                            	</div>
                        	</li>
                        	@if (Route::has('login'))
			            		@auth
									@if (!(Auth::user())) 
									@elseif(!empty(Auth::user()->role_id=='2'))
									<li>
			        					<p class="button-custom order-lg-last mb-0"><a href="{{url('/profile')}}" class="btn btn-primary py-2 px-3">{{trans('menu.profile')}}</a></p>
			        				</li>
			        				@endif
			        				@if (!(Auth::user())) 
			        				@elseif(!empty(Auth::user()->role_id=='1'))
			        				<li>
			        					<p class="button-custom order-lg-last mb-0"><a href="{{url('/profile')}}" class="btn btn-primary py-2 px-3">{{trans('menu.admin')}}</a></p>
			        				</li>
			        				@endif
			        				<li>
			        					<p class="button-custom order-lg-last mb-0"><a href="{{url('/logout')}}" class="btn btn-warning py-2 px-3" onclick=" return confirm('Are You sure you want to logout?');">{{trans('menu.logout')}}</a></p>
			        				</li>
			    				@else
									<li>
	                        			<p class="button-custom order-lg-last mb-0"><a href="{{url('/login')}}" class="btn btn-secondary py-2 px-3">{{trans('menu.login')}}</a></p>
			      					</li>
									<li>
										<p class="button-custom order-lg-last mb-0"><a href="{{url('/register')}}" class="btn btn-info py-2 px-3">{{trans('menu.register')}}</a></p>
									</li>
							 	@endauth
							@endif
                    	</ul>
					</nav>
						
					<!-- Mobile side button -->
					<div class="mobile-side-button"><i class="icon-font icon-menu"></i></div>
					<!-- END Mobile side button -->
					<!-- Mobiile side -->
					<div class="mobile-side">
						<!-- Social link -->
						<div class="social-link">
							<a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
							<a href="javascript:void(0);"><i class="social-icons icon-twitter-social-logotype"></i></a>
							<a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
						</div>
						<!-- END Social link
						<div class="search-module">
							<form action="javascript:void(0);">
								<input type="text" class="input" name="search" placeholder="Search">
								<button type="submit" class="submit"><i class="icon-font icon-search"></i></button>
							</form>
						</div> -->
							<!-- Mobile navigation -->
						<nav class="mobile-navigation">
							<ul>
								<li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{url('/')}}">{{trans('menu.home')}}</a></li>
								<li class="menu-item-has-children">
                                	<a href="">{{trans('menu.autism')}}</a>
	                                <div class="sub-nav">
	                                    <ul class="sub-menu">
	                                      	<li class="{{ Request::is('what-is-autism/type-of-autism') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/type-of-autism')}}">{{trans('menu.type')}}</a></li>
	                                        <li class="{{ Request::is('what-is-autism/early-detection') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/early-detection')}}">{{trans('menu.early_detection')}}</a></li>
	                                        <li class="menu-item-has-children">
	                                            <a href="javascript:void(0);">	{{trans('menu.screening')}}</a>
	                                            <div class="sub-nav">
	                                                <ul class="sub-menu">
	                                                    <li><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening')}}</a></li>
	                                                    <li><a href="javascript:void(0);">{{trans('menu.screening_ins')}}</a></li>
	                                              </ul>
	                                           </div>
	                                        </li>
	                                        <li class="{{ Request::is('what-is-autism/symptom') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/symptom')}}">{{trans('menu.symptom')}}</a></li>
	                                    </ul>
	                                </div>
                            	</li>
                            	<li class="{{ Request::is('treatment') ? 'current-menu-item' : '' }}"><a href="{{url('/treatment')}}">{{trans('menu.treatment')}}</a></li>
							    <li class="menu-item-has-children">
							        <a href="our-team.html">{{trans('menu.therapy')}}</a>
							        <div class="sub-nav">
							            <ul class="sub-menu">
							                <li><a href="{{url('/therapy/sensory')}}">{{trans('menu.sensory_therapy')}}</a></li>
							                <li><a href="{{url('/therapy/hippo')}}">{{trans('menu.hippo')}}</a></li>
							                <li><a href="{{url('/therapy/lego')}}">{{trans('menu.lego')}}</a></li>
							                <li><a href="{{url('/therapy/messy-play')}}">{{trans('menu.mossy')}}</a></li>
							                <li><a href="{{url('/therapy/speech-therapy')}}">{{trans('menu.speech')}}</a></li>
							                <li><a href="{{url('/therapy/psikomotor')}}">{{trans('menu.psikomotor')}}</a></li>
							                <li><a href="{{url('/therapy/cognitive')}}">{{trans('menu.cognitive')}}</a></li>

							                <li class="menu-item-has-children">
							                   <a href="javascript:void(0);">{{trans('menu.edu_games')}}</a>
							                   <div class="sub-nav">
							                      <ul class="sub-menu">
							                         <!-- END Menu item has children -->
							                         <li><a href="{{url('/therapy/edu-games/numbering')}}">{{trans('menu.numbering')}}</a></li>
							                         <li><a href="{{url('/therapy/edu-games/alphabet')}}">{{trans('menu.alphabet')}}</a></li>
							                         <li><a href="{{url('/therapy/edu-games/daily-living')}}">{{trans('menu.daily')}}</a></li>
							                      </ul>
							                   </div>
							                </li>
							             </ul>
							        </div>
							    </li>
							    <li class="menu-item-has-children">
						            <a href="#">{{trans('menu.sensory')}}</a>
						            <div class="sub-nav">
						                <ul class="sub-menu">
						                	<li class=""><a href="{{url('/multi-sensory')}}">{{trans('menu.multy_sensory')}}</a></li>
						                    <li class=""><a href="{{url('/sensory-tool')}}">{{trans('menu.sensory_tool')}}</a></li>
						                </ul>
						            </div>
						        </li>
								@if (Route::has('login'))
						        <li class=" {{ (request()->is('forum-autism')) ? 'current-menu-item' : '' }}"><a href="{{url('/forum-autism')}}">{{trans('menu.forum')}}</a></li>
						        @endif
						        <li class="menu-item-has-children">
						            <a href="/">{{trans('menu.technology')}}</a>
						            <div class="sub-nav">
						                <ul class="sub-menu">
						                	<li class=" {{ (request()->is('technology')) ? 'current-menu-item' : '' }}"><a href="{{url('/technology')}}">{{trans('menu.technology')}}</a></li>
						                	<li class=" {{ (request()->is('news')) ? 'current-menu-item' : '' }}"><a href="{{url('/news')}}">{{trans('menu.news')}}</a></li>
						                    <li class=" {{ (request()->is('resources')) ? 'current-menu-item' : '' }}"><a href="{{url('/resources')}}">{{trans('menu.resources')}}</a></li>
						                    <li class=" {{ (request()->is('/directory')) ? 'current-menu-item' : '' }}"><a href="{{url('/directory')}}">{{trans('menu.directory')}}</a></li>
						                </ul>
						            </div>
						        </li>
						        <li class=" {{ (request()->is('galleries')) ? 'current-menu-item' : '' }}"><a href="{{url('/galleries')}}">{{trans('menu.galleries')}}</a></li>

						       @if (Route::has('login'))
			            		@auth
									@if (!(Auth::user())) 
									@elseif(!empty(Auth::user()->role_id=='2'))
									<li class=" {{ (request()->is('profile')) ? 'current-menu-item' : '' }}"><a href="{{url('/profile')}}">{{trans('menu.profile')}}</a></li>
			        				@endif
			        				@if (!(Auth::user())) 
			        				@elseif(!empty(Auth::user()->role_id=='1'))
			        				<li class=" {{ (request()->is('admin')) ? 'current-menu-item' : '' }}"><a href="{{url('/admin')}}">{{trans('menu.admin')}}</a></li>
			        				@endif
			        				<li>
			        					<p class="button-custom order-lg-last mb-0"><a href="{{url('/logout')}}" class="btn btn-warning py-2 px-3" onclick=" return confirm('Are You sure you want to logout?');">{{trans('menu.logout')}}</a></p>
			        				</li>
			    				@else
									<li class=" {{ (request()->is('login')) ? 'current-menu-item' : '' }}"><a href="{{url('/login')}}">{{trans('menu.login')}}</a></li>
									<li class=" {{ (request()->is('register')) ? 'current-menu-item' : '' }}"><a href="{{url('/register')}}">{{trans('menu.register')}}</a></li>
							 	@endauth
							@endif

						    	<li class="menu-item-has-children">
						        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						            	<img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
						        	</a>
						        	<div class="sub-nav">
						            	<ul class="sub-menu">
						                    @foreach(Config::get('languages') as $lang => $language)
						                        @if($lang != App::getLocale())
						                            <li>
						                                <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>
						                            </li>
						                        @endif
						                    @endforeach
						            	</ul>
						        	</div>
						    	</li>
						    </ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>
@stop

		
		
		