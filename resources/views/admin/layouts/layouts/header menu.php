@section('header')
<!DOCTYPE html>
<html lang="en">

  	<head>
	    <title>Autismwall.info</title>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    
	    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	    <link rel="stylesheet" href="{{url('/drcare/css/open-iconic-bootstrap.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/animate.css')}}">
	    
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.carousel.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.theme.default.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/magnific-popup.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/aos.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/ionicons.min.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/jquery.timepicker.css')}}">


		<!-- Favicon-->
		<link rel="shortcut icon" href="{{asset('favico/fav.ico')}}">

	    
	    <!--<link rel="stylesheet" href="{{url('/drcare/css/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/myicon/font/flaticon.css')}}">-->
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/dr/font/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/icomoon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/style.css')}}">
	    	<link rel="stylesheet" href="{{url('/medina/js/owl.carousel/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/frontend-grid.css')}}">
  	</head>

  	<style type="text/css">
		.navigation {
		    /* border-left: 1px solid #dddddd; */
		    /* border-right: 1px solid #dddddd; */
		    text-transform: uppercase;
		}

		.navigation ul {
		    margin: 0;
		    padding: 0;
		}

		.navigation ul li {
		    display: block;
		    position: relative;
		}

		.navigation ul li a {
		    text-decoration: none;
		}

		.navigation > ul {
		    text-align: center;
		    border-left: 1px solid #dddddd;
		    border-right: 1px solid #dddddd;
		}

		.navigation > ul > li {
		    display: inline-block;
		    vertical-align: middle;
		    margin: 0 16px;
		    text-align: left;
		}

		.navigation > ul > li > a {
		    display: table-cell;
		    height: 70px;
		    vertical-align: middle;
		}

		.navigation > ul > li.current-menu-item {}

		.navigation > ul > li.current-menu-item > a,
		.navigation > ul > li:hover > a {
		    box-shadow: inset 0 -3px 0 0 #23abe1;
		}

		.navigation .sub-nav {
		    position: absolute;
		    top: 70px;
		    left: -5px;
		    width: 150px;
		    visibility: hidden;
		    opacity: 0;
		    -webkit-transition: all 0.3s ease;
		    -moz-transition: all 0.3s ease;
		    -o-transition: all 0.3s ease;
		    -ms-transition: all 0.3s ease;
		    transition: all 0.3s ease;
		    border: 1px solid #dddddd;
		    background: #fff;
		}

		.navigation .sub-nav .sub-nav {
		    top: -1px;
		    left: 100%;
		}

		.navigation ul li.menu-item-has-children:hover > .sub-nav {
		    opacity: 1;
		    visibility: visible;
		    z-index: 2;
		}

		.navigation .sub-nav .sub-menu {}

		.navigation .sub-nav .sub-menu li {
		}

		.navigation .sub-nav .sub-menu li:not(:last-of-type) {
		    border-bottom: 1px solid #ddd;
		}

		.navigation .sub-nav .sub-menu li a {
		    height: 35px;
		    display: table-cell;
		    vertical-align: middle;
		    padding: 4px 0;
		    width: 150px;
		    padding: 3px 15px;
		    font-size: 0.857em;
		    position: relative;
		}

		.navigation .sub-nav .sub-menu li.menu-item-has-children > a:after {
		    content: '\e843';
		    font-family: "icon-font";
		    position: absolute;
		    color: #dddddd;
		    right: 10px;
		    top: 50%;
		    transform: translate(0, -50%);
		    -webkit-transform: translate(0, -50%);
		}

		.navigation .sub-nav .sub-menu li.current-menu-item > a, 
		.navigation .sub-nav .sub-menu li:hover > a {
		    background: #23abe1;
		    color: #fff;
		}

		@media screen and (max-width: 1360px){
}

@media screen and (max-width: 1200px){
	.navigation > ul > li {
		margin: 0px 15px;
	}
	.day-item .cell,
	.day-item .circle {
		width: 100px;
		height: 100px;
	}

	.day-item .circle:before {
		width: 40px;
		margin: -3px 0 -3px -20px;
	}

	.day-item {
		font-size: 13px;
	}
}

@media screen and (max-width: 990px){
	body {
		font-size: 11px;
	}

	.button-style1 {
		font-size: 14px;
		padding: 15px 10px;
	}

	.button-style1.min {
		font-size: 12px;
		padding: 10px 10px
	}

	.button-style2 {
		font-size: 12px;
		padding: 8px 15px;
	}

	.button-style2.min {
		font-size: 11px;
		padding: 7px 15px;
	}

	.button-style2.big {
		font-size: 14px;
		padding: 8px 15px;
	}

	.top-header {
		display: none;
	}

	.header-wrap {
		position: fixed;
		top: 0;
	}

	.navigation {
		display: none;
	}

	.logo-area a {
		height: 45px;
	}

	.logo-area img {
		max-height: 30px;
	}

	.header .search-module {
		display: none;
	}

	.mobile-side-button {
		display: block;
	}

	.mobile-side .search-module {
		display: block;
		padding: 15px 20px;
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
	}

	.contact-item:not(:last-of-type) {
		margin-right: 10px;
	}

	.testimonials-item .text .top, .desc-block .top {
		height: 80px;
	}

	.day-item .cell,
	.day-item .circle {
		width: 70px;
		height: 70px;
	}

	.day-item .circle:before {
		width: 40px;
		margin: -3px 0 -3px -20px;
	}

	.day-item {
		font-size: 9px;
	}

	.footer [class^="fw-col-"]:not(:last-of-type) {
		margin-bottom: 30px;
	}
}

@media screen and (max-width: 768px){
	.button-style1 {
	    font-size: 12px;
	    padding: 10px 10px;
	    min-width: 110px;
	}

	.full-screen {
	    font-size: 9px;
	}

	.full-screen .h1 {
	    margin-bottom: 10px;
	}

	.full-screen p {
	    margin: 10px 0;
	}

	.full-screen .container {
	    /* text-align: right; */
	}

	.full-screen p {
	    font-size: 1.486em;
	}

	.footer [class^="fw-col-"] {
		border:none;
	}

	.testimonials-item .text {
		margin: 0 auto;
		max-width: 480px;
	}

	.testimonials-item .image {
		max-width: 450px;
		width: 100%;
		height: 280px;
		position: relative;
		margin: 0 auto;
	}

	.testimonials-item .image div {
		left: 0;
	}

	.day-item .cell,
	.day-item .circle {
		width: 115px;
		height: 115px;
	}

	.day-item .circle:before {
		width: 60px;
		margin: -3px 0 -3px -30px;
	}

	.day-item {
		font-size: 13px;
	}

	.doctor-col img {
		max-width: 480px;
		width: 80%;
	}

	.contact-col {
		border: none !important;
	}

	.full-screen .h1 {
		font-size: 3em;
	}

	.blog-item.in-row .image {
		margin-bottom: 30px;
	}

	.full-screen .blue-color {
	    color: inherit;
	}

}

@media screen and (max-width: 640px){
}
@media screen and (max-width: 380px){
}
/*------------------------------------------------------------------
[2.2 Search module / .search-module ]
*/

.search-module {
    padding: 23px 15px 0 30px;
}

.search-module form {
    position: relative;
}

.search-module .input {
    padding: 0;
    margin: 0;
    border: none;
    color: #888888;
    font-size: 14px;
    width: 100%;
    line-height: 23px;
    height: 23px;
}

.search-module .submit {
    border: none;
    background: #fff;
    font-size: 19px;
    padding: 0 5px;
    position: absolute;
    right: 0;
    top: 0;
    cursor: pointer;
}

.search-module .submit i {}

/*------------------------------------------------------------------
[2.3 Mobile side / .mobile-side ]
*/

.mobile-side {
    position: fixed;
    top: 45px;
    right: -300px;
    bottom: 0;
    background: #fff;
    border-top: 1px solid #ddd;
    overflow-x: hidden;
    overflow-y: auto;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

.mobile-side.active {
    right: 0;
}

.mobile-side-button {
    float: right;
    font-size: 37px;
    display: none;
    line-height: 45px;
    padding: 0 10px;
}

.mobile-navigation {
    min-width: 300px;
}

.mobile-navigation ul {
    margin: 0;
    padding: 0;
}

.mobile-navigation ul li {
    display: block;
}

.mobile-navigation ul li a {
    text-decoration: none;
    display: block;
}

.mobile-navigation > ul {}

.mobile-navigation > ul > li {
}

.mobile-navigation > ul > li:not(:last-of-type) {
    border-bottom: 1px solid #ddd;
}

.mobile-navigation > ul > li > a {
    font-size: 1.2em;
    padding: 10px 20px;
    position: relative;
}

.mobile-navigation > ul > li.current-menu-item > a,
.mobile-navigation > ul > li.active > a,
.mobile-navigation > ul > li:hover > a {
    color: #2caee2;
}

.mobile-navigation ul li.menu-item-has-children > a:after {
    content: "\e814";
    font-family: 'icon-font';
    font-size: 0.7em;
    position: absolute;
    right: 20px;
    top: 50%;
    transform: translate(0%, -50%);
    -webkit-transform: translate(0%, -50%);
}

.mobile-navigation .sub-nav {
    display: none;
    padding: 0px 0 0px;
    margin: 0 0 0 20px;
}

.mobile-navigation .sub-nav ul {}

.mobile-navigation .sub-nav ul li {
}

.mobile-navigation .sub-nav ul li:not(:last-of-type) {
    border-bottom: 1px solid #eaeaea;
}

.mobile-navigation .sub-nav ul li a {
    padding: 10px 0;
    position: relative;
}

.mobile-navigation .sub-nav ul li.current-menu-item > a,
.mobile-navigation .sub-nav ul li.active > a,
.mobile-navigation .sub-nav ul li:hover > a {
    color: #2caee2;
}
.fw-main-row, .fw-main-row *, .fw-main-row *:before, .fw-main-row *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.icon-menu:before {
    content: '\e82f';
}
.icon-font:before {
    font-family: "icon-font";
    font-style: normal;
    font-weight: normal;
    display: inline-block;
    text-decoration: inherit;
    text-align: center;
    font-variant: normal;
    text-transform: none;
    line-height: 1em;
    -moz-osx-font-smoothing: grayscale;
}
	</style>
  	<body>
    	<nav class="navbar py-4 navbar-expand-lg ftco_navbar navbar-light bg-light flex-row">
    		<div class="container">
	    		<div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
	    			<div class="col-lg-2 pr-4 align-items-center">
			    		<img src="{{asset('img/logo.png')}}" class="img responsive" width="100%" height="80%">
		    		</div>
		    		<div class="col-lg-10 d-none d-md-block">
			    		<div class="row d-flex">
				    		<div class="col-md-4 pr-4 d-flex topper align-items-center">
				    			<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-map"></span></div>
							    <span class="text">Address: Kuala Lumpur, Malaysia</span>
						    </div>
						    <div class="col-md pr-4 d-flex topper align-items-center">
						    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
							    <span class="text">Email: autism.info@gmail.com</span>
						    </div>
						    <div class="col-md pr-4 d-flex topper align-items-center">
						    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
							    <span class="text">Phone: + 603 10290192 </span>
						    </div>
					    </div>
				    </div>
			    </div>
			 </div>
	    </nav>
	  <!-- Header-wrap -->
				<div class="fw-main-row header-wrap">
					<div class="fw-container">
						<div class="fw-row">
							<div class="fw-col-sm-3 fw-col-md-2 logo-area"><a href="/"><img src="images/imgs/logo.svg" alt="Medina template"></a></div>
							<nav class="fw-col-sm-8 fw-col-md-8 navigation">
								<ul>
									<li class="menu-item-has-children">
										<a href="/">Home</a>
										<div class="sub-nav">
											<ul class="sub-menu">
												<li><a href="private-clinic.html">Private Clinic</a></li>
												<li><a href="plastic-surgery.html">Plastic Surgery</a></li>
												<li><a href="dental.html">Dental</a></li>
												<li><a href="doctor-personal-page.html">Doctor personal page</a></li>
											</ul>
										</div>
									</li>
									<!-- END Current menu item -->
									<!-- Menu item has children -->
									<li class="menu-item-has-children">
										<a href="our-team.html">Pages</a>
										<div class="sub-nav">
											<ul class="sub-menu">
												<li><a href="our-team.html">Our team</a></li>
												<li><a href="about.html">About</a></li>
												<li><a href="services.html">Services</a></li>
												<li><a href="prices.html">Prices</a></li>
												<li><a href="typography.html">Typography</a></li>
												<li><a href="404.html">404 page</a></li>
												<li><a href="coming-soon.html">Coming Soon</a></li>
												<!-- Menu item has children -->
												<li class="menu-item-has-children">
													<a href="javascript:void(0);">Menu level</a>
													<div class="sub-nav">
														<ul class="sub-menu">
															<!-- Menu item has children -->
															<li class="menu-item-has-children">
																<a href="javascript:void(0);">Hyperlink 1</a>
																<div class="sub-nav">
																	<ul class="sub-menu">
																		<li><a href="javascript:void(0);">Hyperlink 1</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 2</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 3</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 4</a></li>
																	</ul>
																</div>
															</li>
															<!-- END Menu item has children -->
															<!-- Menu item has children -->
															<li class="menu-item-has-children">
																<a href="javascript:void(0);">Hyperlink 2</a>
																<div class="sub-nav">
																	<ul class="sub-menu">
																		<li><a href="javascript:void(0);">Hyperlink 1</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 2</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 3</a></li>
																		<li><a href="javascript:void(0);">Hyperlink 4</a></li>
																	</ul>
																</div>
															</li>
															<!-- END Menu item has children -->
															<li><a href="javascript:void(0);">Hyperlink 3</a></li>
														</ul>
													</div>
												</li>
												<!-- END Menu item has children -->
											</ul>
										</div>
									</li>
									<!-- END Menu item has children -->
									<li><a href="procedures.html">Procedures</a></li>
									<!-- Current menu item -->
									<li class="current-menu-item menu-item-has-children">
										<a href="blog1.html">Blog</a>
										<div class="sub-nav">
											<ul class="sub-menu">
												<li><a href="blog1.html">Variant 1</a></li>
												<li><a href="blog2.html">Variant 2</a></li>
											</ul>
										</div>
									</li>
									<!-- END Current menu item -->
									<li class="menu-item-has-children">
										<a href="grid-col2.html">Gallery</a>
										<div class="sub-nav">
											<ul class="sub-menu">
												<li class="menu-item-has-children">
													<a href="grid-col2.html">Grid</a>
													<div class="sub-nav">
														<ul class="sub-menu">
															<li><a href="grid-col2.html">Col 2</a></li>
															<li><a href="grid-col3.html">Col 3</a></li>
															<li><a href="grid-col4.html">Col 4</a></li>
														</ul>
													</div>
												</li>
												<li class="menu-item-has-children">
													<a href="masonry-col2.html">Masonry</a>
													<div class="sub-nav">
														<ul class="sub-menu">
															<li><a href="masonry-col2.html">Col 2</a></li>
															<li><a href="masonry-col3.html">Col 3</a></li>
															<li><a href="masonry-col4.html">Col 4</a></li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</nav>
							<div class="fw-col-sm-2 search-module">
								<form action="javascript:void(0);">
									<input type="text" class="input" name="search" placeholder="Search">
									<button type="submit" class="submit"><i class="icon-font icon-search"></i></button>
								</form>
							</div>
							<!-- Mobile side button -->
							<div class="mobile-side-button"><i class="icon-font icon-menu"></i></div>
							<!-- END Mobile side button -->
							<!-- Mobiile side -->
							<div class="mobile-side">
								<!-- Social link -->
								<div class="social-link">
									<a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
									<a href="javascript:void(0);"><i class="social-icons icon-twitter-social-logotype"></i></a>
									<a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
								</div>
								<!-- END Social link -->
								<div class="search-module">
									<form action="javascript:void(0);">
										<input type="text" class="input" name="search" placeholder="Search">
										<button type="submit" class="submit"><i class="icon-font icon-search"></i></button>
									</form>
								</div>
								<!-- Mobile navigation -->
								<nav class="mobile-navigation">
									<ul>
										<li class="menu-item-has-children">
											<a href="/">Home</a>
											<div class="sub-nav">
												<ul class="sub-menu">
													<li><a href="private-clinic.html">Private Clinic</a></li>
													<li><a href="plastic-surgery.html">Plastic Surgery</a></li>
													<li><a href="dental.html">Dental</a></li>
													<li><a href="doctor-personal-page.html">Doctor personal page</a></li>
												</ul>
											</div>
										</li>
										<!-- END Current menu item -->
										<!-- Menu item has children -->
										<li class="menu-item-has-children">
											<a href="our-team.html">Pages</a>
											<div class="sub-nav">
												<ul class="sub-menu">
													<li><a href="our-team.html">Our team</a></li>
													<li><a href="about.html">About</a></li>
													<li><a href="services.html">Services</a></li>
													<li><a href="prices.html">Prices</a></li>
													<li><a href="typography.html">Typography</a></li>
													<li><a href="404.html">404 page</a></li>
													<li><a href="coming-soon.html">Coming Soon</a></li>
													<!-- Menu item has children -->
													<li class="menu-item-has-children">
														<a href="javascript:void(0);">Menu level</a>
														<div class="sub-nav">
															<ul class="sub-menu">
																<!-- Menu item has children -->
																<li class="menu-item-has-children">
																	<a href="javascript:void(0);">Hyperlink 1</a>
																	<div class="sub-nav">
																		<ul class="sub-menu">
																			<li><a href="javascript:void(0);">Hyperlink 1</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 2</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 3</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 4</a></li>
																		</ul>
																	</div>
																</li>
																<!-- END Menu item has children -->
																<!-- Menu item has children -->
																<li class="menu-item-has-children">
																	<a href="javascript:void(0);">Hyperlink 2</a>
																	<div class="sub-nav">
																		<ul class="sub-menu">
																			<li><a href="javascript:void(0);">Hyperlink 1</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 2</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 3</a></li>
																			<li><a href="javascript:void(0);">Hyperlink 4</a></li>
																		</ul>
																	</div>
																</li>
																<!-- END Menu item has children -->
																<li><a href="javascript:void(0);">Hyperlink 3</a></li>
															</ul>
														</div>
													</li>
													<!-- END Menu item has children -->
												</ul>
											</div>
										</li>
										<!-- END Menu item has children -->
										<li><a href="procedures.html">Procedures</a></li>
										<!-- Current menu item -->
										<li class="current-menu-item menu-item-has-children">
											<a href="blog1.html">Blog</a>
											<div class="sub-nav">
												<ul class="sub-menu">
													<li><a href="blog1.html">Variant 1</a></li>
													<li><a href="blog2.html">Variant 2</a></li>
												</ul>
											</div>
										</li>
										<!-- END Current menu item -->
										<li class="menu-item-has-children">
											<a href="grid-col2.html">Gallery</a>
											<div class="sub-nav">
												<ul class="sub-menu">
													<li class="menu-item-has-children">
														<a href="grid-col2.html">Grid</a>
														<div class="sub-nav">
															<ul class="sub-menu">
																<li><a href="grid-col2.html">Col 2</a></li>
																<li><a href="grid-col3.html">Col 3</a></li>
																<li><a href="grid-col4.html">Col 4</a></li>
															</ul>
														</div>
													</li>
													<li class="menu-item-has-children">
														<a href="masonry-col2.html">Masonry</a>
														<div class="sub-nav">
															<ul class="sub-menu">
																<li><a href="masonry-col2.html">Col 2</a></li>
																<li><a href="masonry-col3.html">Col 3</a></li>
																<li><a href="masonry-col4.html">Col 4</a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
										</li>
										<li><a href="contact.html">Contact</a></li>
									</ul>
								</nav>
								<!-- END Mobile navigation -->
							</div>
							<!-- END Mobiile side -->
						</div>
					</div>
				</div>
@stop

		
		
		