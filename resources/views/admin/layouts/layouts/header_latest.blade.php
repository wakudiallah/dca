@section('header')
<!DOCTYPE html>
<html lang="en">

  	<head>
	    <title>Autismwall.info</title>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    
	    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	    <link rel="stylesheet" href="{{url('/drcare/css/open-iconic-bootstrap.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/animate.css')}}">
	    
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.carousel.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.theme.default.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/magnific-popup.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/aos.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/ionicons.min.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/jquery.timepicker.css')}}">


		<!-- Favicon-->
		<link rel="shortcut icon" href="{{asset('favico/fav.ico')}}">

	    
	    <!--<link rel="stylesheet" href="{{url('/drcare/css/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/myicon/font/flaticon.css')}}">-->
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/dr/font/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/icomoon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/style.css')}}">

  	</head>

  	<style type="text/css">
		.navigation {
		    /* border-left: 1px solid #dddddd; */
		    /* border-right: 1px solid #dddddd; */
		    text-transform: uppercase;
		}

		.navigation ul {
		    margin: 0;
		    padding: 0;
		}

		.navigation ul li {
		    display: block;
		    position: relative;
		}

		.navigation ul li a {
		    text-decoration: none;
		}

		.navigation > ul {
		    text-align: center;
		    border-left: 1px solid #dddddd;
		    border-right: 1px solid #dddddd;
		}

		.navigation > ul > li {
		    display: inline-block;
		    vertical-align: middle;
		    margin: 0 16px;
		    text-align: left;
		}

		.navigation > ul > li > a {
		    display: table-cell;
		    height: 70px;
		    vertical-align: middle;
		}

		.navigation > ul > li.current-menu-item {}

		.navigation > ul > li.current-menu-item > a,
		.navigation > ul > li:hover > a {
		    box-shadow: inset 0 -3px 0 0 #23abe1;
		}

		.navigation .sub-nav {
		    position: absolute;
		    top: 70px;
		    left: -5px;
		    width: 150px;
		    visibility: hidden;
		    opacity: 0;
		    -webkit-transition: all 0.3s ease;
		    -moz-transition: all 0.3s ease;
		    -o-transition: all 0.3s ease;
		    -ms-transition: all 0.3s ease;
		    transition: all 0.3s ease;
		    border: 1px solid #dddddd;
		    background: #fff;
		}

		.navigation .sub-nav .sub-nav {
		    top: -1px;
		    left: 100%;
		}

		.navigation ul li.menu-item-has-children:hover > .sub-nav {
		    opacity: 1;
		    visibility: visible;
		    z-index: 2;
		}

		.navigation .sub-nav .sub-menu {}

		.navigation .sub-nav .sub-menu li {
		}

		.navigation .sub-nav .sub-menu li:not(:last-of-type) {
		    border-bottom: 1px solid #ddd;
		}

		.navigation .sub-nav .sub-menu li a {
		    height: 35px;
		    display: table-cell;
		    vertical-align: middle;
		    padding: 4px 0;
		    width: 150px;
		    padding: 3px 15px;
		    font-size: 0.857em;
		    position: relative;
		}

		.navigation .sub-nav .sub-menu li.menu-item-has-children > a:after {
		    content: '\e843';
		    font-family: "icon-font";
		    position: absolute;
		    color: #dddddd;
		    right: 10px;
		    top: 50%;
		    transform: translate(0, -50%);
		    -webkit-transform: translate(0, -50%);
		}

		.navigation .sub-nav .sub-menu li.current-menu-item > a, 
		.navigation .sub-nav .sub-menu li:hover > a {
		    background: #23abe1;
		    color: #fff;
		}

		/*.image_section img{
		    display: block;
		}

		@media (max-width:800px){
		    .image_section img:first-child{
		        display:none;
		    }
		}*/
	</style>
  	<body>
    	<nav class="navbar py-4 navbar-expand-lg ftco_navbar navbar-light bg-light flex-row">
    		<div class="container">
	    		<div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
	    			<div class="col-lg-2 pr-4 col-md-9 image_section align-items-center">
			    		<img src="{{asset('img/logo.png')}}" class="img responsive" width="100%" height="80%">
		    		</div>
		    		<div class="col-lg-10 d-none d-md-block">
			    		<div class="row d-flex">
				    		<div class="col-md-4 pr-4 d-flex topper align-items-center">
				    			<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-map"></span></div>
							    <span class="text">Address: Kuala Lumpur, Malaysia</span>
						    </div>
						    <div class="col-md pr-4 d-flex topper align-items-center">
						    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
							    <span class="text">Email: autism.info@gmail.com</span>
						    </div>
						    <div class="col-md pr-4 d-flex topper align-items-center">
						    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
							    <span class="text">Phone: + 603 10290192 </span>
						    </div>
					    </div>
				    </div>
			    </div>
			 </div>
	    </nav>
	  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
	    	<div class="container d-flex align-items-center">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        		<span class="oi oi-menu"></span>{{trans('menu.menu')}}
	      		</button>
	      		@if (Route::has('login'))
            		@auth
						@if (!(Auth::user())) 
						@elseif(!empty(Auth::user()->role_id=='2'))
        				<p class="button-custom order-lg-last mb-0"><a href="{{url('/profile')}}" class="btn btn-primary py-2 px-3">{{trans('menu.profile')}}</a></p>
        				@endif
        				@if (!(Auth::user())) 
        				@elseif(!empty(Auth::user()->role_id=='1'))
        				<p class="button-custom order-lg-last mb-0"><a href="{{url('/profile')}}" class="btn btn-primary py-2 px-3">{{trans('menu.admin')}}</a></p>
        				@endif
        				<p class="button-custom order-lg-last mb-0"><a href="{{url('/logout')}}" class="btn btn-warning py-2 px-3" onclick=" return confirm('Are You sure you want to logout?');">{{trans('menu.logout')}}</a></p>
    				@else
						<p class="button-custom order-lg-last mb-0"><a href="{{url('/login')}}" class="btn btn-secondary py-2 px-3">{{trans('menu.login')}}</a></p>
	      				<p class="button-custom order-lg-last mb-0"><a href="{{url('/register')}}" class="btn btn-info py-2 px-3">{{trans('menu.register')}}</a></p>
				 	@endauth
				@endif	
	      		
	     	 	<div class="collapse navbar-collapse" id="ftco-nav">
	      			<nav class="fw-col-sm-10 fw-col-md-10 navigation">
	       				 <ul>
                            <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{url('/')}}">{{trans('menu.home')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="">{{trans('menu.autism')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                      	<li class="{{ Request::is('what-is-autism/type-of-autism') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/type-of-autism')}}">{{trans('menu.type')}}</a></li>
                                        <li class="{{ Request::is('what-is-autism/early-detection') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/early-detection')}}">{{trans('menu.early_detection')}}</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">{{trans('menu.screening')}}</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening')}}</a></li>
                                                    <li><a href="javascript:void(0);">{{trans('menu.screening_ins')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                        <li class="{{ Request::is('what-is-autism/symptom') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/symptom')}}">{{trans('menu.symptom')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                           <!-- <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.screening')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening_ins')}}</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li class="{{ Request::is('treatment') ? 'current-menu-item' : '' }}"><a href="{{url('/treatment')}}">{{trans('menu.treatment')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="our-team.html">{{trans('menu.therapy')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="{{url('/therapy/sensory')}}">{{trans('menu.sensory_therapy')}}</a></li>
                                        <li><a href="{{url('/therapy/hippo')}}">{{trans('menu.hippo')}}</a></li>
                                        <li><a href="{{url('/therapy/lego')}}">{{trans('menu.lego')}}</a></li>
                                        <li><a href="{{url('/therapy/messy-play')}}">{{trans('menu.mossy')}}</a></li>
                                        <li><a href="{{url('/therapy/speech-therapy')}}">{{trans('menu.speech')}}</a></li>
                                        <li><a href="{{url('/therapy/psikomotor')}}">{{trans('menu.psikomotor')}}</a></li>
                                        <li><a href="{{url('/therapy/cognitive')}}">{{trans('menu.cognitive')}}</a></li>

                                        <li class="menu-item-has-children">
                                           <a href="javascript:void(0);">{{trans('menu.edu_games')}}</a>
                                           <div class="sub-nav">
                                              <ul class="sub-menu">
                                                 <!-- END Menu item has children -->
                                                 <li><a href="{{url('/therapy/edu-games/numbering')}}">{{trans('menu.numbering')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/alphabet')}}">{{trans('menu.alphabet')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/daily-living')}}">{{trans('menu.daily')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                     </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">{{trans('menu.sensory')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/multi-sensory')}}">{{trans('menu.multy_sensory')}}</a></li>
                                        <li class=""><a href="{{url('/sensory-tool')}}">{{trans('menu.sensory_tool')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            @if (Route::has('login'))
                            <li class=" {{ (request()->is('forumx')) ? 'current-menu-item' : '' }}"><a href="{{url('/forumx')}}">{{trans('menu.forum')}}</a></li>
                            @endif
                            <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.technology')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=" {{ (request()->is('technology')) ? 'current-menu-item' : '' }}"><a href="{{url('/technology')}}">{{trans('menu.technology')}}</a></li>
                                        <li class=" {{ (request()->is('resources')) ? 'current-menu-item' : '' }}"><a href="{{url('/resources')}}">{{trans('menu.resources')}}</a></li>
                                        <li class=" {{ (request()->is('/directory')) ? 'current-menu-item' : '' }}"><a href="{{url('/directory')}}">{{trans('menu.directory')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class=" {{ (request()->is('galleries')) ? 'current-menu-item' : '' }}"><a href="{{url('/galleries')}}">{{trans('menu.galleries')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
                                </a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        @foreach(Config::get('languages') as $lang => $language)
                                            @if($lang != App::getLocale())
                                                <li>
                                                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </nav>
	      		</div>
	      	</div>
	  </nav>
    <!-- END nav -->
@stop

		
		
		