@section('header')
<!DOCTYPE html>
<html lang="en">

  	<head>
	    <title>Autismwall.info</title>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    
	    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

	    <link rel="stylesheet" href="{{url('/drcare/css/open-iconic-bootstrap.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/animate.css')}}">
	    
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.carousel.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/owl.theme.default.min.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/magnific-popup.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/aos.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/ionicons.min.css')}}">

	    <link rel="stylesheet" href="{{url('/drcare/css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/jquery.timepicker.css')}}">

		<!-- Favicon-->
		<link rel="shortcut icon" href="{{asset('favico/fav.ico')}}">

	    <!--<link rel="stylesheet" href="{{url('/drcare/css/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/myicon/font/flaticon.css')}}">-->
	    <link rel="stylesheet" href="{{url('/drcare/fonts/flaticon/dr/font/flaticon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/icomoon.css')}}">
	    <link rel="stylesheet" href="{{url('/drcare/css/style.css')}}">
		<link rel="stylesheet" href="{{url('/drcare/css/header.css')}}">
	    <link rel="stylesheet" href="{{url('/medina/css/mobile.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/medical-icons.css')}}">
		<link rel="stylesheet" href="{{url('/medina/css/social-icons.css')}}">
  	</head>
  	<style type="text/css">
  		.image_section img{
		    display: block;
		}

		@media (max-width:800px){
		    .image_section img:first-child{
		        display:none;
		    }
		}
		.owl-carousel.owl-drag .owl-item {
-ms-touch-action: auto;
touch-action: auto;
-webkit-user-select: all;
-moz-user-select: all;
-ms-user-select: all;
user-select: all;
}
  	</style>
  	<body>
    	<header class="header clearfix">
				<div class="container">
	    		<div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
	    			<div class="col-lg-2 pr-4 col-md-9 image_section align-items-center">
			    		<img src="{{asset('img/logo.png')}}" class="img responsive" width="100%" height="80%">
		    		</div>

		    		
			    </div>
			 </div>
				<!-- END Top header -->
				<!-- Header-wrap -->
				<div class="fw-main-row header-wrap">
					<div class="fw-container">
						<div class="fw-row">
							<!--<div class="fw-col-sm-3 fw-col-md-2 logo-area"><a href="/"><img src="{{asset('img/logo.png')}}"  alt="Medina template"></a></div>-->
							<div class="fw-col-sm-3 fw-col-md-2 logo-area"><a href="/"></a></div>
							<nav class="fw-col-sm-8 fw-col-md-8 navigation">
								<ul>
                            <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{url('/')}}">{{trans('menu.home')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="">{{trans('menu.autism')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                      	<li class="{{ Request::is('what-is-autism/type-of-autism') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/type-of-autism')}}">{{trans('menu.type')}}</a></li>
                                        <li class="{{ Request::is('what-is-autism/early-detection') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/early-detection')}}">{{trans('menu.early_detection')}}</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">{{trans('menu.screening')}}</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening')}}</a></li>
                                                    <li><a href="javascript:void(0);">{{trans('menu.screening_ins')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                        <li class="{{ Request::is('what-is-autism/symptom') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/symptom')}}">{{trans('menu.symptom')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                           <!-- <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.screening')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening_ins')}}</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li class="{{ Request::is('treatment') ? 'current-menu-item' : '' }}"><a href="{{url('/treatment')}}">{{trans('menu.treatment')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="our-team.html">{{trans('menu.therapy')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="{{url('/therapy/multi-sensory')}}">{{trans('menu.sensory_therapy')}}</a></li>
                                        <li><a href="{{url('/therapy/hippo-sensory')}}">{{trans('menu.hippo')}}</a></li>
                                        <li><a href="{{url('/therapy/lego-therapy')}}">{{trans('menu.lego')}}</a></li>
                                        <li><a href="{{url('/therapy/messy-play')}}">{{trans('menu.mossy')}}</a></li>
                                        <li><a href="{{url('/therapy/speech-therapy')}}">{{trans('menu.speech')}}</a></li>
                                        <li><a href="{{url('/therapy/psikomotor')}}">{{trans('menu.psikomotor')}}</a></li>
                                        <li><a href="{{url('/therapy/cognitive')}}">{{trans('menu.cognitive')}}</a></li>

                                        <li class="menu-item-has-children">
                                           <a href="javascript:void(0);">{{trans('menu.edu_games')}}</a>
                                           <div class="sub-nav">
                                              <ul class="sub-menu">
                                                 <!-- END Menu item has children -->
                                                 <li><a href="{{url('/therapy/edu-games/numbering')}}">{{trans('menu.numbering')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/alphabet')}}">{{trans('menu.alphabet')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/daily-living')}}">{{trans('menu.daily')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                     </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">{{trans('menu.sensory')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/multi-sensory')}}">{{trans('menu.multy_sensory')}}</a></li>
                                        <li class=""><a href="{{url('/sensory-tool')}}">{{trans('menu.sensory_tool')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            @if (Route::has('login'))
                            <li class=" {{ (request()->is('forumx')) ? 'current-menu-item' : '' }}"><a href="{{url('/forumx')}}">{{trans('menu.forum')}}</a></li>
                            @endif
                            <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.technology')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=" {{ (request()->is('technology')) ? 'current-menu-item' : '' }}"><a href="{{url('/technology')}}">{{trans('menu.technology')}}</a></li>
                                        <li class=" {{ (request()->is('resources')) ? 'current-menu-item' : '' }}"><a href="{{url('/resources')}}">{{trans('menu.resources')}}</a></li>
                                        <li class=" {{ (request()->is('/directory')) ? 'current-menu-item' : '' }}"><a href="{{url('/directory')}}">{{trans('menu.directory')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class=" {{ (request()->is('galleries')) ? 'current-menu-item' : '' }}"><a href="{{url('/galleries')}}">{{trans('menu.galleries')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
                                </a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        @foreach(Config::get('languages') as $lang => $language)
                                            @if($lang != App::getLocale())
                                                <li>
                                                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
							</nav>
							
							<!-- Mobile side button -->
							<div class="mobile-side-button"><i class="icon-font icon-menu"></i></div>
							<!-- END Mobile side button -->
							<!-- Mobiile side -->
							<div class="mobile-side">
								<!-- Social link -->
								<div class="social-link">
									<a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
									<a href="javascript:void(0);"><i class="social-icons icon-twitter-social-logotype"></i></a>
									<a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
								</div>
								<!-- END Social link -->
								<div class="search-module">
									<form action="javascript:void(0);">
										<input type="text" class="input" name="search" placeholder="Search">
										<button type="submit" class="submit"><i class="icon-font icon-search"></i></button>
									</form>
								</div>
								<!-- Mobile navigation -->
								<nav class="mobile-navigation">
									<ul>
                            <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{url('/')}}">{{trans('menu.home')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="">{{trans('menu.autism')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                      	<li class="{{ Request::is('what-is-autism/type-of-autism') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/type-of-autism')}}">{{trans('menu.type')}}</a></li>
                                        <li class="{{ Request::is('what-is-autism/early-detection') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/early-detection')}}">{{trans('menu.early_detection')}}</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">{{trans('menu.screening')}}</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening')}}</a></li>
                                                    <li><a href="javascript:void(0);">{{trans('menu.screening_ins')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                        <li class="{{ Request::is('what-is-autism/symptom') ? 'current-menu-item' : '' }}"><a href="{{url('/what-is-autism/symptom')}}">{{trans('menu.symptom')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                           <!-- <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.screening')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/what-is-autism/screening')}}">{{trans('menu.screening_ins')}}</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li class="{{ Request::is('treatment') ? 'current-menu-item' : '' }}"><a href="{{url('/treatment')}}">{{trans('menu.treatment')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="our-team.html">{{trans('menu.therapy')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="{{url('/therapy/multi-sensory')}}">{{trans('menu.sensory_therapy')}}</a></li>
                                        <li><a href="{{url('/therapy/hippo-sensory')}}">{{trans('menu.hippo')}}</a></li>
                                        <li><a href="{{url('/therapy/lego-therapy')}}">{{trans('menu.lego')}}</a></li>
                                        <li><a href="{{url('/therapy/messy-play')}}">{{trans('menu.mossy')}}</a></li>
                                        <li><a href="{{url('/therapy/speech-therapy')}}">{{trans('menu.speech')}}</a></li>
                                        <li><a href="{{url('/therapy/psikomotor')}}">{{trans('menu.psikomotor')}}</a></li>
                                        <li><a href="{{url('/therapy/cognitive')}}">{{trans('menu.cognitive')}}</a></li>

                                        <li class="menu-item-has-children">
                                           <a href="javascript:void(0);">{{trans('menu.edu_games')}}</a>
                                           <div class="sub-nav">
                                              <ul class="sub-menu">
                                                 <!-- END Menu item has children -->
                                                 <li><a href="{{url('/therapy/edu-games/numbering')}}">{{trans('menu.numbering')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/alphabet')}}">{{trans('menu.alphabet')}}</a></li>
                                                 <li><a href="{{url('/therapy/edu-games/daily-living')}}">{{trans('menu.daily')}}</a></li>
                                              </ul>
                                           </div>
                                        </li>
                                     </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">{{trans('menu.sensory')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=""><a href="{{url('/multi-sensory')}}">{{trans('menu.multy_sensory')}}</a></li>
                                        <li class=""><a href="{{url('/sensory-tool')}}">{{trans('menu.sensory_tool')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            @if (Route::has('login'))
                            <li class=" {{ (request()->is('forumx')) ? 'current-menu-item' : '' }}"><a href="{{url('/forumx')}}">{{trans('menu.forum')}}</a></li>
                            @endif
                            <li class="menu-item-has-children">
                                <a href="/">{{trans('menu.technology')}}</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                    	<li class=" {{ (request()->is('technology')) ? 'current-menu-item' : '' }}"><a href="{{url('/technology')}}">{{trans('menu.technology')}}</a></li>
                                        <li class=" {{ (request()->is('resources')) ? 'current-menu-item' : '' }}"><a href="{{url('/resources')}}">{{trans('menu.resources')}}</a></li>
                                        <li class=" {{ (request()->is('/directory')) ? 'current-menu-item' : '' }}"><a href="{{url('/directory')}}">{{trans('menu.directory')}}</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class=" {{ (request()->is('galleries')) ? 'current-menu-item' : '' }}"><a href="{{url('/galleries')}}">{{trans('menu.galleries')}}</a></li>
                            <li class="menu-item-has-children">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
                                </a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        @foreach(Config::get('languages') as $lang => $language)
                                            @if($lang != App::getLocale())
                                                <li>
                                                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
								</nav>
								<!-- END Mobile navigation -->
							</div>
							<!-- END Mobiile side -->
						</div>
					</div>
				</div>
				<!-- END Header-wrap -->
			</header>
@stop

		
		
		