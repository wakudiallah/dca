@extends('layouts.main')

@section('container')    
	
    @yield('menu')

	
    <div class="content">
    	@yield('content')
    </div>

    @include('layouts.footer')
    @yield('footer')


@stop

