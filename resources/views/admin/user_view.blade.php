<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "User View";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
</style>

    <?php
	include("asset/inc/nav.php");
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
<div id="main" role="main">
    <div id="content">
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>User View</h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="form-bootstrapWizard">
                                                        <ul class="bootstrapWizard form-wizard">
                                                            <li data-target="#step1">
                                                                <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="hidden-xs title">Maklumat Pelanggan   <i> Customer Information </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step2" >
                                                                <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="hidden-xs title">Maklumat Kontak <i> Contact Details </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step3" >
                                                                <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="hidden-xs title">Maklumat Pekerjaan <i> Information Of Employment </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step4" >
                                                                <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="hidden-xs title">Jumlah Pembiayaan <i> Loan Amount </i> </span> </a>
                                                            </li>
                                                            
                                                            </li>
                                                            <li data-target="#step5" >
                                                                <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="hidden-xs title">Maklumat Pasangan <i> Spouse Information </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step6" >
                                                                <a href="#tab6" data-toggle="tab"> <span class="step">6</span> <span class="hidden-xs title">Perujuk <i> Reference </i></span> </a>
                                                            </li>
                                                             <li data-target="#step7"  >
                                                                <a href="#tab7" data-toggle="tab"> <span class="step">7</span> <span class="hidden-xs title">Latar Belakang Kewangan <i> Financial Background </i> </span> </a>
                                                            </li>
                                                            <li data-target="#step8" >
                                                                <a href="#tab8" data-toggle="tab"> <span class="step">8</span> <span class="hidden-xs title">Muat Naik Dokumen <i>  Upload Document </i></span> </a>
                                                            </li>
                                                             <li data-target="#step9" >
                                                                <a href="#tab9" data-toggle="tab"> <span class="step">9</span> <span class="hidden-xs title">Pengisytiharan <i> Declaration </i></span> </a>
                                                            </li>
                                                             
                                                             
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                            <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Seterusnya / <i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                    
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab1">
                                                         @foreach($data as $data)
                                                            <br>
                                                            <h3><strong>Step 1 </strong> - Maklumat Pelanggan  / <i> Customer Information </i> </h3>
                                                            <div class="col-md-4">
                                                                <br>
                                                      <div class="form-group">
                                                    <label>1. Negara Asal / <i>Country Of Origin </i> :</label>
                                                     <select class="form-control" id="country" name="country" disabled>
                                                     @if(!empty($data->country))
                                                      <option  value="{{$data->country}}">{{$data->country}} </option>
                                                      @endif
                                                    <option>My Malaysia /<i>Malaysian</i></option>
                                                
                                                   
                                                    </select>
                                                     
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}" >
                      
                                                      
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Jenis Pengenalan Diri <i>/ ID Type </i> :</label>
                                                    <select class="form-control" id="card" name="card" disabled>
													  @if(!empty($data->card))
                                                      <option  value="{{$data->card}}">{{$data->card}} </option>
                                                      @endif
                                                    <option>Kad Pengenalan Baru / <i>New IC</i></option>
                                         
                                                    <option>Kad Pengenal Lama / <i> Old IC</i></option>
                                                    <option>KP Tentera / <i> Army Identity Card</i></option>
                                                    <option>KP Polis / <i> Police Identity Card</i></option>
                                                    </select>
                                                   
                                                    </div>
                                                    <div class="form-group">
                                                     <label>3. Kewarganegaraan / <i>Citizen </i> :</label>
                                                    <select class="form-control" id="citizen" name="citizen" disabled>
                                                         @if(!empty($data->Citizen))
                                                      <option  value="{{$data->citizen}}">{{$data->citizen}} </option>
                                                      @endif
                                                    <option value="Warganegara">Warganegara / <i>Citizen</i></option>
													</select>
   
                                                    </div>
                                                    <div class="form-group">
                                                    <label>4. No. Kad Pengenalan Baru / <i>New IC Number </i> :</label>
                                                    <input name="new_ic" type="text" maxlength="12" id="text" value="{{$data->new_ic}}" class="form-control" disabled>
                                                   
                                                    </div>
                                                    <div class="form-group">
                                                    <label>5. No. Kad Pengenalan Lama / <i>Old IC Number </i> :</label>
                                                    <input name="old_ic"type="text" value="{{$data->old_ic}}" maxlength="8" id="text" class="form-control" disabled/>
                                                
                                                   
                                               
                                                    </div>
                                                    <div class="form-group">
                                                    <label>6. Tarikh Lahir <i>/ Date Of Birth </i> :</label>
                                                 
                                                      <?php
                                                       if($data->dob=='0000-00-00' OR $data->dob=='1970-01-01') {
                                                         $lahir='';
                                                       }
                                                       else {
                                                         $lahir =  date('d/m/Y', strtotime($data->dob));
                                                       }
                                                            ?>
                                                    <input type="text" maxlength="14" name="dob" value="{{ $lahir  }}" id='tgl' class="form-control " name="tanggal" disabled/>
                                                  
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                    <label>7. Nama (seperti dalam KP atau Pasport) / <i>Name (As in IC or Passport) </i> :</label>
                                                    <input type="text" maxlength="66" id="text" name="name" value="{{$data->name}}" class="form-control" disabled>
                                                  
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                    <label>8. Nama Pilihan (jika ada) / <i>Preferred Name (if any) </i> :</label>
                                                    <input type="text" maxlength="44" id="text" name="name_prefed" value="{{$data->name_prefed}}"class="form-control" disabled>
                                                
                                                  
                                            
                                                    </div>

                                                     <div class="form-group">
                                            <label>9. Gelaran / <i>Title</i> :</label>
                                           <select class="form-control" name="title" disabled>
                                            @if(!empty($data->title))
                                                      <option  value="{{$data->title}}">{{$data->title}} </option>
                                                      @endif
                                               <option> En /<i> Mr </i> </option>
                                                <option > Datuk /<i> Dato'</i></option>
                                                <option >Tan Sri</option>
                                                <option >Puan /<i> Mrs </i></option>
                                                <option >Datin</option>
                                                <option >Puan Sri</option>
                                                 <option >Cik /<i> Miss </i></option>
                                                <option >Dr</option>
                                                <option >Profesor /<i> Professor </i></option>
                                                <option >Yang Berhormat</option>
                                                <option >Lain-lain...</option>
                                          
                                        </select>
                                                 
                                           
                                        </div>
                                        

                                            
                                         </div>



                                         <div class="col-md-4">
                                                                <br>
                                                                <div class="form-group">
                                            <label>10. Jantina / <i>Gender </i> :</label>
                                            <select class="form-control" name="gender" disabled>
                                             @if(!empty($data->gender))
                                                      <option  value="{{$data->gender}}">{{$data->gender}} </option>
                                                      @endif
                                                      <option >Lelaki / <i>Male</i></option>
                                                <option >Perempuan / <i>Female</i></option>
                                            </select>
                                                    
                                        </div>

                                        <div class="form-group">
                                            <label>11. Taraf Perkahwinan <i>Marital Status </i> :</label>
                                             <select class="form-control"  name="marital" id="status" disabled>
                                              @if(!empty($data->gender))
                                                      <option  value="{{$data->marital}}">{{$data->marital}} </option>
                                                      @endif
                                                  <option >Bujang / <i>Single</option>
                                                  <option>Berkahwin / <i>Married</i></option>
                                                <option>Balu / <i>Widowed</i></option>
                                                <option >Bercerai / <i>Divorced</i></option>
                                               
                                            </select>
                                           
                                                    
                                            </div>

                                        <div class="form-group">
                                        
                                                <label>
                                                    12. Bilangan Tanggungan
                                                    <input disabled type="text" class="form-control" maxlength="2" value="{{$data->dependents}}" name="dependents" onkeypress="return isNumberKey(event)">
                                                </label><br>
                                                
                                           
                                        </div>
                                        <div class="form-group">
                                            <label>13. Bangsa / <i>Race </i> :</label>
                                            <select class="form-control" name="race" disabled>
                                            @if(!empty($data->race))
                                                      <option  value="{{$data->race}}">{{$data->race}} </option>
                                                      @endif
                                                <option>Bumiputera / <i>Melayu</i></option>
                                                <option>India /<i> Indian</i></option>
                                                <option>Cina /<i> Chinese</i></option>
                                                <option>Lain-lain / <i> Others</i></option>
                                            </select>
                                                   
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>14. Agama / <i>Religion </i> :</label>
                                             <select class="form-control" name="relegion" disabled>
                                             @if(!empty($data->relegion))
                                                      <option  value="{{$data->relegion}}">{{$data->relegion}} </option>
                                                      @endif
                                                   <option>Islam / <i>Muslim</i></option>
                                                    <option>Kristian /<i> Christian</option>
                                                    <option >Buddha /<i> Buddhist</i></option>
                                                     <option >Hindu /<i> Hinduism</i></option>
                                                    <option >Lain-lain / <i> Others</i></option>
                                                    
                                                    </select>

                                                  
                                        </div>
                                        <div class="form-group">
                                            <label>15. Pendidikan Tertinggi Diperolehi / <i>Education</i> :</label>
                                              <select class="form-control" name="education" disabled>
                                              @if(!empty($data->education))
                                                      <option  value="{{$data->education}}">{{$data->education}} </option>
                                                      @endif
                                                       <option>Rendah / <i>Primary</i></option>
                                                    <option >Menengah /<i> Secondary</i></option>
                                                    <option >Profesional /<i> Professional</i></i></option>
                                                     <option >Tinggi /<i> Tertiary</i></option>
                                                    <option >Lain-lain / <i> Others</i></option>
                                                    
                                                    </select>
                                                    
                                        </div>
                                        <div class="form-group">
                                            <label>16. Kategori Pelanggan / <i>Customer Category</i> :</label>
                                            <select class="form-control" name="category" disabled>
                                            @if(!empty($data->category))
                                                      <option>{{$data->category}} </option>
                                                      @endif
                                                <option>Anggota Bank Persatuan / <i>Member</i></option>
                                                <option>Bukan Anggota Bank Persatuan / <i>Non Member</i></option>
                                               
                                            </select>
                                                
                                        </div>
                                        <div class="form-group">
                                                    <label>17. Nama Ibu / <i>Mother's Malden Name </i> :</label>
                                                     <input type="text" maxlength="44" name="mother_name"
                                                     value="{{$data->mother_name}}"class="form-control" disabled>
                                                  
                                                   
                                                </div>
                                            </div>
											@if(empty($incomplated_view))
                                             <div class="col-md-4"><br>
                                                 <div class="form-group">
                                                    <label><b>Remark By Analyst :</b></label>
                                                     <textarea disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark'>{{$basic_v->first()->remark}}</textarea>       
                                                </div>
                                             </div>
											 @endif
											 
                                                        </div>
                                                        @endforeach
                                                        <div class="tab-pane" id="tab2">
                                                         @foreach($contact as $contact)
                                                            <br>
                                                            <h3><strong>Step 2</strong> - Maklumat Kontak / <i> Contact Details </i></h3>
                                                    
                                                     <div class="col-md-4">
                                                                <br>
                                                                 <div class="form-group">
                                                                <label>1. Alamat Kediaman /<i> Residential Address </i></label>
                                                             <br>
                                                             <textarea disabled class="form-control" rows="3" maxlength="66"   name="address">{{$contact->address}}</textarea>
                                                  
                                                                </div>
                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$contact->id_praapplication}}" >

                                                                <div class="form-group">
                                                                <label>2. Poskod/ <i>Postcode :</i></label>
                                                             <br><input disabled type="text" maxlength="5" value="{{$contact->postcode}}" name="postcode" id="postcode" class="form-control" onkeypress="return isNumberKey(event)" />
                                                  
                                                                
                                                                
                                                                </div>
                                                                    <div class="form-group">
                                                    <label>3. Bandar / <i>City :</i></label><br>
                                                      <input type="text" disabled maxlength="50"  value="{{$contact->city}}"  id="city" name="city" class="form-control"></br>
                                                   
                                                     
                                                     </div>

                                                        <div class="form-group">
                                                            <label>4. Negeri /<i>State</i> : </label>
                                                     <br><input type="text" disabled maxlength="50"  value="{{$contact->state}}"   id="state" name="state" class="form-control"></br>
                                                  
                                                        </div>

                                                       </div>     

                                                        <div class="col-md-4">
                                                                <br>
                                                    <div class="form-group">
                                                    <label>5. Jenis Pemilikan /<i> Type Of Ownership </i> : </label>
                                                   
                                                      <select class="form-control" name="ownership" disabled>
                                                      @if(!empty($contact->ownership))
                                                      <option  value="{{$contact->ownership}}">{{$contact->ownership}} </option>
                                                      @endif
                                                     <option >Sendiri /<i>Own</i></option>
                                                    <option>Sewa /<i>Rental</i></option>
                                                    <option>Lain-lain / <i>Other..</i></option>
                                                  
                                                 
                                                    </select>
                                                      
                                                     
                                                    </div>
                                                     
                                                              <div class="form-group">
                                                    <label>6. Alamat Surat Menyurat /<i> Correspondence Address </i> : </label>
                                                   <textarea disabled class="form-control" maxlength="66"  rows="3" name="address2">{{$contact->address2}}</textarea>
                                                    
                                                    </div>
                                                      
                                                      <div class="form-group">
                                                         <label>7. Poskod/ <i>Postcode :</i></label>
                                                         <br><input disabled type="text" id="postcode3" maxlength="5"  value="{{$contact->postcode2}}" name="postcode2" class="form-control" onkeypress="return isNumberKey(event)"/>
                                                      </div>
                                                      <div class="form-group">
                                                         <label>8. Bandar / <i>City :</i></label><br>
                                                         <input disabled type="text"  value="{{$contact->city2}}"  maxlength="50"  id="city3" name="city2" class="form-control"></br>
                                                      </div>
                                                      <div class="form-group">
                                                          <label>9. Negeri /<i>State</i> : </label>
                                                         <br><input disabled type="text"  value="{{$contact->state2}}"  maxlength="50" id="state3" name="state2" class="form-control"></br>
                                                      </div> 
                                                       
                                                      
                                                    <div class="form-group">
                                                    <label>10. No. Untuk Dihubungi / <i>Contact No.</i> :</label>
                                                    <ul>
                                                    <li>Kediaman / <i>House </i> : <br><input disabled type="text" size="16"  maxlength="12" value="{{$contact->houseno}}"
                                                    name="houseno" onkeypress="return isNumberKey(event)" >
                                                     
                                                    </li>
                                                      <li>No. Tel. Pejabat  /<i>  Office Phone No.   </i>: <br><input disabled type="text" maxlength="12" size="16"  required value="{{$contact->officephone}}" name="officephone" onkeypress="return isNumberKey(event)"/>
                                                     
                                                    
                                                    </li>
                                                    <li>Tel.Bimbit / <i>Handphone</i> : <br><input disabled required type="text" size="16"  maxlength="12" value="{{$contact->handphone}}" 
                                                    name="handphone"value="{{$data->phone}}" onkeypress="return isNumberKey(event)" >
                                                   
                                                    </li>
                                                    <li>Faks / <i>Fax</i> : <br><input disabled type="text" size="16" maxlength="12" onkeypress="return isNumberKey(event)"  name="fax" value="{{$contact->fax}}">
                                                   
                                                   </li>
                                                      <li>Alamat E-mel / <i>E-mail Address</i> : <br><input disabled type="text" size="16" maxlength="40"  name="emel" value="{{$pra->email}}">
                                                  
                                                   </li>
                                                    </ul>
            
            
                                                    </div>
                                                        </div>
													@if(empty($incomplated_view))
                                                   <div class="col-md-4"><br>
                                                      <div class="form-group">
                                                         <label><b>Remark By Analyst :</b></label>
                                                         <textarea disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_contact'>{{$contact_v->first()->remark}}</textarea>       
                                                      </div>
                                                   </div>
												   @endif
                                                           @endforeach
                                                        </div>
                                                       
                                                        <div class="tab-pane" id="tab3">
                                                            <br>
                                                            <h3><strong>Step 3</strong> - Maklumat Pekerjaan / <i> Information Of Employment </i> </h3>
                                                            <div class="col-md-4">
                                                                <br>
                                                                @foreach($empinfo as $empinfo)
                                                       <div class="form-group">
                                                    <label>1. Nama Majikan <i>/ Employer's Name </i> :</label>
                                                    @if(!empty($empinfo->empname))
														<input type="text" name="emp_name" maxlength="66"  
															value="{{$empinfo->empname}}" class="form-control" disabled>
												    @else
														<input type="text" name="emp_name" maxlength="66"  
															value="{{$pra->employer->name}}" class="form-control" disabled>	 
													@endif
													
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input name="id_praapplication" type="hidden"  value="{{$empinfo->id_praapplication}}" >
                                                   
                                                </div>              
                                        
                                                <div class="form-group">
                                                    <label>2. Alamat Majikan <i>/ Employer's Address </i> :</label>
                                                    <input  disabled type="text" name="address" maxlength="120" value="{{$empinfo->address}}" class="form-control" >
                                                   
                                                    </div>
                                                   <div class="form-group">
                                
                                                      <label>3. Tarikh Mula Berkhidmat / <i> Date Of Joined </i>:</label>
                                                     <?php
                                                  
                                                       if($empinfo->joined=='0000-00-00' OR $empinfo->joined=='1970-01-01') {
                                                         $joined='';
                                                       }
                                                       else {
                                                         $joined =  date('d/m/Y', strtotime($empinfo->joined));
                                                       }
                                                       
                                                       
                                                      ?>
                                                          <input  disabled type="text" maxlength="14"  id="tanggall" name="joined"  value="{{$joined}}"  class="form-control startdate"/>
                                                  
                                                   </div> 
                                
                                
                                
                               
                                       </div> 
                                        <div class="col-md-4">
                                            <br>
                               
                                      <div class="form-group">
                                
                                <label>4. Pangkat / <i> Grade </i>:</label>
                                <br><input type="text"  disabled  id="" maxlength="8" name="grade"  value="{{$empinfo->grade}}" class="form-control">           
                                          
                                      </div>
                                          <div class="form-group">
                                    
                                              <label>5. Taraf Jawatan (Tetap/Sementara/Kontrak) / <i> Employment Status (Permanent/Temporary/Contract) </i>:</label>
                                              <br><input  disabled type="text" id="" maxlength="22" name="empstatus" value="{{$empinfo->empstatus}}" class="form-control">           
                                                
                                          </div>   
                                                 
                                          <div class="form-group">
                                            <label>6. Sektor Pekerjaan / <i>Occupation Sector</i> :</label>
                                            <select  disabled class="form-control" name="occupation_sector">
                                                @if(!empty($empinfo->occupation_sector))
                                                      <option>{{$empinfo->occupation_sector}}</option>
                                                @endif
                                                <option>Kakitangan Kerajaan / <i>Government Sector</i></option>
                                                <option>Pertanian /<i> Agriculture </i></option>
                                                <option>Pendidikan /<i> Education</i></option>
                                                <option>Kewangan /<i> Finance</i></option>
                                                <option>Kesihatan /<i> Health</i></option>
                                                <option>Pembuatan /<i> Manufacturing</i></option>
                                                <option>Perkhidmatan /<i> Services</i></option>
                                                <option>Perniagaan /<i> Business</i></option>
                                                <option>Lain-lain /<i> Others</i></option>
                                                <option>Tidak berkenaan /<i> Not Applicable</i></option>

                                            </select>
                                       </div>
                                          <div class="form-group">
                                             <label>7. Pekerjaan  /<i> Occupation </i> :</label>
                                             <select  disabled class="form-control" name="occupation">
                                                 @if(!empty($empinfo->occupation))
                                                       <option>{{$empinfo->occupation}}</option>
                                                 @endif
                                                 <option>Kerani / Pekerja Am / <i>Clerk / General Worker</i></option>
                                                 <option>Pegawai / Eksekutif /<i> Officer / Executive </i></option>
                                                 <option>Pengurus /<i> Manager</i></option>
                                                 <option>Profesional /<i> Professional</i></option>
                                                 <option>Pendidik / Pensyarah /<i> Teacher / Lecturer</i></option>
                                                 <option>Pelajar /<i> Student</i></option>
                                                 <option>Lain-lain /<i> Others</i></option>
                                                 <option>Tidak berkenaan /<i> Not Applicable</i></option>
                                             </select>
                                       </div>
                                          <div class="form-group">
                                             <label>8. Julat Pendapatan  /<i> Range of Income </i> :</label>
                                             <select disabled  class="form-control" name="range_of_income">
                                                 @if(!empty($empinfo->range_of_income))
                                                       <option>{{$empinfo->range_of_income}}</option>
                                                 @endif
                                                 <option>Bawah RM 1000 / <i>Bellow RM 1000</i></option>
                                                 <option>RM 1000 - RM 3000</option>
                                                 <option>RM 3001 - RM 5000</option>
                                                 <option>Melebihi RM 5000 /<i> Above RM 5000</i></option>
                                                 <option>Tidak berkenaan /<i> Not Applicable</i></option>
                                             </select>
                                       </div>
                                    
                                         
                                         </div>
                                                        @endforeach
											@if(empty($incomplated_view))
                                             <div class="col-md-4"><br>
                                                      <div class="form-group">
                                                         <label><b>Remark By Analyst :</b></label>
                                                         <textarea  disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_empinfo'>{{$empinfo_v->first()->remark}}</textarea>       
                                                      </div>
                                              </div>
											  @endif

                                                        </div>

                                                        <div class="tab-pane" id="tab4">
                                                            <br>
                                                            <h3><strong>Step 4</strong> - Jumlah Pembiayaan  / <i> Loan Amount </i> </h3>
                                                          
                                                               <div class="col-md-4">
                                                                <br>
                                                                  @foreach($loan as $loan)
                                                                        <?php
                                                                        $salary_dsr = $loan->dsr * $total_salary / 100;
                            
                                                                        $max  =  $salary_dsr * 12 * 10 ;
                                                                         ?>
                                                
                                                                  @endforeach
                                                     <div class="form-group">
                                                      <label>1. Pakej   /<i> Package</i>:  </label>
                                                     <input  disabled type="text" disabled class="form-control" name="package"  placeholder="RM" value="{{$pra->package->name}}">
                                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                     <input name="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                    
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Jumlah Pembiayaan  <i>/ Loan Amount  </i> (RM):</label>
                                                    <input  disabled type="text" disabled class="form-control"  name="loanammount" placeholder="RM" value="{{$loanammount->loanammount}}">
                                                   
                                                    </div>
                                 
                                         <div class="form-group">
                                                       <label>3. Pinjaman Maksima <i>/ Max Loan Eligibility  </i>  (RM):</label>
                                                    <input  disabled type="text" class="form-control"  name="maxloan" placeholder="RM" readonly 
                                                  @if(!empty($loanammount->maxloan))
                                                    value="{{$loanammount->maxloan}}"
                                                    @else 
                                                     value="{{$max}}"
                                                    @endif
                                                    />
                                                    
                                                    
                                                    </div>
                                 

                                     <div class="form-group">
                                            <label>4. Cara Bayaran Balik / <i> Payment Method   </i>  : </label>

                                                    <select  disabled class="form-control"  name="method">
                                                     @if(!empty($loanammount->method))
                                                      
                                                       <option >{{$loanammount->method}} </option>
                                                      @endif
                                                   
                                                    <option>Biro </option>
                                                    <option>Gajian <i>/ Salary</i></option>
                                                     <option>Pindahan Gaji Majikan (PGM)</option>
                                                     <option>Post Dated  Cheque (PDC)</option>
                                                        <option>Standing Instruction (SI) / Arahan Tetap (AT)</option>
                                                  
                                                    </select>
                                                   
                                                
                                                   
                                        </div>
                                          <div class="form-group">
                                                    <label>5. No Anggota / <i>Membership No.</i> :</label>
                                                    <input  disabled type="text" class="form-control" maxlength="14" name="memberno" data-mask="99-999-9999999" value="{{$loanammount->memberno}}" >
                                                   
                                          </div>
                                 </div>
                                        <div class="col-md-5">
                                                                <br> <br>
                                        
                                          <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Tempoh</th>
                                                <th>Jumlah Pembiayaan</th>
                                                <th>Ansuran Bulanan</th>
                                                 <th>Kadar Keuntungan</th>
                                                   <th>Pilih</th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            @foreach($tenure as $tenure)
                                            <?php 

                                                   $bunga2 =  $pra->loanamount * $tenure->rate /100   ;
                                                   $bunga = $bunga2 * $tenure->years;
                                                   $total = $pra->loanamount + $bunga ;
                                                   $bulan = $tenure->years * 12 ;
                                                   $installment =  $total / $bulan ;

                                                   

                                                   if($installment  < $salary_dsr ) {

                                                ?>
                                            <tr>
                                                <td style="width:100px">{{$tenure->years}} years</td>
                                                   <td> RM {{ number_format( $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                                                
                                                <td style="width:100px">RM {{ number_format($installment, 0 , ',' , '.' )  }}   / month </td>
                                              
                                                <td  align="center" style="width:50px">{{$tenure->rate}} %</td>
                                                <td  align="center"> 
                                                 <input  disabled  type="radio"  name="id_tenure" value="{{$tenure->id}}" <?php if($tenure->id == $loanammount->id_tenure) {echo 'checked' ; }  ?> >
                                                  </td>
                                            </tr>
                                            <?php } ?>
                                            @endforeach

                                        </tbody>
                                    </table>
                                       
                                                

                                                            </div>
                                              @if(empty($incomplated_view))                 
                                             <div class="col-md-3"><br>
                                                 <div class="form-group">
                                                    <label><b>Remark By Analyst :</b></label>
                                                     <textarea  disabled  class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_loanammount'>{{$loanammount_v->first()->remark}}</textarea>       
                                                </div>
                                             </div>
											 @endif
                                                        </div>

                                                          <div class="tab-pane" id="tab5">
                                                            <br>
                                                            <h3><strong>Step 5</strong> - Maklumat Pasangan / <i> Spouse Information </i>  <span id="statuskawin"> <b>(Not Applicable)</b> </span></h3>
                                                          <div class="col-md-4"  id="tab99">
                                                                <br>
                                                      <div class="form-group"  >
                                                        <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                                    
                                                       @foreach($spouse as $spouse)
                                                    <label>1.  Nama Suami/Isteri (Seperti dalam KP)   / <i>Spouse Name (As in IC)   :  </i>  </label>
                                                 <input  disabled type="text" maxlength="66" c class="form-control" value="{{$spouse->name}}" name="name" >
                                                 

                                                 
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Tarikh Lahir /<i>Date Of Birth:  </i> </label>
                                                       <?php
                                                       if($spouse->dob=='0000-00-00' OR $spouse->dob=='1970-01-01') {
                                                         $lahir2='';
                                                       }
                                                       else {
                                                         $lahir2 =  date('d/m/Y', strtotime($spouse->dob));
                                                       }
                                                      
                                                      ?>
                                                    <input  disabled type="text" class="form-control startdate"  value="{{$lahir2}}" name="dob" >
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    
                                                    
                                                    </div>
                                                   <div class="form-group">
                                
                                                      <label>3. No. Kad Pengenalan Baru /<i>  New IC </i>:</label>
                                                       <input  disabled type="text" value="{{$spouse->new_ic}}" onkeypress="return isNumberKey(event)"  maxlength="12" name="new_ic" class="form-control"/>
                                                     
                                                   </div> 
                                
                                
                                         <div class="form-group">
                                
                                          <label>4. No. Kad Pengenalan Lama /<i>  Old IC </i>:</label>
                                              <input  disabled type="text" maxlength="8"   value="{{$spouse->old_ic}}" name="old_ic" class="form-control"/>
                                                   
                                         
                                         </div>
                                             <div class="form-group">
                                
                                                 <label>5.  No. Tel. / Contact No. </i>:</label>
                                                 <input  disabled type="text" maxlength="66" value="{{$spouse->handphone}}" id="handphone" name="handphone" class="form-control"/>
                                                
                                                          
                                                 </div> 
                                
                                
                                 
                                       </div> 
                                        <div class="col-md-4" id="tab98">
                                            <br>
                                <div class="form-group">
                                
                                <label>6.  Nama & Alamat Majikan /<i>  Name & Employer's Address </i>:</label>
                                     <input  disabled type="text" maxlength="120" value="{{$spouse->emp_name}}" id="tanggall" name="emp_name" class="form-control"/>
                                 
                                                   
                                </div> 
                                
                            
                                 <div class="form-group">
                                
                                <label>7.  Poskod  /<i>  Postcode  </i>:</label>
                                    <input type="text"  disabled maxlength="5" value="{{$spouse->postcode}}" onkeypress="return isNumberKey(event)" id="postcode2"   name="postcode" class="form-control"/>
                                              
                                 </div> 
                                
                            
                                    
                                 
                                            
                            <div class="form-group">
                                
                                    <label>8.  Bandar / Negeri  /<i>  City / State   </i>:</label>
                                    <input type="text"  disabled  maxlength="50" value="{{$spouse->city}}" id="city2" name="city" class="form-control"/>
                                                
                                                </div>
                                                    <div class="form-group">
                                
                                     <label>9.  No. Tel. Pejabat  /<i>  Office Phone No.   </i>:</label>
                                        <input  disabled type="text"  maxlength="12" onkeypress="return isNumberKey(event)"  value="{{$spouse->phone}}"id="tanggall" name="phone" class="form-control"/>
                                                 
                                   
                                    </div> 
                                              @endforeach
                                        
                                         

                                            
                                                </div>
											@if(empty($incomplated_view))
                                                <div class="col-md-4"><br>
                                                 <div class="form-group">
                                                    <label><b>Remark By Analyst :</b></label>
                                                     <textarea  disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_spouse'>{{$spouse_v->first()->remark}}</textarea>       
                                                </div>
                                             </div>
											 @endif
                                                          
                                                        </div>

                                                          <div class="tab-pane" id="tab6">
                                                            <br>
                                                            <h3><strong>Step 6</strong> - Perujuk / <i> Reference </i></h3>
                                                          @foreach($reference as $reference)
                                                          <div class="col-md-4" >
                                                                <br>
                                                                 <div class="form-group" >
                                                                    <label>1.  Nama Perujuk / <i> Reference Name  </i> : </label>
                                                                     <input disabled  type="text" maxlength="66" name="name" value="{{$reference->name}}"  class="form-control">
                                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                     <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                                     
                                                                 </div>

                                                                  <div class="form-group">
                                                                      <label>2.   Alamat Kediaman  /<i> Residential Address  </i> : </label>
                                                                     <br><textarea  disabled class="form-control"   maxlength="66" name="address" rows="3" >{{$reference->address}}</textarea>
                                                                    
                                                                  </div>
                                                                  <div class="form-group">
                                                                         <label>3. Poskod  / <i>Postcode  :</i></label><br>
                                                                     <input  disabled type="text" maxlength="5"  name="postcode" value="{{$reference->postcode}}" class="form-control" onkeypress="return isNumberKey(event)">
                                                                     
                                                                  </div>

                                                                  <div class="form-group">
                                                                           <label>4. Bandar / Negeri /<i> City / State</i> : </label>
                                                                     <input  disabled type="text" maxlength="50" name="state" class="form-control" value="{{$reference->state}}">
                                                                     
                                                                  </div>

                                                       </div>     

                                                               <div class="col-md-4">
                                                                  <br>
                                                                  <div class="form-group">
                                                                     <label>5. No. Kad Pengenalan /<i> IC No  </i> : </label>
                                                                     <input  disabled type="text" maxlength="12"  name="new_ic" onkeypress="return isNumberKey(event)"  value="{{$reference->new_ic}}"="required" class="form-control">
                                                                      
                                                                  </div>
                                                               <div class="form-group">
                                                                    <label>6. Pekerjaan  / <i> Occupation</i> :</label>
                                                                  <input  disabled type="text" maxlength="22"  name="occupation"="required" class="form-control" value="{{$reference->occupation}}">
                                                                     
                                                               </div>
                                                               <div class="form-group">
                                                                  <label>7. Hubungan   / <i> Relationship</i> :</label>
                                                                  <input  disabled type="text" maxlength="22"  name="relationship"   class="form-control" value="{{$reference->relationship}}">
                                                                     
                                                               </div>

                                                               <div class="form-group">
                                                                  <label>8. No. Untuk Dihubungi / <i>Contact No.</i> :</label>
                                                                  <ul>
                                                                     <li>Kediaman / <i>House </i> : <br><input  disabled type="text" onkeypress="return isNumberKey(event)"  maxlength="12" name="houseno"  value="{{$reference->houseno}}" >
                                                                        
                                                                     </li>
                                                                     <li>Tel.Bimbit / <i>Handphone</i> : <br><input  disabled type="text"  maxlength="12" name="handphone"  value="{{$reference->handphone}}" onkeypress="return isNumberKey(event)">
                                                                        
                                                                     </li>
                                                                     <li>Pejabat / <i>Office</i> :<br><input  disabled type="text"  onkeypress="return isNumberKey(event)"  maxlength="12" name="officeno" value="{{$reference->officeno}}">
                                                                      
                                                                     </li>
                                                                  </ul>
                                                                  
                                                               </div>
                                                        </div>
                                                        @endforeach
														@if(empty($incomplated_view))
                                                        <div class="col-md-4"><br>
                                                            <div class="form-group">
                                                                <label><b>Remark By Analyst :</b></label>
                                                                  <textarea  disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_reference'>{{$reference_v->first()->remark}}</textarea>       
                                                            </div>
                                                         </div>
														@endif
                                                        
                                                        </div>

                                                          <div class="tab-pane" id="tab7">
                                                            <br>
                                                            <h3><strong>Step 7</strong> - Latar Belakang Kewangan / <i> Financial Background </i>  </h3>
                                                          
                                                        @foreach($financial  as $financial)
                                                         <div class='row'>
                                                          <div class="col-md-4">
                                                          
                                                                 <br>
                                                                PENDAPATAN (A) /<i> INCOME (A) (RM)</i>
                                                                <br>
                                                                 <div class="form-group">
                                                                 <label>1. Gaji Bulanan : Asas + Elaun Tetap / <i> Monthly Income : Basic + Fixed Allowance </i>  </label>
                                                             <input onchange="toFloat('income1')"  disabled  type="text" maxlength="10" name="income"  onkeypress="return isNumberKey(event)" id="income1" class="form-control income" placeholder="RM" value="{{number_format((float)$financial->income, 2, '.', '')}}">
                 
                                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                                   
                                                                    
                                                                </div>

                                                                <div class="form-group">
                                                                     <label>2.   Lain-lain Pendapatan  <i> Other Income </i> : </label>
                                                                     <input  disabled type="text" onchange="toFloat('income2')" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control income" name="other_income" placeholder="RM" value="{{number_format((float)$financial->other_income, 2, '.', '')}}" id="income2">
                                                                     
                                                                </div>
                                                               <div class="form-group">
                                                                  <label>3. Pendapatan Suami / Isteri /<i> Husband / Wife Income   :</i></label><br>
                                                                    <input  disabled onchange="toFloat('income3')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  class="form-control income" name="spouse_income" placeholder="RM" value="{{number_format((float)$financial->spouse_income, 2, '.', '')}}" id="income3"> 
                                                                    
                                                               </div>

                                                               <div class="form-group">
                                                                   <label>4. Jumlah Pendapatan  <i>Total Income </i> : </label>
                                                                   <input  disabled type="text" maxlength="22" required="required" onkeypress="return isNumberKey(event)"   class="form-control" placeholder="RM" name="total_income" value="{{number_format((float)$financial->income + $financial->other_income +  $financial->spouse_income  , 2, '.', '')}}" id="income4" readonly>
                                                                  
                                                               </div>
                                                                  
                                                              

                                                       </div>     

                                                      <div class="col-md-4">
                                                          
                                                                <br>
                                                                PERBELANJAAN (B) /<i> EXPENSES (B) (RM)</i>
                                                                
                                                                <br>
                                                                 
                                                                <div class="form-group">
                                                                <label>1.   Lain-lain Perbelanjaan / <i> Other Expenses </i> : </label>
                                                                  <input  disabled onchange="toFloat('outcome2')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control income" placeholder="RM" name="expenses" value="{{number_format((float)$financial->expenses, 2, '.', '')}}" id="outcome2">
                                                                  
                                                                </div>
                                                               <div class="form-group">
                                                                 <label>2. Jumlah Ansuran Bulanan /<i> Monthly Payment  :</i></label><br>
                                                                  <input  disabled type="text" onchange="toFloat('outcome3')" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control income" placeholder="RM" name="monthly_payment" value="{{number_format((float)$financial->monthly_payment, 2, '.', '')}}" id="outcome3">
                                                                   
                                                               
                                                               </div>

                                                            <div class="form-group">
                                                                <label>3. Sewa Rumah / <i> House Rental </i> : </label>
                                                               <input  disabled type="text" onchange="toFloat('outcome4')" maxlength="10" required="required" onkeypress="return isNumberKey(event)" class="form-control income" placeholder="RM" name="house_rental" value="{{number_format((float)$financial->house_rental, 2, '.', '')}}" id="outcome4">
                                                                     
                                                            </div>

                                                            <div class="form-group">
                                                              <label>4. Jumlah Perbelanjaan  /<i> Total Expenses </i> : </label>
                                                                <input  disabled type="text" onchange="toFloat('outcome5')" maxlength="10" required="required" class="form-control " placeholder="RM" name="total_expenses" value="{{number_format((float)$financial->cost_living + $financial->expenses + $financial->monthly_payment + $financial->house_rental, 2, '.', '')}} " id="outcome5" readonly>
                                                                     
                                                            </div>
                                                               
                                                            <div class="form-group">
                                                               <label> <b> PENDAPATAN BERSIH/<i> NET INCOME </i> (A - B) (RM): </b></label>
                                                               <input  disabled type="text" maxlength="10"  required="required" class="form-control " placeholder="RM" name="net_income" value="{{number_format((float)($financial->income + $financial->other_income +  $financial->spouse_income) - ( $financial->expenses + $financial->monthly_payment + $financial->house_rental), 2, '.', '')}}" id="totalincome" readonly >
                                                                  
                                                            </div>
                                                               
                                                         
                                                 

                                                       </div>
													   @if(empty($incomplated_view))
                                                         <div class="col-md-4"><br>
                                                            <div class="form-group">
                                                               <label><b>Remark By Analyst :</b></label>
                                                                <textarea  disabled class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 283px;" name='remark_financial'>{{$financial_v->first()->remark}}</textarea>       
                                                           </div>
                                                        </div>
													@endif
                                                         
                                                      <br><br><br><br>
                                                      
                                                                                                      
                                                               
                                                         
                                                        </div>
                                                   
                                                         @endforeach
                                                         <div class="row">
                                                         <hr>
                                                         <div class="col-md-4">
                                                            <br>
                                                                <font color='red'><h2>Applicant Fixed Income</h2></font>
                                                                
                                             
                                                                 
                                                                <div class="form-group">
                                                                <label>1. Monthly Basic Salary </label>
                                                                  <input  disabled onchange="toFloat('fn_monthly_basic')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_monthly_basic" value="{{number_format((float)$financial->fn_monthly_basic, 2, '.', '')}}" id="fn_monthly_basic">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>2. Govt. Service Allowance </label>
                                                                  <input  disabled onchange="toFloat('fn_govt_service')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_govt_service" value="{{number_format((float)$financial->fn_govt_service, 2, '.', '')}}" id="fn_govt_service">                                          
                                                                </div>
                                                                  <div class="form-group">
                                                                <label>3. Financial Incentive Payment Services </label>
                                                                  <input  disabled onchange="toFloat('fn_financial_incentive')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_financial_incentive" value="{{number_format((float)$financial->fn_financial_incentive, 2, '.', '')}}" id="fn_financial_incentive">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>4. Fixed Allowance - Others</label>
                                                                  <input  disabled onchange="toFloat('fn_fixed_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_fixed_allowance" value="{{number_format((float)$financial->fn_fixed_allowance, 2, '.', '')}}" id="fn_fixed_allowance">                                          
                                                                </div>
                                                               
                                                               <div class="form-group">
                                                               <label><font color='blue'><b>Gross Income</b></font></label>
                                                                  <input  disabled onchange="toFloat('fn_gross_income')" readonly type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_gross_income" value="{{number_format((float)$financial->fn_gross_income, 2, '.', '')}}" id="fn_gross_income">                                          
                                                                </div> 
                                                         </div>
                                                            
                                                         <div class="col-md-4"><br> <h2>&nbsp;</h2>
                                                               <div class="form-group">
                                                                <label>5. Housing Allowance</label>
                                                                  <input  disabled onchange="toFloat('fn_housing_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_housing_allowance" value="{{number_format((float)$financial->fn_housing_allowance, 2, '.', '')}}" id="fn_housing_allowance">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>6. Cost of Living Incentive</label>
                                                                  <input  disabled onchange="toFloat('fn_cost_of')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_cost_of" value="{{number_format((float)$financial->fn_cost_of, 2, '.', '')}}" id="fn_cost_of">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>7. Special Allowance</label>
                                                                  <input  disabled onchange="toFloat('fn_special_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_special_allowance" value="{{number_format((float)$financial->fn_special_allowance, 2, '.', '')}}" id="fn_special_allowance">                                          
                                                                </div>  
                                                         </div>
                                                           
                                                         
                                                         </div>
                                                           <div class="row">
                                                            <div class='col-md-4'>
                                                             <br>
                                                                <font color='red'><h2>Applicant Non-Fixed Income</h2></font>
                                                                
                                                               
                                                                 
                                                                <div class="form-group">
                                                                <label>1. Overtime Allowance</label>
                                                                  <input  disabled onchange="toFloat('fn_overtime_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_overtime_allowance" value="{{number_format((float)$financial->fn_overtime_allowance, 2, '.', '')}}" id="fn_overtime_allowance">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>2. Travel/Miles/Transport Allowance </label>
                                                                  <input  disabled onchange="toFloat('fn_travel')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_travel" value="{{number_format((float)$financial->fn_travel, 2, '.', '')}}" id="fn_travel">                                          
                                                                </div>
                                                                  <div class="form-group">
                                                                <label>3. Commission </label>
                                                                  <input  disabled onchange="toFloat('fn_commission')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_commission" value="{{number_format((float)$financial->fn_commission, 2, '.', '')}}" id="fn_commission">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>4. Business Income</label>
                                                                  <input  disabled onchange="toFloat('fn_business_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_business_income" value="{{number_format((float)$financial->fn_business_income, 2, '.', '')}}" id="fn_business_income">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>5. Yearly Dividend Income</label>
                                                                  <input  disabled onchange="toFloat('fn_yearly_devidend')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_yearly_devidend" value="{{number_format((float)$financial->fn_yearly_devidend, 2, '.', '')}}" id="fn_yearly_devidend">                                          
                                                                </div>
                                                               <div class="form-group">
                                                               <label><font color='blue'><b>Total Income</b></font></label>
                                                                  <input  disabled readonly onchange="toFloat('fn_total_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_income" value="{{number_format((float)$financial->fn_total_income, 2, '.', '')}}" id="fn_total_income">                                          
                                                                </div> 
                                                         </div>
                                                            
                                                         <div class="col-md-4"> <br> <h2>&nbsp;</h2>
                                                               <div class="form-group">
                                                                <label>6. Food Allowance</label>
                                                                  <input  disabled onchange="toFloat('fn_food_allowance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_food_allowance" value="{{number_format((float)$financial->fn_food_allowance, 2, '.', '')}}" id="fn_food_allowance">                                          
                                                                </div> 
                                                               <div class="form-group">
                                                                <label>7. Yearly Contractual Bonuses</label>
                                                                  <input  disabled onchange="toFloat('fn_yearly_contractual')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_yearly_contractual" value="{{number_format((float)$financial->fn_yearly_contractual, 2, '.', '')}}" id="fn_yearly_contractual">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>8. Housing/Building Rental</label>
                                                                  <input  disabled onchange="toFloat('fn_housing_building')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_housing_building" value="{{number_format((float)$financial->fn_housing_building, 2, '.', '')}}" id="fn_housing_building">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>9. Non fixed Allowance - Others</label>
                                                                  <input  disabled onchange="toFloat('fn_non_fixed')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_non_fixed" value="{{number_format((float)$financial->fn_non_fixed, 2, '.', '')}}" id="fn_non_fixed">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>10. Revenue/Share Income</label>
                                                                  <input  disabled onchange="toFloat('fn_share_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_share_income" value="{{number_format((float)$financial->fn_share_income, 2, '.', '')}}" id="fn_share_income">                                          
                                                                </div> 
                                                         </div>
                                                           </div>
 
    
                                                         <div class="row">
                                                         <hr>
                                                            <div class='col-md-4'>
                                                             <br>
                                                                <font color='red'><h2>Applicant Payslip Deductions</h2></font>
                                                                
                                                               
                                                                 
                                                                <div class="form-group">
                                                                <label>1. KWSP - Statutory</label>
                                                                  <input  disabled onchange="toFloat('fn_knwsp')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_knwsp" value="{{number_format((float)$financial->fn_knwsp, 2, '.', '')}}" id="fn_knwsp">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>2. Income Tax - Statutory </label>
                                                                  <input  disabled onchange="toFloat('fn_income_tax')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_income_tax" value="{{number_format((float)$financial->fn_income_tax, 2, '.', '')}}" id="fn_income_tax">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>3. CP38 Deduction - Statutory </label>
                                                                  <input  disabled onchange="toFloat('fn_cp38')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_cp38" value="{{number_format((float)$financial->fn_cp38, 2, '.', '')}}" id="fn_cp38">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>4. House Financing </label>
                                                                  <input  disabled onchange="toFloat('fn_house_financing')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_house_financing" value="{{number_format((float)$financial->fn_house_financing, 2, '.', '')}}" id="fn_house_financing">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>5. Others </label>
                                                                  <input  disabled onchange="toFloat('fn_others')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_others" value="{{number_format((float)$financial->fn_others, 2, '.', '')}}" id="fn_others">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>6. Lembaga Tabung Haji </label>
                                                                  <input  disabled onchange="toFloat('fn_lembaga_tabung')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_lembaga_tabung" value="{{number_format((float)$financial->fn_lembaga_tabung, 2, '.', '')}}" id="fn_lembaga_tabung">                                          
                                                                </div>
                                                                <div class="form-group">
                                                                <label>7. Insurance/Tafakul Deductions </label>
                                                                  <input  disabled onchange="toFloat('fn_insurance')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_insurance" value="{{number_format((float)$financial->fn_insurance, 2, '.', '')}}" id="fn_insurance">                                          
                                                                </div>
                                                               <div class="form-group">
                                                               <label><font color='blue'><b>Total Payslip Deductions</b></font>  </label>
                                                                  <input  disabled readonly onchange="toFloat('fn_total_payslip')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_payslip" value="{{number_format((float)$financial->fn_total_payslip, 2, '.', '')}}" id="fn_total_payslip">                                          
                                                                </div>
                                                         
                                                         </div>
                                                            
                                                         <div class="col-md-4"> <br> <h2>&nbsp;</h2><br><br>
                                                               <div class="form-group">
                                                                <label>8. PERKESO - Statutory</label>
                                                                  <input  disabled onchange="toFloat('fn_perkeso')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_perkeso" value="{{number_format((float)$financial->fn_perkeso, 2, '.', '')}}" id="fn_perkeso">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>9. Zakat on Income - Statutory</label>
                                                                  <input  disabled onchange="toFloat('fn_zakat')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_zakat" value="{{number_format((float)$financial->fn_zakat, 2, '.', '')}}" id="fn_zakat">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>10. BPA Deductions</label>
                                                                  <input  disabled onchange="toFloat('fn_bpa')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_bpa" value="{{number_format((float)$financial->fn_bpa, 2, '.', '')}}" id="fn_bpa">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>11. ASB/ASN (v)</label>
                                                                  <input  disabled onchange="toFloat('fn_asb')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_asb" value="{{number_format((float)$financial->fn_asb, 2, '.', '')}}" id="fn_asb">                                          
                                                                </div>
                                       
                                                         </div>
                                                           </div>
                                                
                                                         <div class="row">
                                               
                                                            <div class='col-md-4'>
                                                             <br>
                                                                <font color='red'><h2>Non Pay Slip Deductions</h2></font>
                                                                
                                                               
                                                                 
                                                                <div class="form-group">
                                                                <label>Location</label>
                                                                  <input  disabled type="text" maxlength="50" value="{{$financial->fn_location}}" class="form-control fn_non_paylsip" name="fn_location" placeholder='' id="fn_location">                                          
                                                                </div>
                                                               <div class="form-group">
                                                                <label>Cost of Living </label>
                                                                  <input  disabled onchange="toFloat('fn_cost_of2')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_cost_of2" value="{{number_format((float)$financial->fn_cost_of2, 2, '.', '')}}" id="fn_cost_of2">                                          
                                                                </div>
                                                               <div class="form-group">
                                                               <label><font color='blue'><b>Total Non Payslip Deductions</b></font> </label>
                                                                  <input  disabled readonly onchange="toFloat('fn_total_non')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_non" value="{{number_format((float)$financial->fn_total_non, 2, '.', '')}}" id="fn_total_non">                                          
                                                                </div>
                                                          
                                                               <div class="form-group">
                                                               <label><font color='blue'><b>Total Deductions</b></font> </label>
                                                                  <input  disabled readonly onchange="toFloat('fn_total_deductions')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_deductions" value="{{number_format((float)$financial->fn_total_deductions, 2, '.', '')}}" id="fn_total_deductions">                                          
                                                                </div>
                                                                    <div class="form-group">
                                                               <label><font color='blue'><b>Total Net Income</b></font>  </label>
                                                                  <input  disabled readonly onchange="toFloat('fn_total_net')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_total_net" value="{{number_format((float)$financial->fn_total_net, 2, '.', '')}}" id="fn_total_net">                                          
                                                                </div>
                                                           
                                                 
   
                                                         </div>
                                                            
                                                         <div class="col-md-4"> <br> <h2>&nbsp;</h2>
                                                            
                                                               <br><br><br><br>
                                                              
                                                               <div class="form-group">
                                                                <label>Other Non Payslip Deductions</label>
                                                                  <input  disabled onchange="toFloat('fn_other_non')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fn_other_non" value="{{number_format((float)$financial->fn_other_non, 2, '.', '')}}" id="fn_other_non">                                          
                                                                </div>
                                                                

                                       
                                                         </div>
                                                           </div>            
                                                         
                                                            
                                                            
                                                          </div>
                                                         
                                                         <div class="tab-pane" id="tab8">
                                                            <br>
                                                            <h3><strong>Step 8</strong> - Muat Naik Dokumen / <i>  Upload Document </i> </h3>
                                                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <fieldset>
                                            
                                            <div class="container" style="">
                                                <!-- <div class="row">
         
                                                    <label class="col-md-7 control-label">Surat Kebenaran Potongan Gaji</label>
                                                    <div class="col-md-4">

                                                      <input type="hidden" name="document5"  id="documentx5"   value="Surat Kebenaran Potongan Gaji">
                                                      <input name="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                       &nbsp; <span id="document5"> </span> 
                                                      @if(!empty($document5->name))
                                                      <a class='btn btn-primary' target='_blank' href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document5->upload}}"> Download </a>
                                                     @else
                                                        <a class='bnt btn-danger'>No Attachment Available</a>
                                                      @endif
                                                @if(!empty($document5->name))
                                                    @if($document5->verification =='1') 
                                                         <label class="toggle">
                                                             <input type="radio" name="document5_v" value='1' checked>
                                                              <b><font color='green'>Valid</font></b> 
                                                         </label>
                                                             <label class="toggle"> 
                                                              <input type="radio" name="document5_v" value='0'>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label> 
                                                      
                                                      @elseif($document5->verification=='0') 
                                                          <label class="toggle"> 
                                                              <input type="radio" name="document5_v" value='1'>
                                                               <b><font color='green'>Valid</font></b> 
                                                          </label>
                                                          <label class="toggle"> 
                                                              <input type="radio" name="document5_v" value='0' checked>
                                                               <b><font color='red'>Invalid</font></b> 
                                                          </label> 
                                                      
                                                      @else 
                                                         <label class="toggle">
                                                             <input type="radio" name="document5_v" value='1'>
                                                              <b><font color='green'>Valid</font></b> 
                                                         </label>
                                                         <label class="toggle"> 
                                                              <input type="radio" name="document5_v" value='0'>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label>
                                                      @endif
                                                   @else
                                                       <label class="toggle"> 
                                                              <input type="radio" name="document5_v" value='0' checked>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label> 
                                                   @endif
                                                    </div>
                                                </div>!-->
                                                 <div class="row">
                                                    <label class="col-md-7 control-label">Salinan Kad Pengenalan</label>
                                                    <div class="col-md-4">
                                                  
                                                      <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenal">
                                                      &nbsp; <span id="document6"> </span> 
                                                      @if(!empty($document6->name))
                                                      <a class='btn btn-primary' target='_blank'  href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document6->upload}}"> Download </a>
                                                       @else
                                                               <a class='bnt btn-danger'>No Attachment Available</a>
                                                             @endif


                                               
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <label class="col-md-7 control-label">Salinan Penyata Gaji Untuk 3 Bulan Terkini</label>
                                                    <div class="col-md-4">
                                                  
                                                         <input type="hidden" name="document7"  id="documentx7"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini">
                                                          &nbsp; <span id="document7"> </span> 
                                                         @if(!empty($document7->name))
                                                         <a class='btn btn-primary' target='_blank'  href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document7->upload}}"> Download </a>
                                                         @else
                                                               <a class='bnt btn-danger'>No Attachment Available</a>
                                                             @endif
                                                             
                                                             @if(!empty($document10->name))
                                                      <a class='btn btn-primary' target='_blank'  href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document10->upload}}"> Download </a>
                                                      @endif

                                                       @if(!empty($document11->name))
                                                      <a class='btn btn-primary' target='_blank'  href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document11->upload}}"> Download </a>
                                                      @endif
                                                      
                                                    </div>
                                                </div>
                                                 <!--<div class="row">
                                                    <label class="col-md-7 control-label">Penyata Gaji Asal Terkini</label>
                                                    <div class="col-md-4">
                                                       
                                                      <input type="hidden" name="document8"   id="documentx8"  value="Penyata Gaji Asal Terkini">
                                                      &nbsp; <span id="document8"> </span> 
                                                       @if(!empty($document8->name))
                                                      <a class='btn btn-primary'  target='_blank' href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document8->upload}}"> Download </a>
                                                      @else
                                                               <a class='bnt btn-danger'>No Attachment Available</a>
                                                             @endif
                                              @if(!empty($document8->name))
                                                @if($document8->verification =='1') 
                                                         <label class="toggle">
                                                             <input type="radio" name="document8_v" value='1' checked>
                                                              <b><font color='green'>Valid</font></b> 
                                                         </label>
                                                             <label class="toggle"> 
                                                              <input type="radio" name="document8_v" value='0'>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label> 
                                                      
                                                      @elseif($document8->verification=='0') 
                                                          <label class="toggle"> 
                                                              <input type="radio" name="document8_v" value='1'>
                                                               <b><font color='green'>Valid</font></b> 
                                                          </label>
                                                          <label class="toggle"> 
                                                              <input type="radio" name="document8_v" value='0' checked>
                                                               <b><font color='red'>Invalid</font></b> 
                                                          </label> 
                                                      
                                                      @else 
                                                         <label class="toggle">
                                                             <input type="radio" name="document8_v" value='1'>
                                                              <b><font color='green'>Valid</font></b> 
                                                         </label>
                                                         <label class="toggle"> 
                                                              <input type="radio" name="document8_v" value='0'>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label>
                                                      @endif
                                                    @else
                                                       <label class="toggle"> 
                                                              <input type="radio" name="document8_v" value='0' checked>
                                                               <b><font color='red'>Invalid</font></b> 
                                                         </label> 
                                                   @endif
                                                    </div>
                                                </div>!-->
                                                 <div class="row">
                                                    <label class="col-md-7 control-label">Surat Pengesahan Majikan</label>
                                                    <div class="col-md-4">
                                                         
                                                   <input type="hidden" name="document9"   id="documentx9"  value="Surat Pengesahan Majikan">
                                                   &nbsp; <span id="document9"> </span> 
                                                    @if(!empty($document9->name))
                                                   <a class='btn btn-primary'target='_blank'  href="{{url('/uploads/file/')}}/{{$pra->fullname}}/{{$document9->upload}}"> Download </a>
                                                   @else
                                                               <a class='bnt btn-danger'>No Attachment Available</a>
                                                             @endif
                                             
                                                    </div>
                                                      
                                                </div>
                                            </div>
                                               
                                            </fieldset>
                                                          
                                                        </div>
                                                         <div class="tab-pane" id="tab9">

                                                        
                                                            <br>
                                                            <h3><strong>Step 9</strong> - Pengisytiharan / <i> Declaration </i> </h3>
                                                          Saya/kami dengan ini mengaku dan mengisytiharkan bahawa : <br>
                                                          <i>  I/we hereby agreed and declare that: <br></i> <br>

                                                          i) Maklumat yang diberikan di dalam borang permohonan pembiayaan ini dan dokumen-dokumen lain adalah benar tanpa menyembunyikan maklumat yang mungkin mempengaruhi permohonan saya/ kami dan pihak Co-opbank Persatuan berhak menolak / menarik balik sekiranya maklumat tersebut tidak benar dan palsu. <br>
                                                          <i> All the information stated in the application form and the relevant supporting documents are true/genuine. Co-opbank Persatuan reserve the right to reject/withdraw this application in any case of fraud/forged documents. <br></i>
                                                          ii) Segala transaksi yang akan dilaksanakan tidak berkaitan apa jua perkara yang tidak dibenarkan dibawah 'AMLATFA 2001' (Akta Pencegahan Pengubahan Wang Haram, Pencegahan Keganasan dan Hasil daripada Akta Haram 2001).<br>
                                                          <i>All the related transaction hereafter is not connected to Anti Money Laundering, Anti-Terrorism Financing and Proceed of Unlawful Activities Act 2001<br></i>
                                                          iii) Tidak dikenakan tindakan kebankrapan dan bukanlah seorang yang muflis seperti yang tertakluk dibawah Seksyen 3 Akta Kebankrapan 1967. Saya / kami akan memaklumkan kepada pihak  Co-opbank  Persatuan dalam masa 7 hari sekiranya saya / kami berada dalam proses kebankrapan.<br><i>No bankruptcy proceeding are being taken against me/us and I/we are not a bankrupt under Section 3 of the Bankruptcy Act 1967, I undertake to notify Co-opbank Persatuan within 7 days should there be any bankruptcy proceeding is taken against me/us. <br></i>
                                                          iv) Membenarkan pihak  Co-opbank Persatuan menghubungi majikan  atau mana-mana pihak untuk mendapatkan sebarang keterangan mengenai saya / kami dan seterusnya membenarkan mereka  memberi keterangan yang diperlukan oleh pihak  Co-opbank Persatuan.<br>
                                                          <i>Allow Co-opbank Persatuan to contact my employer or any related parties to obtain the relevant information on me / us and thereafter allow them to provide the necessary information. <br></i>
                                                          v) Tiada sebarang permohonan pembiayaan di institusi kewangan lain serentak/dalam tempoh masa yang terdekat ketika borang permohonan ini dihantar ke  Co-opbank Persatuan. <br> <i>I/we declare that I/we did not submit any other financing request to other financial institution simultaneously. <br></i>
                                                          <br>
Saya/kami dengan ini memberi kebenaran kepada Co-opbank Persatuan yang tidak boleh dibatal tanpa syarat untuk mendedahkan maklumat berkaitan dengan kemudahan kewangan saya / kami di Co-opbank Persatuan bagi apa-apa tujuan sekalipun kepada Cawangan Co-opbank Persatuan , Subsidiari, Bank Negara Malaysia (BNM), Unit Kredit Pusat (Central Credit Unit), Biro Maklumat Guaman dan agensi lain yang diluluskan oleh BNM atau mana-mana pihak lain yang mempunyai bidang kuasa ke atas Co-opbank Persatuan, dan pemegang serahan hak yang dibenarkan dan diberikuasa oleh Co-opbank Persatuan. Saya / kami membenarkan pihak Co-opbank Persatuan mendapatkan maklumat peribadi kredit saya / kami dari CTOS, RAM dan mana-mana pihak berkuasa lain yang berkaitan. Maklumat yang diperolehi hanya boleh digunakan bagi tujuan permohonan pembiayaan sahaja.  <br> <i> I/we agreed to allow Co-opbank Persatuan to reveal /forward all relevant information relating to this financing to any Co-opbank Persatuan Branch, Subsidiaries, Central Bank of Malaysia (BNM), Central Credit Bureau and any other agencies approved by Central Bank of Malaysia (BNM) or any other supervising Co-opbank Persatuan. I/we hereby agree to allow Co-opbank Persatuan to obtain the relevant information about me/us from CTOS, RAM or any other parties. All relevant information obtained are meant to be used for this specific financing only.  <br></i>
<br>
                                                        
                                                          <table align="center">
                                                          <tr>
                                                          <td valign="center" width="30"> 
                                                          <input type="checkbox" id="pertama" name="pertama" value="1" checked disabled > 
                                                          </td>
                                                        
                                                          <td>
                                                          Saya/kami bersetuju dengan terma dan syarat di atas / <br>  <i> I/we agreed with the above term and conditions </i> </td>
                                                          </tr>
                                                          </table>

                                                           <br>
                                                          <table border="1">
                                                          <tr>
                                                          <td align="center">  <b>AKTA PERLINDUNGAN DATA PERIBADI 2010 (APDP)</b> <br> <b>NOTIS PRIVASI  </b></td>
                                                          </tr>
                                                        
                                                          <td>
                                                            Pelanggan yang dihormati, <br>

                                                          <br> Selaras dengan peguatkuasaan APDP pada 15 November 2013,  Co-opbank Persatuan komited untuk mematuhi semua peruntukan APDP yang bertujuan untuk melindungi data peribadi anda. Mengikut peruntukan di bawah APDP  Co-opbank Persatuan perlu mendapatkan  persetujuan anda apabila mengumpul dan memproses data peribadi anda. Sila ambil maklum, maklumat berkenaan adalah penting untuk   Co-opbank Persatuan menilai permohonan anda sebelum permohonan anda diproses ke peringkat selanjutnya. Maklumat  berkenaan juga dapat membantu  Co-opbank  Persatuan meningkatkan perkhidmatan. <br> <br>
                                                          <b>Memproses Data</b> <br><br>

                                                          Data peribadi anda boleh diakses oleh  Co-opbank Persatuan dan/atau ejen sah  Co-opbank Persatuan untuk tujuan berikut  : 
                                                          <ol type="i">
                                                          <li> Berkomunikasi dengan anda</li>
                                                          <li> Menyediakan perkhidmatan kepada anda termasuk anak syarikat  Co-opbank Persatuan (jika ada)</li>
                                                     
                                                          <li> Menilai permohonan anda bagi apa-apa produk  Co-opbank Persatuan</li>
                                                          <li> Memproses apa-apa transaksi pembayaran</li>
                                                          <li> Menguruskan dan mengekalkan akaun dan kemudahan</li>
                                                          <li> Mengesahkan kedudukan kewangan menerusi pemeriksaan rujukan kredit</li>
                                                          <li> Menjawab pertanyaan dan aduan anda serta menyelesaikan pertikaian secara am</li>
                                                           <li> Menguruskan pembabitan anda dalam sebarang peraduan dan/atau kempen</li>
                                                          <li> Melaksanakan aktiviti dalaman</li>
                                                          <li>Melakukan kaji selidik pasaran dan analisis bagi meningkatkan produk dan perkhidmatan  Co-opbank Persatuan</li>
                                                          <li> Memenuhi keperluan pihak berkuasa/keperluan perundangan atau mana-mana Undang-Undang</li>
                                                          <li>Menguruskan faedah dan manfaat anda</li>
                                                          <li> Menguruskan pembayaran dermasiswa, kematian atau takaful yang berkaitan</li>
                                                          <li> Menguruskan pemulihan hutang dan kutipan</li>
                                                         
                                                          </ol>
                                                          

                                                        
                                                        <b>Penzahiran Data Peribadi  </b> <br>
                                                        <br>
                                                        Adalah dimaklumkan dalam sebarang situasi dan jika diperlukan ketika melaksanakan tujuan-tujuan yang dinyatakan di atas,  Co-opbank Persatuan boleh menzahirkan maklumat peribadi anda kepada pihak berikut :
                                                         <ol type="i">
                                                         <li>Mana-mana organisasi yang dilantik untuk berkhidmat sebagai ejen  Co-opbank Persatuan</li>
                                                         <li>Mana-mana organisasi untuk melengkapkan transaksi yang diminta oleh anda</li>
                                                         <li>Mana-mana penyedia perkhidmatan kewangan yang diminta oleh anda</li>
                                                         <li>Mana-mana penyedia perkhidmatan  Co-opbank Persatuan</li>
                                                         <li>Mana-mana pihak  yang dilantik oleh  Co-opbank Persatuan</li>
                                                         <li>Anak-anak syarikat  Co-opbank Persatuan</li>
                                                         <li>Penasihat profesional</li>
                                                         <li>Mana-mana pihak yang memberi jaminan atas permohonan anda</li>
                                                         <li>Mana-mana pihak yang dibenarkan di dalam peruntukan Undang-Undang, Ketetapan mahkamah atau pihak berkuasa</li>
                                                         <li>Mana-mana agensi/syarikat pelaporan kredit yang berkhidmat dengan kami</li>
                                                         <li>Agensi penguatkuasaan atau pengawalseliaan</li>


                                                         </ol>
                                                         </td>
                                                          </tr>
                                                           <tr>
                                                          <td align="center"> <b>PERAKUAN </b></td> 
                                                          </tr>
                                                         <tr>
                                                         <td>

                                                         <br>
                                                          Dengan ini, saya/kami mengesahkan bahawa :
                                                          <ul>
                                                          <li>  <input type="checkbox" id="satu"disabled name="satu" value="1" checked >  Saya/kami bersetuju tertakluk di bawah nota privasi di atas  </li>
                                                          <li> <input type="checkbox" id="dua" disabled name="dua" value="1" checked >Saya/kami dengan ini bersetuju memberi persetujuan secara nyata ("explicit consent") kepada  Co-opbank Persatuan untuk mengumpul, memproses dan/atau menzahirkan data peribadi saya (termasuk data sensitif) seperti di dalam Nota Privasi di atas</li>
                                                          <li> <input type="checkbox" id="tiga" disabled name="tiga" value="1" checked>  Data peribadi yang diberikan oleh saya/kami adalah tepat, lengkap, terkini dan benar. Saya/kami berjanji akan memaklumkan kepada  Co-opbank Persatuan secepat mungkin jika terdapat perubahan data peribadi saya/kami 
                                                          </li>

                                                          </ul>

                                                          <br>
                                                           Pada masa tertentu  Co-opbank Persatuan mungkin akan menghantar apa-apa bentuk promosi dan pemasaran yang mungkin menarik minat anda. Jika anda mahu menerima komunikasi tersebut, sila lengkapkan ruangan di bawah dengan menandakan diruangan pilihan anda.
                                                          <br>
                                                          @if ($term->promosi=='1')
                                                          <input type="radio" name="pernyataan" value="1" checked disabled> Ya, Saya/kami bersetuju 
                                                          
                                                          <br>
                                                          <input type="radio" name="pernyataan" value="0" disabled> Tidak, Saya/kami tidak bersetuju
                                                         @elseif ($term->promosi=='0')
                                                            <input type="radio" name="pernyataan" value="1"  disabled> Ya, Saya/kami bersetuju
                                                             <br>
                                                          <input type="radio" name="pernyataan" value="0" checked disabled> Tidak, Saya/kami tidak bersetuju
                                                         @endif
                                                          </td>
                                                          </tr>
                                                          </table>
                                                  
                                                  
                                                  
                                                  
                                                  
                                                         <input type="hidden" name="_token" id='token_term' value="{{ csrf_token() }}">
                                                         <input name="id_praapplication" id="id_praapplication_term" type="hidden"  value="{{$data->id_praapplication}}" >
                                                         <input name="dec_approved" id="dec_approved" type="hidden"  value="2" >
       
                                 
                                                         <input name="dec_rejected" id="dec_rejected" type="hidden"  value="1" >
                                                         <input name="dec_waiting1" id="dec_waiting1" type="hidden"  value="3" >
                                                         <input name="dec_waiting2" id="dec_waiting2" type="hidden"  value="4" >
                                                    
                                                   @if (($user->role=='1') OR ($user->role=='4')) 
                                                  
                                                      <div class="row">
                                                         <div class="col-md-4"><br>
														   @if ($term->id_branch=='0')  
																<p><b>This Application has not been routed</b></p>
															@else
                                                           <div class="form-group">
                                                               <label><b>Branch Route</b></label>
                                                               <select disabled name="branch" id='branch' class=" select2">
                                                                  <option value=''>--Select Branch--</option>
                                                                  @foreach ($branch as $branch)
                                                                     @if ($branch->id==$term->id_branch) 
                                                                     <?php $selected ="selected"; ?>
                                                                  @else
                                                                      <?php $selected =" "; ?>
                                                                     @endif
                                                                     <option value='{{ $branch->id }}' {{ $selected }} >{{ $branch->branchname}}</option>
                                                                  @endforeach
                                                       
                                                               </select>
                                                            </div>
															@endif
                                                            <div class="form-group">
                                                               <label><b>Remark / Reason :</b></label>
                                                                <textarea disabled id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'>{{$term->verification_remark}}</textarea>       
                                                           </div>
                                                            
                                                            
                                                        </div>
                                                      </div>
                                                            <div class="row">
                                                               <div class="col-sm-2">
                                                                  <a href='{{ url('/admin') }}' class="btn btn-primary" >
                                                                  &nbsp; << Back to Dashboard &nbsp; &nbsp;
                                                                  </a> 
                                                               </div>
                                                               <!--<div class="col-sm-2">
                                                                  <a id="disagree" class="btn btn-danger" >
                                                                  Application Rejected
                                                                  </a> 
                                                               </div>
                                                                  <div class="col-sm-2">
                                                                  <a id="waiting1" class="btn btn-warning" >
                                                                  Waiting User Response
                                                                  </a> 
                                                               </div>
                                                                  <div class="col-sm-2">
                                                                  <a id="waiting2" class="btn btn-warning" >
                                                                  Waiting Document
                                                                  </a> 
                                                               </div>-->
                                                            </div>
                                                        
                                                         @endif
                                                            
                                                          
                                                        </div>
                                                      

                                                     
                
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                         <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Seterusnya / <i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                       
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
     </div>
</div>
            <br>
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  country_v: {
                    required: true
                  },
        
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
               
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {
                        var country_v = $('input[name=country_v]:checked').val();
                        var card_v = $('input[name=card_v]:checked').val();
                        var citizen_v = $('input[name=citizen_v]:checked').val();
                        var new_ic_basic_v = $('input[name=new_ic_basic_v]:checked').val();
                        var dob_basic_v = $('input[name=dob_basic_v]:checked').val();
                        var old_ic_basic_v = $('input[name=old_ic_basic_v]:checked').val();
                        var name_basic_v = $('input[name=name_basic_v]:checked').val();
                        var name_prefed_v = $('input[name=name_prefed_v]:checked').val();
                        var title_v = $('input[name=title_v]:checked').val();
                        var gender_v = $('input[name=gender_v]:checked').val();
                        var marital_v = $('input[name=marital_v]:checked').val();
                        var dependents_v = $('input[name=dependents_v]:checked').val();
                        var race_v = $('input[name=race_v]:checked').val();
                        var relegion_v = $('input[name=relegion_v]:checked').val();
                        var education_v = $('input[name=education_v]:checked').val();
                        var category_v = $('input[name=category_v]:checked').val();
                        var mother_name_v = $('input[name=mother_name_v]:checked').val();
              
                        // Step 2
                        var address_contact_v = $('input[name=address_contact_v]:checked').val();
                        var postcode_contact_v = $('input[name=postcode_contact_v]:checked').val();
                        var city_contact_v = $('input[name=city_contact_v]:checked').val();
                        var state_contact_v = $('input[name=state_contact_v]:checked').val();
                        var ownership_v = $('input[name=ownership_v]:checked').val();
                        var houseno_contact_v = $('input[name=houseno_contact_v]:checked').val();
                        var handphone_v = $('input[name=handphone_v]:checked').val();
                        var fax_v = $('input[name=fax_v]:checked').val();
                        var officephone_v = $('input[name=officephone_v]:checked').val();
                        var emel_v = $('input[name=emel_v]:checked').val();
                        
                        // Step 3
                        var emp_name_v = $('input[name=emp_name_v]:checked').val();
                        var address_empinfo_v = $('input[name=address_empinfo_v]:checked').val();
                        var joined_v = $('input[name=joined_v]:checked').val();
                        var grade_v = $('input[name=grade_v]:checked').val();
                        var empstatus_v = $('input[name=empstatus_v]:checked').val();
                      
                        // Step 4
                        var package_v = $('input[name=package_v]:checked').val();
                        var loanammount_v = $('input[name=loanammount_v]:checked').val();
                        var maxloan_v = $('input[name=maxloan_v]:checked').val();
                        var method_v = $('input[name=method_v]:checked').val();
                        var memberno_v = $('input[name=memberno_v]:checked').val();
                        var id_tenure_v = $('input[name=id_tenure_v]:checked').val();
                        // Step 5
                        var name_spouse_v = $('input[name=name_spouse_v]:checked').val();
                        var dob_spouse_v = $('input[name=dob_spouse_v]:checked').val();
                        var new_ic_spouse_v = $('input[name=new_ic_spouse_v]:checked').val();
                        var old_ic_spouse_v = $('input[name=old_ic_spouse_v]:checked').val();
                        var emp_name_spouse_v = $('input[name=emp_name_spouse_v]:checked').val();
                        var postcode_spouse_v = $('input[name=postcode_spouse_v]:checked').val();
                        var phone_spouse_v = $('input[name=phone_spouse_v]:checked').val();
                        var city_spouse_v = $('input[name=city_spouse_v]:checked').val();
                        var spouse_handphone_v = $('input[name=spouse_handphone_v]:checked').val();
                        
                        // Step 6
                        var name_reference_v = $('input[name=name_reference_v]:checked').val();
                        var address_reference_v = $('input[name=address_reference_v]:checked').val();
                        var postcode_reference_v = $('input[name=postcode_reference_v]:checked').val();
                        var state_reference_v = $('input[name=state_reference_v]:checked').val();
                        var new_ic_reference_v = $('input[name=new_ic_reference_v]:checked').val();
                        var relationship_reference_v = $('input[name=relationship_reference_v]:checked').val();
                        var houseno_reference_v = $('input[name=houseno_reference_v]:checked').val();
                        var handphone_reference_v = $('input[name=handphone_reference_v]:checked').val();
                        var officeno_reference_v = $('input[name=officeno_reference_v]:checked').val();
                        var occupation_reference_v = $('input[name=occupation_reference_v]:checked').val();
                        
                        // Step 7
                        var income_financial = $('input[name=income_financial]:checked').val();
                        var other_income_financial = $('input[name=other_income_financial]:checked').val();
                        var spouse_income_financial = $('input[name=spouse_income_financial]:checked').val();
                        var total_income_financial = $('input[name=total_income_financial]:checked').val();
                        var expenses_financial = $('input[name=expenses_financial]:checked').val();
                        var monthly_payment_v = $('input[name=monthly_payment_v]:checked').val();
                        var house_rental_v = $('input[name=house_rental_v]:checked').val();
                        var total_expenses_v = $('input[name=total_expenses_v]:checked').val();
                        var net_income_v = $('input[name=net_income_v]:checked').val();
                        var name_reference_v = $('input[name=name_reference_v]:checked').val();
                        // Step 8
                        var document5_v = $('input[name=document5_v]:checked').val();
                        var document6_v = $('input[name=document6_v]:checked').val();
                        var document7_v = $('input[name=document7_v]:checked').val();     
                        var document9_v = $('input[name=document9_v]:checked').val();
                        
                        if (index=='1') {
                            if (country_v=='0' || card_v=='0' || citizen_v=='0' || new_ic_basic_v=='0' || dob_basic_v=='0' || old_ic_basic_v=='0'
                              || old_ic_basic_v=='0' || name_basic_v=='0' || name_prefed_v=='0' || title_v=='0'
                              || gender_v=='0' || marital_v=='0' || dependents_v=='0' || race_v=='0'
                              || relegion_v=='0' || education_v=='0' || category_v=='0' || mother_name_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
                        else if (index=='2') {
                           if (address_contact_v=='0' || postcode_contact_v=='0' || city_contact_v=='0'
                                 || state_contact_v=='0' || ownership_v=='0' || houseno_contact_v=='0'
                                 || handphone_v=='0' || fax_v=='0' || officephone_v=='0' || emel_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
                        else if (index=='3') {
                           if (emp_name_v=='0' || address_empinfo_v=='0' || joined_v=='0'
                                 || grade_v=='0' || empstatus_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
                        else if (index=='4') {
                           if (package_v=='0' || loanammount_v=='0' || maxloan_v=='0'
                                 || method_v=='0' || memberno_v=='0' || id_tenure_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
                        else if (index=='5') {
                           var marital = $('#status').val();
                           if (marital=='Berkahwin / Married') {
                               if (name_spouse_v=='0' || dob_spouse_v=='0' || new_ic_spouse_v=='0'
                                 || old_ic_spouse_v=='0' || emp_name_spouse_v=='0' || postcode_spouse_v=='0'
                                  || phone_spouse_v=='0' || city_spouse_v=='0' || spouse_handphone_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                           }
                          
                        }
                        else if (index=='6') {
                            if (name_reference_v=='0' || address_reference_v=='0' || postcode_reference_v=='0'
                                 || state_reference_v=='0' || handphone_reference_v=='0' || new_ic_reference_v=='0'
                                  || relationship_reference_v=='0' || houseno_reference_v=='0' || officeno_reference_v=='0'
                                  || occupation_reference_v=='0' ) {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }

                        else if (index=='7') {
                            if (income_financial=='0' || other_income_financial=='0' || spouse_income_financial=='0'
                                 || total_income_financial=='0' || expenses_financial=='0' || monthly_payment_v=='0'
                                  || house_rental_v=='0' || total_expenses_v=='0' || net_income_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
                        
                        else if (index=='8') {
                            if (document5_v=='0' || document6_v=='0' || document7_v=='0'
                                 || document9_v=='0') {
                                 alert('Please make sure all field are valid !');
                                 $validator.focusInvalid();
                                  return false;
                              }
                        }
               

                 
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  
                }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>
<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

 <script type="text/javascript">

$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>
<script type="text/javascript">

$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city3").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>
  <script type="text/javascript">

$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city2").val(data[k].post_office+' / '+data[k].state.state_name );
                  
                    });

                }
            });

   
});
</script>

  <script type="text/javascript">

$( "#status" ).change(function() {
    var status = $('#status').val();
     
    if(status > 1 ) {

         $("#tab99").hide();
          $("#tab98").hide();
    }
    else {

         $("#tab99").show();
         $("#tab98").show();
    }

   
});
</script>


<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'form/upload';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
            $("#document1").html("<a href='"+"{{url('/upload/file/')}}/"+data.file+"'>borang permohonan lengkap</a>");
             $("#document1a").hide();

        }
       
    }).prop('', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : '');
});
</script>

  <script type="text/javascript">

$( "#agree" ).click(function() {

    
      var id_praapplication = $('#id_praapplication_term').val();
      var _token = $('#token_term').val();
      var declaration = $('#dec_approved').val();
      var branch = $('#branch').val();
      var remark = $('#remark_verification').val();
      
      if ((remark=='') || (branch=='')) {
      //code
       alert("Please Select Branch AND Fill a Remark!");
      }
      else {
      var r = confirm("Are You Sure to Route this Application to Branch?");
      if (r == true) {
         $.ajax({

               type: "POST",
              
               url: '{{ url('/verifiedForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () {
                  
               },

               success: function () {
                
                   alert("Verification Success, Application Approved Route to Branch !");
                   location.href='{{url('/')}}/admin';
                

               },
               error: function () {
                  
               }
           });
     }
   }
});
</script>
   
<script type="text/javascript">

$( "#disagree" ).click(function() {
   var id_praapplication = $('#id_praapplication_term').val();
   var _token = $('#token_term').val();
   var declaration = $('#dec_rejected').val();
   var branch = $('#branch').val();
   var remark = $('#remark_verification').val();
   if (remark=='') {
      //code
       alert("Please fill Remark !");
   }
   else {
      var r = confirm("Are You Sure to Rejected this Application?");
      if (r == true) {
     
         $.ajax({
               type: "POST",
               url: '{{ url('/verifiedForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () { 
               },
               success: function () {
                   alert("Application Rejected !");
                   location.href='{{url('/')}}/admin';
               },
               error: function () {
               }
           });
     }
   }
});
</script>
   
<script type="text/javascript">
$( "#waiting1" ).click(function() {
   var id_praapplication = $('#id_praapplication_term').val();
   var _token = $('#token_term').val();
   var declaration = $('#dec_waiting1').val();
   var branch = $('#branch').val();
   var remark = $('#remark_verification').val();
   if (remark=='') {
      //code
       alert("Please fill Remark !");
   }
   else {
      var r = confirm("Are You Sure Want to Waiting User Response?");
      if (r == true) {
     
         $.ajax({
               type: "POST",
               url: '{{ url('/verifiedForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () { 
               },
               success: function () {
                   alert("Application Waiting for User Response !");
                   location.href='{{url('/')}}/admin';
               },
               error: function () {
               }
           });
     }
   }
});
</script>
   
<script type="text/javascript">
$( "#waiting2" ).click(function() {
   var id_praapplication = $('#id_praapplication_term').val();
   var _token = $('#token_term').val();
   var declaration = $('#dec_waiting2').val();
   var branch = $('#branch').val();
   var remark = $('#remark_verification').val();
  
   if (remark=='') {
      //code
       alert("Please fill Remark !");
   }
   else {
   
      var r = confirm("Are You Sure Want to Waiting Document?");
      if (r == true) {
  
      $.ajax({
            type: "POST",
            url: '{{ url('/verifiedForm/term/') }}',
            data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
  
            cache: false,
            beforeSend: function () {
                     
            },
            success: function () {
                alert("Application Waiting for Document !");
                location.href='{{url('/')}}/admin';
            },
            error: function () {
            }
        });
      }
   }
});
</script>

 <script type="text/javascript">

$( ".income" ).change(function() {

var income1 = $('#income1').val();

var income2 = $('#income2').val();

var income3 = $('#income3').val();

var outcome1 = $('#outcome1').val();

var outcome2 = $('#outcome2').val();

var outcome3 = $('#outcome3').val();

var outcome4 = $('#outcome4').val();


var total = parseFloat(income1) + parseFloat(income2)  + parseFloat(income3) ;
    $("#income4").val(parseFloat(total).toFixed(2));


var total2 = parseFloat(outcome2)  + parseFloat(outcome3) + parseFloat(outcome4)   ;
    $("#outcome5").val( parseFloat(total2).toFixed(2));
       ;


    $("#totalincome").val( parseFloat(total - total2).toFixed(2));

   
});
</script>
   
   
 <script type="text/javascript">

$( ".fn" ).change(function() {
var fn_monthly_basic = $('#fn_monthly_basic').val();
var fn_govt_service = $('#fn_govt_service').val();
var fn_financial_incentive = $('#fn_financial_incentive').val();
var fn_fixed_allowance = $('#fn_fixed_allowance').val();
var fn_housing_allowance = $('#fn_housing_allowance').val();
var fn_cost_of = $('#fn_cost_of').val();
var fn_special_allowance = $('#fn_special_allowance').val();

var fn_gross_income = parseFloat(fn_monthly_basic) + parseFloat(fn_govt_service)  + parseFloat(fn_financial_incentive)
      + parseFloat(fn_fixed_allowance) + parseFloat(fn_housing_allowance) + parseFloat(fn_cost_of) + parseFloat(fn_special_allowance)  ;
    $("#fn_gross_income").val(parseFloat(fn_gross_income).toFixed(2));
    
    
// Non fixed Variable
var fn_overtime_allowance = $('#fn_overtime_allowance').val();
var fn_travel = $('#fn_travel').val();
var fn_commission = $('#fn_commission').val();
var fn_business_income = $('#fn_business_income').val();
var fn_yearly_devidend = $('#fn_yearly_devidend').val();
var fn_food_allowance = $('#fn_food_allowance').val();
var fn_yearly_contractual = $('#fn_yearly_contractual').val();
var fn_housing_building = $('#fn_housing_building').val();
var fn_non_fixed = $('#fn_non_fixed').val();
var fn_share_income = $('#fn_share_income').val();
var fn_gross_income = $('#fn_gross_income').val();

  
var fn_total_income = parseFloat(fn_overtime_allowance) + parseFloat(fn_travel)  + parseFloat(fn_commission)
      + parseFloat(fn_business_income) + parseFloat(fn_yearly_devidend) + parseFloat(fn_food_allowance) + parseFloat(fn_yearly_contractual)
      + parseFloat(fn_housing_building) + parseFloat(fn_non_fixed) + parseFloat(fn_share_income) + parseFloat(fn_gross_income) ;
    $("#fn_total_income").val(parseFloat(fn_total_income).toFixed(2));

// Payslip Variable
var fn_knwsp = $('#fn_knwsp').val();
var fn_income_tax = $('#fn_income_tax').val();
var fn_cp38 = $('#fn_cp38').val();
var fn_house_financing = $('#fn_house_financing').val();
var fn_others = $('#fn_others').val();
var fn_lembaga_tabung = $('#fn_lembaga_tabung').val();
var fn_insurance = $('#fn_insurance').val();
var fn_perkeso = $('#fn_perkeso').val();
var fn_zakat = $('#fn_zakat').val();
var fn_bpa = $('#fn_bpa').val();
var fn_asb = $('#fn_asb').val();

var fn_total_payslip = parseFloat(fn_knwsp) + parseFloat(fn_income_tax) + parseFloat(fn_cp38)  + parseFloat(fn_house_financing)
      + parseFloat(fn_others) + parseFloat(fn_lembaga_tabung) + parseFloat(fn_insurance) + parseFloat(fn_perkeso)
      + parseFloat(fn_zakat) + parseFloat(fn_bpa) + parseFloat(fn_asb);
    $("#fn_total_payslip").val(parseFloat(fn_total_payslip).toFixed(2));

// non Pay Slip Variable
var fn_cost_of2 = $('#fn_cost_of2').val();
var fn_other_non = $('#fn_other_non').val();

var fn_total_non = parseFloat(fn_cost_of2) + parseFloat(fn_other_non);
    $("#fn_total_non").val(parseFloat(fn_total_non).toFixed(2));
    
// Total Deductions
var fn_total_payslip = $('#fn_total_payslip').val();
var fn_total_non = $('#fn_total_non').val();

var fn_total_deductions = parseFloat(fn_total_payslip) + parseFloat(fn_total_non);
    $("#fn_total_deductions").val(parseFloat(fn_total_deductions).toFixed(2));
    
// Total net income
var fn_total_income = $('#fn_total_income').val();
var fn_total_deductions = $('#fn_total_deductions').val();

var fn_total_net = parseFloat(fn_total_income) - parseFloat(fn_total_deductions);
    $("#fn_total_net").val(parseFloat(fn_total_net).toFixed(2));
    
});
</script>
   
<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>
   <script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("select option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });

</script>

@if($data->marital != 'Berkahwin / Married')
  <script type="text/javascript">

  $(document).ready(function() {
    

        $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").show();
   
});
  </script>
  @else 
  <script type="text/javascript">

  $(document).ready(function() {
    

    $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").hide();
   
});
  </script>

@endif 

  <script type="text/javascript">

$( "#status" ).change(function() {
    var status = $('#status').val();
     
    if(status  != 'Berkahwin / Married' ) {
       
        $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").show();
    }
    else {
   
      $("#tab5 :input").attr("disabled", true);
            $("#statuskawin").hide();
    }

   
});
</script>
<script>
   $('#tgl').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });
</script>
