
<p>New status changed by {{$branchname}} Branch</p>
<p>Detail Customer :</p>
<p>IC Number :  {{$cus_ic}}</p>
<p>Name  :  {{$cus_name}} </p>
<p>Application Status Changed to <b>{{$s_string}} </b> </p>
<p>Remark : {{$remark_bank}} </p>
@if($status=='1')
<p>Approved Loan : RM {{$aproved_loan}}</p>
<p>Approved Tenure : {{$aproved_tenure}} Year(s)</p>
@elseif($status=='2')
<p>Rejected Reason : {{$reason}}</p>
@endif

<p></p>
<p>Thank you</p>

<p>MBSB Personal Financing-i</p>