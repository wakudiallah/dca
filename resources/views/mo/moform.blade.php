<?php
    //initilize the page
    require_once("asset/inc/init.php");
    //require UI configuration (nav, ribbon, etc.)
    require_once("asset/inc/config.ui.php");
    $page_title = "New Applicant";
    $page_css[] = "your_style.css";
    include("asset/inc/header.php");
?>

<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }
    table.border {
        border-collapse: separate;
        border-spacing: 10px; /* cellspacing */
        *border-collapse: expression('separate', cellSpacing = '10px');
    }
    td.border {
        padding: 10px; /* cellpadding */
    }
    .ui-helper-hidden-accessible { display:none; }
    .ui-helper-hidden-accessible { position: absolute; left:-999em; }
    .select2-hidden-accessible {
    display: none !important; 
    visibility: hidden !important;
}
</style>
<link
 rel="stylesheet"
 href="http://code.jquery.com/ui/1.9.0/themes/smoothness/jquery-ui.css" />
<?php
    include("asset/inc/nav.php");
    //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
    //$breadcrumbs["New Crumb"] => "http://url.com"
    $breadcrumbs["Home"] = "";
    include("asset/inc/ribbon.php");
?>

<div id="main" role="main">
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                            <h2>New Applicant - {{$data->first()->name}}</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                                <div class="widget-body">
                                    <div class="row">
                                        <form id="wizard-1" novalidate="novalidate">
                                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                                <div class="form-bootstrapWizard">
                                                    <ul class="bootstrapWizard form-wizard">
                                                        <li data-target="#step1">
                                                            <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Personal Particulars (Main Applicant)</span></a>
                                                        </li>
                                                        <li data-target="#step2" class="not-active">
                                                            <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Employment Details (Main Applicant)</span></a>
                                                        </li>
                                                        <li data-target="#step3" class="not-active">
                                                            <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Particular of Spouse & Emergency Contact**</span></a>
                                                        </li>
                                                        <li data-target="#step4" class="not-active">
                                                            <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Income Information (Main Applicant)</span></a>
                                                        </li>
                                                        <li data-target="#step5" class="not-active">
                                                            <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title">Your Commitments with Other Credit Providers (Non-Banks Only)**</span></a>
                                                        </li>
                                                        <li data-target="#step6" class="not-active">
                                                            <a href="#tab6" data-toggle="tab"> <span class="step">6</span> <span class="title">Financing Details</span></a>
                                                        </li>
                                                        <li data-target="#step7" class="not-active">
                                                            <a href="#tab7" data-toggle="tab"> <span class="step">7</span> <span class="title">Applicant For Personal Financing-i Facility</span></a>
                                                        </li>
                                                        <li data-target="#step8" class="not-active">
                                                            <a href="#tab8" data-toggle="tab"> <span class="step">8</span> <span class="title">Upload Document</span> </a>
                                                        </li>
                                                        <li data-target="#step9" class="not-active">
                                                            <a href="#tab9" data-toggle="tab"> <span class="step">9</span> <span class="title">Declaration/Disclosure By Applicant</span></a>
                                                        </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <ul class="pager wizard no-margin">
                                                                <!--<li class="previous first ">
                                                                <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a></li>-->
                                                                <li class="next">
                                                                    <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"> Seterusnya / <i> Next </i> </a>
                                                                </li>
                                                                <li class="previous">
                                                                    <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab1">
                                                        @foreach($data as $data)
                                                        <br>
                                                        <h3><strong>Step 1 </strong> - Personal Particulars (Main Applicant)/ <i>Butir-Butir Peribadi (Pemohon Utama)</i> </h3>
                                                        <div class="col-md-6"><br>
                                                            <div class="form-group">
                                                                <label>1. Salutation / <i>Gelaran</i> :</label>
                                                                {{csrf_field()}}
                                                                <input type="hidden" name="id_praapplication" value="{{$pra->id}}"/>
                                                                <select class=" select2" name="title" id="salutation" required="required">
                                                                    <option selected=""></option>
                                                                     @foreach ($title as $title)
                                                                        @if($data->title !=NULL)
                                                                            <?php 
                                                                                if($data->title==$title->id) {
                                                                                  $selected = "selected";
                                                                                }
                                                                                else {
                                                                                   $selected = "";
                                                                                } 
                                                                            ?>
                                                                                <option {{$selected}} value="{{$title->id}}">{{$title->title_desc}}</option>
                                                                        @else
                                                                            <option value="{{$title->id}}">{{$title->title_desc}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                                <input type="text" placeholder="Specify / Sila Nyatakan" name="title_others" id="salutation_others" class="form-control" required @if($data->title=="20") value="{{$data->title_others}}" @endif>
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>2. Full Name (As per ID Document) / <i>Nama Penuh (Seperti dalam  Dokumen Pengenalan Diri)</i> :</label>
                                                                <input type="text" minlength="2" maxlength="200" id="fullname" name="name" value="{{$data->name}}" class="form-control" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return fullname(event);" >
                                                            </div>
                                                            @if($data->id_type=='IN')
                                                            <div class="form-group">
                                                                <label>3. My Kad No. / <i>No MyKad</i> :</label>
                                                                 <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="new_ic" name="new_ic" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif  value="{{$data->new_ic}}" class="form-control">
                                                            </div>
                                                            @else
                                                             <div class="form-group">
                                                                <label>3. My Kad No. / <i>No MyKad</i> :</label>
                                                                 <input  type="text" id="new_ics" onkeyup="this.value = this.value.toUpperCase()" name="new_ic"  placeholder="Other"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="7" value="{{$data->new_ic}}" class="form-control">
                                                            </div>
                                                            @endif
                                                            <div class="form-group">
                                                                <label>4. Date Of Birth / <i>Tarikh Lahir</i> :  </label>
                                                                <?php 
                                                                    $lahir =  date('d/m/Y', strtotime($data->dob));
                                                                ?>
                                                                <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value="{{ $lahir  }}"  class="form-control startdate" id="dob"  />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>5. Country of Birth/ <i>Tempat Lahir</i> :</label>
                                                                <select name="country_dob" id="country_dob" class=" select2">
                                                                    <option selected=""></option>
                                                                     @foreach ($country as $country)
                                                                @if($data->country_dob !=NULL)
                                                                    <?php 
                                                                        if($data->country_dob==$country->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$country->id}}">{{$country->country_desc}}</option>
                                                                @else
                                                                    <option value="{{$country->id}}" {{ ( $country->id == '30') ? 'selected' : '' }} >{{$country->country_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                                </select><br><br>
                                                        State / <i>Negeri</i>
                                                        <select name="state_dob" id="state_dob" class="select2">
                                                              <option disabled="" selected="">SELECT</option> 
                                                            @foreach ($state as $state)
                                                                @if($data->state_dob !=NULL)
                                                                    <?php 
                                                                        if($data->state_dob==$state->state_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$state->state_code}}">{{$state->state_name}}</option>
                                                                @else
                                                                    <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                            </div>
                                                             @if($data->id_type=='4')
                                                            <div class="form-group">
                                                                <label>6. Police  / Military No./<i>No. Polis / Tentera </i> :</label>
                                                                <input  type="text" id="policy_number" onkeyup="this.value = this.value.toUpperCase()" name="policy_number"  placeholder="Other"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="12">
                                                            </div>
                                                            @else
                                                              <input  type="hidden" id="policy_number" onkeyup="this.value = this.value.toUpperCase()" name="policy_number"  placeholder="Other"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="12">
                                                            @endif
                                                            <div class="form-group">
                                                                <label>7. Gender / <i>Jantina</i> :</label>
                                                                <select class=" select2" name="gender" id="gender" required="requeired">

                                                                    @if(!empty($data->gender))
                                                                        @if($data->gender=='M')
                                                                        <option value="M">Male / <i>Lelaki </i></option>
                                                                        @elseif($data->gender=='F')
                                                                        <option value="F">Female / <i>Perempuan </i></option>
                                                                        @endif
                                                                    @endif
                                                                    <option value="M">Male / <i>Lelaki </i></option>
                                                                    <option value="F">Female / <i>Perempuan </i></option>
                                                                </select>
                                                            </div>

                                                    <div class="form-group">

                                                        <label>8. Home Address / <i>Alamat Rumah</i> </label>

                                                        <br>
                                                        <?php
                                                         $address1d= strtoupper($data->address);
                                                         $address2d= strtoupper($data->address2);
                                                         $address3d= strtoupper($data->address3);
                                                        ?>
                                                        <input type="text" class="form-control"  name="address" required="requeired" value="{{$address1d}}" placeholder="Address Line 1" minlength="2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()" id="address"> 

                                                         <br>

                                                            <input type="text" class="form-control"  name="address2"  placeholder="Address Line 2" value="{{$address2d}}" maxlength="40" onkeyup="this.value = this.value.toUpperCase()" id="address2">



                                                         <br>

                                                            <input type="text" class="form-control"  name="address3" id="address3" placeholder="Address Line 3" value="{{$address3d}}" maxlength="40" onkeyup="this.value = this.value.toUpperCase()">

                                                         <label> Postcode/ <i>Poskod :</i></label>

                                                            <br><input type="tel" minlength="4" maxlength="5" name="postcode" id="postcode" class="form-control" onkeypress="return isNumberKey(event)" value="{{$data->postcode}}" />

                                                        <label>State / <i>Negeri :</i></label><br>

                                                            <input type="text"    minlength="2" maxlength="50"  id="state" name="state" class="form-control" value="{{$data->state}}">

                                                            <input type="hidden"    maxlength="50"  id="state_code" name="state_code" class="form-control" value="{{$data->state_code}}">

                                                       

                                                    </div>



                                                    <div class="form-group">

                                                        <label>9. Ownership Status / <i>Taraf Pemilikan</i>:</label>

                                                            <select class="select2" name="ownership" id="ownership" required="requeired">

                                                               @foreach ($own as $own)
                                                                @if($data->ownership !=NULL)
                                                                    <?php 
                                                                        if($data->ownership==$own->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$own->id}}">{{$own->ownership}}</option>
                                                                @else
                                                                    <option value="{{$own->id}}">{{$title->ownership}}</option>
                                                                @endif
                                                            @endforeach

                                                            </select>

                                                    </div> 

                                                </div>

                                                <div class="col-md-6"><br>
                                                    @if($data->address_correspondence=='1')
                                                      <input type="checkbox" name="checker" checked="" value="1" id="checker">Click Yes if Home address same with home address
                                                      @elseif($data->address_correspondence=='2')
                                                      <input type="checkbox" name="checker" id="checker" value="2">Click Yes if Correspondence address same with home address
                                                    @endif
                                                    @if($data->address_correspondence==NULL)
                                                    <input type="checkbox" name="checker" id="checker" value="1">Click Yes if correspondence address same with home address
                                                    @endif
                                                    <div class="form-group">

                                                        <label>10. Correspondence Address/<i> Alamat Surat Menyurat </i>: </label>
                                                             <?php
                                                         $address1c= strtoupper($data->corres_address1);
                                                         $address2c= strtoupper($data->corres_address2);
                                                         $address3c= strtoupper($data->corres_address3);
                                                        ?>
                                                            <input class="form-control" minlength="2" maxlength="40"  name="corres_address1" placeholder="Corresspondence Address 1" value="{{$address1c}}" onkeyup="this.value = this.value.toUpperCase()" id="corres_address1" required=""><br>

                                                            <input class="form-control" value="{{$address2c}}"  name="corres_address2" placeholder="Correspondence Address 2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()" id="corres_address2"><br>

                                                            <input class="form-control" value="{{$address3c}}"  name="corres_address3" placeholder="Correspondence Address 3" maxlength="40" onkeyup="this.value = this.value.toUpperCase()" id="corres_address3">

                                                        <label>Postcode / <i>Poskod :</i></label>

                                                            <br><input type="tel" id="postcode2" minlength="4" maxlength="5"   name="corres_postcode" class="form-control" onkeypress="return isNumberKey(event)" value="{{$data->corres_postcode}}" required="" />

                                                        <label>State / <i>Negeri :</i></label><br>

                                                            <input type="text"    maxlength="50"  id="state2" name="corres_state" class="form-control"  value="{{$data->corres_state}}" required="">

                                                            <input type="hidden"    maxlength="50"  id="state_code2" name="corres_state2" class="form-control"  value="{{$data->corres_state1}}">

                                                        <label>Home Telephone/ <i>Telefon Rumah :</i></label><br>

                                                            <input type="tel" id="homephone" minlength="8" maxlength="13"  minlength="8" name="corres_homephone" class="form-control" onkeypress="return isNumberKey(event)" value="{{$data->corres_homephone}}"/>

                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>

                                                              @if(empty($data->corres_email))

                                                                <?php $corres_mobilephone = $pra->phone; ?>

                                                            @else

                                                                 <?php $corres_mobilephone = $data->corres_mobilephone; ?>

                                                            @endif

                                                            <input type="tel" id="mobilephone" maxlength="13" minlength="9"  name="corres_mobilephone" class="form-control" onkeypress="return isNumberKey(event)" value="{{$corres_mobilephone}}"/>

                                                        <label>Email Address / <i>Alamat E-mel :</i></label><br>



                                                            @if(empty($data->corres_email))

                                                                <?php $corres_email = $pra->acus_email; ?>

                                                            @else

                                                                 <?php $corres_email = $data->corres_email; ?>

                                                            @endif

                                                            <input type="text"  minlength="2"  maxlength="50"  id="email" name="corres_email" class="form-control" value="{{$corres_email}}" required onkeyup="this.value = this.value.toLowerCase()" >

                                                       

                                                    </div>

                                                     <div class="form-group">
                                                            <label>11. Resident Status / <i>Status Pemastautin </i> :</label>
                                                                <select class=" form-control" id="resident" name="resident" required>
                                                                    @foreach ($resident as $resident)
                                                                @if($data->resident !=NULL)
                                                                    <?php 
                                                                        if($data->resident==$resident->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$resident->id}}">{{$resident->resident}}</option>
                                                                @else
                                                                    <option value="{{$resident->id}}">{{$resident->resident}}</option>
                                                                @endif
                                                            @endforeach
                                                                </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>12. Nationality / <i>Kewarganegaraan</i> :</label>
                                                                <select class="select2" name="nationality" id="nationality"required>
                                                                    @foreach ($nationality as $nationality)
                                                                @if($data->nationality !=NULL)
                                                                    <?php 
                                                                        if($data->nationality==$nationality->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$nationality->id}}">{{$nationality->nationality}}</option>
                                                                @else
                                                                    <option value="{{$nationality->id}}">{{$nationality->nationality}}</option>
                                                                @endif
                                                            @endforeach
                                                                </select>
                                                        </div>
                                                     

                                                    <div class="form-group">
                                                        <div id="country_origin">
                                                            <label>Country Origin / <i>Negara Asal</i> :</label>
                                                                <select name="country_origin" id="country_origin" class="select2" required="required">
                                                                    <option disabled="" selected="">SELECT</option> 
                                                                       @foreach ($origin as $origin)
                                                                    @if($data->origin !=NULL)
                                                                        <?php 
                                                                            if($data->origin==$origin->id) {
                                                                              $selected = "selected";
                                                                            }
                                                                            else {
                                                                               $selected = "";
                                                                            } 
                                                                        ?>
                                                                            <option {{$selected}} value="{{$origin->id}}">{{$origin->country_desc}}</option>
                                                                    @else
                                                                        <option value="{{$origin->id}}">{{$origin->country_desc}}</option>
                                                                    @endif
                                                                @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                    <div class="form-group">

                                                        <label>13. Race / <i>Bangsa </i> :</label>

                                                            <select class=" form-control" id="race" name="race" required="requeired">


 @foreach ($race as $race)
                                                                    @if($data->race !=NULL)
                                                                        <?php 
                                                                            if($data->race==$race->id) {
                                                                              $selected = "selected";
                                                                            }
                                                                            else {
                                                                               $selected = "";
                                                                            } 
                                                                        ?>
                                                                            <option {{$selected}} value="{{$race->id}}">{{$race->race}}</option>
                                                                    @else
                                                                        <option value="{{$race->id}}">{{$race->race}}</option>
                                                                    @endif
                                                                @endforeach

                                                                </select>



                                                                 <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="race_others" id="race_others" required @if($data->race=="11") value="{{$data->race_others}}" @endif >

                                        

                                                        </div>



                                                        <div class="form-group">

                                                            <label>14. Bumiputera Status / <i>Status Bumiputera </i> :</label>

                                                                <select class=" form-control" id="bumiputera" name="bumiputera" required>

                                                                    @if(!empty($data->bumiputera))

                                                                        @if($data->bumiputera=='Y')

                                                                            <option value="Y">Yes / <i>Ya</i></option>

                                                                        @elseif($data->bumiputera=='N')

                                                                            <option value="N">No / <i>Tidak</i></option>

                                                                        @endif

                                                                            <option value="Y">Yes / <i>Ya</i></option>

                                                                            <option value="N">No / <i>Tidak</i></option>

                                                                    @else

                                                                        <option selected=""></option>

                                                                        <option value="Y">Yes / <i>Ya</i></option>

                                                                        <option value="N">No / <i>Tidak</i></option>

                                                                    @endif

                                                                </select>

                                                        </div>





                                                        <div class="form-group">

                                                            <label>15. Religion / <i>Agama</i> :</label>

                                                                <select class="select2" id="religion" name="religion" required>

                                                                   @foreach ($religion as $religion)
                                                                @if($data->religion !=NULL)
                                                                    <?php 
                                                                        if($data->religion==$religion->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$religion->id}}">{{$religion->religion}}</option>
                                                                @else
                                                                    <option value="{{$religion->id}}">{{$religion->religion}}</option>
                                                                @endif
                                                            @endforeach

                                                                </select>



                                                                  <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="religion_others" id="religion_others" required @if($data->religion=="6") value="{{$data->religion_others}}" @endif >

                                                          

                                                        </div>



                                                        <div class="form-group">

                                                            <label>16. Marital Status / <i>Taraf Perkahwinan </i> :</label>

                                                                <select class="select2" name="marital" id="marital" required="required">
                                                                    @foreach ($marital as $marital)
                                                                @if($data->marital !=NULL)
                                                                    <?php 
                                                                        if($data->marital==$marital->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$marital->id}}">{{$marital->marital}}</option>
                                                                @else
                                                                    <option value="{{$marital->id}}">{{$marital->marital}}</option>
                                                                @endif
                                                            @endforeach
                                                                </select>

                                                          

                                                        </div>



                                                        <div class="form-group">

                                                            <label>17. No. of Dependants / <i>Jumlah Tanggungan</i></label>

                                                                <input type="tel" class="form-control" maxlength="2" value="{{$data->dependents}}" name="dependents" onkeypress="return isNumberKey(event)" required="requeired">

                                                        </div>

                                                         <div class="form-group">

                                                            <label>18. Education Level / <i>Taraf Pendidikan</i> :</label>

                                                                <select class="form-control" id="education" name="education" required="requeired">

                                                                 @foreach ($education as $education)
                                                                @if($data->education !=NULL)
                                                                    <?php 
                                                                        if($data->education==$education->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$education->id}}">{{$education->education}}</option>
                                                                @else
                                                                    <option value="{{$education->id}}">{{$education->education}}</option>
                                                                @endif
                                                            @endforeach
                                                                 </select>

                                                          

                                                        </div>

                                                    </div>

                                                  

                                                </div>

                                                @endforeach



                                                    <div class="tab-pane" id="tab2">

                                                  

                                                    <br>

                                                    <h3><strong>Step 2</strong> - Employment Details (Main Applicant)/ <i>Butir-Butir Pekerjaan (Pemohon Utama)</i> </h3>

                                                    {{csrf_field()}}

                                                     <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}"/>

                                                            @foreach($empinfo as $empinfo)

                                                    <div class="col-md-6">

                                                                <br>

                                                    <div class="form-group">

                                                        <label>1. Employment Type/ <i>Jenis Pekerjaan</i> :</label>

                                                            <select class=" form-control" id="emptype" name="emptype" required="requeired">

                                                           @foreach ($emp_type as $emp_type)
                                                                @if($empinfo->emptype !=NULL)
                                                                    <?php 
                                                                        if($empinfo->emptype==$emp_type->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$emp_type->id}}">{{$emp_type->employment_type}}</option>
                                                                @else
                                                                    <option value="{{$emp_type->id}}">{{$emp_type->employment_type}}</option>
                                                                @endif
                                                            @endforeach
                                                                </select>

                                                        <input type="text" minlength="2" maxlength="100" class=" form-control" placeholder="Sila Nyatakan" id="emptype_others" name="emptype_others" required @if($empinfo->emptype=="5") value="{{$empinfo->emptype_others}}"  @endif>

                                                       

                                                    </div>

                                                    <div class="form-group">

                                                    <label>2. Employment Status / <i>Status Pekerjaan </i> :</label>

                                                        <select class=" form-control"  name="empstatus" id="empstatus" required="requeired">

                                                            <option selected=""></option>
                                                             @foreach ($empstatus as $empstatus)
                                                                @if($empinfo->empstatus !=NULL)
                                                                    <?php 
                                                                        if($empinfo->empstatus==$empstatus->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$empstatus->id}}">{{$empstatus->bnmcode}}</option>
                                                                @else
                                                                    <option value="{{$empstatus->id}}">{{$empstatus->bnmcode}}</option>
                                                                @endif
                                                            @endforeach

                                                        </select>

                                                        

                                                    </div>

                                                    <div class="form-group">

                                                    <label>3. Occupation / <i> Pekerjaan </i> :</label>

                                                      



                                                         <select name="occupation" id="occupation"class=" select2" required="">

                                                             <option selected=""></option>
   @foreach ($occupation as $occupation)
                                                                @if($empinfo->occupation !=NULL)
                                                                    <?php 
                                                                        if($empinfo->occupation==$occupation->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$occupation->id}}">{{$occupation->occupation_desc}}</option>
                                                                @else
                                                                    <option value="{{$occupation->id}}">{{$occupation->occupation_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>

                                                        </select>

                                                       

                                                        

                                                    </div>

                                                    

                                                    <div class="form-group">

                                                        <label>4. Name of Employer/Business / <i>Nama Majikan/Perniagaan </i> :</label>

                                                            @if(!empty($empinfo->empname))

                                                                <input type="text" name="empname" minlength="2" maxlength="60"  value="{{$empinfo->empname}}" class="form-control" required="requeired">

                                                            @else

                                                                <input type="text" name="empname" minlength="2" maxlength="60"  

                                                                value="{{$pra->employer->name}}" class="form-control" required="requeired">  

                                                            @endif

                                                       

                                                    </div>



                                                    <div class="form-group">

                                                        <label>5. Department Name / <i>Nama Jabatan</i> :</label>

                                                            <input type="text" minlength="1" maxlength="50" value="{{$empinfo->dept_name}}" id="dept_name" name="dept_name" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="">

                                                     

                                                    </div>



                                                    <div class="form-group">

                                                        <label>6. Division & Unit / <i>Bahagian & Unit</i> :</label>

                                                            <input type="text" minlength="1" maxlength="50" value="{{$empinfo->division}}" id="division" name="division" class="form-control" onkeyup="this.value = this.value.toUpperCase()" required="">

                                                       

                                                    </div>

                                                   

                                                    <div class="form-group">

                                                        <label>7. Address of Employer/Business / <i>Alamat Majikan/Perniagaan</i> :</label>
                                                            <?php
                                                                $address1e= strtoupper($empinfo->address);
                                                                $address2e= strtoupper($empinfo->address2);
                                                                $address3e= strtoupper($empinfo->address2);
                                                            ?>
                                                            <input type="text" class="form-control" name="address" value="{{$address1e}}"  placeholder="Address Line 1" required="" minlength="2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()"><br>
                                                            <input type="text" class="form-control" name="address2" value="{{$address2e}}" placeholder="Address Line 2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()"><br>
                                                            <input type="text" class="form-control" name="address3" value="{{$address3e}}" placeholder="Address Line 3" maxlength="40" onkeyup="this.value = this.value.toUpperCase()"><br>



                                                        <label> Postcode/ <i>Poskod :</i></label>

                                                                <br><input  required="" type="tel"  value="{{$empinfo->postcode}}" minlength="4" maxlength="5" name="postcode" id="postcode3" class="form-control" onkeypress="return isNumberKey(event)" />

                                                            <label>State / <i>Negeri :</i></label><br>

                                                                <input type="text"   value="{{$empinfo->state}}"  minlength="2" maxlength="50"  id="state3" name="state" class="form-control">



                                                                 <input type="hidden"   value="{{$empinfo->state_code}}"   maxlength="50"  id="state_code3" name="state_code3" class="form-control">

                                                           

                                                        </div> 

                                                    </div>

                                                     <div class="col-md-6">

                                                    <br>

                                                    <div class="form-group">

                                                        <label>8. Nature of Business / <i>Jenis Perniagaan</i> :</label>

                                                            <!--<input type="text"  maxlength="50" value="{{$empinfo->nature_business}}" id="nature_business" name="nature_business" class="form-control"/>-->
                                                            <select name="nature_business" id="nature_business" class="select2" required="">
                                                             <option selected=""></option>
                                                             @foreach ($nature_business as $nature_business)
                                                                @if($empinfo->nature_business !=NULL)
                                                                    <?php 
                                                                        if($empinfo->nature_business==$nature_business->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$nature_business->id}}">{{$nature_business->occupation_desc}}</option>
                                                                @else
                                                                    <option value="{{$nature_business->id}}">{{$nature_business->occupation_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                      

                                                    </div>



                                                    <div class="form-group">

                                                        <label>9. Start Date of Work / <i>Tarikh Mula Bekerja</i> :</label>

                                                            <?php 

                                                                $joined =  date('d/m/Y', strtotime($empinfo->joined));

                                                                 $today=date('d/m/Y');

                                                            ?>

                                                            @if(empty($empinfo->joined))

                                                            <input type="text"  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  class="form-control startdate" name="joined" value="" required="" />
                                                            @elseif($joined=='01/01/1970')

                                                            <input type="text"  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  class="form-control startdate" name="joined" value="" required>
                                                            @else

                                                            <input type="text"  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  class="form-control startdate" name="joined" value="{{$joined}}" required="" />

                                                            @endif

                                                       

                                                    </div>



                                                    <div class="form-group">

                                                        <label>10. Position / <i>Jawatan</i> :</label>

                                                            

                                                            <select name="position" id="position" class=" form-control" required="">

                                                             <option selected=""></option>

                                                           @foreach ($position as $position)
                                                                @if($empinfo->position !=NULL)
                                                                    <?php 
                                                                        if($empinfo->position==$position->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$position->id}}">{{$position->position_desc}}</option>
                                                                @else
                                                                    <option value="{{$position->id}}">{{$position->position_desc}}</option>
                                                                @endif
                                                            @endforeach

                                                        </select>

                                                      

                                                    </div>



                                                    <div class="form-group">

                                                        <label>11. Office Telephone / <i>Telefon Pejabat :</i></label><br>

                                                            <input type="tel" required="" id="office_phone" minlength="8" maxlength="13"   name="office_phone" class="form-control" onkeypress="return isNumberKey(event)" value="{{$empinfo->office_phone}}"/>

                                                    </div>

                                                    <div class="form-group">



                                                        <label>11. Office Fax / <i>Faks Pejabat :</i></label><br>

                                                            <input type="tel" id="office_fax" value="{{$empinfo->office_fax}}" minlength="8" maxlength="13"   name="office_fax" class="form-control" onkeypress="return isNumberKey(event)"/>

                                                      

                                                    </div>
                                                    


                                                </div>

                                                 

                                                    </div>

                                                    @endforeach 



                                                <div class="tab-pane" id="tab3">

                                                    <br>

                                                    <h3><strong>Step 3</strong> - Particular of Spouse & Emergency Contact/ <i> Maklumat Suami-Isteri & Rujukan Kecemasan**</i></h3> 

                                                    <input type="hidden" name="id_praapplication" value="{{$pra->id}}">

                                                    {{csrf_field()}}

                                                    

                                                    <div class="col-md-6"  id="tab99">

                                                        <br>

                                                        <h4><strong>PARTICULAR OF SPOUSE / <i>MAKLUMAT SUAMI-ISTERI</i></strong></h4>

                                                    <div id="spouse-group">

                                                    <div class="form-group" id="spouse_name" >

                                                        @foreach($spouse as $spouse)
                                                        <?php
                                                         $spousen= strtoupper($spouse->name);
                                                        ?>
                                                        <label>1. Full Name   / <i>  Nama Penuh   :  </i>  </label>

                                                            <input type="text" maxlength="100" class="form-control couple" value="{{$spousen}}" name="name" id="spousename1" required="required" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return spousename1(event);">

                                                      

                                                    </div>



                                                    <div class="form-group">

                                                        <label>2. Home/Office Telephone / <i>Telefon Rumah/Pejabat :</i></label><br>

                                                            <input type="tel" id="home_phone" maxlength="13"   name="homephone" class="form-control couple" value="{{$spouse->homephone}}" onkeypress="return isNumberKey(event)"/>

                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>

                                                            <input type="tel" id="handphone" maxlength="13"   name="mobilephone" class="form-control couple" value="{{$spouse->mobilephone}}" onkeypress="return isNumberKey(event)"/>

                                                       

                                                    </div>



                                                    <div class="form-group" id="spouse_occupation">

                                                        <label>3. Employment Type/<i>Jenis Pekerjaan </i> :</label>

                                                            <select class="select2 couple"  name="emptype" id="spouse_emptype" required="required">



                                                                     @foreach ($emp_type_sp as $emp_type2)
                                                                @if($spouse->emptype !=NULL)
                                                                    <?php 
                                                                        if($spouse->emptype==$emp_type2->code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$emp_type2->code}}">{{$emp_type2->employment_type}}</option>
                                                                @else
                                                                    <option value="{{$emp_type2->code}}">{{$emp_type2->employment_type}}</option>
                                                                @endif
                                                                @endforeach
                                                             </select>
                                                           <input type="text" class="form-control" placeholder="Sila Nyatakan" id="spouse_emptype_others" name="emptype_others" required @if($spouse->emptype=="5") value="{{$spouse->emptype_others}}"  @endif >
                                                           <input type="text" class="form-control" id="spouse_emptype_kosong">

                                                    </div>

                                                    </div>

                                                           @endforeach



                                                    <?php $sumref=1; ?>

                              

                                                    <br>

                                                    <h4><strong>EMERGENCY CONTACT / RUJUKAN KECEMASAN </strong></h4>



                                                    ** (Family Members/Relatives not staying with you)/ (Ahli Keluarga/Saudara terdekat yang tidak tinggal bersama anda)<br> <br>



                                                    <b>Contact 1 / <i> Rujukan 1 </i></b>

                                                    <div class="form-group">
                                                         <?php
                                                         $ref1= strtoupper($reference->name1);
                                                         $ref2= strtoupper($reference->name2);
                                                        ?>
                                                        <label>1. Full Name / <i> Nama Penuh </i> </label>

                                                            <input type="text" minlength="2" maxlength="100" id="name1" name="name1" value="{{$ref1}}" class="form-control" required="required" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return name1(event);">

                                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                                     <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">

                                                     

                                                    </div>



                                                    <div class="form-group">

                                                        <label>2. Home Telephone / <i>Telefon Rumah :</i></label><br>

                                                            <input type="tel" id="home_phone1"minlength="8"  maxlength="13"   name="home_phone1" class="form-control" value="{{$reference->home_phone1}}" onkeypress="return isNumberKey(event)"/>



                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>

                                                            <input type="tel" id="mobilephone1"minlength="8"  maxlength="13"   name="mobilephone1" class="form-control" value="{{$reference->mobilephone1}}" onkeypress="return isNumberKey(event)"/ required="">

                                                       

                                                    </div>



                                                    <div class="form-group">

                                                        <label>3. Relationship / <i> Hubungan</i> </label>

                                                       

                                                              <select name="relationship1" id="relationship1" class=" form-control" required="">

                                                             <option selected=""></option>

                                                          @foreach ($relationship as $relationship)
                                                                @if($reference->relationship1 !=NULL)
                                                                    <?php 
                                                                        if($reference->relationship1==$relationship->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$relationship->id}}">{{$relationship->relationship_desc}}</option>
                                                                @else
                                                                    <option value="{{$relationship->id}}">{{$relationship->relationship_desc}}</option>
                                                                @endif
                                                            @endforeach

                                                        </select>

                                                     

                                                    </div>

                                              

                                    </div>



                                                    <div class="col-md-6">

                                                        <br><br>

                                                          <b>Contact 2 / <i> Rujukan 2 </i></b>

                                                    <div class="form-group">

                                                        <label>1. Full Name / <i> Nama Penuh </i> </label>

                                                            <input type="text" minlength="2" maxlength="100" id="name2" name="name2" value="{{$ref2}}" class="form-control" required="required" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return name2(event);">

                                                      

                                                    </div>



                                                    <div class="form-group">

                                                        <label>2. Home Telephone / <i>Telefon Rumah :</i></label><br>

                                                            <input type="tel" id="home_phone2" minlength="8" maxlength="13"   name="home_phone2" class="form-control" value="{{$reference->home_phone2}}" onkeypress="return isNumberKey(event)"/>



                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>

                                                            <input type="tel" id="mobilephone2" minlength="8" maxlength="13"   name="mobilephone2" class="form-control" value="{{$reference->mobilephone2}}" onkeypress="return isNumberKey(event)"/ required="">

                                                       

                                                    </div>



                                                    <div class="form-group">

                                                        <label>3. Relationship / <i> Hubungan</i> </label>

                                                            <select name="relationship2" id="relationship2" class=" form-control" required="">

                                                             <option selected=""></option>
 @foreach ($relationship2 as $relationship2)
                                                                @if($reference->relationship2 !=NULL)
                                                                    <?php 
                                                                        if($reference->relationship2==$relationship2->id) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$relationship2->id}}">{{$relationship2->relationship_desc}}</option>
                                                                @else
                                                                     <option value="{{$relationship2->id}}">{{$relationship2->relationship_desc}}</option>
                                                                @endif
                                                            @endforeach

                                                        </select>

                                                     

                                                    </div>

                                                    </div>

                                       

                                            </div>

                                             <div class="tab-pane" id="tab4">

                                                    <br>

                                                        <h3><strong>Step 4</strong> - Income Information (Main Applicant)/  <i>Maklumat Pendapatan (Pemohon Utama)</i> </h3>


                                                    <div class="col-md-6">
                                                           <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                            <input name="id_praapplication" type="hidden"  value="{{$data->id_praapplication}}">   <br>
                                                  
                                                    <div class="form-group">
                                                        <label>1. Monthly Income  / <i> Pendapatan Bulanan </i>  </label>
                                                        <!-- <input type="text" maxlength="10" name="monthly_income"  id="monthly_income" class="form-control income fn" placeholder="RM" onchange="toFloat('monthly_income')" value="" onkeypress="return isNumberKey(event)">-->

                                                        <input type="hidden" minlength="2" maxlength="10" name="monthly_incomex"  id="monthly_income" class="form-control fn income hidden" placeholder="RM" onchange="toFloat('monthly_income')" value="{{number_format((float)$financial->monthly_income, 2, '.', '')}}" onkeypress="return isNumberKey(event)"> 

                                                        <input class="form-control CurrencyInput fn" id="a" name="monthly_income_hidden" placeholder="RM" value="{{number_format((float)$financial->monthly_income, 2)}}" type="tel" onkeypress="return isNumberKey(event)">

                                                        <!-- save n hidden -->
                                                        <input type="hidden" class="form-control CurrencyInput fn" id="a_display" name="monthly_income" placeholder="RM" value="{{number_format((float)$financial->monthly_income, 2, '.', '')}}">
                                                        <!-- end save n hidden -->

                                                    </div>

                                                    <div class="form-group">
                                                        <label>2.   Other Income  /<i> Pendapatan Lain</i> : </label>
                                                        <!-- <input type="text" maxlength="10" required="required" class="form-control income fn" name="other_income" placeholder="RM" value="" onchange="toFloat('other_income')"   onkeypress="return isNumberKey(event)"> -->

                                                        <input type="tel" minlength="2" maxlength="10" required="required" class="form-control fn income hidden" name="other_incomex" placeholder="RM" value="{{number_format((float)$financial->other_income, 2, '.', '')}}" id="other_income" onchange="toFloat('other_income')"   >

                                                        <input class="form-control CurrencyInput2 fn" id="b" name="other_income_hidden" placeholder="RM" value="{{number_format((float)$financial->other_income, 2)}}" onkeypress="return isNumberKey(event)" type="tel">

                                                        <!-- save n hidden -->
                                                        <input type="hidden" class="form-control CurrencyInput2 fn" id="b_display" name="other_income" placeholder="RM" value="{{number_format((float)$financial->other_income, 2, '.', '')}}">
                                                        <!-- end save n hidden -->
                                                        
                                                    </div>

                                                    <div class="form-group">
                                                        <label>3. Total Income / <i>Jumlah Pendapatan </i> : </label>
                                                            <!-- <input   type="text" maxlength="10" required="required" class="form-control" placeholder="RM" name="total_income fn" value="" id="total_income" readonly >-->
                                                            
                                                            <input   type="text" minlength="2" maxlength="10" required="required" class="form-control fn hidden" placeholder="RM" name="total_incomex" value="{{number_format((float)$financial->monthly_income + $financial->other_income, 2, '.', '')}}" id="total_income" readonly >

                                                            <input class="form-control total_nya" id="total_nya" placeholder="RM" name="total_income_display" value="{{number_format((float)$financial->monthly_income + $financial->other_income, 2)}}" readonly>

                                                            <!-- save n hidden -->
                                                            <input type="hidden" class="form-control total_hidden" id="total_hidden" placeholder="RM" name="total_income" value="{{number_format((float)$financial->monthly_income + $financial->other_income, 2, '.', '')}}">
                                                            <!-- end save n hidden -->
                                                    </div>
                                                </div> 

                                      

                                                  </div>

                                                <div class="tab-pane" id="tab5">

                                                    <br>

                                                    <h3><strong>Step 5</strong> - Your Commitments with Other Credit Providers (Non-Banks Only)**/ <i>Komitmen Dengan Pembiaya Kredit Lain (Bukan Bank Sahaja)**</i></h3> 



                                                    <div class="col-lg-12"> <br>

                                                              ** (e.g. AEON Credit, PTPTN, MARA, etc.)/ (cth. AEON Credit, PTPTN, MARA, dll.)

                                                    </div> 



                                                    <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">

                                                    {{csrf_field()}}

                                                    

                                                    <div class="col-md-6"  id="tab99">

                                                      

                                                        <br>

                                                        <h3><strong>Commitment 1<i>/ Komitmen 1</i></strong></h3>

                                                         <div class="form-group" >

                                                            <label>1. Name of Entity  / <i>  Nama Entiti   :  </i>  </label>

                                                                <input type="text" minlength="2" maxlength="100" class="form-control" value="@if(!(empty($commitments->name1))){{$commitments->name1}} @endif" name="name1" id="" onkeyup="this.value = this.value.toUpperCase()">

                                                        </div>

                                                         <div class="form-group" >

                                                            <label>2. Type of Financing  / <i>  Jenis Pembiayaan  :  </i>  </label>

                                                                <input type="text" minlength="2" maxlength="100" class="form-control" value="@if(!(empty($commitments->financing1))){{$commitments->financing1}}@endif" name="financing1" id="" onkeyup="this.value = this.value.toUpperCase()">

                                                        </div>

                                                         <div class="form-group" >

                                                            <label>3. Monthly Payment  / <i>  Bayaran Bulanan  :  </i>  </label>

                                                              <div class="input-group">

                                                                    <span class="input-group-addon">RM</span>

                                                                    <input type="tel"  minlength="2" maxlength="8" class="form-control" value="@if(!(empty($commitments->monthly_payment1))){{$commitments->monthly_payment1}}@endif" name="monthly_payment1" id="" onkeypress="return isNumberKey(event)" >

                                                            </div>

                                                        </div>

                                                         <div class="form-group" >

                                                            <label>4. Remaining Financing Term  / <i>  Baki Tempoh Pembiayaan  :  </i>  </label>

                                                                <input type="tel" minlength="1" maxlength="2" class="" size="3" value="@if(!(empty($commitments->remaining1))){{$commitments->remaining1}}@endif" name="remaining1" id="" onkeypress="return isNumberKey(event)" > Years/<i>Tahun</i>

                                                        </div>

                                                        

                                                    </div>

                                                     <div class="col-md-6"  id="tab99">

                                                            <br>

                                                            <h3><strong>Commitment 2<i>/ Komitmen 2</i></strong></h3>

                                                        <div class="form-group" >

                                                            <label>1. Name of Entity  / <i>  Nama Entiti   :  </i>  </label>

                                                                <input type="text" minlength="2" maxlength="100" class="form-control" value="@if(!(empty($commitments->name2))){{$commitments->name2}}@endif" name="name2" id="" onkeyup="this.value = this.value.toUpperCase()">

                                                        </div>

                                                         <div class="form-group" >

                                                            <label>2. Type of Financing  / <i>  Jenis Pembiayaan  :  </i>  </label>

                                                                <input type="text" minlength="2" maxlength="100" class="form-control" value="@if(!(empty($commitments->financing2))){{$commitments->financing2}}@endif" name="financing2" id="" onkeyup="this.value = this.value.toUpperCase()">

                                                        </div>

                                                       

                                                         <div class="form-group" >

                                                            <label>3. Monthly Payment  / <i>  Bayaran Bulanan  :  </i>  </label>

                                                                <div class="input-group">

                                                                  <span class="input-group-addon">RM</span>

                                                                <input type="tel" minlength="2" maxlength="8" class="form-control" value="@if(!(empty($commitments->monthly_payment2))){{$commitments->monthly_payment2}}@endif" name="monthly_payment2" id="" onkeypress="return isNumberKey(event)">

                                                            

                                                            </div>

                                                        </div>

                                                          <div class="form-group" >

                                                            <label>4. Remaining Financing Term  / <i>  Baki Tempoh Pembiayaan  :  </i>  </label>

                                                                <input type="tel" minlength="1" maxlength="2" size="3" class="" value="@if(!(empty($commitments->remaining2))){{$commitments->remaining2}}@endif" name="remaining2" id="" onkeypress="return isNumberKey(event)"> Years/<i>Tahun</i>

                                                        </div>

                                                 

                                                       

                                                    </div>

                                           

                                                </div>



                                            <div class="tab-pane" id="tab6">

                                                <br>

                                                <h3><strong>Step 6</strong> -  Financing Details/ <i> Maklumat Pembiayaan</i></h3>  

                                                <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">

                                                {{csrf_field()}}
                                                     <div class="col-md-3"  id="tab99"></div>
                                                <div class="col-md-6"  id="tab99">

                                                        <br>

                                                        <h3><strong>Facility Applied/ <i>Kemudahan Yang Dipohon</i></strong></h3>

                                                    <div class="form-group" >

                                                        <label>1. Package Applied  / <i>  Pakej Dipohon  :  </i>  </label>

                                                            <input type="text" minlength="2" maxlength="66" class="form-control" value="{{$loanammount->package}}" name="package" id="package" required="required">

                                                       

                                                       

                                                    </div>

                                                     <div class="form-group" id="financing_detaila" >

                                                        <label>2. Product Bundling  / <i> Gabungan Produk  :  </i>  </label><br>

                                                        <td>&nbsp;</td>

                                                            <td>

                                                                <input type="radio" @if($financial->product_bundling=="1") checked @endif  name="product_bundling" id="pb_yes" value="1" class="product_bundling">

                                                            </td>

                                                            <td><b>Yes (Specify)</b>/Ya (Nyatakan)</td>

                                                            <td>&nbsp;&nbsp;&nbsp;</td>

                                                            <td>

                                                                 <input type="radio" @if($financial->product_bundling=="0") checked @endif   name="product_bundling" id="pb_no" value="0" class="product_bundling">

                                                            </td>

                                                            <td><b>No</b>/Tidak</td><br>

                                                                <input type="text" required="" id="product_bundling_specify" name="product_bundling_specify" class="form-control" placeholder="Specify/ Nyatakan" value="{{$financial->product_bundling_specify}}" minlength="2" maxlength="150" onkeyup="this.value = this.value.toUpperCase()"/>

                                                    



                                                            </div>

                                                             <div class="form-group" id="financing_detail" >

                                                        <label>3. Cross Selling  / <i> Jualan Silang  :  </i>  </label><br>

                                                            <td>&nbsp;</td>

                                                            <td>

                                                                <input type="radio" required="" @if($financial->cross_selling=="1") checked @endif name="cross_selling" value="1" id="cs_yes">

                                                            </td>

                                                            <td><b>Yes (Specify)</b>/Ya (Nyatakan)</td>

                                                            <td>&nbsp;&nbsp;&nbsp;</td>

                                                            <td>

                                                                 <input type="radio" required="" @if($financial->cross_selling=="0") checked @endif  name="cross_selling" value="0" id="cs_no">

                                                            </td>

                                                            <td><b>No</b>/Tidak</td><br>

                                                    

                                                              <input type="text" required="" id="cross_selling_specify" name="cross_selling_specify" class="form-control" placeholder="Specify/ Nyatakan" minlength="2" maxlength="150" value="{{$financial->cross_selling_specify}}" onkeyup="this.value = this.value.toUpperCase()"/>

                                                     

                                                    



                                                            </div>

                                                      

                                                    <div class="form-group" ><br>

                                                     <h3><strong>Information on Takaful Coverage/<i> Maklumat untuk Perlindungan Takaful</i></strong></h3>



                                                        <label>4. Takaful Coverage  / <i> Perlindungan Takaful  :  </i>  </label><br>

                                                        &nbsp; &nbsp;Group Credit Term Takaful (GCTT)/ Perlindungan Takaful Berkelompok Bertempoh (GCTT)<br>

                                                            <td>&nbsp;</td>

                                                            <td>

                                                                <input type="radio" required="" name="takaful_coverage" @if($financial->takaful_coverage=="1") checked @endif id="tf_yes" value="1" >

                                                            </td>

                                                            <td><b>Yes</b>/Ya</td>

                                                            <td>&nbsp;&nbsp;&nbsp;</td>

                                                            <td>

                                                                <input type="radio" name="takaful_coverage" @if($financial->takaful_coverage=="0") checked @endif id="tf_no" value="0" >

                                                            </td>

                                                            <td><b>No</b>/Tidak</td><br>



                                                            

                                             

                                                    </div>

                                                    

                                                    <div class="form-group" id="spouse_name" >

                                                        <h3><strong>Details of Financing /<i> Butir-butir Pembiayaan</i></strong></h3>

                                                        <label>5. Financing Amount  /<i> Jumlah Pembiayaan </i> : </label>

                                                          <div class="input-group">

                                                                <span class="input-group-addon">RM</span>

                                                            <input type="text" minlength="2" maxlength="66" class="form-control" value="{{$dsr_b->financing_amount}}" name="financing_amount" id="" required="required" readonly="">

                                                        </div>

                                                 

                                                    </div>

                                                     <div class="form-group" id="spouse_name" >

                                                        <label>6. Tenure  (Years)/<i> Tempoh (Tahun)</i> : </label>

                                                        @if($loanammount->new_tenure==0)

                                                           <?php  $tenure = $loanammount->tenure->years; ?>

                                                        @else

                                                           <?php $tenure = $loanammount->new_tenure; ?>

                                                        @endif

                                                         <input type="text" maxlength="2" class="form-control" value="{{$dsr_b->tenure}}" name="tenure" id="" required="required" readonly="">

                                                   

                                                    </div>



                                                    </div>

                                                 
  <div class="col-md-3"  id="tab99"></div>
                                         

                                            </div>

                                            <div class="tab-pane" id="tab7">

                                                  

                                                    <br>

                                                    <h3><strong>Step 7</strong> Application for Personal Financing-<i>i</i> Facility/ <i> Permohonan Untuk Kemudahan Pembiayaan Peribadi-i</i> </h3>

                                                    {{csrf_field()}}

                                                     <input type="hidden" name="id_praapplication" value="{{$pra->id_pra}}">


  <div class="col-md-3"  id="tab99"></div>
                                                    <div class="col-md-6">

                                                                <br>

                                                    <div class="form-group"> 

                                                        <label>1. Purpose of Facility/ <i>Tujuan Pembiayaan</i> :</label>

                                                            <select class="form-control" id="purpose_facility" name="purpose_facility" required="required">

                                                              @if(!empty($financial->purpose_facility))

                                                                      @if($financial->purpose_facility=='115')

                                                                    <option value="PF01">Personal Use/<i> Kegunaan Peribadi</i></option>

                                                                    @elseif($financial->purpose_facility=='113')

                                                                    <option value="PF02">Education/<i> Pendidikan</i></i></option>

                                                                    @elseif($financial->purpose_facility=='112')

                                                                    <option value="PF03">Business/<i> Perniagaan</i></option>

                                                                    @elseif($financial->purpose_facility=='PPP')

                                                                    <option value="PF04">Others/<i> Lain-lain</i></option>

                                                                    @endif

                                                                @endif

                                                                    <option value="PF01">Personal Use/<i> Kegunaan Peribadi</i></option>

                                                                    <option value="PF02">Education/<i> Pendidikan</i></option>

                                                                    <option value="PF03">Business/<i> Perniagaan</i></option>

                                                                    <option value="PF04">Others/<i> Lain-lain</i></option>

                                                                </select>

                                                       

                                                    

                                                        

                                                    </div>

                                                    <div class="form-group">

                                                        <label>2. Type of Customer/ <i>Jenis Pelanggan</i> :</label>

                                                            <select class="form-control" id="type_customer" name="type_customer" required="requeired">

                                                            @if(!empty($financial->type_customer))

                                                                    @if($financial->type_customer=='115')

                                                                    <option value="Y">Biro/ <i>Biro</i></option>

                                                                    @elseif($financial->type_customer=='113')

                                                                   <option value="N">Non-Biro/ <i>Bukan Biro</i></option>

                                                                    @elseif($financial->type_customer=='112')

                                                                    <option value="P">Private Sector/ <i>Swasta</i></option>

                                                                    @elseif($financial->type_customer=='PPP')

                                                                    <option value="G">Federal/State AG/<i>Akauntan Negara/Negeri</i></option>

                                                                    @endif

                                                                @endif

                                                                    <option value="Y">Biro/ <i>Biro</i></option>

                                                                    <option value="N">Non-Biro/ <i>Bukan Biro</i></option>

                                                                    <option value="P">Private Sector/ <i>Swasta</i></option>

                                                                    <option value="G">Federal/State AG/<i>Akauntan Negara/Negeri</i></option>

                                                                </select>

                                             

                                                    </div>

                                                    <div class="form-group">

                                                        <label>3. Payment Mode/ <i>Cara Bayaran</i> :</label>

                                                            <select class="form-control" id="payment_mode" name="payment_mode" required="requeired">

                                                                 <option value='noselect11' selected disabled>Please Select</option>
                                                                            @foreach($method as $method) 
                                                                                @if($method->id==$financial->payment_mode) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif
                                                                                <option {{$selected}} value="{{$method->id}}">{{$method->payment_mode}}</option>
                                                                             @endforeach

                                                             
                                                                </select>

                                                    </div>
                                                    <br>

                                                    <h3><b>Application for Settlement (If Any)/ <i> Permohonan Penyelesaian Pembiayaan Lain (Jika Berkenaan)</i> </b></h3>

                                                    <div class="form-group">

                                                        <label>Name of Banks/Non Banks <i>Nama Bank/Bukan Bank**</i> :</label><br>

                                                        ** (e.g. AEON Credit, PTPTN, MARA, etc.)/ (cth. AEON Credit, PTPTN, MARA, dll.)<br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank1}}" name="bank1" id="" placeholder=" Name of Bank/Non Banks 1" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank2}}" name="bank2" id="" placeholder=" Name of Bank/Non Banks 2" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank3}}" name="bank3" id="" placeholder=" Name of Bank/Non Banks 3" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank4}}" name="bank4" id="" placeholder=" Name of Bank/Non Banks 4" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank5}}" name="bank5" id="" placeholder=" Name of Bank/Non Banks 5" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank6}}" name="bank6" id="" placeholder=" Name of Bank/Non Banks 6" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                    </div>

                                                     <div class="form-group">

                                                        <label>Account to Credit Financing Amount /<i>Akaun di mana Amaun Pembiayaan Dikreditkan</i> :</label>

                                                        <label>Bank's Name/ <i>Nama Bank</i> :</label>

                                                        <input type="text" minlength="2" maxlength="100" class="form-control" value="{{$financial->bank_name}}" name="bank_name" id="bank_name" placeholder="" required onkeyup="this.value = this.value.toUpperCase()">

                                                    </div>

                                                    <div class="form-group">

                                                        <label>Account No./ <i>No. Akaun</i> :</label>

                                                        <input type="tel" minlength="2" maxlength="20" class="form-control" value="{{$financial->account_no}}" name="account_no" id="account_no" placeholder="" required onkeypress="return isNumberKey(event)" >

                                                    </div>

                                                </div>

                                        <div class="col-md-3"  id="tab99"></div>

                                            </div>

                                          <div class="tab-pane" id="tab8">

                                                <br>

                                                <h3><strong>Step 8</strong> - Upload Document/<i> Muat Naik dokumen</i> (<i>Document in PDF/JPG/PNG / Dokumen dalam format PDF/JPG/PNG</i> </h3>

                                                <div class="container" style="">

                                                     {{csrf_field()}}

                                                     <input name="id_praapplication" type="hidden"  value="{{$pra->id_pra}}" >



                                                            <div class="form-group">

                                                    <label class="col-md-4 control-label">Salinan Kad Pengenalan <sup>*</sup></label>

                                                    <div class="col-md-8">

                            <input id="fileupload6"  @if(empty($document6->name)) required @endif  class="btn btn-default" type="file" name="file6"  >

                            <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenalan">

                            &nbsp; <span id="document6"> </span> 

                            <input type='hidden' value='@if(!empty($document6->name)){{$document6->upload}} @endif' id='a6' name='a6'/>

                            @if(!empty($document6->name))

                              <span id="document6a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document6->upload}}"> {{$document6->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 



                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document6->upload}}"> {{$document6->name}} </a>

                            @endif

                                  

                                                    </div>

                                                </div> &nbsp; <hr><br>





                                         <div class="form-group">

                            <label class="col-md-4 control-label">Salinan Penyata Gaji Untuk 3 Bulan Terkini <sup>*</sup></label>

                            <div class="col-md-8">

                           <input id="fileupload7"  @if(empty($document7->name)) required @endif   class="btn btn-default" type="file" name="file7" >

                            <input type="hidden" name="document7"  id="documentx7"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini">

                            &nbsp; <span id="document7"> </span> 



                            <input type='hidden' value='@if(!empty($document7->name)){{$document7->upload}} @endif' id='a7' name='a7'/>

                            @if(!empty($document7->name))

                              <span id="document7a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document7->upload}}"> {{$document7->name}}</a>

                            @endif

                            <br>

                             <i> Jika dokumen penyata gaji lebih daripada satu, sila muat naik dibawah </i>

                                                    

                             <input id="fileupload10"  class="btn btn-default" type="file" name="file10" >

                            <input type="hidden" name="document10"  id="documentx10"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 1))">

                            &nbsp; <span id="document10"> </span> 



                            @if(!empty($document10->name))

                              <span id="document10a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document10->upload}}"> {{$document10->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 



                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document10->upload}}"> {{$document10->name}}</a>

                            @endif



                            <input id="fileupload11"  class="btn btn-default" type="file" name="file11" >

                            <input type="hidden" name="document11"  id="documentx11"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 2)">

                            &nbsp; <span id="document11"> </span> 

                            @if(!empty($document11->name))

                              <span id="document11a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document11->upload}}"> {{$document11->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                                <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document11->upload}}"> {{$document11->name}}</a>

                            @endif

            

             

                                                    </div>

                                                </div>

                         

                                               

                                        

                         

                                               

                                                      &nbsp; <hr><br>

                                                      

                                                    <div class="form-group">

                                                        <label class="col-md-4 control-label">Settlement Letter/ <i>Penyata Penyelesaian Awal</i></label>

                                                    <div class="col-md-8">

                                                        <input id="fileupload1"  @if(empty($document1->name)) @endif   class="btn btn-default" type="file" name="file1" >

                            <input type="hidden" name="document1"  id="documentx1"  value="Salinan Penyata Penyelesaian Awal">

                            &nbsp; <span id="document1"> </span> 

                            @if(!empty($document1->name))

                              <span id="document1a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document1->upload}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document1->upload}}"> {{$document1->name}}</a>

                            @endif

                            <br><br>

                           If more than one document, please upload below/ <br><i>Jika dokumen penyata penyelesaian awal lebih daripada satu, sila muat naik dibawah </i>

                                                    

                             <input id="fileupload2"  class="btn btn-default" type="file" name="file2" >

                            <input type="hidden" name="document2"  id="documentx2"  value="Salinan Penyata Penyelesaian Awal (optional 1)">

                            &nbsp; <span id="document2"> </span> 

                            @if(!empty($document2->name))

                              <span id="document2a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document2->upload}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                              <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document2->upload}}"> {{$document2->name}}</a>

                            @endif



                            <input id="fileupload3"  class="btn btn-default" type="file" name="file3" >

                            <input type="hidden" name="document3"  id="documentx3"  value="Salinan Penyata Penyelesaian Awal  (optional 2)">

                            &nbsp; <span id="document3"> </span> 

                            @if(!empty($document3->name))

                              <span id="document3a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document3->upload}}"> {{$document3->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document3->upload}}"> {{$document3->name}}</a>

                            @endif

            

             

                                                    </div>

                                                </div>

                         

                                               

                                                      <br> &nbsp; </br>

                                                <div class="form-group">

                                                    <label class="col-md-4 control-label">Surat Pengesahan Majikan<sup>*</sup></label>

                                                    <div class="col-md-8">

                            <input id="fileupload9"  @if(empty($document9->name)) required @endif  class="btn btn-default" type="file" name="file9"  >

                            <input type="hidden" name="document9"   id="documentx9"  value="Surat Pengesahan Majikan">

                            &nbsp; <span id="document9"> </span> 

                            @if(!empty($document9->name))

                              <span id="document9a"><a target='_blank' class="hidden-xs hidden-sm" href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document9->upload}}"> {{$document9->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document9->upload}}"> {{$document9->name}}</a>

                            @endif

                                  

                                                    </div>

                                                </div>

                                            </div>

                                               

                                            </fieldset>

                                              

                                            </div>

                                             

                                            <div class="tab-pane" id="tab9">



                                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                                                <input name="id_praapplication" type="hidden" id="id_praapplication" value="{{$data->id_praapplication}}" >



                                                <input type="hidden" name="password" id="password_copy">

                                                <input type="hidden" autocomplete="false" name="email" value="{{$corres_email}}" id="email_copy">



                                                <input type='hidden' name='latitude' id='latitude' >

                                                <input type='hidden' name='longitude' id='longitude'>

                                                <input type='hidden' name='location' id='location' >









                                                <br>

                                                <h3><strong>Step 9</strong> -ROUTE TO CUSTOMER </i>  </h3>
                                                <div class="row">
                                                    <div class="col-lg-1"> </div>
                                                     <div align="justify" class="col-lg-10">
                                                          <input type="hidden" name="_token" id="token_term" value="{{csrf_token()}}">

                                                                 <input type="hidden" name="id_praapplication_term" id="id_praapplication_term" value="{{$pra->id_pra}}">
                                                            @if($view=='verify')

                                                        <div class="row">
                                                            <div class="col-md-5"><br>
                                                            

                                                            <div class="form-group">
                                                               <label><b>Remark / Reason :</b></label>
                                                                <textarea onkeyup="this.value = this.value.toUpperCase()" id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'>{{$term->verification_remark}}</textarea >
                                                           </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <a id="route" class="btn btn-success">
                                                                &nbsp; Route to Customer &nbsp; &nbsp;
                                                            <a> 
                                                        </div>
                                                       <!--<div class="col-sm-3">
                                                          <a id="disagree" class="btn btn-danger" >
                                                          Application Rejected
                                                          </a> 
                                                    </div>-->
                                                </div>
                                                @else($view=='view')
                                                <div class="row">
                                                    <div class="col-md-3"  id="tab99"></div>
                                                    <div class="col-md-6"><br>
                                                        <div class="form-group">
                                                           <label><b>Remark / Reason :</b></label>
                                                            <textarea disabled id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'>{{$term->LogLogin->log_remark}}</textarea>       
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="row">
                                                    <div class="col-md-3"  id="tab99"></div>
                                                    <div class="col-sm-6">
                                                        <a href='{{ url('/admin') }}' class="btn btn-primary">
                                                            &nbsp; << Back to Dashboard &nbsp; &nbsp;
                                                        </a> 
                                                    <div class="col-md-3"  id="tab99"></div>
                                                </div>
                                                 <div class="col-md-3"  id="tab99"></div>
                                             </div>
                                             @endif

                                                            


                                                          </div>

                                                            <div class="col-lg-1"></div>



                                                          </div>



                                                          <div class="row">

                                                          <div class="col-lg-1">

                                                          </div>

                                                            <div align="justify" class="col-lg-10">



                                                   

                                                      



                                                          </div>

                                                          <div class="col-lg-1">

                                                          </div>

                                                           </div>





                                                     

                                                </div>





                                                      



                                                     

                

                                                       <div class="form-actions">

                                                            <div class="row">

                                                                <div class="col-sm-12">

                                                                    <ul class="pager wizard no-margin">

                                                                        <!--<li class="previous first ">

                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>

                                                                        </li>-->

                                                                        <li class="next">

                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"> Seterusnya / <i> Next </i> </a>

                                                                        </li>

                                                                        <li class="previous ">

                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>

                                                                        </li>

                                                                        <!--<li class="next last">

                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>

                                                                        </li>-->

                                                                        

                                                                    </ul>

                                                                </div>

                                                            </div>

                                                        </div>

                

                                                    </div>

                                                </div>

                                            </form>

                                        </div>

                

                                    </div>

                                    <!-- end widget content -->

                

                                </div>

                                <!-- end widget div -->

                

                            </div>

                            <!-- end widget -->

                

                        </article>

                        <!-- WIDGET END -->

                

                        <!-- NEW WIDGET START -->

                        <a

                        <!-- WIDGET ENffD -->

                

                    </div>

                

                    <!-- end row -->

                

                </section>

            </div>

            <br>


   

<!-- Modal -->



      
@if($view=='view')
<script type="text/javascript">
    $(document).ready(function(){
        $("#wizard-1 :input").prop("disabled", true);
    });
</script>

@endif
<script>
    function isNumberKey(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
            if (key.length == 0) return;
                var regex = /^[0-9.,\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
            }
    }
</script>

<script>
    function toFloat(z) {
      var x = document.getElementById(z);
      x.value = parseFloat(x.value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    }

    $( ".fn" ).keyup(function() {
        var nmi = $('#monthly_income').val().replace(/,/g,'');
        var a = $('#other_income').val().replace(/,/g,'');
        var total = parseFloat(nmi) + parseFloat(a);
        $("#total_income").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#marital').change(function() {
        if( $(this).val() == "M" ) {
                $('.couple').prop( "disabled", false );
        } else {       
          $('.couple').prop( "disabled", true );
        }
      });
     
    });
</script>

<script type="text/javascript">
    $('input.CurrencyInput').on('blur', function() {
      const value = this.value.replace(/,/g, '');
      this.value = parseFloat(value).toLocaleString('en-US', {
        style: 'decimal',
        maximumFractionDigits: 2,
        minimumFractionDigits: 2
      });
    });

    $('input.CurrencyInput2').on('blur', function() {
      const value = this.value.replace(/,/g, '');
      this.value = parseFloat(value).toLocaleString('en-US', {
        style: 'decimal',
        maximumFractionDigits: 2,
        minimumFractionDigits: 2
      });
    });

    $( ".fn" ).keyup(function() {
        var a = $('#a').val().replace(/,/g,'');
        var b = $('#b').val().replace(/,/g,'');
        var total = parseFloat(a) + parseFloat(b);
    
        $("#a_display").val(parseFloat(a).toFixed(2).replace(/,/g, "")); 
        $("#b_display").val(parseFloat(b).toFixed(2).replace(/,/g, "")); 
        $("#total_nya").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
        $("#total_hidden").val(parseFloat(total).toFixed(2).replace(/,/g, ""));              
    });
</script>

<!-- <script type="text/javascript">
    document.getElementById("BasicSalary_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
        .toFixed(2)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_BasicSalary_x").value = this.value.replace(/,/g, "");     
    }
</script>-->

<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
        //Bootstrap Wizard Validations
            var $validator = $("#wizard-1").validate({
                rules: {
                    email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                fullname: {
                    maxlength: 100
                },
              
                homephone : {
                    minlength:8,
                    maxlength:13
                },
                mobilephone1 : {
                    required : true,
                    minlength:9,
                    maxlength:13
                },
                mobilephone2 : {
                    required : true,
                    minlength:9,
                    maxlength:13
                },
                mobilephone : {
                    required : true,
                    minlength:9,
                    maxlength:13
                },
                office_phone : {
                    minlength:9,
                    maxlength:13
                },
                office_phone : {
                    required : true,
                    minlength:8,
                    maxlength:13
                },
                handphone : {
                    minlength:9,
                    maxlength:13
                },
                home_phone : {
                    minlength:8,
                    maxlength:13
                },
                home_phone1 : {
                    minlength:8,
                    maxlength:13
                },
                home_phone2 : {
                    minlength:8,
                    maxlength:13
                },
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                country: {
                    required: true
                },
                city: {
                    required: false
                },
                postcode: {
                    required: false,
                    minlength: 4
                },
                wphone: {
                    required: true,
                    minlength: 10
                },
                hphone: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                fname: "Please specify your First name",
                lname: "Please specify your Last name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            },

            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },

            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },

                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                    var $valid = $("#wizard-1").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                        return false;
                        } else {
                            $.ajax({
                            type: "PUT",
                            url: '{{ url('/moform/') }}'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),
                            cache: false,

                            beforeSend: function () {
                            },

                            success: function () {
                            },

                            error: function () {
                            }
                        });

                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                }
            }
        });
        // fuelux wizard
        var wizard = $('.wizard').wizard();
            wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
            });
        })
</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->

<script type="text/javascript">
    var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
    $('.startdate').datepicker({
        dateFormat : 'dd/mm/yy',
        changeYear: true,
        changeMonth: true,
        yearRange: "-72:+0",
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
    $('.date').datepicker({
        dateFormat : 'dd/mm/yy',
        changeYear: true,
        changeMonth: true,
        yearRange: "-72:+0",
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
</script>
<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script type="text/javascript">
    $( "#postcode2" ).change(function() {
        var postcode = $('#postcode2').val();
        $.ajax({
            url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        //$("#city").val(data[k].post_office );
                        $("#state2").val(data[k].state.state_name );
                        $("#state_code2").val(data[k].state.state_code );
                    });
                }
        });
    });
</script>

<script type="text/javascript">
    $( "#postcode" ).keyup(function() {
    var postcode = $('#postcode').val();
    $.ajax({
        url: "<?php  print url('/'); ?>/postcode/"+postcode,
            dataType: 'json',
                data: {

                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        $("#state").val(data[k].state.state_name );
                        $("#state_code").val(data[k].state.state_code );
                    });
                }
            });
        });
</script>

<script type="text/javascript">
    $( "#postcode3" ).change(function() {
        var postcode = $('#postcode3').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                },

                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        //$("#city").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                        $("#state_code3").val(data[k].state.state_code );
                    });
                }
            });
        });
</script>


<?php for ($x = 1; $x <= 12; $x++) {  ?>
<script>
    $(function () {
        'use strict';

        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
            '//jquery-file-upload.appspot.com/' : '{{url('/')}}/form/upload/{{$x}}';
            $('#fileupload{{$x}}').fileupload({
                url: url,
                dataType: 'json',
                success: function ( data) {
                    var text = $('#documentx{{$x}}').val();
                    $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
                    $("#document{{$x}}a").hide();

                document.getElementById("fileupload{{$x}}").required = false;
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>
<?php } ?>

<script type="text/javascript">
$( "#route" ).click(function() {
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var remark = $('#remark_verification').val();
    var status = 77;
    if (remark=='') {
        alert("Please  Fill a Remark!");
    }
    else {
      var r = confirm("Are You Sure to Approve  this Documents");
      if (r == true) {
        $.ajax({
            type: "POST",
            url: '{{ url('/verifiedMoForm/route/') }}',
            data: { status : status, id_praapplication: id_praapplication, _token : _token, remark : remark},
            cache: false,
            beforeSend: function () {
            },
            success: function () {
               alert("Send Success !");
               location.href='{{url('/')}}/admin';
           },
           error: function () {
           }
       });
    }
   }
});
</script>

<script type="text/javascript">
   

$( "#password" ).keyup(function() {
    var password = $('#password').val();
        $("#password_copy").val(password);
});

$( "#email" ).keyup(function() {
    var email = $('#email').val();
        $("#email_copy").val(email);
});

$( "#submit_application" ).click(function() {
    //var icnumber = $('#new_ic').val();
    //var remark = $('#remark_verification').val();
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var purchase_application = $('input[name="purchase_application"]:checked').val();
    var appointment_mbsb = $('input[name="appointment_mbsb"]:checked').val();
    var credit_yes= $('#credit_yes:checked').val();
    var credit_no = $('#credit_no:checked').val();
    var consent_for_yes = $('#consent_for_yes:checked').val();
    var consent_for_no = $('#consent_for_no:checked').val();
    var high_networth_yes = $('#high_networth_yes:checked').val();
    var high_networth_no = $('#high_networth_no:checked').val();
    var politically_yes = $('#politically_yes:checked').val();
    var politically_no = $('#politically_no:checked').val();
    var product_disclosure = $('input[name="product_disclosure"]:checked').val();   // 7.10
    var fullname = $('#fullname').val();
    var mykad = $('#mykad').val();
    var passport = $('#passport').val();
    var relationship = $('#relationship').val();
    var name1 = $('#pep_name1').val();
    var relationship1 = $('#pep_relationship1').val();
    var status1 = $('#pep_status1').val();
    var prominent1 = $('#pep_prominent1').val();
    var name2 = $('#pep_name2').val();
    var relationship2 = $('#pep_relationship2').val();
    var status2 = $('#pep_status2').val();
    var prominent2 = $('#pep_prominent2').val();
    var name3 = $('#pep_name3').val();
    var relationship3 = $('#pep_relationship3').val();
    var status3 = $('#pep_status3').val();
    var prominent3 = $('#pep_prominent3').val();

    $.ajax({
    type: "POST",                       
        url: '{{ url('/moform/term/') }}',
        data:   $('#tab9 :input').serialize(),
        cache: false,
        beforeSend: function () {  
            $("#message").html("<div class='alert alert-default alert-dismissable'>"+
            "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
            "Please Wait..</div>");                            
        },
        success: function (data) { 
            if(data.status==0) {
                // $("#message").hide();
                $("#message").html("<div class='alert alert-danger alert-dismissable'>"+
                "<a href='#'' cl//ass='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                data.message+"</div>");
            }
            else {
                alert("Application Successfully Routed to Branch!");
                window.location.href='{{url('/')}}/admin';
            }    
        },
        error: function () {  
            alert("Something Wrong!");                                                             
        }
    });
});

$( "#agreed" ).click(function() {
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var branch = $('#branch').val();
    var declaration = 2;
    var remark = $('#remark_verification').val();

    if ((remark=='') || (branch=='')) {
        //code
        bootbox.alert("Please Select Branch AND Fill a Remark!");
      }
      else {
      var r = confirm("Are You Sure to Route this Application to Branch?");
      if (r == true) {
         $.ajax({
               type: "POST",
               url: '{{ url('/verifiedMoForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
               cache: false,
               beforeSend: function () {
               },

               success: function () {
                   alert("Verification Success, Application Approved Route to Branch !");
                   location.href='{{url('/')}}/admin';
               },
               error: function () {
               }
           });
     }
   }
});
</script>

<script type="text/javascript">
    var $loading = $('#loadingmessage').hide();
    $(document)
        .ajaxStart(function () {
          $loading.show();
        })
        .ajaxStop(function () {
         $loading.hide();
    });
</script>

<script type="text/javascript">
    $( "#disagree" ).click(function() {
        var id_praapplication = $('#id_praapplication_term').val();
        var _token = $('#token_term').val();
        var declaration = 3;
        var branch = $('#branch').val();
        var remark = $('#remark_verification').val();
        if (remark=='') {
          //code
           alert("Please fill Remark !");
        }
       else {
        var r = confirm("Are You Sure to Rejected this Application?");
        if (r == true) {
            $.ajax({
                   type: "POST",
                   url: '{{ url('/verifiedMoForm/term/') }}',
                   data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
                   cache: false,
                   beforeSend: function () { 
                   },
                   success: function () {
                       alert("Application Rejected !");
                       location.href='{{url('/')}}/admin';
                   },
                   error: function () {
                   }
               });
         }
       }
    });
</script>


<!-- 
<script type="text/javascript">

    $( ".income" ).keyup(function() {
    var monthly_income = $('#monthly_income').val();
    var other_income = $('#other_income').val();

    var total_income = parseFloat(monthly_income) + parseFloat(other_income);
        $("#total_income").val(parseFloat(total_income).toFixed(2)); 
    });

</script> -->


<script type="text/javascript">
$(document).ready(function() {

var found = [];
    $("select[name='title'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='gender'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='ownership'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='country'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='race'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='bumiputera'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='religion'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='marital'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='education'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='empstatus'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='spouse_emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='purpose_facility'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='type_customer'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='payment_mode'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });
});
</script>

<script type="text/javascript">
    $( "#old_ic" ).change(function() {
        var old_ic = $('#old_ic').val();
        if(old_ic == 'A1340304' | old_ic =='A2124626'  | old_ic =='4999546' | old_ic== 'A1962144'  | old_ic== 'A3563969')
            {
                lert("sorry, your olc ic blocked by system");
                $('#old_ic').val("");
            } 
    });
</script>

<script>
function alertWithoutNotice(message){
    setTimeout(function(){
        alert(message);
    }, 1000);
}
</script>


<!--

  <script type="text/javascript">



$( "#marital" ).change(function() {

    var status = $('#marital').val();

    var spouse_emptype = $('#spouse_emptype').val();

     

    if(status  != 'M' ) {

       

        

        $("#spouse-group :input").attr("disabled", true);

            $("#statuskawin").show();

              $("#spouse_emptype_kosong").show();

            $("#spouse_emptype_others").hide();

             $("#spouse_emptype").hide();

    }

    else {

   

      

        $("#spouse-group :input").attr("disabled", false);

            $("#statuskawin").hide();

            $("#spouse_emptype").show();

            $("#spouse_emptype_kosong").hide();



            if(spouse_emptype  != 'Others (Specify)/ Lain-lain (Nyatakan)') {

       

                $("#spouse_emptype_others").hide();

            }

            else {



                $("#spouse_emptype_others").show();

            }



    }



   

});

</script>

-->






<script type="text/javascript">

    

    var FormStuff = {

  

  init: function() {

    this.applyConditionalRequired();

    this.bindUIActions();

  },

  

  bindUIActions: function() {

    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);

  },

  

  applyConditionalRequired: function() {

    

    $(".require-if-active").each(function() {

      var el = $(this);

      if ($(el.data("require-pair")).is(":checked")) {

        el.prop("required", true);

      } else {

        el.prop("required", false);

      }

    });

    

  }

  

};



FormStuff.init();

</script>



<script type="text/javascript">

function minmax(value, min, max) 

{

    if(parseInt(value) < min || isNaN(value)) 

        return 0; 

    else if(parseInt(value) > max) 

        return max; 

    else return value;

}



function minmax_tempoh(value, min, max) 

{

    if(parseInt(value) < min || isNaN(value)) 

        return 24; 

    else if(parseInt(value) > max) 

        return max; 

    else return value;

}

</script>



<!-- <script type="text/javascript">

 window.onload = function() {

    document.getElementById('ifYes').style.display = 'none';

    document.getElementById('ifNo').style.display = 'none';

} -->
@if($data->marital != '3')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
            $("#spouse_emptype").hide();
            $("#spouse_emptype_others").hide();
            $("#spouse_emptype_kosong").show();
        });
   </script>
@else 
    <script type="text/javascript">
        $(document).ready(function() {
            $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();
            @if($spouse->emptype != '5')
                $("#spouse_emptype_others").hide();
            @else 
                $("#spouse_emptype_others").show();       
            @endif 
        });
  </script>
@endif 

<script type="text/javascript">
$( "#marital" ).change(function() {
    var status = $('#marital').val();
    var spouse_emptype = $('#spouse_emptype').val();
    if(status  != '3' ) {
        $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
              $("#spouse_emptype_kosong").show();
            $("#spouse_emptype_others").hide();
             $("#spouse_emptype").hide();
    }
    else {
        $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();

            if(spouse_emptype  != '5') {
       
                $("#spouse_emptype_others").hide();
            }
            else {

                $("#spouse_emptype_others").show();
            }
    }
});
</script>
<script type="text/javascript">
function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'block';
        document.getElementById('ifNo').style.display = 'none';
        document.getElementById('redhat1').style.display = 'none';
        document.getElementById('aix1').style.display = 'none';
    } 
    else if(document.getElementById('noCheck').checked) {
        document.getElementById('ifNo').style.display = 'block';
        document.getElementById('ifYes').style.display = 'none';
        document.getElementById('redhat1').style.display = 'none';
        document.getElementById('aix1').style.display = 'none';
   }
}
</script>


@if($data->title != '20')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#salutation" ).change(function() {
    var salutation = $('#salutation').val();
     
    if(salutation  != '20' ) {
       
            $("#salutation_others").hide();
    }
    else {
   

            $("#salutation_others").show();
    }

   
});
</script>

@if($empinfo->emptype != 'O5')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#emptype" ).change(function() {
    var emptype = $('#emptype').val();
     
    if(emptype  != '5' ) {
       
            $("#emptype_others").hide();
    }
    else {
   

            $("#emptype_others").show();
    }

   
});
</script>

@if($spouse->emptype != '5')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#spouse_emptype" ).change(function() {
    var emptype = $('#spouse_emptype').val();
     
    if(emptype  != '5') {
       
            $("#spouse_emptype_others").hide();
    }
    else {
   

            $("#spouse_emptype_others").show();
    }

   
});
</script>


@if($financial->product_bundling != '1')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").show();
       
      });
  </script>

@endif 

<script type="text/javascript">

$( "#pb_yes" ).change(function() {
    var pb_yes = $('#pb_yes').val();
      $("#product_bundling_specify").show();
});

$( "#pb_no" ).change(function() {
    var pb_no = $('#pb_no').val();  

     $("#product_bundling_specify").hide();

   
});
</script>

@if($financial->cross_selling != '1')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#cs_yes" ).change(function() {
    var cs_yes = $('#cs_yes').val();
      $("#cross_selling_specify").show();
});


$( "#cs_no" ).change(function() {
    var cs_no = $('#cs_no').val();  

     $("#cross_selling_specify").hide();

   
});

</script>



@if($data->country == '3')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_others").show();
                $("#country_origin").hide();
       
      });
  </script>
  @elseif($data->country == 'Permanent Resident / Penduduk Tetap')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").show();
                $("#country_others").hide();
       
      });
  </script>
  @else 

      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").hide();
                $("#country_others").hide();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#country" ).change(function() {
    var country = $('#country').val();
     
    if(country  == '3') {
       
            $("#country_others").show();
            $("#country_origin").hide();
    }
    else if(country  == 'Permanent Resident / Penduduk Tetap') {
       
            $("#country_origin").show();
            $("#country_others").hide();
       
    }
    else {
   

            $("#country_origin").hide();
            $("#country_others").hide();
    }

   
});
</script>
@if($data->country_dob == '30')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#state_dob").show();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#state_dob").hide();
       
      });
  </script>

@endif 
  <script type="text/javascript">

$( "#country_dob" ).change(function() {
    var country_dob = $('#country_dob').val();
     
    if((country_dob  == '30')) {
       
            $("#state_dob").show();
    }
    else {
   

            $("#state_dob").hide();
    }

   
});
</script>

@if(($data->race == '11') || ($data->race == 'OTL'))
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").show();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").hide();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#race" ).change(function() {
    var race = $('#race').val();
     
    if((race  == '11') ||(race  == 'OTL')) {
       
            $("#race_others").show();
    }
    else {
   

            $("#race_others").hide();
    }

   
});
</script>

@if($data->religion != '6')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#religion" ).change(function() {
    var religion = $('#religion').val();
     
    if(religion  != '6') {
       
            $("#religion_others").hide();
    }
    else {
   

            $("#religion_others").show();
    }

   
});
</script>

<script type="text/javascript">
    $("#namexx").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
</script>
<!--only 1 space-->
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
  
  var input = document.getElementById('namexx');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
  
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $("#fullname").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#name1").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#name2").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#spousename1").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("input#checker").bind("click",function(o){
    if($("input#checker:checked").length){
      $("#corres_address1").val($("#address").val());
      $("#corres_address2").val($("#address2").val());
      $("#corres_address3").val($("#address3").val());
      $("#postcode2").val($("#postcode").val());
      $("#state2").val($("#state").val());
      $("#state_code2").val($("#state_code").val());
    }
    else{
      $("#corres_address1").val("");
      $("#corres_address2").val("");
      $("#corres_address3").val("");
      $("#postcode2").val("");
     $("#state_code2").val("");
    }
    });
  }
  );
</script>


@if($data->nationality != 'NC')
  <script type="text/javascript">
      $(document).ready(function() {
        $("#country_origin").hide();
      });
  </script>
@else 
<script type="text/javascript">
 $(document).ready(function() {
  $("#country_origin").show();
});
  </script>
@endif 

<script type="text/javascript">
$( "#nationality" ).change(function() {
    var nationality = $('#nationality').val();
    if(nationality  != 'NC' ) {
            $("#country_origin").hide();
    }
    else {
        $("#country_origin").show();
    }
});
</script>






















