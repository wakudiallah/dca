<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Personal Financing-i ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>
@extends('layouts.dca.template')
<style type="text/css">
.content-box{
  background-color: #d1d2d4;
      height: auto;
      width: 100%;
      border: 2px solid #0155a2;
      box-sizing: content-box;
      margin-bottom: 10px;
      }
    .content-box>.box-heading {
        color: #fff;
        background-color: #0155a2;
        border-color: #0155a2;
    }
    .content-box .box-heading {
        font-size: 16px;
        text-align: center;
    }
    .box-heading {
        padding: 10px 15px;
    }
    .box-body {
        padding: 8px;
    }
    .box-body h5{
        text-align: center;
        font-size: 16px;
    }
    .box {
        width: 100%;
        margin: 5px auto;
        text-align: center;
    }

    .box .button {
        font-weight: bold;
        font-size: 16px;
        color: #0155a2;
        border: 3px solid grey;
        border-radius: 10px;
        text-decoration: none;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }
    .button:hover {
        background: #808080;
        color: #fff;
        text-decoration: none;
    }

    .titlesmall {
        margin-left:0;
    }
    .retailSection a {
        color: #0055a2;
    }
    .modal-content{background-color: #d1d2d4;}
    a.button.nav-link.app {
        background-color: #0155a2;
        border: 3px solid #0155a2;
        color: #fff;
    }
    a.button.nav-link.app:hover {
        /* background-color: green; */
        color: #d1d2d4;
    }

    .detail{
        margin-bottom: 10px !important;
    }
</style>
<style type="text/css">
    .img-mbsb{
        margin-bottom: 50px !important;
        width: 300px !important;
        height: 100px !important;

        display: block;
      margin-left: auto;
      margin-right: auto;
      width: 50%;
    }
    .left{
        color: #ffffff !important;     
        z-index: 1;
    }
    .content {
        padding-top: 0px !important;
        padding-bottom: 80px;
    }
    .front-right{
        margin-top: 24px !important;

    }
    .front-left{
        margin-top: 180px !important;
        padding-right: 100px !important;
    }
</style>


  <div class="">   <!-- container disable -->
        <div class="content">
            
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                        <div class="front-left">
                            <h2 class="mb40">Welcome to MBSB Bank Online Application Form</h2>
                            @include('layouts.dca.left_front')
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 front-right">
                        <div class="consultantion-form"><br>
                            <img src="https://mbsb.insko.my/public/asset/img/logo_bank.png" class="img-mbsb" >
                            <h2 class="mb30">Please Fill In Your Personal Details</h2>
                        {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">First Name <sup>*</sup></label>
                                <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"   @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif size="55" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name"> New IC Number <sup>*</sup></label>
                                <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has("icnumber"))  value="{{ Session::get('icnumber') }}" @endif class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Handphone Number <sup>*</sup></label>
                                 <input type="number" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" class="form-control fn" step=".01" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                            </div>
                        </div>
                       
                        <div class="col-md-12 wrap-form-control validate-input" data-validate = "IC Number">
                            <div class="form-group">
                                <label class="control-label" for="name">Employment <sup>*</sup></label>
                                <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                    <option value="0">--Select Employment--</option>
                                    @foreach ($employment as $employment)
                                    <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                                    @endforeach
                                </select> 
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Employer <sup>*</sup></label>
                                <input class="form-control" type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Employer"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif maxlength="100">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Monthly Income (Basic Salary + Allowance + Other income) (RM) <sup>*</sup></label>
                               <input id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                            </div>
                        </div>

                        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Allowance (RM) <sup>*</sup></label>
                                <input class="form-control" @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106)|| (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                            </div>
                        </div>-->

                         <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Total Deduction (RM) <sup>*</sup></label>
                                <input id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">Package <sup>*</sup></label>
                                <input class="form-control" type="text" name="package" id="package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @endif  readonly>
                                <input type="hidden" name="package_id" id="package_id" @if (Session::has('package_id'))  value="{{ Session::get('package_id') }}" @else value="1" @endif class="form-control" readonly>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 ">
                            <div class="form-group">
                                <label class="control-label" for="name">Financing Amount(RM) <sup>*</sup></label>
                               <input id="LoanAmount" name="LoanAmount" placeholder="Loan Amount (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >
                            </div>
                        </div> 
                        
                          <div class="col-lg-12 col-md-12">
                            <button type="reset" name="singlebutton" class="btn btn-danger  btn-lg ">Reset</button>
                             <button type="submit" name="singlebutton" class="btn btn-primary  btn-lg ">Calculate</button>
                        </div>
                          
                        </form>
                          
                        
                    </div>
                    <div class="col-md-1"></div>
                    </div>
                
        </div>
    </div>

     @include('praapplication.js')

