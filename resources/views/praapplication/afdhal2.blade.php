<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Personal Financing-i ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>
<style type="text/css">
.content-box{
  background-color: #d1d2d4;
      height: auto;
      width: 100%;
      border: 2px solid #0155a2;
      box-sizing: content-box;
      margin-bottom: 10px;
      }
    .content-box>.box-heading {
        color: #fff;
        background-color: #0155a2;
        border-color: #0155a2;
    }
    .content-box .box-heading {
        font-size: 16px;
        text-align: center;
    }
    .box-heading {
        padding: 10px 15px;
    }
    .box-body {
        padding: 8px;
    }
    .box-body h5{
        text-align: center;
        font-size: 16px;
    }
    .box {
        width: 100%;
        margin: 5px auto;
        text-align: center;
    }

    .box .button {
        font-weight: bold;
        font-size: 16px;
        color: #0155a2;
        border: 3px solid grey;
        border-radius: 10px;
        text-decoration: none;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }
    .button:hover {
        background: #808080;
        color: #fff;
        text-decoration: none;
    }

    .titlesmall {
        margin-left:0;
    }
    .retailSection a {
        color: #0055a2;
    }
    .modal-content{background-color: #d1d2d4;}
    a.button.nav-link.app {
        background-color: #0155a2;
        border: 3px solid #0155a2;
        color: #fff;
    }
    a.button.nav-link.app:hover {
        /* background-color: green; */
        color: #d1d2d4;
    }

    .detail{
        margin-bottom: 10px !important;
    }
</style>


<div id="main" role="main">
    <br><br><br><br>
    <!-- MAIN CONTENT -->
    <div id="content" class="container" style=" max-width: 100% !important;margin: 0 auto;">
        @include('sweetalert::alert')  
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

        <div class="row" style="margin-bottom: 30px !important">
            <div class="col-sm-12 col-md-12 col-lg-12 ">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"><img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/1.jpg" width="1200px" height="1200px" alt="First slide"/></a>
                        </div>
                        <div class="item">
                           <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"> <img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/3.jpg" width="1200px" height="1200px" alt="Third slide"/></a>
                        </div>
                        <div class="item">
                           <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"> <img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/5.png" width="1200px" height="1200px" alt="Forth slide"/></a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="">
            @include('auth.login_modal') 
        </div>

        <div class="row">
              <?php if (!(Auth::user())) {?>
            <div class="col-sm-12 col-md-12 col-lg-6 hidden-xs">
                 <?php } ?>
                  <?php if ((Auth::user())) {?>
                     <div class="col-sm-12 col-md-12 col-lg-12 hidden-xs">
                 <?php } ?>
               <h2>Personal Financing-<i>i</i></h2>
                    <p>Learn about our Shariah compliant personal financing solutions through our host of attractive packages that best suit your financing needs.</p>
                    <ul>
                        <li>Competitive financing rates</li>
                        <li>Financing amount of up to RM400,000</li>
                        <li>Financing tenure of up to 10 years</li>
                        <li>No guarantor required</li>
                        <li>No hidden charges</li>
                        <li>Fast approval</li>
                        <li>Applicable for public servant and private sector employees</li>
                    </ul>
                <div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="content-box">
                            <div class="box-heading">Mumtaz<i>-i</i></div>
                            <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount of up to RM250,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="content-box">
                            <div class="box-heading">Afdhal-<i>i</i></div>
                                <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount of up to RM400,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="content-box">
                            <div class="box-heading">Private Sector<i>-i</i></div>
                            <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount up to RM300,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if (!(Auth::user())) {?>
                <div class="col-sm-12 col-md-12 col-lg-6 ">
                    <div class="well no-padding">
                        {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                            <header>
                                <p class="txt-color-white"><b>Check Financing Eligibility</b> </p>
                            </header>

                              <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-12 col-md-12 col-lg-12">
                            <section class="col col-12 col-lg-12 col-md-12 col-xs-12">
                                <label class="label"> <b> Full Name &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name" @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif size="55">
                                    <b class="tooltip tooltip-bottom-right">FullName</b>
                                </label>
                            </section>
                        </div>
                    </div>
                     <input  name="type" id="type" value="IN"  type="hidden"  >
                    <div class="row">
                       
                        <div class="col-xs-6 col-12" id="ifNewIc" style="display: show">
                             <section class="col col-12 col-xs-12 col-md-12">
                            <label class="label"> <b> IC Number &nbsp;<sup>*</sup></b></label>
                            <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                <i class="icon-append fa fa-user"></i>
                                <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="Ex: 931111120101"  @if (Session::has("icnumber"))  value="{{ Session::get('icnumber') }}" @endif >
                                <b class="tooltip tooltip-bottom-right">IC Number</b>
                            </label>
                        </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Handphone Number &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                

                     <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="PhoneNumber" name="PhoneNumber" minlength="12" maxlength="12" placeholder="Ex: 011000000"  @if (Session::has("phone"))  value="{{ Session::get('phone') }}" @endif >
                                    <b class="tooltip tooltip-bottom-right">Handphone Number</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                       
                        <div class="col-xs-12 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Employment &nbsp;<sup>*</sup></b></label>
                                <label class="select">
                                    <select  name="Employment" id="Employment" class="select2"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                        <option value="0">--Select Employment--</option>
                                        @foreach ($employment as $employment)
                                        <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                                        @endforeach
                                    </select> <i></i>
                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                 </label>
                            </section>
                         </div>
                    </div>
                </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Employer &nbsp;<sup>*</sup></b></label>

                                    <label id="majikan2" class="input @if (Session::has('employer')) state-error @endif ">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Employer"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif maxlength="100">
                                        <b class="tooltip tooltip-bottom-right"> Employer</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Basic Salary (RM) &nbsp;<sup>*</sup></b>  </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif  id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                                        <b class="tooltip tooltip-bottom-right">Basic Salary (RM) </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Allowance (RM) &nbsp;<sup>*</sup></b></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                          <input @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  >
                                        <b class="tooltip tooltip-bottom-right">Allowance (RM)</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"><b>Total Deduction in payslip only (RM) &nbsp;<sup>*</sup></b> </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"   >
                                        <b class="tooltip tooltip-bottom-right"> Total Deduction (RM)  </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Package &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <input type="text" name="package" id="package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @endif class="form-control" readonly>

                                  

                                    <input type="hidden" name="package_id" id="package_id" @if (Session::has('package_id'))  value="{{ Session::get('package_id') }}" @else value="1" @endif class="form-control" readonly>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b>Financing Amount(RM) &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-credit-card"></i>
                                        <input id="LoanAmount" name="LoanAmount" placeholder="Financing Amount (RM)" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  type="text" required=""   class="fn" step=".01"  @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >
                                        <b class="tooltip tooltip-bottom-right">Financing Amount (RM)</b>
                                </label>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </section>
                        </div>
                    </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                         <b>   Calculate  </b>
                        </button>
                        <div id="response"></div>
                    </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
                <?php } ?>
                    </div>
                </div>
        </div>
    </div>

    
   @include('layouts.js')

