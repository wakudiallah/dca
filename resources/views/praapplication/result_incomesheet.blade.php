<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "MBSB Personal Financing-i";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
           @if (Session::has('message'))
    
                    <script>
                        function pesan() {
                            bootbox.alert("<b>{{Session::get('message')}}</b>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>
            @endif             
            @if (count($errors) > 0)
                <script>
                     function pesan() {
                          bootbox.alert("<b>@foreach ($errors->all() as $error) {{ $error }} <br> @endforeach</b>");
                      }
                      window.onload = pesan;

                      
              </script>

          @endif
           
          
         <div class="row">
          <div class="col-xs-11 col-sm-12 col-md-3 col-lg-3">


        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">


    <br>
                
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
            <div class="well no-padding">

                              @foreach($pra as $pra)

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
                        
    <div class='smart-form client-form'id='smart-form-register2'>
        <header> <p class="txt-color-white"><b>   Financing Eligibility </b> </p> </header>
        <form action='{{url('/')}}/praapplication/{{$id}}' method='post' enctype="multipart/form-data">
            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <label class="label"> <b>  Package </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <b><input type="text" id="Package2" value="{{$pra->package->name}}"  name="Package2" placeholder="Package" disabled="disabled"></b>
                            <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id_pra}}" >
                             <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <b class="tooltip tooltip-bottom-right">Package</b>
                        </label>
                    </section>
                    <section class="col col-6">
                        <label class="label"> <b> <font color="red" size="2.5" > Financing amount up to  </font>  </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>

                            @foreach($loan as $loan)
                            <?php
                        
 function pembulatan($uang) {
                    $puluhan = substr($uang, -3);
                        if($puluhan<500) {
                            $akhir = $uang - $puluhan; 
                        } 
                        else {
                            $akhir = $uang - $puluhan;
                        }
                    return $akhir;
                }
                       $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                    $ndi = ($zbasicsalary - $zdeduction) -  1300;
                    $max  =  $salary_dsr * 12 * 10 ;

                        if(!empty($get_package->max_financing))  {
                            $ansuran = intval($salary_dsr)-1;
                               
                                    $bunga = $get_package->flat_rate/100;
                                   //$bunga = $pra->package->effective_rate / 100;
                                
                               
                            $pinjaman = 0;

                               for ($i = 0; $i <= $get_package->max_financing; $i++) {
                              $bungapinjaman = $i  * $bunga * $durasi ;
                              $totalpinjaman = $i + $bungapinjaman ;
                              $durasitahun = $durasi * 12;
                              $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                              if ($ansuran2 < $ndi)
                              {
                                  $pinjaman = $i;
                                 
                              }
                            
                          }   

                                if($pinjaman > 1) {
                                    $bulat = pembulatan($pinjaman);
                                    $loanx =   number_format((float)$bulat, 0 , ',' , ',' ) ;  
                                    $loanz = $bulat;
                                }
                                else {
                                    $loanx =   number_format((float)$get_package->max_financing, 0 , ',' , ',' ) ; 
                                    $loanz = $get_package->max_financing;
                                }
                            }
                        else 
                        { 
                            $bulat = pembulatan(10 * $total_salary);
                            $loanx =   number_format((float)$bulat, 0 , ',' , ',' ) ; 
                            $loanz = $bulat;
                            
                                $loanx =   number_format((float)$loanz, 0 , ',' , ',' ) ; 
                            

                        }
                            ?>
                                    <!--{{$pra->package->effective_rate}}-->
                            @endforeach
                            @if($pra->id_package!=3)
                            <b><input readonly type="text" id="MaxLoan"  
                                value=" RM {{$get_package->max_financing}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                 class="merah" requierd> </b>
                                <input readonly type="hidden" id="maxloanz"  
                                value="{{$get_package->max_financing}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                @else
                                <?php
                                  $max_other = $total_salary *10;
                                ?>
                                <input readonly type="text" id="MaxLoan"  
                                value=" RM {{$max_other}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                 class="merah" requierd> </b>
                                <input readonly type="hidden" id="maxloanz"  
                                value="{{$max_other}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                @endif
                        </label>
                    </section>
                </div>

                <div class="row">
                    <section class="col col-12">
                        <label class="label"><b>  Financing Amount (RM) </b> </label>
                        <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}"  placeholder="RM " 
                            onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                            <b class="tooltip tooltip-bottom-right">Financing Amount </b>
                        </label>
                    </section>
                                        
                    <section class="col col-12">
                        <label class="label"> <b>Total income</b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" id="pendapatan" value="RM {{  number_format((float)$total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                            <b class="tooltip tooltip-bottom-right">Total income</b>
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </label>
                    </section>
                    <section class="col col-12">
                        <label class="label"> <b>Maximum Installment </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" id="ansuran_maksima" value="RM {{  number_format((float)$ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Maximum Installment" readonly>
                            <b class="tooltip tooltip-bottom-right">Maximum Installment</b>
                        </label>
                    </section>
                    <section class="col col-6">
                        <label class="label"> &nbsp; </label>
                            <footer>
                                <button class="btn btn-primary " type="submit">
                                    <b> Recalculate </b>
                                </button>
                            </footer>
                    </section>
                </div>
            </fieldset>
        </form> 
        @if($pra->loanamount <= $loanz)
        <fieldset>
            <table class="table table-bordered table-hover" border='1'>
                <thead>
                    <tr>
                        <th valign="middle"><b>Tenures</b></th>
                        <th class="hidden-xs"><b>Financing Amount</b></th>
                        <th><b>Monthly installment</b></th>
                        <th><b>Choose </b></th>
                     </tr>
                </thead>
                <tbody>
                    <?php
                       
                        $amt = $pra->loanamount;
                        $loans = $pra->loanamount;
                        $i=(($pra->package->effective_rate));
                        $i2=($pra->package->flat_rate);

                        $in=(($pra->package->effective_rate));
                        $in2=($pra->package->flat_rate);

                        $year2 = 2;
                        $year3 = 3;
                        $year4 = 4;
                        $year5 = 5;
                        $year6 = 6;
                        $year7 = 7;
                        $year8 = 8;
                        $year9 = 9;
                        $year10 =10;

                         function pmt2( $amt , $i2, $year2 ) {

                        $int = $i2/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year2);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt3( $amt , $i2, $year3 ) {

                        $int = $i2/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year3);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }


                         function pmt4( $amt , $i, $year4 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year4);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt5( $amt , $i, $year5 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year5);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt6( $amt , $i, $year6 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year6);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt7( $amt , $i, $year7 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year7);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt8( $amt , $i, $year8 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year8);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt9( $amt , $i, $year9 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year9);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                        function pmt10( $amt , $i, $year10 ) {

                        $int = $i/1200;
                        $int1 = 1+$int;
                        $r1 = pow($int1, $year10);

                        $pmt = $amt*($int*$r1)/($r1-1);

                            return $pmt;

                        }

                     

                        $amount2 =  round(pmt2( $loans, $in2, $year2*12),0);
                        $amount3 =  round(pmt3( $loans, $in2, $year3*12),0);
                        $amount4 =  round(pmt4( $loans, $in, $year4*12),0);
                        $amount5 =  round(pmt5( $loans, $in, $year5*12),0);
                        $amount6 =  round(pmt6( $loans, $in, $year6*12),0);
                        $amount7 =  round(pmt7( $loans, $in, $year7*12),0);
                        $amount8 =  round(pmt8( $loans, $in, $year8*12),0);
                        $amount9 =  round(pmt9( $loans, $in, $year9*12),0);
                        $amount10 =  round(pmt10( $loans, $in, $year10*12),0);
                       
                        
                        
                       
                    ?>

                  

                    <?php
                        if(($amount2 <= $ndi)){
                            $y = '2';
                        }
                        elseif(($amount3 <= $ndi)){
                             $y = '3';
                        }
                        elseif(($amount4 <= $ndi)){
                             $y = '4';
                         }
                        elseif(($amount5 <= $ndi)){
                             $y = '5';
                         }
                        elseif(($amount6 <= $ndi)){
                             $y = '6';
                         }
                        elseif(($amount7 <= $ndi)){
                             $y = '7';
                         }
                        elseif(($amount8 <= $ndi)){
                             $y = '8';
                         }
                        elseif(($amount9 <= $ndi)){
                             $y = '9';
                         }
                        elseif(($amount10 <= $ndi)){
                             $y = '10';
                        }
                        else{
                            $y='0';
                        }
                    ?>
                @if($y!=0)
                    <?php for ($x = $y; $x <= 10; $x++) {  ?>
                    <tr>
                        <td>@if(($x==2) AND ($amount2 <= $ndi))
                                2 years
                            @elseif(($x==3) AND ($amount3 <= $ndi))
                                3 years
                            @elseif(($x==4) AND ($amount4 <= $ndi))
                                4 years
                            @elseif(($x==5) AND ($amount5 <= $ndi))
                                5 years
                            @elseif(($x==6) AND ($amount6 <= $ndi))
                                6 years
                            @elseif(($x==7) AND ($amount7 <= $ndi))
                                7 years
                            @elseif(($x==8) AND ($amount8 <= $ndi))
                                8 years
                            @elseif(($x==9) AND ($amount9 <= $ndi))
                                9 years
                            @elseif(($x==10) AND ($amount10 <= $ndi))
                                10 years
                            @endif</td>
                        <td class="hidden-xs"> RM {{  number_format((float) $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                        <td>
                            @if($x==2)
                                RM {{$amount2}}
                            @elseif($x==3)
                                RM {{$amount3}}
                            @elseif($x==4)
                                RM {{$amount4}}
                            @elseif($x==5)
                                RM {{$amount5}}
                            @elseif($x==6)
                                RM {{$amount6}}
                            @elseif($x==7)
                                RM {{$amount7}}
                            @elseif($x==8)
                                RM {{$amount8}}
                            @elseif($x==9)
                                RM {{$amount9}}
                            @elseif($x==10)
                                RM {{$amount10}}
                            @endif
                        </td>
                        <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="{{$x}}" required>
                        <input type="hidden" name="rates" id="rates" class="rates" value="{{$i}}">
                        <input type="hidden" name="rate" id="rate" class="rate" value="{{$i2}}">
                        <input type="hidden" name="amount2" id="amount2" class="amount2" value="{{$amount2}}">
                        <input type="hidden" name="amount3" id="amount3" class="amount3" value="{{$amount3}}">
                        <input type="hidden" name="amount4" id="amount4" class="amount4" value="{{$amount4}}">
                        <input type="hidden" name="amount5" id="amount5" class="amount5" value="{{$amount5}}">
                        <input type="hidden" name="amount6" id="amount6" class="amount6" value="{{$amount6}}">
                        <input type="hidden" name="amount7" id="amount7" class="amount7" value="{{$amount7}}">
                        <input type="hidden" name="amount8" id="amount8" class="amount8" value="{{$amount8}}">
                        <input type="hidden" name="amount9" id="amount9" class="amount9" value="{{$amount9}}">
                        <input type="hidden" name="amount10" id="amount10" class="amount10" value="{{$amount10}}">
                         <input type="hidden" name="interests" id="interests" class="interests" value="{{$pra->package->effective_rate}}">
                        </td>
                    </tr>
                   
                   
                    <?php } ?>
                    @else

                     @endif
                </tbody>
            </table> 
        </fieldset>
        @else
        <?php
       

            return redirect('praapplication/'.$id)->with('message', 'Sorry, you are not qualified, Please Input New Financing Amount');
        ?>
        @endif

        <i style="font-size: 12px!important; font-family: Arial,Helvetica,sans-serif !important;margin-left: 12px">  <sup>*</sup>{{ number_format((float)$pra->package->effective_rate, 2 )}} % p.a (Effective Rate) / {{ number_format((float)$pra->package->flat_rate, 2 )}} % p.a (Flat Rate equivalent for 3 years)</i><br>
        <i style="font-size: 12px;font-family: Arial,Helvetica,sans-serif !important; margin-left: 12px">  <sup>*</sup>MBSB Bank’s current Base Rate (BR) is {{ number_format((float)$pra->package->base_rate, 2 )}}% p.a. and ceiling profit rate is 15%</i>
        <fieldset>
            <div class="form-group">
                <input type='checkbox' id='terms'  name='terms' value='1' required="required" style="font-size: 12px!important;font-family: Arial,Helvetica,sans-serif !important; text-align: justify!important">&nbsp;I /we express consent and authorize MBSB Bank to process any information that I/we have provided to MBSB Bank for purpose of crosselling marketing and promotion.<br>
            </div>
        <fieldset>
            <footer>
                <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                    <b> Submit</b>
                </button>
            </footer>
        <div>
    </form>
</div>
                        
                    
                    </div>
                    
                    
                </div>
                                
                
            </div>
        

        </div>
        

        </div>
    </div>
           


        <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Register</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                     <input name="id_pras" id="id_pra" type="hidden"  value="{{$pra->id_pra}}" >
                     <input type="hidden" id="ic" name="ic" value="{{$pra->icnumber}}" >
                        <fieldset>
                            <section >
                                <label class="label"><br>E-mail</label>
                                <label class="input">
                                    <i class="icon-append fa fa-envelope"></i>
                                    <input type="hidden" name="idpra" value="{{$pra->id}}">
                                    <input type="email" id="email" name="Email" required placeholder="Email" >
                                    <b class="tooltip tooltip-bottom-right">E-mail</b>
                                </label>
                            </section>
                            <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >
                            @endforeach     
                            <section>
                                <label class="label">Password</label>
                                <label class="input">
                                    <i class="icon-append fa fa-key "></i>
                                    <input type="Password" required id="password" name="password" placeholder="Password" autocomplete="off">
                                    <b class="tooltip tooltip-bottom-right">
                                    Must be between 8 – 36 characters long </b>
                                </label>
                            </section>
                            <section>
                                <label class="label">Password Confirmation</label>
                                <label class="input">
                                    <i class="icon-append fa fa-key "></i>
                                    <input type="Password" id="password_confirmation" name="password_confirmation" required placeholder="Password Confirmation" autocomplete="off">
                                    <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                                </label>
                            </section>
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                            Register
                        </button>
                    {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal -->
     

<!-- ==========================CONTENT ENDS HERE ========================== -->

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                email : {
                    required : true,
                    validate_email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36,
                    pattern:/^\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])\d{6}$/
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    validate_email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password',
                    pattern:'Please enter a valid New IC Number'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>

<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var maxloanz = $('#maxloanz').val();
    var package = $('#Package2').val();
    var loanamount = $('#LoanAmount2').val();
     var interests = $('#interests').val();
    if(tenure==2){
         var installment = $('#amount2').val();
        var rate = $('#rate').val();
    } 
    else if(tenure==3){
        var installment = $('#amount3').val();
        var rate = $('#rate').val();
    }
    else if(tenure==4){
        var installment = $('#amount4').val();
        var rate = $('#rates').val();
    }
    else if(tenure==5){
        var installment = $('#amount5').val();
        var rate = $('#rates').val();
    }
    else if(tenure==6){
        var installment = $('#amount6').val();
        var rate = $('#rates').val();
    }
    else if(tenure==7){
        var installment = $('#amount7').val();
        var rate = $('#rates').val();
    }
    else if(tenure==8){
        var installment = $('#amount8').val();
        var rate = $('#rates').val();
    }
    else if(tenure==9){
        var installment = $('#amount9').val();
        var rate = $('#rates').val();
    }
    else if(tenure==10){
        var installment = $('#amount10').val();
        var rate = $('#rates').val();
    }

  $.ajax({
        type: "PUT",
        url: '{{ url('/form/') }}'+'/99',
        data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, maxloan : maxloanz, package : package, rate : rate, installment : installment,loanamount:loanamount,interests:interests
        },
        success: function (data, status) {
        }
    });
});
</script>

<script type="text/javascript">

    $(document).ready(function() {

    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var LoanAmount = $('#LoanAmount2').val();
    var maxloanz = $('#maxloanz').val();
      
    $.ajax({
        type: "PUT",
        url: '{{ url('/form/') }}'+'/9',
        data: { id_praapplication: id_praapplication,  _token : _token, loanammount : LoanAmount, maxloan : maxloanz 
        },
        success: function (data, status) {
        }
        });
    });
</script>

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    
<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
    var tenure = $('input[name=tenure]:checked').val();
    var terms = $('input[name=terms]:checked').val();
    if(tenure>0){
        if(terms>0){
            $('#myModal').modal('show');
        }
        else{
             bootbox.alert('Please check consent box if you want to proceed');
        }
    }else{
         bootbox.alert('Please Select Tenure');
    }
   
    });
});
</script>


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


