<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Personal Financing-i ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>

<div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     
    @include('sweetalert::alert')

    
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">


           
          <div class="">
            @include('auth.login_modal') 
        </div>
	<div class="row">
		<div class="col-xs-10 col-sm-10 col-md-6 col-lg-3"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="well no-padding">
                {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                <header>
                  <p class="txt-color-white"><b>  Check Financing Eligibility  </b> </p>
                </header>

                <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-12 col-md-12 col-lg-12">
                            <section class="col col-12 col-lg-12 col-md-12 col-xs-12">
                                <label class="label"> <b> Full Name &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif size="55">
                                    <b class="tooltip tooltip-bottom-right">FullName</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> ID Type &nbsp;<sup>*</sup></b></label>
                                  <label class="input">
                                    <i class="icon-append fa fa-id-card"></i>
                                    <input readonly="" disabled name="" id="" value="NEW IC - 12 NUMERIC"  size="55" >
                                    <input  name="type" id="type" value="IN"  type="hidden"  >
                                    <b class="tooltip tooltip-bottom-right">Full Name</b>
                                </label>
                              
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifNewIc" style="display: show">
                             <section class="col col-12 col-xs-12 col-md-12">
                            <label class="label"> <b> IC Number &nbsp;<sup>*</sup></b></label>
                            <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                <i class="icon-append fa fa-user"></i>
                                <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="Ex: 931111120101"  @if (Session::has("icnumber"))  value="{{ Session::get('icnumber') }}" @endif >
                                <b class="tooltip tooltip-bottom-right">IC Number</b>
                            </label>
                        </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifOther" style="display: none">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Other ID Type &nbsp;<sup>*</sup></b></label>
                                <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                  <i class="icon-append fa fa-user"></i>
                                    <input  type="text" id="others" onkeyup="this.value = this.value.toUpperCase()" name="other"  placeholder="Other ID Type"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="12" required="" >
                                    <b class="tooltip tooltip-bottom-right">Other ID Type</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12" id="ifDOB" style="display: none">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b>DOB (dd/mm/yyyy)</b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                    <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value=""  class="form-control startdate" id="dob" required=""/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifgender" style="display: none">
                             <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Gender </b></label>
                                <label class="select">
                                    <select name="gender" id="gender" class="form-control" required="">
                                        <!--<option>--Select--</option>-->
                                        <option value="11">Male</option>
                                        <option value="22">Female</option>
                                    </select> <i></i>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Handphone Number &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                

                     <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="PhoneNumber" name="PhoneNumber" minlength="12" maxlength="12" placeholder="Ex: 011000000"  @if (Session::has("phone"))  value="{{ Session::get('phone') }}" @endif >
                                    <b class="tooltip tooltip-bottom-right">Handphone Number</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Employment &nbsp;<sup>*</sup></b></label>
                                <label class="select">
                                    <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                        <option value="0">--Select Employment--</option>
                                        @foreach ($employment as $employment)
                                        <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                                        @endforeach
                                    </select> <i></i>
                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                 </label>
                            </section>
                         </div>
                    </div>
                </fieldset>
                    <fieldset>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Employer &nbsp;<sup>*</sup></b></label>

                                    <label id="majikan2" class="input @if (Session::has('employer')) state-error @endif ">
                                        <i class="icon-append fa fa-briefcase"></i>
                                        <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Employer"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif maxlength="100">
                                        <b class="tooltip tooltip-bottom-right"> Employer</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Basic Salary (RM) &nbsp;<sup>*</sup></b>  </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif  id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                                        <b class="tooltip tooltip-bottom-right">Basic Salary (RM) </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"> <b> Allowance (RM) &nbsp;<sup>*</sup></b></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                          <input @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  >
                                        <b class="tooltip tooltip-bottom-right">Allowance (RM)</b>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-6 col-12">
                                <section class="col col-12 col-xs-12 col-md-12">
                                    <label class="label"><b> Total Deduction in payslip only (RM) &nbsp;<sup>*</sup></b> </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-credit-card"></i>
                                        <input @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"   >
                                        <b class="tooltip tooltip-bottom-right"> Total Deduction (RM)  </b>
                                    </label>
                                </section>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b> Package &nbsp;<sup>*</sup></b> </label>
                                <label class="input">
                                    <input type="text" name="package" id="package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @endif class="form-control" readonly>

                                  

                                    <input type="hidden" name="package_id" id="package_id" @if (Session::has('package_id'))  value="{{ Session::get('package_id') }}" @else value="1" @endif class="form-control" readonly>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-xs-12 col-md-12">
                                <label class="label"> <b>Financing Amount(RM) &nbsp;<sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-credit-card"></i>
                                        <input id="LoanAmount" name="LoanAmount" placeholder="Financing Amount (RM)" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  type="text" required=""   class="fn" step=".01"  @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >
                                        <b class="tooltip tooltip-bottom-right">Financing Amount (RM)</b>
                                </label>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </section>
                        </div>
                    </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                         <b>   Calculate  </b>
                        </button>
                        <div id="response"></div>
                    </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
            
                                   
        </div>
      </div>

    </div></div>
     
@include('layouts.js')