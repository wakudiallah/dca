    @extends('layouts.dca.template')

    @section('content')



    <style type="text/css">
        .btn-recalculate{
            margin-bottom: 100px !important;
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #ffffff;
            opacity: 1;
        }
    </style>


    @foreach($pra as $pra)

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
    

    @include('layouts.dca.menu')

    

    <!-- services -->
     <div class="content">
        <div class="">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30 hidden-xs" >
                    <div class="front-left">
                        

                        @include('layouts.dca.left_front')
                     </div>
                </div>


                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb30">
                    <div class="consultantion-form">
                        
                        <form action='{{url('/')}}/praapplication/{{$id}}' method='post' enctype="multipart/form-data">
                            <fieldset>
                                <div class="row">
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                          <label class="control-label" for="name">Package <sup>*</sup></label>
                                            <input type="text" id="Package2" value="{{$pra->package->name}}"  name="Package2" placeholder="Package" disabled="disabled" class="form-control"></b>
                                            <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id_pra}}" >
                                             <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                           <label class="control-label" for="name">Financing Amount up to (RM) <sup>*</sup></label>
                                       
                                    @foreach($loan as $loan)
                                        <?php
                                        
                                        function pembulatan($uang) {
                                            $puluhan = substr($uang, -3);
                                                if($puluhan<500) {
                                                    $akhir = $uang - $puluhan; 
                                                } 
                                                else {
                                                    $akhir = $uang - $puluhan;
                                                }
                                            return $akhir;
                                        }
                                       $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                        $ndi = ($zbasicsalary - $zdeduction) -  1300;
                                        $max  =  $salary_dsr * 12 * 10 ;

                                        if(!empty($get_package->max_financing))  {
                                            $ansuran = intval($salary_dsr)-1;
                                                $bunga = $get_package->flat_rate/100;
                                               //$bunga = $pra->package->effective_rate / 100;
                                                
                                               
                                            $pinjaman = 0;

                                               for ($i = 0; $i <= $get_package->max_financing; $i++) {
                                              $bungapinjaman = $i  * $bunga * $durasi ;
                                              $totalpinjaman = $i + $bungapinjaman ;
                                              $durasitahun = $durasi * 12;
                                              $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                              if ($ansuran2 < $ndi)
                                              {
                                                  $pinjaman = $i;
                                                 
                                              }
                                            
                                          }   

                                                if($pinjaman > 1) {
                                                    $bulat = pembulatan($pinjaman);
                                                    $loanx =   number_format((float)$bulat, 0 , ',' , ',' ) ;  
                                                    $loanz = $bulat;
                                                }
                                                else {
                                                    $loanx =   number_format((float)$get_package->max_financing, 0 , ',' , ',' ) ; 
                                                    $loanz = $get_package->max_financing;
                                                }
                                            }
                                        else 
                                        { 
                                            $bulat = pembulatan(10 * $total_salary);
                                            $loanx =   number_format((float)$bulat, 0 , ',' , ',' ) ; 
                                            $loanz = $bulat;
                                            
                                                $loanx =   number_format((float)$loanz, 0 , ',' , ',' ) ; 
                                            

                                        }
                                            ?>
                                                    <!--{{$pra->package->effective_rate}}-->
                                            @endforeach
                                          
                                            @if($pra->package->is_time_salary==0)
                                            <b><input readonly type="text" id="MaxLoan"  
                                                value=" RM {{  number_format((float)$get_package->max_financing, 2 , ',' , ',' )}}" class="form-control" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                                 class="merah" requierd> </b>
                                                <input readonly type="hidden" id="maxloanz"  
                                                value="{{$get_package->max_financing}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                                @else
                                                <?php
                                                  $max_other = $total_salary *$pra->package->time_salary;
                                                ?>
                                                <input readonly type="text" id="MaxLoan"  
                                                value=" RM {{  number_format((float)$max_other, 2 , ',' , ',' )}}" class="form-control" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                                 class="merah" requierd> </b>
                                                <input readonly type="hidden" id="maxloanz"  
                                                value="{{$max_other}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                                @endif
                                         </div>
                                    </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="name" style="color: red">Financing Amount (RM) <sup>*</sup></label>
                                       
                                         <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}"  placeholder="RM " 
                                            onkeyup="this.value = minmax(this.value, 0, {{$loanz}})" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                         <label class="control-label" for="name">Total Income (RM) <sup>*</sup></label>
                                        <input type="text" id="pendapatan" value="RM {{  number_format((float)$total_salary, 2 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled" class="form-control">
                                    </div>
                                </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                          <label class="control-label" for="name">Maximum Installment (RM) <sup>*</sup></label>
                                        <input type="text" id="ansuran_maksima" value="RM {{  number_format((float)$ndi, 2 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Maximum Installment" readonly class="form-control">
                                    </div>
                                </div>
                                
                            
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" name="singlebutton" class="btn btn-primary btn-lg btn-recalculate">Recalculate</button>
                                </div>
                              
                                   
                                </div>
                            </fieldset>
                        </form> 
                            @if($pra->loanamount <= $loanz)
                            <fieldset>
                                <table class="table table-bordered table-hover" border='1'>
                                    <thead>
                                        <tr>
                                            <th valign="middle" align="center"><b>Tenures</b></th>
                                            <!--<th class="" align="center" ><b>Financing Amount</b></th>-->
                                            <th align="center"><b>Monthly installment</b></th>
                                            <th align="center"><b>Choose</b></th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $total_loan = $pra->loanamount;
                                            $loans = $total_loan;
                                            $interest =$pra->package->flat_rate;
                                            $interest1 =$pra->package->effective_rate/12*10;
                                            $interest11 =   number_format((float)$interest1, 2 ) ;
                                            $interest3 =(($pra->package->effective_rate/12)+0.026)*10;
                                            $interest31 =   number_format((float)$interest3, 2 ) ; 

                                            $year2 = 2;
                                            $year3 = 3;
                                            $year4 = 4;
                                            $year5 = 5;
                                            $year6 = 6;
                                            $year7 = 7;
                                            $year8 = 8;
                                            $year9 = 9;
                                            $year10 =10;

                                           function pmt2($interest, $year2, $loans) {
                                               $months = 12*$year2;
                                               $interests = $interest / 1200;
                                               $amount2 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount2,0,",","");
                                            }

                                            function pmt3($interest, $year3, $loans) {
                                               $months = 12*$year3;
                                               $interests = $interest / 1200;
                                               $amount3 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount3,0,",","");
                                            }
                                            function pmt4($interest3, $year4, $loans) {
                                               $months = 12*$year4;
                                               $interests = $interest3 / 1000;
                                               $amount4 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount4,0,",","");
                                            }
                                            function pmt5($interest3, $year5, $loans) {
                                               $months = 12*$year5;
                                               $interests = $interest3 / 1000;
                                               $amount5 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount5,0,",","");
                                            }
                                             function pmt6($interest3, $year6, $loans) {
                                               $months = 12*$year6;
                                               $interests = $interest3 / 1000;
                                               $amount6 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount6,0,",","");
                                            }
                                             function pmt7($interest3, $year7, $loans) {
                                               $months = 12*$year7;
                                               $interests = $interest3 / 1000;
                                               $amount7 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount7,0,",","");
                                            }
                                             function pmt8($interest3, $year8, $loans) {
                                               $months = 12*$year8;
                                               $interests = $interest3 / 1000;
                                               $amount8 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount8,0,",","");
                                            }
                                             function pmt9($interest3, $year9, $loans) {
                                               $months = 12*$year9;
                                               $interests = $interest3 / 1000;
                                               $amount9 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                                               return  number_format((float)$amount9,0,",","");
                                            }
                                             function pmt10($interest3, $year10, $loans) {
                                               $months = 12*$year10;
                                               $interests = $interest3 / 1000;
                                               $amount10 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));

                                               return  number_format((float)$amount10,0,",","");
                                            }

                                            $amount2 = pmt2($interest, $year2, $loans);
                                            $amount3 = pmt3($interest, $year3, $loans);
                                            $amount4 = pmt4($interest3, $year4, $loans);
                                            $amount5 = pmt5($interest3, $year5, $loans);
                                            $amount6 = pmt6($interest3, $year6, $loans);
                                            $amount7 = pmt7($interest3, $year7, $loans);
                                            $amount8 = pmt8($interest3, $year8, $loans);
                                            $amount9 = pmt9($interest3, $year9, $loans);
                                            $amount10 = pmt10($interest3, $year10, $loans);
                                            
                                            
                                            //dd($amount2);
                                           
                                        ?>

                                        <?php
                                            if(($amount2 <= $ndi)){
                                                $y = '2';
                                            }
                                            elseif(($amount3 <= $ndi)){
                                                 $y = '3';
                                            }
                                            elseif(($amount4 <= $ndi)){
                                                 $y = '4';
                                             }
                                            elseif(($amount5 <= $ndi)){
                                                 $y = '5';
                                             }
                                            elseif(($amount6 <= $ndi)){
                                                 $y = '6';
                                             }
                                            elseif(($amount7 <= $ndi)){
                                                 $y = '7';
                                             }
                                            elseif(($amount8 <= $ndi)){
                                                 $y = '8';
                                             }
                                            elseif(($amount9 <= $ndi)){
                                                 $y = '9';
                                             }
                                            elseif(($amount10 <= $ndi)){
                                                 $y = '10';
                                            }
                                            else{
                                                $y='0';
                                            }
                                        ?>
                                    @if($y!=0)
                                        <?php for ($x = $y; $x <= 10; $x++) {  ?>
                                        <tr>
                                            <td>@if(($x==2) AND ($amount2 <= $ndi))
                                                    2 years
                                                @elseif(($x==3) AND ($amount3 <= $ndi))
                                                    3 years
                                                @elseif(($x==4) AND ($amount4 <= $ndi))
                                                    4 years
                                                @elseif(($x==5) AND ($amount5 <= $ndi))
                                                    5 years
                                                @elseif(($x==6) AND ($amount6 <= $ndi))
                                                    6 years
                                                @elseif(($x==7) AND ($amount7 <= $ndi))
                                                    7 years
                                                @elseif(($x==8) AND ($amount8 <= $ndi))
                                                    8 years
                                                @elseif(($x==9) AND ($amount9 <= $ndi))
                                                    9 years
                                                @elseif(($x==10) AND ($amount10 <= $ndi))
                                                    10 years
                                                @endif</td>
                                            <!--<td class="hidden-xs"> RM {{  number_format((float) $pra->loanamount, 2 , ',' , ',' )  }}  </td>-->
                                            <td>
                                                @if($x==2)
                                                    RM {{$amount2}}
                                                @elseif($x==3)
                                                    RM {{$amount3}}
                                                @elseif($x==4)
                                                    RM {{$amount4}}
                                                @elseif($x==5)
                                                    RM {{$amount5}}
                                                @elseif($x==6)
                                                    RM {{$amount6}}
                                                @elseif($x==7)
                                                    RM {{$amount7}}
                                                @elseif($x==8)
                                                    RM {{$amount8}}
                                                @elseif($x==9)
                                                    RM {{$amount9}}
                                                @elseif($x==10)
                                                    RM {{$amount10}}
                                                @endif
                                            </td>
                                            <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="{{$x}}" required>
                                            <input type="hidden" name="rates" id="rates" class="rates" value="{{$interest31}}">
                                            <input type="hidden" name="rate" id="rate" class="rate" value="{{$interest11}}">
                                            <input type="hidden" name="amount2" id="amount2" class="amount2" value="{{$amount2}}">
                                            <input type="hidden" name="amount3" id="amount3" class="amount3" value="{{$amount3}}">
                                            <input type="hidden" name="amount4" id="amount4" class="amount4" value="{{$amount4}}">
                                            <input type="hidden" name="amount5" id="amount5" class="amount5" value="{{$amount5}}">
                                            <input type="hidden" name="amount6" id="amount6" class="amount6" value="{{$amount6}}">
                                            <input type="hidden" name="amount7" id="amount7" class="amount7" value="{{$amount7}}">
                                            <input type="hidden" name="amount8" id="amount8" class="amount8" value="{{$amount8}}">
                                            <input type="hidden" name="amount9" id="amount9" class="amount9" value="{{$amount9}}">
                                            <input type="hidden" name="amount10" id="amount10" class="amount10" value="{{$amount10}}">
                                             <input type="hidden" name="interests" id="interests" class="interests" value="{{$pra->package->effective_rate}}">
                                            </td>
                                        </tr>
                                       
                                       
                                        <?php } ?>
                                        @else

                                         @endif
                                    </tbody>
                                </table> 
                            </fieldset>
                            @else
                            <?php
                           

                                return redirect('praapplication/'.$id)->with('message', 'Sorry, you are not qualified, Please Input New Financing Amount');
                            ?>
                            @endif

                            <i style="font-size: 12px!important; font-family: Arial,Helvetica,sans-serif !important;margin-left: 12px">  <sup>*</sup>{{ number_format((float)$pra->package->effective_rate, 2 )}} % p.a (Effective Rate) / {{ number_format((float)$pra->package->flat_rate, 2 )}} % p.a (Flat Rate equivalent for 3 years)</i><br>
                            <i style="font-size: 12px;font-family: Arial,Helvetica,sans-serif !important; margin-left: 12px">  <sup>*</sup>MBSB Bank’s current Base Rate (BR) is {{ number_format((float)$pra->package->base_rate, 2 )}}% p.a. and ceiling profit rate is 15%</i>
                            <fieldset>
                                <div class="form-group">
                                    <input type='checkbox'  id='terms'  name='terms' value='1' required="required" style="font-size: 12px!important;font-family: Arial,Helvetica,sans-serif !important; text-align: justify!important">&nbsp;I /we express consent and authorize MBSB Bank to process any information that I/we have provided to MBSB Bank for purpose of crosselling marketing and promotion.<br>
                                </div>

                            <fieldset>
                            <!-- <footer> -->
                                <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                                    <b> Submit</b>
                                </button>

                            <!--</footer>-->
                            <div>
                        </form><br>
                        <div class="form-group">
                <span>Disclaimer</span>
                <p class="fs-4" style="text-align: justify;"> The information shown is indicative and for illustration only. MBSB Bank does not guarantee the accuracy of the calculation and accepts no liability for any inaccuracies or omissions. The above does not constitute an offer of credit nor does it guarantee that the credit facility applied for will be approved. All applications for financing are subject to credit evaluation and approval and MBSB Bank reserves the right to approve or reject such applications without assigning any reason therefore.</p>
            </div>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Register</h4>
                                    </div>
                                    <div class="modal-body">
                                        {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                         <input name="id_pras" id="id_pra" type="hidden"  value="{{$pra->id_pra}}" >
                                         <input type="hidden" id="ic" name="ic" value="{{$pra->icnumber}}" >
                                            <fieldset>
                                                 <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="company">E-mail</label>
                                                        <input type="hidden" name="idpra" value="{{$pra->id}}">
                                                        <input type="email" id="email" name="Email" required  class="form-control" style="text-transform: lowercase !important;">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="company">Password</label>
                                                        <input type="hidden" name="idpra" value="{{$pra->id}}">
                                                        <input type="Password" required id="password" name="password" placeholder="" autocomplete="off" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="company">Password Confirmation</label>
                                                        <input type="Password" id="password_confirmation" name="password_confirmation" required placeholder="" autocomplete="off" class="form-control">
                                                    </div>
                                                </div>
                                                <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >
                                                @endforeach   
                                                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                            </fieldset>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel
                                            </button>
                                            <button type="submit" name="submit" class="btn btn-primary">
                                                Register
                                            </button>
                                        {!! Form::close() !!}   
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- Modal -->
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  

 @include('praapplication.result_js')


@endsection