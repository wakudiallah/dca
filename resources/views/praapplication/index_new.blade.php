    @extends('layouts.front.template')

    @section('content')

    <style type="text/css">

    body{
        font-family: 'Helvetica', 'Arial', sans-serif !important;
    }

    .login100-more::before {
        content: "" !important;
        display: block !important;
        position: absolute !important;
        z-index: -1 !important;
        width: 100% !important;
        height: 100% !important;
        top: 0 !important;
        left: 0 !important;
        background: #e8519e !important;
        background: -webkit-linear-gradient(bottom, #17a2b8, #007bff) !important;
        background: -o-linear-gradient(bottom, #e8519e, #c77ff2) !important;
        background: -moz-linear-gradient(bottom, #e8519e, #c77ff2) !important;
        background: linear-gradient(bottom, #e8519e, #c77ff2) !important;
        opacity: 0.8 !important;
    }


    .center {
      display: block;
      margin-left: auto;
      margin-right: auto;
      width: 50%;
    }

    .img-mbsb{
        margin-bottom: 20px !important;
        width: 150px;
        height: 50px;
    }

    .login100-form{
        padding-right: 80px !important; 
        padding-left: 80px !important; 
    }


        .title-left{
            margin-top: 300px;
            margin-left: 50px;
            position: fixed;
        }

        .color-blue{
            "color: #0056A3 !important
        }

        .color-red{
            "color: red !important
        }
</style>




    <div class="limiter">
        <div class="container-login100">
            <!--<div class="col-md-6 login100-more" style="background-image: url('dca/images/background-blue.jpg');"></div>-->
            <div class="col-md-6 login100-more hidden-xs" style="background-color: #0056A3 !important;">
                <div class="title-left hidden-xs" >
                    <h1 style="color: white !important">Hello!</h1>
                    <h3 style="color: #ffffff !important">Welcome to MBSB Bank Online Application Form</h3>
                    <p>Now its very easy to apply for a Prime Rich Current Account-i</p>
                </div>
            </div>



            <div class="col-md-6 right-mbsb wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                <img src="https://mbsb.insko.my/public/asset/img/logo_bank.png" class="img-mbsb center" >
                <style type="text/css">
                    .error{
                        color: red !important;
                    }
                </style>

                {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                    <span class="login100-form-title p-b-59" style="color: #0056A3 !important">
                        Pleae Fill In Your Personal Details
                    </span>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate="Name is required">
                        <span class="color-blue label-input100">Full Name <sup>*</sup></span>
                        <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif size="55" class="input100">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "IC Number">
                        <span class="label-input100">New IC Number <sup>*</sup></span>
                            <input  type="text" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                            || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                            || (95<event.keyCode && event.keyCode<106)
                            || (event.keyCode==8) || (event.keyCode==9) 
                            || (event.keyCode>34 && event.keyCode<40) 
                            || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="Ex: 931111120101"  @if (Session::has("icnumber"))  value="{{ Session::get('icnumber') }}" @endif class="input100">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate="Handphone Number required">
                        <span class="color-blue label-input100">Handphone Number <sup>*</sup></span>
                             <input type="text" id="PhoneNumber" class="input100" pattern="/^-?\d+\.?\d*$/" onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46) )" onKeyPress="if(this.value.length==12) return false;" name="PhoneNumber" minlength="12" maxlength="12" placeholder="Ex: 011000000" @if (Session::has("phone"))  value="{{ Session::get('phone') }}" @endif>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "IC Number">
                        <span class="label-input100">Employment <sup>*</sup></span>
                        <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                            <option value="0">--Select Employment--</option>
                            @foreach ($employment as $employment)
                            <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                            @endforeach
                        </select> <i></i>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Employer is required">
                        <span class="label-input100">Employer<sup>*</sup></span>
                        <input class="input100" type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Employer"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif maxlength="100">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Basic Salary is required">
                        <span class="label-input100">Basic Salary (RM)<sup>*</sup></span>                            <input class="input100 fn" @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif  id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="text" required=""  step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;" >
                            <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Allowance is required">
                            <span class="label-input100">Allowance (RM)<sup>*</sup></span>
                            <input class="input100" @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif  id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="text" required=""   class="fn" step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  >
                            <span class="focus-input100"></span>
                    </div>
 
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Deduction is required">
                            <span class="label-input100">Total Deduction in payslip only (RM)<sup>*</sup></span>
                            <input class="input100 fn" @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="text" required=""   step=".01" onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"   >
                        <span class="focus-input100"></span>
                    </div>
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Package is required">
                        <span class="label-input100">Package<sup>*</sup></span>
                            <input class="input100" type="text" name="package" id="package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @endif  readonly>
                            <input type="hidden" name="package_id" id="package_id" @if (Session::has('package_id'))  value="{{ Session::get('package_id') }}" @else value="1" @endif class="form-control" readonly>
                        <span class="focus-input100"></span>
                    </div>
                    
                    <div class="col-md-12 wrap-input100 validate-input" data-validate = "Finance Ammount is required">
                        <span class="label-input100">Financing Amount(RM) <sup>*</sup></span>
                        <input class="fn input100" id="LoanAmount" name="LoanAmount" placeholder="Financing Amount (RM)" onkeydown="return ( event.ctrlKey || event.altKey 
                        || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                        || (95<event.keyCode && event.keyCode<106)
                        || (event.keyCode==8) || (event.keyCode==9) 
                        || (event.keyCode>34 && event.keyCode<40) 
                        || (event.keyCode==46) )" onKeyPress="if(this.value.length==6) return false;"  type="text" required=""  step=".01"  @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >
                        <span class="focus-input100"></span>
                    </div>
                     <button type="submit" class="btn btn-primary">
                         <b> Calculate</b>
                    </button>
                </form>
            </div>
        </div>
    </div>



 @include('praapplication.praapp')
