<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Branch";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	    @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                         

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th>IC Number</th>
                                                    <th>Name</th>
													<th>Phone</th>
                                                 
                                                    <th>File Created</th>
                                                    <th>Status</th>
                                                    <th>Route to</th>
                                                    <th>Activity</th>
                                             
                                                     <th>Download</th>
													<th>Action</th>
                                                      

												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                 @foreach ($terma as $term)
                                                  
                                              <tr>
                                              
                                                <td>
                                                   {{ $i }}                              
                                                </td>
                                                  
                                                  <td>
                                              {{ $term->Basic->new_ic }}
                                              <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                                <div id='block{{$i}}'></div>
                                                <script type="text/javascript">

                                                    $(document).ready(function() {
                                                    
                                                    var old_icx = $('#ic99{{$i}}').val();
                                                    
                                                       
                                                    if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                    {
                                                    $('#block{{$i}}').html("<span class='label label-danger'>IC Blocked </span>");
                                                    
                                                    }
                                                    });
                                                  </script>

                                                  
                                                  
                                                  </td>
                                                <td>{{ $term->Basic->name }}</td>
												<td>{{ $term->PraApplication->phone }}</td>
                                        
                                                <td>{{$term->file_created }}</td>
                                                <td>
													@if($term->verification_result_by_bank ==0)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-default'>In Process</span>
                                                    @elseif($term->verification_result_by_bank ==1)                                                
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-success'>Approved</span>
                                                    @elseif($term->verification_result_by_bank ==2)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Rejected</span>
                                                    @elseif($term->verification_result_by_bank ==3)
                                                      <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-warning'>Pending Approval</span>
                                                 
													@endif  
                                                </td>
                                                      <td>
                                                    @if($term->id_branch=='0')
                                                         <div align='center'> - </div> 
                                                    @else                                            
                                                        <b>{{ $term->Branch->branchname }}</b>
                                                    @endif   
                                                </td>
                                                  <td>
                                                 <?php if(!empty($term->Log_download->first()->id)) {
                                                  $max = 0;
												
												
                                                      foreach ($term->Log_download as $log) {
                                                          if (strpos($log->activity,"Route to") || $log->user->role=="2"  || $log->user->role=="3") {
                                                              
                                                                  if($max < $log->id ) {
                                                                  $max = $log->id ;
                                                                  $tanggal = $log->downloaded_at ;
                                                                  $user_download =$log->user->name;
                                                                 // echo $user_download; echo "<br>";
                                                                  }
                                                          
                                                          
                                                          }
                        
                                                       }
                                                       if (!empty($tanggal)) {
                                                        echo "<a href='#' data-toggle='modal' data-target='#myModal".$i."'>".$tanggal." by ".$user_download."</a>";
                                                       
                                                       }
                                                       else {
                                                         echo "-";
                                                       }
													
													}
													else {
													  echo "-";
													}
													
													?>
													
                                                  
                                                  
													
                                                  
                                                  
                                                  
                                                  </td>
                                       
                                              
                                                <td>
												
												<a href="{{url('/')}}/admin/downloadzip/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                  <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a> </td>
												<td>
												@if($term->verification_result_by_bank <>5)
												
                                                    @if($term->verification_result_by_bank ==1)
                                                     <span class="label label-success">Approved</span>
                                                    
                                                    @elseif($term->verification_result_by_bank ==2)
                                                     <span class="label label-danger">Rejected</span>
                                                        
                                                    @else
                                                         <a href='#' data-toggle='modal' data-target='#amyModal{{$i}}'>Change Status</a>
												  <!-- Modal -->
													<div class="modal fade" id="amyModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="amyModal{{$i}}" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="addBranch">Change Status Application </h4>
																</div>
																<div class="modal-body">
																 {!! Form::open(['url' => 'admin/changed_bybank','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
																  <fieldset>
																		
																			
																			<section >
																			  <label class="label">Status </label>
																				<label class="input">
																			   
																					<select name='status' id='status{{$i}}' required class='form-control'>
																				
																						  @if($term->verification_result_by_bank==0)
																							<option disabled selected id="status" value='0'>-Choose Action-</option>
																						
																							<option value='3'>Pending Approval</option>
																							<option value='1'>Approved</option>
																							<option value='2'>Rejected</option>
																							@elseif($term->verification_result_by_bank==1)
																							
																							<option value='3'>Pending Approval</option>
																							<option selected value='1'>Approved</option>
																							<option value='2'>Rejected</option>
																							@elseif($term->verification_result_by_bank==2)
																							
																							<option value='3'>Pending Approval</option>
																							<option value='1'>Approved</option>
																							<option selected value='2'>Rejected</option>
																							@elseif($term->verification_result_by_bank==3)
																							
																							<option selected value='3'>Pending Approval</option>
																							<option value='1'>Approved</option>
																							<option value='2'>Rejected</option>
																				
																							@else
																						
																							<option value='3'>Pending Approval</option>
																							<option value='1'>Approved</option>
																							<option value='2'>Rejected</option>
																					
																						   @endif		  
																					</select>
																					
																					<b class="tooltip tooltip-bottom-right">Change Status</b>
																				</label>
																			</section>
					
																	
																			    @if($term->verification_result_by_bank=='1')
																				  <section id="loan{{$i}}" style="display:inline";>
																				@else
																				 <section id="loan{{$i}}" style="display:none";>
																				@endif
																			   
																				  <label class="label">Loan Amount (RM)</label>
																					<label class="input"><i class="icon-append">RM </i>
																					   @if( $term->aproved_loan=='0')
																						<input type="text" onchange="toFloat('loan')"  id='loan' name="loan" value="{{ number_format( $term->praapplication->loanamount, 2 , '.' , '' )  }}" class='form-control loan'  onkeypress="return isNumberKey(event)" />
																					  @else
																						<input type="text" name="loan" value='{{ $term->aproved_loan }}' class='form-control'/>
																					  
																					  @endif
																					  <b class="tooltip tooltip-top-right"><i class="txt-color-teal"></i> Loan Amount (RM)</b> </label>
																					</label><br>
																				</section>
																				  @if($term->verification_result_by_bank=='1')
																				  <section id="tenor{{$i}}" style="display:inline;">
																				@else
																				   <section id="tenor{{$i}}" style="display:none;">
																				@endif
																				 
																				  <label class="label">Tenure (Year)</label>
																					<label class="input"><i class="icon-append">Years  </i>
																					  @if( $term->aproved_tenure=='0')
																						
																						<input type="text" name="tenure" value='' class='form-control'/>
																					  
																					@else
																					  <input type="text" name="tenure" value='' class='form-control'/>
																					  
																					@endif

																					  	<b class="tooltip tooltip-top-right"><i class="txt-color-teal"></i> Type only number Eg: 7</b> </label>
																					</label><br>
																				</section>
																				@if($term->verification_result_by_bank=='2')
																				  <section id="reason{{$i}}" style="display:inline;">
																				@else
																				  <section id="reason{{$i}}" style="display:none;">
																				@endif
																				  
																				  <label class="label">Reason</label>
																					<label class="input">
																						<input type="text" name="reason" value='{{$term->rejected_reason }}' class='form-control'/>
																					  <b class="tooltip tooltip-bottom-right">Reason</b>
																					</label><br>
																				</section>
																				  
																				<section >
																			  <label class="label">Remark </label>
																				 <label class="input">
								 
																					<textarea id='remark' name='remark_bank' class='form-control' rows="4" cols="73">{{ $term->remark_bank }}</textarea>
																					<b class="tooltip tooltip-bottom-right">Remark</b>
																				</label>
																			</section>
																		   
																		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
																		  <input type="hidden" name="id_praapplication" value="{{ $term->id_praapplication }}">
																		  <input type="hidden" name="branchname" value="{{ $term->Branch->branchname }}">
																		  <input type="hidden" name="cus_ic" value="{{ $term->Basic->new_ic}}">
																		  <input type="hidden" name="cus_name" value="{{ $term->Basic->name }}">
																		  <input type="hidden" name="cus_email" value="{{ $term->PraApplication->email }}">
																	 </fieldset>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Cancel
																	</button>
																	<button type="submit" name="submit" class="btn btn-lg txt-color-darken">
																				   Submit
																				</button>
																			   
																	   {!! Form::close() !!}   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
                                                    @endif
												  @else
													<div align='center'>-</div>
												  @endif
												</td>
											  </tr>
												  <script>
												  $('#status{{$i}}').change(function() {
													 if ($(this).val() === '1') {
														 document.getElementById("loan{{$i}}").style.display='inline';
														 document.getElementById("tenor{{$i}}").style.display='inline';
													  } else {
														  document.getElementById("loan{{$i}}").style.display='none';
														  document.getElementById("tenor{{$i}}").style.display='none';
													  }
													  if ($(this).val() === '2') {
														 document.getElementById("reason{{$i}}").style.display='inline';
											  
													  } else {
														  document.getElementById("reason{{$i}}").style.display='none';
												
													  }
											  });
												</script>
                                              <?php
                                              $i++; ?>
                                            @endforeach
											</tbody>
										</table>
                                          
                                          <br>
                                           <?php $j=1; foreach($terma as $termxz) { 

                                                  if(!empty($termxz->Log_download->first()->id)) { ?>
                                                    <!-- Modal -->
													<div class="modal fade" id="myModal{{$j}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="myModalLabel"><b>Log Activity : {{ $termxz->Basic->name }} - {{ $termxz->Basic->new_ic }} of @if($termxz->id_branch=='0')
                                                         - 
                                                    @else                                            
                                                        <b>{{ $termxz->Branch->branchname }}</b>
                                                    @endif  Branch</b></h4>
																
                                                                
                                                                </div>
																<div class="modal-body">												
																	  			<table id="datatable_col_reorder{{$j}}" class='table table-striped table-bordered table-hover'>
																		<thead>
																		  <tr>
																			<th>Date</th>
																		  <th>User</th>
                                                                            <th>Activity</th>
																			  <th>Remark</th>
																			  <th>Reason</th>
																		  </tr>
																		
																		</thead>
																		  	<tbody>
																		  @if(!empty($termxz->Log_download->first()->id))
												
																			@foreach ($termxz->Log_download as $logfull)
																				@if (strpos($logfull->activity,"Route to") || $logfull->user->role=="2"  || $logfull->user->role=="3") 
											
																					
																						  <tr>
																								<td> {{  $logfull->downloaded_at  }}</td>
																								<td> {{ $logfull->user->name }}</td>
																								<td> {{ $logfull->activity }}</td>
																								<td> {{ $logfull->log_remark }}</td>
																								@if ($logfull->log_reason=="")
																								<td> &nbsp; </td>
																								@else
																								<td> {{ $logfull->log_reason }}</td>
																								@endif
																								
																						  </tr>
																						
																						
																				@endif
																			 @endforeach
																		</tbody>
																			
																		  @else 
																			<td colspan='2'>No Data</td>
																		  @endif
																		  
																							  
																	</table>							   																   													
																  </div>
																  <div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Close
																	</button>
																																	   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder{{$j}}').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder{{$j}}'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													<?php
													}
													$j++;
													}
													?>

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
  <script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>





          
