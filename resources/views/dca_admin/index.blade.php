
<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "DCA Admin";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @include('sweetalert::alert')
      
      <section id="widget-grid" class="">
                 
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics Aplication By All Marketing Officers</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                        <?php   
                            function ringgit($nilai, $pecahan = 0) 
                                { 
                                    return number_format($nilai, $pecahan, '.', ','); 
                                }
                            $sumapp= 0;
                            $sum_overal= 0;
                            $sum_new= 0; 
                            $sum_pending= 0; 
                            $sum_assign= 0; 
                        ?>
                                
                        @foreach ($terma as $termb)
                            <?php 
                                    $sumapp = $sumapp + 1; ?>
                             @if(($termb->status ==2) AND ($termb->assign_to !=''))
                                    <?php   $sum_new = $sum_new + 1; 
                                    ?>
                            @endif
                           @if(($termb->status ==2) || ($termb->assign_to ==''))
                                    <?php   $sum_pending = $sum_pending + 1; 
                                    ?>
                            @endif
                            @if($termb->status >=3)
                                    <?php   $sum_assign = $sum_assign + 1; 
                                    ?>
                            @endif
                        @endforeach
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Total Application</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total New Case </td>
                                            <td>{{ $sum_new }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Assigned </td>
                                            <td>{{ $sum_assign }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending Assign </td>
                                            <td>{{ $sum_pending }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Overal Case</b></td>
                                            <td><b>{{ $sumapp }}</b></td>
                                        </tr>                                     
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
                        
                        
        <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application List</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>                         
                                        <tr>
                                            <th>No.</th>
                                            <th class="hidden-xs">IC Number</th>
                                            <th>Name</th>
                                            <th class="hidden-xs">Phone</th>
                                            <th>Submit Date</th>
                                            <th>Status</th>
                                            <th>Assign To</th>
                                            <th>Assign By</th>
                                            <th>History</th>
                                            <th>Action</th>
                                            <th>Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; ?>
                                        @foreach ($terma as $term)
                                            <tr>
                                                <td>
                                                   {{ $i }}                          
                                                </td>
                                                <td class="hidden-xs">
                                                    {{ $term->Basic->new_ic }}
                                                    <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                                </td>
                                                <td>{{ $term->Basic->name }}</td>
                                                <td class="hidden-xs">{{ $term->PraApplication->phone }}</td>
                                                <td>{{$term->file_created }}</td>
                                                <td><div align='center'>
                                                @if($term->status==3)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                @elseif($term->status==4)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-default'><b>Customer Ready Fill Up Form</b></span>
                                                @elseif($term->status==5)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'><b>Pending Verification</b></span>
                                                @elseif($term->status==99)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-danger'><b>Documents Rejected</b></span>
                                                @elseif($term->verification_result ==1)                                                
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-success'>Ready for 2nd Verification</span>
                                                @elseif($term->status ==6)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-primary'>Submitted to Processor/WAPS</span>
                                                        <br>
                                                        @if($term->id_branch=='0')
                                                            <div align='center'> - </div> 
                                                        @else                                            
                                                            <i>{{ $term->Branch->branchname }}</i>
                                                        @endif 
                                                        <br>
                                                        @if($term->financial->l3_jumlah > 0)
                                                            <font color="red"><i>Overlapping Case</i></font>
                                                        @endif
                                                @elseif($term->status ==8)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Rejected</span>
                                                @elseif($term->status ==7)
                                                    <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Route Back to User</span>
                                                @endif
                                                </div>
                                            </td>
                                         
                                                <td><div align='center'>
                                                    
                                                    {{$term->MO->name}}
                                                    
                                                    </div>  
                                                </td>
                                                <td><div align='center'>
                                                    @if(!empty($term->assign_by))
                                                    {{$term->DCA->name}}
                                                    @else
                                                     Registered by Referral Link
                                                    @endif
                                                    </div>  
                                                </td>
                                               
                                                   <td>
                                                   <a href="JavaScript:newPopup('{{url('/')}}/activities/{{$term->id_praapplication}}');" class='btn btn-sm btn-primary' ><i class='fa fa-history'></i>  History</a>
                                                  </td>
                                                 <td align='center'>
                                                @if($term->status==3)
                                                    @can('add_dsrs')
                                                        <a href="{{ url('/admin/step1/'.$term->id_praapplication.'/verify') }}" class='btn btn-sm btn-warning' ><i class='fa fa-file'></i> Verify Docs</a>
                                                    @endcan
                                                @endif
                                                @if(($term->status>=4))
                                                    @can('view_dsrs')
                                                    <a href="{{ url('/admin/step1_view/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    @endcan
                                                @endif
                                                @if(($term->status==6)||($term->status==8))
                                                    @can('view_forms')
                                                    <a href="{{ url('/admin/user_detail_view/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                    @endcan
                                                @endif
                                                @if($term->status=='5')
                                                @can('edit_forms')
                                                    <a href="{{ url('/admin/user_detail/'.$term->id_praapplication.'/verify') }}" class='btn btn-sm btn-danger fa fa-book' >Verify Form</a>
                                                    @endcan
                                                @endif 
                                                @if($term->edit=='1')
                                                    @if($term->verification_result !=3)
                                                        @if($term->verification_result !=2)
                                                            <a href="{{url('/')}}/form/approveedit/{{$term->id_praapplication}}" 
                                                            onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                        @endif 
                                                    @endif 
                                                @endif

                                                @if($term->status=='7')
                                                    
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="amyModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="amyModal{{$i}}" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                &times;
                                                                            </button>
                                                                            <h4 class="modal-title" id="addBranch">Route Back Application to User</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                         {!! Form::open(['url' => 'form/routeback','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                                                          <fieldset>
                                                                                    <section >
                                                                                          <label class="label">Customer Name</label>
                                                                                            <label class="input">
                                                                                            <input type='text' value='{{ $term->Basic->name}}' readonly disabled/>
                                                                                            <b class="tooltip tooltip-bottom-right">Customer Name</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                                    <section >
                                                                                          <label class="label">IC Number</label>
                                                                                            <label class="input">
                                                                                                <input type='text' value='{{ $term->Basic->new_ic}}' readonly disabled/>
                                                                                            <b class="tooltip tooltip-bottom-right">IC Number</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                                    <section >
                                                                                          <label class="label">Reason</label>
                                                                                            <label class="input">
                                                                                                <textarea id='reason' name='reason' class='form-control' rows="4" cols="73"></textarea>
                                                                                            <b class="tooltip tooltip-bottom-right">Reason</b>
                                                                                            </label><br>
                                                                                    </section>
                                                                        
                                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                  <input type="hidden" name="id_praapplication" value="{{ $term->id_praapplication }}">
                                                                                  <input type="hidden" name="cus_ic" value="{{ $term->Basic->new_ic}}">
                                                                                  <input type="hidden" name="cus_name" value="{{ $term->Basic->name }}">
                                                                                  <input type="hidden" name="cus_email" value="{{ $term->PraApplication->email }}">
                                                                             </fieldset>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                                Cancel
                                                                            </button>
                                                                            <button type="submit" name="submit" class="btn btn-lg txt-color-darken">
                                                                                           Submit
                                                                                        </button>
                                                                                       
                                                                               {!! Form::close() !!}   
                                                                        </div>
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                @endif  
                                           
                                            </td>
                                            <td> 
                                                @can('view_zips')
                                                <a href="{{url('/')}}/admin/downloadzip/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                @endcan
                                                @can('view_application_forms')
                                                    @if(($term->status==6)||($term->status==8))
                                                        <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}"><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                    @endif
                                                @endcan
                                            </td>
                                            </tr>
                                              <?php
                                              $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                         
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
        
            
            /* // DOM Position key index //
        
            l - Length changing (dropdown)
            f - Filtering input (search)
            t - The Table! (datatable)
            i - Information (records)
            p - Pagination (paging)
            r - pRocessing 
            < and > - div elements
            <"#id" and > - div with an id
            <"class" and > - div with a class
            <"#id.class" and > - div with an id and class
            
            Also see: http://legacy.datatables.net/usage/features
            */  
    
            /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };
    
                
    
            /* END BASIC */
            
                /* COLUMN SHOW - HIDE */
    $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_col_reorder) {
                responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_col_reorder.respond();
        }           
    });
    
    /* END COLUMN SHOW - HIDE */
        })
        </script>
          
                            

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };
                $('#dt_basic').dataTable({
                      
                        "scrollX": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                        "t"+
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth" : true,
                    "preDrawCallback" : function() {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback" : function(nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback" : function(oSettings) {
                        responsiveHelper_dt_basic.respond();
                    }
                });
                
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>




          
