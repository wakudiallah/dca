<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "DCA Administrator";

$page_css[] = "your_style.css";
include("asset/inc/header.php");

include("asset/inc/nav.php");

?>



<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<script type="text/javascript" src=""></script>
 <!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bin/push.min.js") }}"></script>

<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
       @include('sweetalert::alert')  
       
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Assign to FA</h2>
                        </header><!-- widget div-->
                        
                        <div>

                            


                            <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/post-assign-to-fa')}}" >
                            

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>                         
                                        <tr>
                                            
                                            <th width="10%">
                                               
                                                    <input type="checkbox" id="selectAll">
                                                   
                                            </th>
                                            <th>IC Number</th>
                                            <th>Name</th>
                                            <th>Submit Date</th>
                                            <th>Branch</th>
                                            <th>Company Name</th>
                                             <th>History</th>
                                            <th>Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; ?>
                                        @foreach ($terma as $term)
                                        <tr>
                                            <td align="center">
                                                <div class="checkbox">

                                                  <input type="hidden"  name="ci[]" value="{{$term->id}}">
                                                  
                                                  <input type="hidden"  name="id2[]" value="{{$term->id}}">

                                                  <input type="hidden"  name="id3[]" value="{{$term->id_praapplication}}">

                                                    <label><input type="checkbox" value="{{$term->id}}" name="id[]" class="friends"></label>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $term->Basic->new_ic }}
                                            </td>
                                            <td>
                                                <?php $name = strtoupper($term->Basic->name ); ?>{{$name}}  
                                            </td>
                                             
                                             <td>{{$term->file_created }}</td>
                                             <td>
                                                @if(!empty($term->id_branch))
                                                    {{ $term->Branch->branchname }}
                                                @endif
                                            </td>
                                             <td>
                                                {{ $term->PraApplication->majikan }}
                                            </td>
                                            <td><a href="JavaScript:newPopup('{{url('/')}}/activities/{{$term->id_praapplication}}');" class='btn btn-sm btn-primary' ><i class='fa fa-history'></i>  History</a></td>
                                              <td><div align='center'>
                                                   
                                                        @if($term->status==2)
                                                            <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                        @endif
                                                 
                                                </div>
                                            </td>
                                        @endforeach


                                        </tr>
                                         <?php
                                        $i++; ?>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="col-md-5"></div>

                                                <div class="col-md-7">
                                                <select id="fa" class="select2" name="fa" required="">
                                                     <option   selected="selected" value="0">Select FA</option>
                                                  @foreach ($fa as $data)
                                                  <option value="{{$data->id}}" >{{$data->name}} --  
                                                    @if(!empty($data->branch_code))
                                                    {{$data->Branch->branchname}} 
                                                @endif</option>
                                                  @endforeach
                                                </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <button type="submit" name="submit" class="btn btn-primary" >
                                                   Assign
                                                </button>

                                            </div>
                                        </div>
                        
                                    </tbody>


                                </table>

                               {!! Form::close() !!} 

                                 

                            </div>
                        </div>

                        
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
  var $selectAll = $('#selectAll'); // main checkbox inside table thead
  var $table = $('.table'); // table selector 
  var $tdCheckbox = $table.find('tbody input:checkbox'); // checboxes inside table body
  var tdCheckboxChecked = 0; // checked checboxes

  // Select or deselect all checkboxes depending on main checkbox change
  $selectAll.on('click', function () {
    $tdCheckbox.prop('checked', this.checked);
  });

  // Toggle main checkbox state to checked when all checkboxes inside tbody tag is checked
  $tdCheckbox.on('change', function(e){
    tdCheckboxChecked = $table.find('tbody input:checkbox:checked').length; // Get count of checkboxes that is checked
    // if all checkboxes are checked, then set property of main checkbox to "true", else set to "false"
    $selectAll.prop('checked', (tdCheckboxChecked === $tdCheckbox.length));
  })
});
</script>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
          
            "scrollX": true,
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });
                
</script>



<script type="text/javascript">
    $( '#popup-validation' ).on('submit', function(e) {
        if($('#fa').val() == '0'){
            alert("Please select a fa.");
            $('#fa').focus();
            return false;
        }
        else if($( 'input[class^="friends"]:checked' ).length === 0) {
          alert( 'Please Select the applicant' );
          e.preventDefault();
       }
       
    
    });

</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>


<!-- End not select -->











          
