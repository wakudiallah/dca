<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Registered Customer Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");

include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            
                           

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                              
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
									 {!! Form::open(['url' => 'report/registered','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                              <fieldset>
													<section>
                                                          <label class="label">Branch</label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                  <select name="branch" id="branch" class="form-control" >
                                    <option disabled="" value="0" selected="">--- SELECT BRANCH ---</option> 
                                    @foreach($branch as $branch)
                                     
                                    
                                      <option  value="{{$branch->branch_code}}">{{$branch->branchname}}</option>
                                    @endforeach
                                </select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section>
                                                        <section>
                                                          <label class="label">FA</label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select id="fa" class="select2" name="fa" required="">
                                                     <option   selected="selected" value="0">Select FA</option>
                                                  @foreach ($fa as $data)
                                                  <option value="{{$data->mo_id}}" >{{$data->name}} </option>
                                                  @endforeach
                                                </select>

                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section>
                                                         
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                              
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Generate
                                                            </button>
                                                           
                                                   {!! Form::close() !!} 
																					   
										
													
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>


          
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="branch"]').on('change', function() {
          
            var stateID = $(this).val();
           
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/select_fa/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        $('select[name="fa"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="fa"]').append('<option value="'+ key +'">'+ value +'</option>');
                          //  $("#id_branch").val(data[k].state.state_code );
                        });


                    }
                });
            }else{
                $('select[name="fa"]').empty();
            }
        });
    });
</script>